package net.kaourantin;

import flash.utils.ByteArray;
import flash.display.BitmapData;
import flash.geom.Rectangle;
import haxe.ds.Vector.Vector;

class PNGEnc {

	public static function encode(img:BitmapData, type:UInt = 0):ByteArray 
	{
		// Create output byte array
		var png:ByteArray = new ByteArray();
		// Write PNG signature
		png.writeUnsignedInt(0x89504e47);
		png.writeUnsignedInt(0x0D0A1A0A);
		// Build IHDR chunk
		var IHDR:ByteArray = new ByteArray();
		IHDR.writeInt(img.width);
		IHDR.writeInt(img.height);
		if(img.transparent || type == 0)
		{
			IHDR.writeUnsignedInt(0x08060000); // 32bit RGBA
		}
		else
		{
			IHDR.writeUnsignedInt(0x08020000); //24bit RGB
		}
		IHDR.writeByte(0);
		writeChunk(png,0x49484452,IHDR);
		// Build IDAT chunk
		var IDAT:ByteArray= new ByteArray();
		
		switch(type)
		{
			case 0:
				writeRaw(img, IDAT);
			case 1:
				writeSub(img, IDAT);
		}
		
		IDAT.compress();
		writeChunk(png,0x49444154,IDAT);
		// Build IEND chunk
		writeChunk(png,0x49454E44,null);
		// return PNG
		
		return png;
	}
	
	private static function writeRaw(img:BitmapData, IDAT:ByteArray):Void
	{
		var h:Int = img.height;
		var w:Int = img.width;
		var transparent:Bool = img.transparent;
		
		for (i in 0...h) 
		{
			// no filter
			if ( !transparent ) 
			{
				var subImage:ByteArray = img.getPixels(
					new Rectangle(0, i, w, 1));
				//Here we overwrite the alpha value of the first pixel
				//to be the filter 0 flag
				subImage[0] = 0;
				IDAT.writeBytes(subImage);
				//And we add a byte at the end to wrap the alpha values
				IDAT.writeByte(0xff);
			} 
			else 
			{
				IDAT.writeByte(0);
				var p:UInt;
				for (j in 0...w) 
				{
					p = img.getPixel32(j,i);
					IDAT.writeUnsignedInt(((p&0xFFFFFF) << 8)|(p>>>24));
				}
			}
		}
	}
	
	private static function writeSub(img:BitmapData, IDAT:ByteArray):Void
	{
		var r1:UInt;
		var g1:UInt;
		var b1:UInt;
		var a1:UInt;
		
		var r2:UInt;
		var g2:UInt;
		var b2:UInt;
		var a2:UInt;
		
		var r3:UInt;
		var g3:UInt;
		var b3:UInt;
		var a3:UInt;
		
		var p:UInt;
		var h:Int = img.height;
		var w:Int = img.width;
		
		for (i in 0...h) 
		{
			// no filter
			IDAT.writeByte(1);
			if ( !img.transparent ) 
			{
				r1 = 0;
				g1 = 0;
				b1 = 0;
				a1 = 0xff;
				for (j in 0...w) 
				{
					p = img.getPixel(j,i);
					
					r2 = p >> 16 & 0xff;
					g2 = p >> 8  & 0xff;
					b2 = p & 0xff;
					
					r3 = (r2 - r1 + 256) & 0xff;
					g3 = (g2 - g1 + 256) & 0xff;
					b3 = (b2 - b1 + 256) & 0xff;
					
					IDAT.writeByte(r3);
					IDAT.writeByte(g3);
					IDAT.writeByte(b3);
					
					r1 = r2;
					g1 = g2;
					b1 = b2;
					a1 = 0;
				}
			} 
			else 
			{
				r1 = 0;
				g1 = 0;
				b1 = 0;
				a1 = 0;
				for (j in 0...w) 
				{
					p = img.getPixel32(j,i);
					
					a2 = p >> 24 & 0xff;
					r2 = p >> 16 & 0xff;
					g2 = p >> 8  & 0xff;
					b2 = p & 0xff;
					
					r3 = (r2 - r1 + 256) & 0xff;
					g3 = (g2 - g1 + 256) & 0xff;
					b3 = (b2 - b1 + 256) & 0xff;
					a3 = (a2 - a1 + 256) & 0xff;
					
					IDAT.writeByte(r3);
					IDAT.writeByte(g3);
					IDAT.writeByte(b3);
					IDAT.writeByte(a3);
					
					r1 = r2;
					g1 = g2;
					b1 = b2;
					a1 = a2;
				}
			}
		}
	}

	private static var crcTable:Vector<UInt> = null;

	private static function writeChunk(png:ByteArray, type:UInt, data:ByteArray):Void
	{
		var c:UInt;
		if (crcTable == null) 
		{
			crcTable = new Vector<UInt>(256);
			for (n in 0...256) 
			{
				c = n;
				for (k in 0...8) 
					if (c & 1 == 0) 
						c = c >>> 1;
					else
						c = 0xedb88320 ^ c >>> 1;
				crcTable[n] = c;
			}
		}
		var len:UInt = if (data != null) data.length; else 0;
			
		png.writeUnsignedInt(len);
		var p:UInt = png.position;
		png.writeUnsignedInt(type);
		if ( data != null ) 
		{
			png.writeBytes(data);
		}
		var e:UInt = png.position;
		png.position = p;
		c = 0xffffffff;
		for (i in 0...(e-p)) 
			c = crcTable[(c ^ png.readUnsignedByte()) & 0xff] ^ (c >>> 8);
			
		c = c ^ 0xffffffff;
		png.position = e;
		png.writeUnsignedInt(c);
	}
}