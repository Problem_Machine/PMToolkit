package com.problemmachine.bitmap.bitmapprocess.i;

import flash.display.BitmapData;

interface IBitmapProcess 
{
	function update(time:Float):Void;
	function process(source:BitmapData):Void;
	function reset():Void;
}