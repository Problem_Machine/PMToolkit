package com.problemmachine.eve.game.lighting;
import com.problemmachine.bitmap.bitmapmanager.BitmapManager;
import com.problemmachine.tools.math.IntMath;
import com.problemmachine.util.color.colormatrix.ColorMatrix;
import flash.Lib;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.BitmapDataChannel;
import flash.display.GradientType;
import flash.display.Graphics;
import flash.display.Sprite;
import flash.filters.ColorMatrixFilter;
import flash.filters.DisplacementMapFilter;
import flash.filters.DisplacementMapFilterMode;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.utils.ByteArray;

class Reflection extends LightSystem
{
	static private var sTempBitmap:BitmapData = new BitmapData(1, 1);
	
	public var colorMatrix:ColorMatrix;
	
	private var mBitmap:Bitmap;
	private var mWaveDisplacementMap:BitmapData;
	private var mRipples:Array<Ripple>;
	public var masks:Array<Bitmap>;
	public var horizonLine:Float;
	public var vignette:Float;
	public var displacementPatternPath(default, set):String;
	public var texturePatternPath(default, set):String;
	public var scrollSpeedX:Float;
	public var scrollSpeedY:Float;
	private var mDisplacementPattern:BitmapData;
	private var mTexturePattern:BitmapData;
	private var mCurrentWaveScroll:Point;

	public function new() 
	{
		super();
		masks = [];
		mBitmap = new Bitmap();
		mRipples = [];
		addChild(mBitmap);
		horizonLine = Lib.current.stage.stageHeight * 0.5;
		vignette = 100;
		scrollSpeedX = scrollSpeedY = 0;
		mCurrentWaveScroll = new Point();
	}
	
	private inline function set_displacementPatternPath(val:String):String
	{
		if (val != displacementPatternPath)
		{
			if (displacementPatternPath != null && displacementPatternPath != "")
			{
				BitmapManager.unlock(displacementPatternPath, this);
				mDisplacementPattern = null;
			}
			displacementPatternPath = val;
			if (displacementPatternPath != null && displacementPatternPath != "")
				BitmapManager.loadAndLock(displacementPatternPath, this);
		}
		return val;
	}
	
	private inline function set_texturePatternPath(val:String):String
	{
		if (val != texturePatternPath)
		{
			if (texturePatternPath != null && texturePatternPath != "")
			{
				BitmapManager.unlock(texturePatternPath, this);
				mTexturePattern = null;
			}
			texturePatternPath = val;
			if (texturePatternPath != null && texturePatternPath != "")
				BitmapManager.loadAndLock(texturePatternPath, this);
		}
		return val;
	}
	
	override public function clear():Void 
	{
		if (mBitmap.bitmapData != null)
			mBitmap.bitmapData.fillRect(new Rectangle(0, 0, mBitmap.bitmapData.width, mBitmap.bitmapData.height), 0x00000000);
		if (mWaveDisplacementMap != null)
			mWaveDisplacementMap.fillRect(new Rectangle(0, 0, mWaveDisplacementMap.width, mWaveDisplacementMap.height), 0x00008080);
	}
	
	public function renderReflection(source:BitmapData, camera:Point):Void
	{
		if (mBitmap.bitmapData == null || mBitmap.bitmapData.width != source.width || mBitmap.bitmapData.height != source.height)
		{
			sTempBitmap.dispose();
			sTempBitmap = new BitmapData(source.width, source.height, true, 0x00000000);
			if (mBitmap.bitmapData != null)
				mBitmap.bitmapData.dispose();
			mBitmap.bitmapData = new BitmapData(source.width, source.height, true, 0x00000000);
			if (mWaveDisplacementMap != null)
				mWaveDisplacementMap.dispose();
			mWaveDisplacementMap = new BitmapData(source.width, source.height, false, 0x80808080);
		}
		var drawRegion:Rectangle = 
			{
				var min:Point = new Point(source.width, source.height);
				var max:Point = new Point();
				for (m in masks)
				{
					min.x = Math.min(m.x - camera.x, min.x);
					min.y = Math.min(m.y - camera.y, min.y);
					max.x = Math.max(m.x + m.bitmapData.width - camera.x, max.x);
					max.y = Math.max(m.y + m.bitmapData.height - camera.y, max.y);
				}
				if (min.x < -10)
					min.x = -10;
				if (min.y < -10)
					min.y = -10;
				if (max.x > source.width + 10)
					max.x = source.width + 10;
				if (max.y > source.height + 10)
					max.y = source.height + 10;
				if (min.x >= max.x || min.y >= max.y)
					return;
				new Rectangle(min.x, min.y, max.x - min.x, max.y - min.y);
			}
		sTempBitmap.fillRect(new Rectangle(0, 0, source.width, source.height), 0x00000000);
		if (displacementPatternPath != null && displacementPatternPath != "" && mDisplacementPattern == null && BitmapManager.query(displacementPatternPath) == Ready)
			mDisplacementPattern = BitmapManager.request(displacementPatternPath);
		if (texturePatternPath != null && texturePatternPath != "" && mTexturePattern == null && BitmapManager.query(texturePatternPath) == Ready)
			mTexturePattern = BitmapManager.request(texturePatternPath);
		
		// 1 - copy foreground
		// 1 - draw texture on top
		// 3 - overlay flipped foreground with gradient fade (vignette)
		// 4 - create wave map for displacement
		// 5 - add ripples to displacement map
		// 6 - apply displacement map
		// 7 - Use masks to draw
		// 8 - apply color matrix
			
		mBitmap.bitmapData.copyPixels(source, new Rectangle(0, 0, source.width, source.height), new Point());// drawRegion, drawTarget);
		if (mTexturePattern != null)
		{
			var r:Rectangle = new Rectangle(0, 0, mTexturePattern.width, mTexturePattern.height);
			var p:Point = new Point();
			for (y in 0...(Math.ceil(mBitmap.bitmapData.height / mTexturePattern.height)))
			{
				for (x in 0...(Math.ceil(mBitmap.bitmapData.width / mTexturePattern.width)))				
				{
					if (p.x + r.width >= drawRegion.left && p.x <= drawRegion.right && p.y + r.height >= drawRegion.top && p.y <= drawRegion.bottom)
						mBitmap.bitmapData.copyPixels(mTexturePattern, r, p, null, null, true);
					p.x += mTexturePattern.width;
				}
				p.x = 0;
				p.y += mTexturePattern.height;
			}
		}
		
		{
			var m:Matrix = new Matrix();
			m.translate(0, camera.y - horizonLine);
			m.scale(1, -1);
			m.translate(0, horizonLine - camera.y);
			var s:Sprite = new Sprite();
			var gradMat:Matrix = new Matrix();
			var reflectEdge:Float = (horizonLine - camera.y) * 2;
			
			gradMat.createGradientBox(drawRegion.width, drawRegion.height, Math.PI * 0.5);
			s.graphics.beginGradientFill(GradientType.LINEAR, [0xFFFFFF, 0xFFFFFF], [1, 0], 
				[0xFE * Math.max(0, Math.min(1, (reflectEdge - drawRegion.y - vignette) / drawRegion.height)), 0xFF * Math.max(0, Math.min(1, (reflectEdge - drawRegion.y) / drawRegion.height))], 
				gradMat);
			s.graphics.drawRect(0, 0, drawRegion.width, drawRegion.height);
			var bmp:BitmapData = new BitmapData(Math.ceil(drawRegion.width), Math.ceil(drawRegion.height), true, 0x00000000);
			bmp.draw(s);
			sTempBitmap.draw(source, m, null, null, drawRegion);
			mBitmap.bitmapData.copyPixels(sTempBitmap, drawRegion, new Point(drawRegion.x, drawRegion.y), bmp, new Point(), true);
			bmp.dispose();
			sTempBitmap.fillRect(new Rectangle(0, 0, source.width, source.height), 0x00000000);
		}
		
		if (mDisplacementPattern != null)
		{
			var r:Rectangle = new Rectangle(0, 0, mDisplacementPattern.width, mDisplacementPattern.height);
			var p:Point = new Point( -mDisplacementPattern.width + mCurrentWaveScroll.x, -mDisplacementPattern.height + mCurrentWaveScroll.y);
			for (y in 0...(Math.ceil(mWaveDisplacementMap.height / mDisplacementPattern.height) + 1))
			{
				for (x in 0...(Math.ceil(mWaveDisplacementMap.width / mDisplacementPattern.width) + 1))				
				{
					if (p.x + r.width >= drawRegion.left && p.x <= drawRegion.right && p.y + r.height >= drawRegion.top && p.y <= drawRegion.bottom)
						mWaveDisplacementMap.copyPixels(mDisplacementPattern, r, p);
					p.x += mDisplacementPattern.width;
				}
				p.x = -mDisplacementPattern.width + mCurrentWaveScroll.x;
				p.y += mDisplacementPattern.height;
			}
		}
			
		for (r in mRipples)
			if (r.x - camera.x + r.distance >= drawRegion.left && r.x - camera.x - r.distance <= drawRegion.right && 
				r.y - camera.y + r.distance >= drawRegion.top && r.y - camera.y- r.distance <= drawRegion.bottom)
					r.drawDisplacement(mWaveDisplacementMap, camera, 0.5);
				
		for (r in mRipples)
			if (r.x - camera.x + r.distance >= drawRegion.left && r.x - camera.x - r.distance <= drawRegion.right && 
				r.y - camera.y + r.distance >= drawRegion.top && r.y - camera.y- r.distance <= drawRegion.bottom)
					r.drawHighlight(mBitmap.bitmapData, camera, 0.5);
					
		if (mWaveDisplacementMap != null)
			mBitmap.bitmapData.applyFilter(mBitmap.bitmapData, new Rectangle(0, 0, mBitmap.bitmapData.width, mBitmap.bitmapData.height), new Point(),//drawRegion, drawTarget, 
				new DisplacementMapFilter(mWaveDisplacementMap, new Point(), BitmapDataChannel.BLUE, BitmapDataChannel.GREEN, 100, 100));
		
		for (m in masks)
			sTempBitmap.copyPixels(mBitmap.bitmapData, new Rectangle(0, 0, source.width, source.height), new Point(), m.bitmapData, new Point(camera.x - m.x, camera.y - m.y), true);
		var tmp:BitmapData = mBitmap.bitmapData;
		mBitmap.bitmapData = sTempBitmap;
		sTempBitmap = tmp;
		if (colorMatrix != null && !colorMatrix.isIdentity())
			mBitmap.bitmapData.applyFilter(mBitmap.bitmapData, new Rectangle(0, 0, source.width, source.height), new Point(), new ColorMatrixFilter(colorMatrix.matrix));
	}
	
	public function addRipple(x:Float, y:Float, intensity:Float, distance:Float, duration:Float):Void
	{
		mRipples.push(new Ripple(x, y, intensity, distance, duration));
	}
	
	public function update(time:Float):Void
	{
		var remove:Array<Int> = [];
		for (i in 0...mRipples.length)
		{
			mRipples[i].update(time);
			if (mRipples[i].expired)
				remove.push(i);
		}
		while (remove.length > 0)
			mRipples.splice(remove.pop(), 1);
			
		if (mDisplacementPattern != null)
		{
			mCurrentWaveScroll.x = (mCurrentWaveScroll.x + scrollSpeedX * time) % mDisplacementPattern.width;
			mCurrentWaveScroll.y = (mCurrentWaveScroll.y + scrollSpeedY * time) % mDisplacementPattern.height;
		}
	}
	
	public function draw(surface:BitmapData):Void
	{
		if (mBitmap.bitmapData != null)
			surface.draw(mBitmap.bitmapData);
	}
	
	override public function reset():Void 
	{
		super.reset();
		masks = [];
	}
}

private class Ripple
{
	public var x:Float;
	public var y:Float;
	public var intensity:Float;
	public var distance:Float;
	public var duration:Float;
	public var expired(get, never):Bool;
	
	private var radius:Float;
	private var width:Float;
	private var alpha:Float;
	private var life:Float;
	
	public function new(x:Float, y:Float, intensity:Float, distance:Float, duration:Float)
	{
		this.x = x;
		this.y = y;
		this.intensity = intensity;
		this.distance = distance;
		this.duration = duration;
		this.life = 0;
		this.radius = 0;
		this.alpha = 1;
		this.width = 1.1 * intensity;
	}
	
	private inline function get_expired():Bool
	{
		return life > duration;
	}
	
	public  function update(time:Float):Void
	{
		life += time;
		if (life > duration)
			return;
		var amount:Float = ((duration - life) / duration);
		radius = distance * (1 - amount);
		alpha = amount;
		width = (1.1 - amount) * intensity;
	}
	
	public function drawDisplacement(surface:BitmapData, camera:Point, angle:Float):Void
	{
		if (life > duration)
			return;
		var s:Sprite = new Sprite();
		var g:Graphics = s.graphics;
		var size:Float = radius + width;
		var gradMat:Matrix = new Matrix();
		gradMat.createGradientBox(size * 2, size * 2, 0, -size, -size);
		var drawMat:Matrix = new Matrix();
		drawMat.scale(1, angle);
		drawMat.translate(x - camera.x, y - camera.y);
		// Top Left
		g.beginGradientFill(GradientType.RADIAL, [0x8080, 0xFFFF, 0x0000, 0x8080], [0, alpha, alpha, 0], 
			[Math.floor(((radius - width) / size) * 0xFF), Math.floor(((radius - width) / size) * 0xFF), Math.round((radius / size) * 0xFF), 0xFF], gradMat);
		g.drawCircle(0, 0, size);
		surface.draw(s, drawMat, null, null, new Rectangle(x - camera.x - size, y - camera.y - size * angle, size, size * angle));
		
		// Top Right
		g.clear();
		g.beginGradientFill(GradientType.RADIAL, [0x8080, 0x00FF, 0xFF00, 0x8080], [0, alpha, alpha, 0], 
			[Math.floor(((radius - width) / size) * 0xFF), Math.floor(((radius - width) / size) * 0xFF), Math.round((radius / size) * 0xFF), 0xFF], gradMat);
		g.drawCircle(0, 0, size);
		surface.draw(s, drawMat, null, null, new Rectangle(x - camera.x, y - camera.y - size * angle, size, size * angle));
		
		// Bottom Left
		g.clear();
		g.beginGradientFill(GradientType.RADIAL, [0x8080, 0xFF00, 0x00FF, 0x8080], [0, alpha, alpha, 0], 
			[Math.floor(((radius - width) / size) * 0xFF), Math.floor(((radius - width) / size) * 0xFF), Math.round((radius / size) * 0xFF), 0xFF], gradMat);
		g.drawCircle(0, 0, size);
		surface.draw(s, drawMat, null, null, new Rectangle(x - camera.x - size, y - camera.y, size, size * angle));
		
		// Bottom Right
		g.clear();
		g.beginGradientFill(GradientType.RADIAL, [0x8080, 0x0000, 0xFFFF, 0x8080], [0, alpha, alpha, 0], 
			[Math.floor(((radius - width) / size) * 0xFF), Math.floor(((radius - width) / size) * 0xFF), Math.round((radius / size) * 0xFF), 0xFF], gradMat);
		g.drawCircle(0, 0, size);
		surface.draw(s, drawMat, null, null, new Rectangle(x - camera.x, y - camera.y, size, size * angle));
	}
	
	public function drawHighlight(surface:BitmapData, camera:Point, angle:Float):Void
	{
		if (life > duration)
			return;
		var s:Sprite = new Sprite();
		var g:Graphics = s.graphics;
		var gradMat:Matrix = new Matrix();
		gradMat.createGradientBox(radius * 2, radius * 2, 0, -radius, -radius);
		var drawMat:Matrix = new Matrix();
		drawMat.scale(1, angle);
		drawMat.translate(x - camera.x, y - camera.y);
		
		g.beginGradientFill(GradientType.RADIAL, [0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF], [0, 0.5, 0], 
			[0xF0, 0xF8, 0xFF], gradMat);
		g.drawCircle(0, 0, radius + 1);
		surface.draw(s, drawMat);
	}
}