package com.problemmachine.eve.game.lighting;
import com.problemmachine.util.color.Color;
import flash.display.BitmapData;
import flash.display.BlendMode;
import flash.display.GradientType;
import flash.display.Graphics;
import flash.display.Sprite;
import flash.geom.Matrix;
import flash.geom.Point;

class LightSource
{
	public var cutOut:Bool;
	public var position:Point;
	public var innerColor:Color;
	public var innerRadius:Float;
	public var outerColor:Color;
	public var outerRadius:Float;
	public var intensity:Float;
	public var pattern:LightPattern;
	
	public function new() 
	{
		position = new Point();
		innerColor = Color.createFromRGB(0xFF, 0xFF, 0xFF);
		innerRadius = 100;
		outerColor = Color.createFromRGB(0xFF, 0xFF, 0xFF);
		outerRadius = 500;
		intensity = 0.5;
		cutOut = false;
		pattern = null;
	}
	
	public function cut(target:Graphics, camera:Point):Void
	{
		target.drawCircle(position.x - camera.x, position.y - camera.y, outerRadius);
	}
	
	public function draw(target:Graphics, camera:Point):Void
	{
		//TODO: Early exit if light is off-screen
		var m:Matrix = new Matrix();
		if (pattern != null)
		{
			m.translate(position.x - camera.x - outerRadius + pattern.offset.x, position.y - camera.y - outerRadius + pattern.offset.y);
			m.scale(pattern.scale, pattern.scale);
			target.beginBitmapFill(pattern.data, m, true);
		}
		else if (cutOut)
		{
			m.createGradientBox(outerRadius * 2, outerRadius * 2, 0, position.x - camera.x - outerRadius, position.y - camera.y - outerRadius);
			target.beginGradientFill(GradientType.RADIAL, [innerColor.raw, outerColor.raw], [0, 1], 
				[Math.floor((innerRadius / outerRadius) * 0xFF), 0xFF], m);
		}
		else
		{
			m.createGradientBox(outerRadius * 2, outerRadius * 2, 0, position.x - camera.x - outerRadius, position.y - camera.y - outerRadius);
			target.beginGradientFill(GradientType.RADIAL, [innerColor.raw, outerColor.raw], [intensity, 0], 
				[Math.floor((innerRadius / outerRadius) * 0xFF), 0xFF], m);
		}
		target.drawCircle(position.x - camera.x, position.y - camera.y, outerRadius);
	}
}