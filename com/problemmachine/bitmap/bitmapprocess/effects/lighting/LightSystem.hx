package com.problemmachine.eve.game.lighting;
import com.problemmachine.bitmap.bitmapmanager.BitmapManager;
import com.problemmachine.bitmap.bitmapmanager.BitmapManagerEvent;
import com.problemmachine.bitmap.bitmapprocess.i.IBitmapProcess;
import com.problemmachine.util.color.Color;
import flash.display.BitmapData;
import flash.display.BlendMode;
import flash.display.Graphics;
import flash.display.Sprite;
import flash.geom.Matrix;
import flash.geom.Point;

class LightSystem implements IBitmapProcess
{
	public var lightSources:Array<LightSource>;
	public var offset:Point;
	
	public var color:Color;
	public var intensity:Float;
	public var pattern(default, set):String;
	public var patternScale:Float;
	public var patternOffset:Point;
	private var patternData:BitmapData;
	
	private var drawSprite:Graphics;
	
	public function new(?color:Color, intensity:Float = 0, ?pattern:String, patternScale:Float = 1, ?patternOffset:Point) 
	{
		super();
		drawSprite = new();
		offset = new();
		
		if (color != null)
			this.color = color;
		else
			this.color = Color.createFromRGB(0, 0, 0);
		this.intensity = intensity;
		this.pattern = pattern;
		if (patternOffset == null)
			patternOffset = new Point();
		this.patternOffset = patternOffset;
		this.patternScale = patternScale;
		
		reset();
	}
	
	private inline function set_pattern(val:String):String
	{
		if (val != pattern)
		{
			if (pattern != null && pattern != "")
			{
				BitmapManager.unlock(pattern, this);
				if (patternData == null)
					BitmapManager.stopListeningForCompletion(pattern, this);
			}
			pattern = val;
			if (pattern != null && pattern != "")
			{
				BitmapManager.lock(pattern, this);
				if (BitmapManager.query(pattern) == Ready)
					patternData = BitmapManager.request(pattern);
				else
				{
					BitmapManager.startListeningForCompletion(pattern, patternListener);
					BitmapManager.load(pattern);
				}
			}
			
		}
		return pattern;
	}
	
	private function patternListener(e:BitmapManagerEvent):Void
	{
		patternData = e.data;
		BitmapManager.stopListeningForCompletion(pattern, this);
	}
	
	public function clear():Void
	{
		
	}
	
	private function drawAmbient(width:Float, height:Float):Void
	{
		if (intensity == 0)
			return;
		if (patternData != null)
		{
			var m:Matrix = new Matrix();
			m.translate(pattern.offset.x, pattern.offset.y);
			m.scale(pattern.scale, pattern.scale);
			drawSprite.graphics.beginBitmapFill(patternData, m, true);
		}
		else
		{
			drawSprite.graphics.beginFill(color.raw, intensity);
		}
		drawSprite.graphics.drawRect(0, 0, width, height);
	}
	
	public function update(time:Float):Void
	
	public function draw(source:BitmapData):Void
	{			
		drawSprite.graphics.clear();
		drawAmbient(source.width, source.height);
		for (l in lightSources)
			if (l.cutOut)
				l.cut(graphics, offset);
		for (l in drawSprite.graphics)
			l.draw(graphics, offset);
	}
	
	public function reset():Void
	{
		lights = []; 
		pattern = null;
		patternData = null;
		patternOffset = new Point();
		patternScale = 1;
	}
	
}