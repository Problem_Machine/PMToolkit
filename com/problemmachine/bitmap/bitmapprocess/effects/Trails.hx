package com.problemmachine.bitmap.bitmapprocess.effects;

import com.problemmachine.bitmap.bitmapprocess.interface.IBitmapProcess;
import flash.display.BitmapData;
import flash.errors.RangeError;
import flash.filters.BlurFilter;
import flash.geom.ColorTransform;
import flash.geom.Point;
import flash.geom.Rectangle;

class Trails implements com.problemmachine.bitmap.bitmapprocess.i.IBitmapProcess 
{
	public var blurFilter(default, default):BlurFilter;
	public var colorTransform(default, default):ColorTransform;
	public var blendMode(default, default):String;
	private var lastFrame:BitmapData;
	
	public function new(?blurFilter:BlurFilter, ?colorTransform:ColorTransform, ?blendMode:String, backFrames:Int = 2) 
	{
		this.blurFilter = blurFilter;
		this.colorTransform = colorTransform;
		this.blur = blur;
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.alpha = alpha;
		this.blendMode = blendMode;
		lastFrame = null;
	}
	
	public function update(time:Float):Void
	{
		
	}
	
	public function process(source:BitmapData):Void 
	{
		if (lastFrame != null)
			source.draw(lastFrame, null, colorTransform, blendMode);
		if (lastFrame == null)
			lastFrame = new BitmapData(source.width, source.height, source.transparent, 0x00000000);
		if (blurFilter != null)
			lastFrame.applyFilter(source, new Rectangle(0, 0, source.width, source.height), new Point(), blurFilter);
		else
			lastFrame.copyPixels(source, new Rectangle(0, 0, source.width, source.height), new Point());
	}	
	
	public function reset():Void
	{
		lastFrame.dispose();
		lastFrame = null;
		
	}
}