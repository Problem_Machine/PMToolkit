package com.problemmachine.bitmap.bitmapbrowser;
import flash.events.Event;

/**
 * ...
 * @author 
 */
class BitmapBrowserEvent extends Event
{
	public static inline var CANCEL:String = "Cancel Image Select";
	public static inline var CONFIRM:String = "Confirm Image Select";
	
	public var path(default, null):String;

	public function new(path:String, type:String, bubbles:Bool = false, cancelable:Bool = false) 
	{
		super(type, bubbles, cancelable);
		this.path = path;
	}
	
}