package com.problemmachine.bitmap.bitmapbrowser;
import com.problemmachine.bitmap.bitmapmanager.BitmapManager;
import com.problemmachine.bitmap.bitmapmanager.BitmapManagerEvent;
import com.problemmachine.bitmap.bitmapmanager.BitmapManagerQuery;
import com.problemmachine.ui.scrollwindow.ScrollWindow;
import flash.display.Bitmap;
import flash.display.BlendMode;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.text.TextFieldType;
import flash.text.TextFormat;
import flash.text.TextFormatAlign;

class BitmapBrowser extends Sprite
{
	private var mWindow:ScrollWindow;
	private var mVerticalAlign:Bool;
	private var mNodes:Array<BitmapBrowserNode>;
	private var mNodeWidth:Float;
	private var mNodeHeight:Float;
	private var mTextSize:Float;
	
	private var mFilterTextBoxBack:Sprite;
	private var mFilterTextField:TextField;
	private var mFilterTextInputField:TextField;
	
	private var mSelectedImageDisplay:Sprite;
	private var mSelectedImageMask:Sprite;
	private var mSelectedImage:Bitmap;
	private var mSelectedImagePath:String;
	private var mSelectedNodeHighlight:Sprite;
	
	private var mConfirmButton:Sprite;
	private var mConfirmText:TextField;
	private var mCancelButton:Sprite;
	private var mCancelText:TextField;
	
	public var passiveFilter(default, default):String = "";

	public function new(width:Float, height:Float, color1:UInt, color2:UInt, nodeWidth:Float = 200, nodeHeight:Float = 150, 
		textSize:Int = 16, barThickness:Float = 30, alignVertically:Bool = true) 
	{
		super();
		mVerticalAlign = alignVertically;
		mNodeWidth = nodeWidth;
		mNodeHeight = nodeHeight;
		mTextSize = textSize;
		graphics.beginFill(color1);
		graphics.drawRoundRect(0, 0, width, height, 5);
		
		mSelectedImageDisplay = new Sprite();
		mSelectedImageDisplay.graphics.lineStyle(5, color1);
		mSelectedImageDisplay.graphics.beginFill(color2);
		mSelectedImageDisplay.graphics.drawRoundRect(0, 0, width * 0.25, height - 100, 5);
		mSelectedImageDisplay.x = width - mSelectedImageDisplay.width;
		addChild(mSelectedImageDisplay);
		mSelectedImageMask = new Sprite();
		mSelectedImageMask.graphics.beginFill(0xFFFFFF);
		mSelectedImageMask.graphics.drawRoundRect(2.5, 2.5, width * 0.25 - 5, height - 105, 5);
		mSelectedImageDisplay.addChild(mSelectedImageMask);
		mSelectedImage = new Bitmap();
		mSelectedImage.mask = mSelectedImageMask;
		mSelectedImage.alpha = 0.8;
		mSelectedImagePath = "";
		
		mSelectedNodeHighlight = new Sprite();
		mSelectedNodeHighlight.graphics.lineStyle(20, 0xFFFFFF);
		mSelectedNodeHighlight.graphics.beginFill(color1, 0.5);
		mSelectedNodeHighlight.graphics.drawRoundRect(-10, -10, nodeWidth + 20, nodeHeight + 20, 5);
		mSelectedNodeHighlight.blendMode = BlendMode.ADD;
		
		mFilterTextBoxBack = new Sprite();
		mFilterTextBoxBack.graphics.lineStyle(5, color1);
		mFilterTextBoxBack.graphics.beginFill(color2);
		mFilterTextBoxBack.graphics.drawRoundRect(0, 0, width - mSelectedImageDisplay.width, 30, 5);
		addChild(mFilterTextBoxBack);
		
		mFilterTextField = new TextField();
		mFilterTextField.defaultTextFormat = new TextFormat(null, textSize, color1);
		mFilterTextField.text = "Filters: ";
		mFilterTextField.x = 5;
		mFilterTextField.y = 5;
		mFilterTextField.height = 20;
		mFilterTextField.width = mFilterTextField.textWidth + 4;
		mFilterTextBoxBack.addChild(mFilterTextField);
		
		mFilterTextInputField = new TextField();
		mFilterTextInputField.defaultTextFormat = new TextFormat(null, textSize, color1);
		mFilterTextInputField.border = true;
		mFilterTextInputField.borderColor = color1;
		mFilterTextInputField.type = TextFieldType.INPUT;
		mFilterTextInputField.x = mFilterTextField.width + mFilterTextField.x + 5;
		mFilterTextInputField.y = 5;
		mFilterTextInputField.height = 20;
		mFilterTextInputField.width = mFilterTextBoxBack.width - mFilterTextField.width - 20;
		mFilterTextInputField.addEventListener(Event.CHANGE, filterChangeListener);
		mFilterTextBoxBack.addChild(mFilterTextInputField);
		
		mWindow = new ScrollWindow(width - mSelectedImageDisplay.width, height - mFilterTextBoxBack.height, color1, color2, barThickness);
		mWindow.enableMovement = false;
		mWindow.enableHorizontalScroll = !alignVertically;
		mWindow.enableVerticalScroll = alignVertically;
		mWindow.panel.y = mFilterTextBoxBack.height;
		addChild(mWindow.panel);
		
		mNodes = new Array<BitmapBrowserNode>();
		addEventListener(Event.ADDED_TO_STAGE, stageListener);
		addEventListener(Event.REMOVED_FROM_STAGE, stageListener);
		
		mCancelButton = new Sprite();
		mCancelButton.graphics.lineStyle(5, color2);
		mCancelButton.graphics.beginFill(color1);
		mCancelButton.graphics.drawRect(2.5, 2.5, mSelectedImageDisplay.width - 5, 45);
		mCancelButton.graphics.endFill();
		mCancelButton.x = mSelectedImageDisplay.x;
		mCancelButton.y = height - 100;
		mCancelButton.buttonMode = mCancelButton.useHandCursor = true;
		addChild(mCancelButton);
		
		mCancelText = new TextField();
		mCancelText.defaultTextFormat = new TextFormat(null, textSize * 2, color2, null, null, null, null, null, TextFormatAlign.CENTER);
		mCancelText.text = "Cancel";
		mCancelText.x = 5;
		mCancelText.width = mCancelButton.width - 10;
		mCancelText.height = 44;
		mCancelText.mouseEnabled = false;
		mCancelButton.addChild(mCancelText);
		mCancelButton.addEventListener(MouseEvent.CLICK, function(e:MouseEvent):Void
			{	dispatchEvent(new BitmapBrowserEvent("", BitmapBrowserEvent.CANCEL));	parent.removeChild(this);	} );
		
		mConfirmButton = new Sprite();
		mConfirmText = new TextField();
		drawConfirmButton(false);
		mConfirmButton.x = mSelectedImageDisplay.x;
		mConfirmButton.y = height - 50;
		addChild(mConfirmButton);
		
		mConfirmText.defaultTextFormat = new TextFormat(null, textSize * 2, 0x444444, null, null, null, null, null, TextFormatAlign.CENTER);
		mConfirmText.text = "Confirm";
		mConfirmText.x = 5;
		mConfirmText.width = mCancelButton.width - 10;
		mConfirmText.height = 44;
		mConfirmText.mouseEnabled = false;
		mConfirmButton.addChild(mConfirmText);
		mConfirmButton.addEventListener(MouseEvent.CLICK, function(e:MouseEvent):Void
			{	dispatchEvent(new BitmapBrowserEvent(mSelectedImagePath, BitmapBrowserEvent.CONFIRM));	parent.removeChild(this);	} );
	}
	
	public function selectImage(path:String):Void
	{
		for (n in mNodes)
			if (n.path == path)
			{
				selectNode(n);
				return;
			}
	}
	
	private function stageListener(e:Event):Void
	{
		if (e.type == Event.ADDED_TO_STAGE)
		{
			populate();
			displayBitmaps();
			addEventListener(Event.ENTER_FRAME, updateNodeListener);
		}
		else
		{
			if (mSelectedImagePath != "")
				BitmapManager.unlock(mSelectedImagePath, this);
			mSelectedImagePath = "";
			mSelectedImage.bitmapData = null;
			removeEventListener(Event.ENTER_FRAME, updateNodeListener);
			if (mSelectedNodeHighlight.parent != null)
				mSelectedNodeHighlight.parent.removeChild(mSelectedNodeHighlight);
			while (mNodes.length > 0)
			{
				var n:BitmapBrowserNode = mNodes.pop();
				n.removeEventListener(MouseEvent.CLICK, selectListener);
				if (n.parent != null)
					n.parent.removeChild(n);
				n.dispose();
			}
		}
	}
	
	private function updateNodeListener(e:Event):Void
	{
		for (n in mNodes)
			if (n.parent != null)
				n.ready = mWindow.objectCurrentlyVisible(n);
	}
	
	private function filterChangeListener(e:Event):Void
	{
		displayBitmaps((cast e.target).text);
		if (mSelectedNodeHighlight.parent != null)
			mSelectedNodeHighlight.parent.removeChild(mSelectedNodeHighlight);
		
		for (n in mNodes)
			if (n.path == mSelectedImagePath)
			{
				if (n.parent != null)
				{
					mSelectedNodeHighlight.x = n.x;
					mSelectedNodeHighlight.y = n.y;
					mWindow.addChild(mSelectedNodeHighlight);
				}
				break;
			}
	}
	
	public function populate():Void
	{
		while (mNodes.length > 0)
		{
			var n = mNodes.pop();
			n.removeEventListener(MouseEvent.CLICK, selectListener);
			if (n.parent != null)
				n.parent.removeChild(n);
			n.dispose();
		}
		var arr:Array<String> = BitmapManager.getAllKnownResources();
		for (s in arr)
		{
			var n:BitmapBrowserNode = new BitmapBrowserNode(s, mNodeWidth, mNodeHeight, mWindow.color2, mWindow.color1, Math.ceil(mTextSize * (2 / 3)));
			n.addEventListener(MouseEvent.CLICK, selectListener);
			mNodes.push(n);
		}
	}
	
	private function displayBitmaps(filter:String = ""):Void
	{
		filter += " " + passiveFilter;
		
		var filters:Array<String> = filter.split(" ");
		for (i in 0...filters.length)
			if (filters[filters.length - 1 - i] == "")
				filters.splice(filters.length - 1 - i, 1);
		if (filters.length > 0)
		{
			for (n in mNodes)
			{
				var valid:Bool = true;
				var p:String = n.path.toLowerCase();
				for (f in filters)
					if (p.indexOf(f.toLowerCase()) == -1)
					{
						valid = false;
						break;
					}
				if (valid)
				{
					if (n.parent == null)
						mWindow.addChild(n);
				}
				else if (n.parent != null)					
					n.parent.removeChild(n);
			}
		}
		else for (n in mNodes)
			if (n.parent == null)
				mWindow.addChild(n);
		
		arrangeNodes();
	}
	
	private function selectListener(e:MouseEvent):Void
	{
		selectNode(cast e.target);
	}
	
	private function selectNode(node:BitmapBrowserNode):Void
	{
		if (mSelectedImage.parent != null)
			mSelectedImage.parent.removeChild(mSelectedImage);
		if (mSelectedImagePath != "")
			BitmapManager.unlock(mSelectedImagePath, this);
		mSelectedImagePath = node.path;
		BitmapManager.lock(mSelectedImagePath, this);
		mSelectedImage.bitmapData = BitmapManager.request(mSelectedImagePath);
		var ratio:Float = mSelectedImage.width / mSelectedImage.height;
		if (ratio > mSelectedImageDisplay.width / mSelectedImageDisplay.height)
		{
			mSelectedImage.width = mSelectedImageMask.width * 0.9;
			mSelectedImage.height = mSelectedImage.width / ratio;
		}
		else
		{
			mSelectedImage.height = mSelectedImageMask.height * 0.9;
			mSelectedImage.width = mSelectedImage.height * ratio;
		}
		mSelectedImageDisplay.addChild(mSelectedImage);
		mSelectedNodeHighlight.x = node.x;
		mSelectedNodeHighlight.y = node.y;
		mWindow.addChild(mSelectedNodeHighlight);
		drawConfirmButton(true);
	}
	
	private function drawConfirmButton(enabled:Bool):Void
	{
		if (enabled)
		{
			mConfirmButton.graphics.lineStyle(5, mWindow.color2);
			mConfirmButton.graphics.beginFill(mWindow.color1);
			mConfirmText.textColor = mWindow.color2;
		}
		else
		{
			mConfirmButton.graphics.lineStyle(5, 0x444444);
			mConfirmButton.graphics.beginFill(0x666666);
			mConfirmText.textColor = 0x444444;
		}
		mConfirmButton.graphics.drawRect(2.5, 2.5, mSelectedImageDisplay.width - 5, 45);
		mConfirmButton.graphics.endFill();
		mConfirmButton.mouseEnabled = mConfirmButton.buttonMode = mConfirmButton.useHandCursor = enabled;
	}
	
	private function arrangeNodes():Void
	{
		var tempX:Float = 10;
		var tempY:Float = 10;
		var biggest:Float = 0;
		for (n in mNodes)
		{
			if (n.parent == null)
				continue;
			if (mVerticalAlign)
			{
				if (tempX + n.width > mWindow.width)
				{
					tempX = 10;
					tempY += biggest;
					biggest = 0;
				}
				if (n.height > biggest)
					biggest = n.height;
			}
			else
			{
				if (tempY + n.height > mWindow.height)
				{
					tempY = 10;
					tempX += biggest;
					biggest = 0;
				}
				if (n.width > biggest)
					biggest = n.width;
			}	
					
						
			n.x = tempX;
			n.y = tempY;
			
			if (mVerticalAlign)
				tempX += n.width;
			else
				tempY += n.height;
		}
	}
}

private class BitmapBrowserNode extends Sprite
{
	public var path(default, null):String;
	public var ready(get, set):Bool;
	
	private var mLocked:Bool;
	private var mPathDisplay:TextField;
	private var mImageDisplay:Bitmap;
	private var mMask:Sprite;
	
	public function new(path:String, width:Float, height:Float, backColor:UInt, textColor:UInt, textSize:Int)
	{
		super();
		this.path = path;
		mLocked = false;
		buttonMode = true;
		useHandCursor = true;
		
		graphics.beginFill(backColor);
		graphics.lineStyle(5, textColor, 0.5);
		graphics.drawRoundRect(0, 0, width, height, Math.min(width, height) * 0.05);
		graphics.endFill();
		mMask = new Sprite();
		mMask.graphics.beginFill(0xFFFFFF);
		mMask.graphics.drawRoundRect(0, 0, width, height, Math.min(width, height) * 0.05);
		mMask.graphics.endFill();
		addChild(mMask);
			
		mImageDisplay = new Bitmap();
		mImageDisplay.x = width * 0.05;
		mImageDisplay.y = height * 0.05;
		mImageDisplay.alpha = 0.5;
		mImageDisplay.mask = mMask;
		
		mPathDisplay = new TextField();
		mPathDisplay.width = width - 10;
		mPathDisplay.x = 5;
		mPathDisplay.height = height * 0.98;
		mPathDisplay.y = height * 0.01;
		mPathDisplay.mouseEnabled = false;
		mPathDisplay.multiline = true;
		mPathDisplay.defaultTextFormat = new TextFormat("Consolas", textSize, textColor);
		{
			var txt:String = path;
			var arr:Array<String> = txt.split("\\");
			
			for (i in 0...arr.length)
			{
				if (i < arr.length - 1)
				{
					mPathDisplay.text += arr[i];
					mPathDisplay.text += "\\\r";
					for (j in 0...(i + 1))
						mPathDisplay.text += "-> ";
				}
				else
				{
					var arr2:Array<String> = arr[i].split(".");
					for (j in 0...arr2.length)
					{
						if (j < arr2.length - 2)
						{
							mPathDisplay.text += arr2[j];
							mPathDisplay.text += ".\r";
							for (k in 0...i)
								mPathDisplay.text += "   ";
						}
						else
						{
							mPathDisplay.text += arr2[j];
							if (j == arr2.length - 2)
								mPathDisplay.text += ".";
						}
					}
				}
			}
		}
		addChild(mPathDisplay);
	}
	
	public function dispose():Void
	{
		ready = false;
		mPathDisplay = null;
		mImageDisplay = null;
	}
	
	private function fileListener(?e:BitmapManagerEvent):Void
	{
		BitmapManager.stopListeningForCompletion(path, fileListener);
		mImageDisplay.bitmapData = BitmapManager.request(path);
		var ratio:Float = mImageDisplay.width / mImageDisplay.height;
		if (ratio > width / height)
		{
			mImageDisplay.width = width * 0.9;
			mImageDisplay.height = mImageDisplay.width / ratio;
		}
		else
		{
			mImageDisplay.height = height * 0.9;
			mImageDisplay.width = mImageDisplay.height * ratio;
		}
		addChild(mImageDisplay);
	}
	
	private function get_ready():Bool
	{
		return (mLocked && BitmapManager.query(path) == BitmapManagerQuery.Ready);
	}
	
	private function set_ready(val:Bool):Bool
	{
		if (val != mLocked)
		{
			if (val)
			{
				if (!mLocked)
					lock();
			}
			else
			{
				mImageDisplay.bitmapData = null;
				if (mLocked)
					unlock();
			}
		}
		return val;
	}
	
	private function lock():Void
	{
		if (BitmapManager.query(path) == BitmapManagerQuery.Ready)
		{
			BitmapManager.lock(path, this);
			fileListener();
		}
		else
		{
			BitmapManager.startListeningForCompletion(path, fileListener);
			BitmapManager.loadAndLock(path, this);
		}
		mLocked = true;
	}
	
	private function unlock():Void
	{
		BitmapManager.stopListeningForCompletion(path, fileListener);
		BitmapManager.unlock(path, this);
		mImageDisplay.bitmapData = null;
		if (mImageDisplay.parent != null)
			removeChild(mImageDisplay);
		mLocked = false;
	}
}