package com.problemmachine.bitmap.bitmapmanager;

import com.problemmachine.tools.file.FileTools;
import com.problemmachine.tools.file.FileToolsEvent;
import com.problemmachine.util.lock.Lock;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.errors.Error;
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.Lib;
#if air
import flash.filesystem.File;
import flash.display.Loader;
#end
import flash.events.TimerEvent;
import flash.geom.Rectangle;
import flash.utils.Timer;
import haxe.CallStack;
import haxe.io.Bytes;
import haxe.io.BytesInput;
import hxd.res.NanoJpeg;

class BitmapManager 
{
	public static var rootDirectory(default, set):String = FileTools.rootDirectory;
	public static var autoReload(default, set) = false;
	private static var sAutoReloadTimer:Timer = null;
	private static var sAutoReloadIterator:Iterator<BitmapManagerAsset> = null;
	
	private static inline var DEFAULT_MAXIMUM_BYTES:UInt = 1024 * 1024 * 1024; 	// 1 gig
	private static inline var DEFAULT_TARGET_BYTES:UInt = 800 * 1024 * 1024; 	// 800 mb
	private static var sMaximumBytes:UInt = DEFAULT_MAXIMUM_BYTES;
	private static var sTargetBytes:UInt = DEFAULT_TARGET_BYTES;
	private static var sCurrentBytes:UInt = 0;
	
	private static var sAssets:Map<String, BitmapManagerAsset> = new Map<String, BitmapManagerAsset>();
	
	private static var sPendingRequests:Array<BitmapManagerAsset> = new Array<BitmapManagerAsset>();
	private static var sDispatcher:EventDispatcher = new EventDispatcher();
	private static var sIdle:Bool = true;
	
	public static var verbose:Bool = true;
	
	@:access(com.problemmachine.bitmap.bitmapmanager.BitmapManagerAsset.fileName)
	private static function set_rootDirectory(val:String):String
	{
		FileTools.formatPath(val);
		if (val.charAt(val.length - 1) != "\\")
			val += "\\";
		if (val != FileTools.formatPath(rootDirectory))
			for (a in sAssets)
				a.fileName = FileTools.getRelativePathFromAbsolutePath(val, rootDirectory + a.fileName);
		rootDirectory = val;
	
		return val;
	}
	
	private static function set_autoReload(val:Bool):Bool
	{
		if (val != autoReload)
		{
			autoReload = val;
			if (autoReload)
			{
				sAutoReloadIterator = sAssets.iterator();
				sAutoReloadTimer = new Timer(100);
				sAutoReloadTimer.addEventListener(TimerEvent.TIMER, autoReloadListener);
				sAutoReloadTimer.start();
			}
			else
			{
				sAutoReloadTimer.stop();
				sAutoReloadTimer.removeEventListener(TimerEvent.TIMER, autoReloadListener);
				sAutoReloadTimer = null;
			}
		}
		return val;
	}
	
	private static function autoReloadListener(e:TimerEvent):Void
	{
		if (!sAutoReloadIterator.hasNext())
			sAutoReloadIterator = sAssets.iterator();
		var fileName:String = sAutoReloadIterator.next().fileName;
		if (sAssets.get(fileName).data != null)
			load(fileName);
	}
	
	public static inline function isRecognizedType(ext:String):Bool
	{
		ext = ext.toLowerCase();
		//return (ext == "jpg" || ext == "jpeg" || ext == "png" || ext == "gif");
		return (ext == "jpg" || ext == "jpeg" || ext == "png");
	}
	
	public static function addAllResourcesInDirectory(path:String, includeSubDirectories:Bool = true):Void
	{		
		var files:Array<String> = FileTools.getAllFilesIn(rootDirectory, true);
		for (s in files)
			if (isRecognizedType(FileTools.extensionOf(s)))
				addResource(FileTools.getRelativePathFromAbsolutePath(rootDirectory, s));
	}
	
	public static inline function getAllKnownResources():Array<String>
	{
		var arr = new Array<String>();
		for (s in sAssets)
			arr.push(s.fileName);
		return arr;
	}
	
	public static inline function getAllLockedResources():Array<String>
	{
		var arr = new Array<String>();
		for (s in sAssets)
			if (s.isLocked)
				arr.push(s.fileName);
		return arr;
	}
	
	public static inline function reloadAll(immediate:Bool = false):Void
	{
		for (a in sAssets)
			if (immediate)
				immediateLoad(a.fileName);
			else
				load(a.fileName);
	}
	
	public static function reverseLookUp(bmp:BitmapData):String
	{
		if (bmp == null)
			return null;
		for (a in sAssets)
			if (a.data == bmp)
				return a.fileName;
				
		return null;
	}
	
	public static function immediateLoad(fileName:String, ?source:Dynamic):BitmapData
	{
		var path:String = (fileName.indexOf(":") < 0) ? rootDirectory + fileName : fileName;
		// Create the asset class instance if needed
		var g:BitmapManagerAsset = addResource(fileName);
		if (source != null)
			lock(fileName, source);
		var reloading:Bool = false;
		if (g.data != null && FileTools.fileModifiedTime(path) == g.lastModified)
			return g.data;
			
		if (verbose)
			trace(Std.string(Lib.getTimer()) + " -- ImmediateLoadrequest " + path);
		
		if (g.data != null)
		{
			sCurrentBytes -= cast(Std.int(g.data.width * g.data.height * 4), UInt);
			g.data.dispose();
			reloading = true;
		}
		
		#if (air || flash)
			// haxelib format required for Air/Flash
			var bytes:Bytes = Bytes.ofData(FileTools.immediateReadFrom(path));
			switch(FileTools.extensionOf(fileName).toLowerCase())
			{
				case "png":
					var data = new format.png.Reader(new BytesInput(bytes)).read();
					var hdr = format.png.Tools.getHeader(data);
					g.data = new BitmapData(hdr.width, hdr.height, true, 0x00000000);
					var pixels = format.png.Tools.extract32(data);
					var px = pixels.getData();
					px.position = 0;
					g.data.setPixels(new Rectangle(0, 0, g.data.width, g.data.height), px);
				case "jpg", "jpeg":
					var data = NanoJpeg.decode(bytes);
					g.data = new BitmapData(data.width, data.height, false, 0x00000000);
					g.data.setPixels(new Rectangle(0, 0, g.data.width, g.data.height), data.pixels.getData());
				case "bmp":
					var data = new format.bmp.Reader(new BytesInput(bytes)).read();
					g.data = new BitmapData(data.header.width, data.header.height, false, 0x00000000);
					g.data.setPixels(new Rectangle(0, 0, g.data.width, g.data.height), data.pixels.getData());
				default:
					throw ("ERROR: File extension " + FileTools.extensionOf(fileName) + " not recognized or not supported");
			}
		#else
			g.data = BitmapData.fromBytes(FileTools.immediateReadFrom(path));
		#end
		g.lastModified = FileTools.fileModifiedTime(path);
		
		sCurrentBytes += cast(Std.int(g.data.width * g.data.height * 4), UInt);
		sDispatcher.dispatchEvent(new BitmapManagerEvent(g.fileName.toLowerCase(), g.data));
		
		free();
		return g.data;
	}
	
	public static function load(fileName:String, priority:Int = 0):Void
	{
		var path:String = (fileName.indexOf(":") < 0) ? rootDirectory + fileName : fileName;
		// Create the asset class instance if needed
		var g:BitmapManagerAsset = addResource(fileName);
		if (g.data != null && FileTools.fileModifiedTime(path) == g.lastModified)
			return;
		if (verbose)
			trace(Std.string(Lib.getTimer()) + " -- Loadrequest " + path + " from " + CallStack.callStack()[1]);
		
		g.priority = priority;
		if (sPendingRequests.indexOf(g) < 0)
		{
			var added:Bool = false;
			// skip the first one because that one's currently loading and things can get messed up if we try to change it out
			for (i in 1...sPendingRequests.length)
				if (g.priority >= sPendingRequests[i].priority)
				{
					sPendingRequests.insert(i, g);
					added = true;
					break;
				}
			if (!added)
				sPendingRequests.push(g);
		}
			
		// If the BitmapManager is idle, start loading assets
		if (sIdle)
			loadNext();
	}
	
	public static function loadAndLock(fileName:String, source:Dynamic, priority:Int = 0):Void
	{
		fileName = FileTools.formatPath(fileName);
		load(fileName, priority);
		lock(fileName, source);
	}
	
	public static function query(fileName:String):BitmapManagerQuery
	{
		fileName = FileTools.formatPath(fileName);
		for (a in sAssets)
			if (FileTools.comparePaths(fileName, a.fileName))
			{
				if (a.data != null)
					return BitmapManagerQuery.Ready;
				else
				{
					var pending:Bool = false;
					for (a in sPendingRequests)
						if (FileTools.comparePaths(a.fileName, fileName))
						{
							pending = true;
							break;
						}
					return pending ? BitmapManagerQuery.NotReady : BitmapManagerQuery.NotActive;
				}
			}
		return BitmapManagerQuery.NotFound;
	}
	
	public static function request(fileName:String):BitmapData
	{
		fileName = FileTools.formatPath(fileName);
		for (a in sAssets)
			if (FileTools.comparePaths(fileName, a.fileName))
				return a.data;
		throw new Error("ERROR: BitmapManager could not find asset " + fileName + " to meet request");
	}
	
	public static function startListeningForCompletion(fileName:String, listener:Dynamic -> Void):Void
	{
		fileName = FileTools.formatPath(fileName).toLowerCase();
		sDispatcher.addEventListener(fileName, listener);
	}
	
	public static function stopListeningForCompletion(fileName:String, listener:Dynamic -> Void):Void
	{
		fileName = FileTools.formatPath(fileName).toLowerCase();
		sDispatcher.removeEventListener(fileName, listener);
	}
	
	public static function setMaxBytes(max:UInt):Void
	{
		sMaximumBytes = max;
		free();
	}
	
	public static inline function lock(fileName:String, source:Dynamic):Void
	{
		fileName = FileTools.formatPath(fileName);
		var a:BitmapManagerAsset = sAssets.get(fileName.toLowerCase());
		if (a == null)
			throw new Error("ERROR: Could not lock asset " + fileName + " because the asset was not found");
		else if (source != null)
			a.lock(source);
	}
	
	public static inline function unlock(fileName:String, source:Dynamic):Void
	{
		fileName = FileTools.formatPath(fileName);
		var a:BitmapManagerAsset = sAssets.get(fileName.toLowerCase());
		if (a == null)
			trace ("WARNING: Could not unlock asset " + fileName + " because the asset was not found");
		else
			a.unlock(source);
	}
	
	public static inline function isLocked(fileName:String):Bool
	{
		fileName = FileTools.formatPath(fileName);
		var a:BitmapManagerAsset = sAssets.get(fileName.toLowerCase());
		if (a == null)
		{
			trace("WARNING: Could not query lock status of asset " + fileName + " because the asset was not found");
			return false;
		}
		else
			return a.isLocked;
	}
	
	private inline static function addResource(fileName:String):BitmapManagerAsset
	{
		fileName = FileTools.formatPath(fileName);	
		
		// If it's not, add it to the end.
		if (sAssets.exists(fileName))
			return sAssets.get(fileName)
		else
		{
			var a:BitmapManagerAsset = new BitmapManagerAsset(fileName);
			sAssets.set(fileName, a);
			return a;
		}
	}
	
	private static function loadNext(?e:FileToolsEvent):Void
	{
		sIdle = false;
		var g:BitmapManagerAsset;
		if (e != null)
		{
			g = sPendingRequests[0];
			if (!sAssets.exists(g.fileName.toLowerCase()))
				throw new Error("ERROR: Pending request could not be verified");
			
			var b:BitmapData = cast e.data;
			if (g.data != null)
			{
				sCurrentBytes -= cast(Std.int(g.data.width * g.data.height * 4), UInt);
				g.data.dispose();
			}
			g.data = b;
			g.lastModified = FileTools.fileModifiedTime(rootDirectory + FileTools.formatPath(g.fileName));
			g.priority = 0;
			sDispatcher.dispatchEvent(new BitmapManagerEvent(g.fileName.toLowerCase(), g.data));
			sCurrentBytes += cast(Std.int(b.width * b.height * 4), UInt);
			
			sPendingRequests.shift();
			
			free();
		}
		
		if (sPendingRequests.length == 0)
		{
			sIdle = true;
			return;
		}
		var nextPath:String = sPendingRequests[0].fileName;
		if (nextPath.indexOf(":") < 0) 
			nextPath = rootDirectory + nextPath;
		FileTools.asyncReadFrom(nextPath, loadNext, loadError);
		if (verbose)
			trace(Std.string(Lib.getTimer()) + " -- Loading " + nextPath);
	}
	
	private static function loadError(e:FileToolsEvent):Void
	{
		trace("WARNING: Could not load resource " + e.path);
		
		sPendingRequests.shift();
		
		loadNext();
	}
	
	private static function free():Void
	{
		var a:Array<BitmapManagerAsset> = [];
		for (asset in sAssets.iterator())
			if (asset.data != null)
				a.push(asset);
		a.sort(BitmapManagerAsset.compare);
		while (sCurrentBytes > sTargetBytes)
		{
			var g:BitmapManagerAsset = a.pop();
			if (g.isLocked)
			{
				if (sCurrentBytes > sMaximumBytes && verbose)
					trace("WARNING: Locked assets exceed maximum capacity set by BitmapManager! " + sCurrentBytes + "/" + sMaximumBytes);
				return;
			}
			sCurrentBytes -= g.data.width * g.data.height * 4;
			g.unready();
		}
	}
}

private class BitmapManagerAsset 
{
	private var mLock:Lock<Dynamic>;
	private var mData:BitmapData;
	public var priority:Int;
	public var fileName(default, null):String;
	public var lastRequested(default, null):UInt;
	public var lastModified:Float;
	
	public var data(get, set):BitmapData;
	public var isLocked(get, never):Bool;
	
	public function new(fileName:String, priority:Int = 0)
	{		
		mData = null;
		this.fileName = fileName;
		this.priority = priority;
		lastRequested = flash.Lib.getTimer();
		mLock = new Lock<Dynamic>();
	}
	
	public static inline function compare(a:BitmapManagerAsset, b:BitmapManagerAsset):Int
	{
		if (a.mData == null)
			return 1;
		else if (b.mData == null)
			return -1;
		else if (!a.isLocked && b.isLocked)
			return 1;
		else if (b.isLocked && !a.isLocked)
			return -1;		
		else if (a.lastRequested > b.lastRequested)
			return -1;
		else
			return 1;
	}
	
	public inline function lock(source:Dynamic):Void
	{	mLock.lock(source);		}
	
	public inline function unlock(source:Dynamic):Void
	{	mLock.unlock(source);	}
	
	public inline function unready():Void
	{
		if (mData != null)
			mData.dispose();
		mData = null;
	}
	
	private inline function transferLocks(b:BitmapManagerAsset):Void
	{
		b.mLock = mLock;
		mLock = null;
	}
	
	private inline function get_data():BitmapData
	{
		lastRequested = Lib.getTimer();
		return mData;
	}
	
	private inline function set_data(bmp:BitmapData):BitmapData
	{
		if (mData != bmp && mData != null)
			mData.dispose();
		return mData = bmp;
	}
	
	public inline function get_isLocked():Bool
	{	return mLock.isLocked;	}
}