package com.problemmachine.bitmap.bitmapmanager;

enum BitmapManagerQuery
{
	Ready;
	NotReady;
	NotActive;
	NotFound;
}