package com.problemmachine.bitmap.bitmapmanager;
import flash.display.BitmapData;
import flash.events.Event;

class BitmapManagerEvent extends Event
{
	public var data (default, null):BitmapData;
	
	public function new(type:String, data:BitmapData, bubbles:Bool = false, cancelable:Bool = false) 
	{
		this.data = data;
		super(type, bubbles, cancelable);
		
	}
	
}