package com.problemmachine.virtualcontroller ;
import com.problemmachine.tools.keyboard.KeyboardTools;
import flash.errors.Error;
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.GameInputEvent;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.Lib;
import flash.ui.GameInputControl;
import flash.ui.GameInputDevice;

class VirtualControl extends EventDispatcher
{	
	static public inline var KEYBOARD:String = "Keyboard";
	static public inline var MOUSE:String = "Mouse";
	
	static public inline var MOUSE_LEFT:Int = 0;
	static public inline var MOUSE_RIGHT:Int = 1;
	static public inline var MOUSE_MIDDLE:Int = 2;
	static public inline var MOUSE_HORIZONTAL:Int = 3;
	static public inline var MOUSE_VERTICAL:Int = 4;
	
	public var device(default, null):Dynamic;
	public var input(default, null):Int;
	public var value(default, null):Float;
	
	private var mControl:GameInputControl;
	public var style:VirtualControlStyle;

	@:allow(com.problemmachine.virtualcontroller.VirtualControllerManager)
	private function new(device:Dynamic, input:Int) 
	{
		super(this);
		this.device = device;
		this.input = input;
		this.value = 0;
		this.style = Button;
		mControl = null;
		
		if (device == KEYBOARD)
		{
			KeyboardTools.startListeningForKeyDown(input, keyboardListener);
			KeyboardTools.startListeningForKeyUp(input, keyboardListener);
		}
		else if (device == MOUSE)
		{
			switch(input)
			{
				case MOUSE_HORIZONTAL, MOUSE_VERTICAL:
					this.style = Axis;
					Lib.current.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseListener);
				case MOUSE_LEFT:
					Lib.current.stage.addEventListener(MouseEvent.MOUSE_DOWN, mouseListener);
					Lib.current.stage.addEventListener(MouseEvent.MOUSE_UP, mouseListener);
				case MOUSE_RIGHT:
					Lib.current.stage.addEventListener(MouseEvent.RIGHT_MOUSE_DOWN, mouseListener);
					Lib.current.stage.addEventListener(MouseEvent.RIGHT_MOUSE_UP, mouseListener);
				case MOUSE_MIDDLE:
					Lib.current.stage.addEventListener(MouseEvent.MIDDLE_MOUSE_DOWN, mouseListener);
					Lib.current.stage.addEventListener(MouseEvent.MIDDLE_MOUSE_UP, mouseListener);
				default:
					throw new Error("ERROR: VirtualControllerControl.new(): Invalid mouse input value " + input);
			}
		}
		else if (Std.is(device, GameInputDevice))
		{
			VirtualControllerManager.sGameInput.addEventListener(GameInputEvent.DEVICE_REMOVED, removedListener);
			mControl = cast(device, GameInputDevice).getControlAt(input);
			if (mControl.minValue < 0)
				this.style = Axis;
			mControl.addEventListener(Event.CHANGE, controllerListener);
		}
		else
		{
			throw new Error("ERROR: VirtualControllerControl.new(): device is unrecognized " + device);
		}
	}
	
	private function keyboardListener(e:KeyboardEvent):Void
	{
		value = (e.type == KeyboardEvent.KEY_DOWN) ? 1 : 0;
		dispatchEvent(new Event(Event.CHANGE));
	}
	
	private function mouseListener(e:MouseEvent):Void
	{
		switch(input)
		{
			case MOUSE_LEFT:
				value = (e.type == MouseEvent.MOUSE_DOWN) ? 1 : 0;
			case MOUSE_RIGHT:
				value = (e.type == MouseEvent.RIGHT_MOUSE_DOWN) ? 1 : 0;
			case MOUSE_MIDDLE:
				value = (e.type == MouseEvent.MIDDLE_MOUSE_DOWN) ? 1 : 0;
			case MOUSE_HORIZONTAL:
				value = (Lib.current.stage.mouseX / Lib.current.stage.width) * 2 - 1;
			case MOUSE_VERTICAL:
				value = (Lib.current.stage.mouseY / Lib.current.stage.height) * 2 - 1;
		}
		dispatchEvent(new Event(Event.CHANGE));
	}
	
	private function controllerListener(e:Event):Void
	{
		value = mControl.device.enabled ? mControl.value : 0;
		dispatchEvent(new Event(Event.CHANGE));
	}
	
	private function removedListener(e:GameInputEvent):Void
	{
		if (e.device == device)
		{
			var change:Bool = (value != 0);
			value = 0;
			dispatchEvent(new Event(Event.CHANGE));
		}
	}
	
	public function dispose():Void
	{
		if (device == KEYBOARD)
		{
			KeyboardTools.stopListeningForKeyDown(input, keyboardListener);
			KeyboardTools.stopListeningForKeyUp(input, keyboardListener);
		}
		else if (device == MOUSE)
		{
			Lib.current.stage.removeEventListener(MouseEvent.MOUSE_MOVE, mouseListener);
			Lib.current.stage.removeEventListener(MouseEvent.MOUSE_DOWN, mouseListener);
			Lib.current.stage.removeEventListener(MouseEvent.MOUSE_UP, mouseListener);
			Lib.current.stage.removeEventListener(MouseEvent.RIGHT_MOUSE_DOWN, mouseListener);
			Lib.current.stage.removeEventListener(MouseEvent.RIGHT_MOUSE_UP, mouseListener);
			Lib.current.stage.removeEventListener(MouseEvent.MIDDLE_MOUSE_DOWN, mouseListener);
			Lib.current.stage.removeEventListener(MouseEvent.MIDDLE_MOUSE_UP, mouseListener);
		}
		else
		{
			VirtualControllerManager.sGameInput.removeEventListener(GameInputEvent.DEVICE_REMOVED, removedListener);
			mControl.removeEventListener(Event.CHANGE, controllerListener);
		}
		device = null;
		mControl = null;
	}	
}