package com.problemmachine.virtualcontroller ;
import com.problemmachine.tools.xml.XMLTools;
import com.problemmachine.virtualcontroller.event.VirtualControllerEvent;
import com.problemmachine.virtualcontroller.VirtualControllerManager;
import com.problemmachine.util.variablewrapper.VariableWrapper;
import flash.errors.Error;
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.ui.GameInputDevice;

class VirtualController extends EventDispatcher
{
	static private inline var POSITIVE:String = "_POSITIVE";
	static private inline var NEGATIVE:String = "_NEGATIVE";
	
	static public inline var FACE_1:String = 				"Face1";
	static public inline var FACE_2:String = 				"Face2";
	static public inline var FACE_3:String = 				"Face3";
	static public inline var FACE_4:String = 				"Face4";
	static public inline var FACE_5:String = 				"Face5";
	static public inline var FACE_6:String = 				"Face6";
	
	static public inline var FACE_LEFT:String = 			FACE_1;
	static public inline var FACE_BOTTOM:String = 			FACE_2;
	static public inline var FACE_TOP:String = 				FACE_3;
	static public inline var FACE_RIGHT:String = 			FACE_4;
	
	static public inline var D_PAD_LEFT:String = 			"DPadLeft";
	static public inline var D_PAD_DOWN:String = 			"DPadDown";
	static public inline var D_PAD_UP:String = 				"DPadUp";
	static public inline var D_PAD_RIGHT:String =	 		"DPadRight";
	
	static public inline var LEFT_STICK_CLICK:String 				= "LStickClick";
	static public inline var LEFT_STICK_X_AXIS:String 				= "LStickXAxis";
	static public inline var LEFT_STICK_Y_AXIS:String 				= "LStickYAxis";
	static public inline var LEFT_STICK_X_AXIS_POSITIVE:String 		= LEFT_STICK_X_AXIS + POSITIVE;
	static public inline var LEFT_STICK_X_AXIS_NEGATIVE:String 		= LEFT_STICK_X_AXIS + NEGATIVE;
	static public inline var LEFT_STICK_Y_AXIS_POSITIVE:String 		= LEFT_STICK_Y_AXIS + POSITIVE;
	static public inline var LEFT_STICK_Y_AXIS_NEGATIVE:String 		= LEFT_STICK_Y_AXIS + NEGATIVE;
	static public inline var LEFT_STICK_X_AXIS_LEFT:String 			= LEFT_STICK_X_AXIS_NEGATIVE;
	static public inline var LEFT_STICK_X_AXIS_RIGHT:String 		= LEFT_STICK_X_AXIS_POSITIVE;
	static public inline var LEFT_STICK_Y_AXIS_UP:String 			= LEFT_STICK_Y_AXIS_NEGATIVE;
	static public inline var LEFT_STICK_Y_AXIS_DOWN:String 			= LEFT_STICK_Y_AXIS_POSITIVE;
	
	static public inline var RIGHT_STICK_CLICK:String 				= "RStickClick";
	static public inline var RIGHT_STICK_X_AXIS:String 				= "RStickXAxis";
	static public inline var RIGHT_STICK_Y_AXIS:String 				= "RStickYAxis";	
	static public inline var RIGHT_STICK_X_AXIS_POSITIVE:String 	= RIGHT_STICK_X_AXIS + POSITIVE;
	static public inline var RIGHT_STICK_X_AXIS_NEGATIVE:String 	= RIGHT_STICK_X_AXIS + NEGATIVE;
	static public inline var RIGHT_STICK_Y_AXIS_POSITIVE:String 	= RIGHT_STICK_Y_AXIS + POSITIVE;
	static public inline var RIGHT_STICK_Y_AXIS_NEGATIVE:String 	= RIGHT_STICK_Y_AXIS + NEGATIVE;
	static public inline var RIGHT_STICK_X_AXIS_LEFT:String 		= RIGHT_STICK_X_AXIS_NEGATIVE;
	static public inline var RIGHT_STICK_X_AXIS_RIGHT:String 		= RIGHT_STICK_X_AXIS_POSITIVE;
	static public inline var RIGHT_STICK_Y_AXIS_UP:String 			= RIGHT_STICK_Y_AXIS_NEGATIVE;
	static public inline var RIGHT_STICK_Y_AXIS_DOWN:String 		= RIGHT_STICK_Y_AXIS_POSITIVE;
	
	static public inline var L_1:String = 					"L1";
	static public inline var L_2:String = 					"L2";
	static public inline var L_BUTTON:String =				L_1;
	static public inline var L_TRIGGER:String =				L_2;
	
	static public inline var R_1:String = 					"R1";
	static public inline var R_2:String = 					"R2";
	static public inline var R_BUTTON:String =				R_1;
	static public inline var R_TRIGGER:String =				R_2;
	
	static public inline var SELECT:String = 				"Select";
	static public inline var BACK:String = 					SELECT;
	
	static public inline var START:String = 				"Start";
	static public inline var RUN:String = 					START;
	
	private var mControlBindings:Map<String, VirtualControl>;
	public var index(default, null):Int;
	
	@:allow(com.problemmachine.virtualcontroller.VirtualControllerManager)
	private var mDevice:GameInputDevice;
	
	@:allow(com.problemmachine.virtualcontroller.VirtualControllerManager)
	private function new(index:Int) 
	{
		super(this);
		this.index = index;
		mDevice = null;
		mControlBindings = new Map<String,  VirtualControl>();
	}
	
	public function getBindTargets():Array<String>
	{
		var arr:Array<String> = [];
		for (s in mControlBindings.keys())
			arr.push(s);
		return arr;
	}
	
	public function getControl(control:String):VirtualControl
	{
		if (mControlBindings.exists(control))
			return mControlBindings.get(control);
		else
		{
			if (control.lastIndexOf(NEGATIVE) == control.length - NEGATIVE.length)
				control = control.substring(0, control.lastIndexOf(NEGATIVE));
			else if (control.lastIndexOf(POSITIVE) == control.length - POSITIVE.length)
				control = control.substring(0, control.lastIndexOf(POSITIVE));
			else
				return null;
			return getControl(control);
		}
	}
	
	public function get(control:String):Float
	{	
		if (mControlBindings.exists(control))
			return mControlBindings.get(control).value;		
		else
		{
			var neg:Bool = false;
			if (control.lastIndexOf(NEGATIVE) == control.length - NEGATIVE.length)
			{
				control = control.substring(0, control.lastIndexOf(NEGATIVE));
				neg = true;
			}
			else if (control.lastIndexOf(POSITIVE) == control.length - POSITIVE.length)
				control = control.substring(0, control.lastIndexOf(POSITIVE));
			else
				return 0;
			var c:VirtualControl = getControl(control);
			if (c == null)
				return 0;
			if (c.style == Axis)
			{
				if (neg && c.value < 0)
					return (Math.abs(c.value));
				else if (!neg && c.value > 0)
					return c.value;
				else
					return 0;
			}
			else
				return c.value;
		}
	}
	
	public function hasControl(control:String):Bool
	{	
		if (mControlBindings.exists(control))
			return true;		
		else
		{
			if (control.lastIndexOf(NEGATIVE) == control.length - NEGATIVE.length)
				control = control.substring(0, control.lastIndexOf(NEGATIVE));
			else if (control.lastIndexOf(POSITIVE) == control.length - POSITIVE.length)
				control = control.substring(0, control.lastIndexOf(POSITIVE));
			else
				return false;
			return hasControl(control);
		}
	}
	
	public function bindControl(source:VirtualControl, target:String):Void
	{
		if (target.indexOf(NEGATIVE) > 0 && target.lastIndexOf(NEGATIVE) == target.length - NEGATIVE.length)
			target = target.substring(0, target.lastIndexOf(NEGATIVE));
		else if (target.indexOf(POSITIVE) > 0 && target.lastIndexOf(POSITIVE) == target.length - POSITIVE.length)
			target = target.substring(0, target.lastIndexOf(POSITIVE));
		mControlBindings.set(target, source);
		source.addEventListener(Event.CHANGE, changeListener);
	}
	
	public function unbindControl(target:String):Void
	{
		if (mControlBindings.exists(target))
		{
			mControlBindings.get(target).removeEventListener(Event.CHANGE, changeListener);
			mControlBindings.remove(target);
		}
		else
		{
			if (target.lastIndexOf(NEGATIVE) == target.length - NEGATIVE.length)
				target = target.substring(0, target.lastIndexOf(NEGATIVE));
			else if (target.lastIndexOf(POSITIVE) == target.length - POSITIVE.length)
				target = target.substring(0, target.lastIndexOf(POSITIVE));
			else
				return;
			unbindControl(target);
		}
	}
	
	private function changeListener(e:Event):Void
	{
		var c:VirtualControl = cast e.target;
		var control:String = "";
		for (s in mControlBindings.keys())
			if (mControlBindings.get(s) == c)
			{
				control = s;
				break;
			}
		if (c.style == Axis)
		{
			if (c.value < 0)
				dispatchEvent(new VirtualControllerEvent(VirtualControllerEvent.AXIS_CHANGE, this, control + NEGATIVE, Math.abs(c.value)));
			else if (c.value > 0)
				dispatchEvent(new VirtualControllerEvent(VirtualControllerEvent.AXIS_CHANGE, this, control + POSITIVE, c.value));
			dispatchEvent(new VirtualControllerEvent(VirtualControllerEvent.AXIS_CHANGE, this, control, c.value));
		}
		else
			dispatchEvent(new VirtualControllerEvent(VirtualControllerEvent.BUTTON_CHANGE, this, control, c.value));
	}
	
	public function toXML():Xml
	{
		if (mDevice != null)
			trace("VirtualController.toXML() - WARNING: Created XML object of a physical controller will not retain physical device information");
		var xml:Xml = Xml.parse("<virtualController/>").firstElement();
		for (s in mControlBindings.keys())
		{
			var c:VirtualControl = mControlBindings.get(s);
			if (!Std.is(c.device, String))
			{
				trace("VirtualController.toXML() - WARNING: Cannot convert physical device type to Virtual Controller XML");
				continue;
			}
			xml.addChild(Xml.parse(
				"<control>\n" + 
				"	<target>" + s + "</target>\n" + 
				"	<device>" + (cast c.device) + "</device>\n" + 
				"	<input>" + Std.string(c.input) + "</input>\n" + 
				"</control>"));
		}
			
		return xml;
	}
	
	public function loadFromXML(xml:Xml):Void
	{
		for (c in mControlBindings.iterator())
			c.dispose();
		mControlBindings = new Map<String, VirtualControl>();
		for (x in xml.elementsNamed("control"))
			mControlBindings.set(XMLTools.getVal(xml, "target"), new VirtualControl(XMLTools.getVal(xml, "device"), Std.parseInt(XMLTools.getVal(xml, "input"))));
	}
	
	public function dispose():Void
	{
		for (c in mControlBindings.iterator())
		{
			c.removeEventListener(Event.CHANGE, changeListener);
			c.dispose();
		}
		mControlBindings = null;
	}
	
}