package com.problemmachine.virtualcontroller ;
import com.problemmachine.virtualcontroller.event.VirtualControllerEvent;
import flash.errors.Error;
import flash.events.Event;
import flash.events.GameInputEvent;
import flash.ui.GameInput;
import flash.ui.GameInputDevice;
import haxe.xml.Fast;

class VirtualControllerManager
{
	static private var sVirtualControllers:Array<VirtualController> = [];
	static private var sControllerListeners:Array<Map<String, Array<Dynamic->Void>>> = [];
	static private var sControlBindings:Map<String, Array<VirtualControlBinding>> = new Map<String, Array<VirtualControlBinding>>();
	
	static public var autoAddGameControllers(default, set):Bool = false;
	@:allow(com.problemmachine.virtualcontroller.VirtualControl)
	static private var sGameInput:GameInput = new GameInput();
	
	static private function set_autoAddGameControllers(val:Bool):Bool
	{
		if (val != autoAddGameControllers)
		{
			for (i in 0...GameInput.numDevices)
			{
				var found:Bool = false;
				for (v in sVirtualControllers)
					if (v.mDevice == GameInput.getDeviceAt(i))
					{
						found = true;
						break;
					}
				if (found)
					break;
				addVirtualController(GameInput.getDeviceAt(i));
			}
			sGameInput.addEventListener(GameInputEvent.DEVICE_ADDED, controllerAddedListener);
			autoAddGameControllers = val;
		}
		return val;
	}
	
	static private function controllerAddedListener(e:GameInputEvent):Void
	{
		for (v in sVirtualControllers)
			if (v.mDevice == e.device)
				return;
		addVirtualController(e.device);
	}
	
	static public function addDefaultControllerBindings(controllerName:String, bindings:Array<VirtualControlBinding>):Void
	{
		if (sControlBindings.exists(controllerName))
			trace ("Warning: importControllerBindingMapsFromXML() - binding " + controllerName + " already exists");
		sControlBindings.set(controllerName, bindings);
	}
	
	static public function setControllerBinding(controller:Int, binding:VirtualControlBinding):Void
	{
		sVirtualControllers[controller].bindControl(new VirtualControl(binding.source, binding.control), binding.target);
	}
	
	static public function removeControllerBinding(controller:Int, target:String):Void
	{
		sVirtualControllers[controller].unbindControl(target);
	}
	
	static public function exportControllerBindingMapsToXML():Xml
	{
		var xml:Xml = Xml.parse("<controlBindings/>").firstElement();
		for (k in sControlBindings.keys())
		{
			var arr:Array<VirtualControlBinding> = sControlBindings.get(k);
			var x:Xml = Xml.parse(
				"<controller>\n" +
				"	<name>" + k + "</name>\n" +
				"</controller>").firstElement();
			for (c in arr)
			{
				var xx:Xml = Xml.parse(
								"<binding>\n" + 
								"	<control>" + Std.string(c.control) + "</control>\n" +
								"	<target>" + c.target + "</target>\n" +
								"</binding>\n").firstElement();
				if (Std.is(c.source, String) && c.source != k)
					xx.addChild(Xml.parse("<source>" + c.source + "</source>").firstElement());
				x.addChild(xx);
			}			
			xml.addChild(x);
		}
		return xml;
	}
	
	static public function importControllerBindingMapsFromXML(xml:Xml):Void
	{
		var f:Fast = new Fast(xml);
		for (xml in f.nodes.controller)
		{
			var name:String = xml.node.name.innerData;
			var bindings:Array<VirtualControlBinding> = [];
			for (x in xml.nodes.binding)
			{
				var source:String = x.hasNode.source ? x.node.source.innerData : null;
				var control:Int = Std.parseInt(x.node.control.innerData);
				var target:String = x.node.target.innerData;
				bindings.push(new VirtualControlBinding(target, control, source));
			}
			addDefaultControllerBindings(name, bindings);
		}
	}
	
	static public function getPhysicalControllerID(device:GameInputDevice):Int
	{
		for (i in 0...sVirtualControllers.length)
			if (sVirtualControllers[i].mDevice == device)
				return i;
		return -1;
	}
	
	static public function getControlStatus(controller:Int, control:String):Float
	{
		return (sVirtualControllers[controller].get(control));
	}
	
	static public inline function controllerHasControl(controller:Int, control:String):Bool
	{
		if (sVirtualControllers.length <= controller || controller < 0)
			return false;
		else return sVirtualControllers[controller].hasControl(control);
	}
	
	static public function startListeningForControlChange(controller:Int, control:String, listener:Dynamic->Void):Void
	{
		while (sControllerListeners.length <= controller)
			sControllerListeners.push(new Map<String, Array<Dynamic->Void>>());
		if (!sControllerListeners[controller].exists(control))
			sControllerListeners[controller].set(control, []);
		sControllerListeners[controller].get(control).push(listener);
	}
	
	static public function stopListeningForControlChange(controller:Int, control:String, listener:Dynamic->Void):Void
	{
		if (sControllerListeners.length <= controller)
			return;
		if (!sControllerListeners[controller].exists(control))
			return;
		sControllerListeners[controller].get(control).remove(listener);
		if (sControllerListeners[controller].get(control).length == 0)
			sControllerListeners[controller].remove(control);
	}
	
	static private function changeListener(e:VirtualControllerEvent):Void
	{
		var c:VirtualController = e.controller;
		var i:VirtualControl = c.getControl(e.control);
		var m:Map<String, Array<Dynamic->Void>> = sControllerListeners[sVirtualControllers.indexOf(c)];
		if (m != null && m.exists(e.control))
		{
			var arr:Array<Dynamic->Void> = m.get(e.control);
			for (l in arr)
				l(e);
		}
	}
	
	static public function addVirtualControllerFromXML(xml:Xml):Int
	{
		var f:Fast = new Fast(xml);
		var v:VirtualController = new VirtualController(sVirtualControllers.length);
		
		var name:String = f.node.name.innerData;
		for (x in f.nodes.binding)
		{
			var source:String = x.node.source.innerData;
			var control:Int = Std.parseInt(x.node.control.innerData);
			var target:String = x.node.target.innerData;
			v.bindControl(new VirtualControl(source, control), target);
		}
		sVirtualControllers.push(v);
		v.addEventListener(VirtualControllerEvent.AXIS_CHANGE, changeListener);
		v.addEventListener(VirtualControllerEvent.BUTTON_CHANGE, changeListener);
		return (sVirtualControllers.length - 1);
	}
	
	static public function addVirtualController(?device:Dynamic):Int
	{
		var c:VirtualController = new VirtualController(sVirtualControllers.length);
		if (Std.is(device, GameInputDevice))
		{
			var d:GameInputDevice = cast device;
			d.enabled = true;
			trace("adding physical device " + device.name);
			var s:String = null;
			for (key in sControlBindings.keys())
			{
				s = key;
				if (d.name.toLowerCase().indexOf(s.toLowerCase()) < 0)
					s = null;
				if (s != null)
					break;
			}
			if (s != null)
			{
				var arr:Array<VirtualControlBinding> = sControlBindings.get(s);
				for (v in arr)
					c.bindControl(new VirtualControl(d, v.control), v.target);
			}
			c.mDevice = d;
		}
		else if (Std.is(device, String))
		{
			var d:String = cast device;
			d = d.toLowerCase();
			trace("adding virtual device " + d);
			var s:String = null;
			for (key in sControlBindings.keys())
				if (d.indexOf(key.toLowerCase()) > 0)
				{
					s = key;
					break;
				}
			if (s != null)
			{
				var arr:Array<VirtualControlBinding> = sControlBindings.get(s);
				for (v in arr)
					c.bindControl(new VirtualControl(v.source, v.control), v.target);
			}
		}
		else if (device != null)
			throw new Error("ERROR: VirtualControllerManager.addVirtualController() - Unrecognized device type " + device);
		c.addEventListener(VirtualControllerEvent.AXIS_CHANGE, changeListener);
		c.addEventListener(VirtualControllerEvent.BUTTON_CHANGE, changeListener);
		sVirtualControllers.push(c);
		return sVirtualControllers.length - 1;
	}
	
	static public function getVirtualController(index:Int):VirtualController
	{
		return sVirtualControllers[index];
	}
}