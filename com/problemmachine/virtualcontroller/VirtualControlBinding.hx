package com.problemmachine.virtualcontroller;

class VirtualControlBinding
{
	public var target(default, null):String;
	public var control(default, null):Int;
	public var source(default, null):String;
	
	public function new(target:String, control:Int, source:String)
	{
		this.target = target;
		this.control = control;
		this.source = source;
	}
}