package com.problemmachine.virtualcontroller.event ;

import com.problemmachine.virtualcontroller.VirtualController;
import flash.events.Event;

class VirtualControllerEvent extends Event
{
	public static inline var BUTTON_CHANGE:String = "ButtonChange";
	public static inline var AXIS_CHANGE:String = "AxisChange";
	
	public var controller:VirtualController;
	public var control:String;
	public var value:Float;

	public function new(type:String, controller:VirtualController, control:String, value:Float, bubbles:Bool=false, cancelable:Bool=false) 
	{
		super(type, bubbles, cancelable);
		this.controller = controller;
		this.control = control;
		this.value = value;
	}
	
}