package com.problemmachine.particlesystem.collision;
import com.problemmachine.particlesystem.internal.ParticleInstance;
import com.problemmachine.particlesystem.internal.ParticleType;

interface IParticleCollider 
{
	function verifySpawnPossible(type:ParticleType):Bool;
	function particleCollision(type:ParticleType, particles:Array<ParticleInstance>):Void;
	function toXML():Xml;
	function loadXML(xml:Xml):Void;
}