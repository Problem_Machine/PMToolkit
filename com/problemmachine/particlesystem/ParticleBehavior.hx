package com.problemmachine.particlesystem;
import com.problemmachine.particlesystem.ParticleParameter;
import com.problemmachine.particlesystem.internal.ParticleInstance;
import com.problemmachine.particlesystem.internal.ParticleOp;
import com.problemmachine.particlesystem.internal.ParticleType;
import com.problemmachine.tools.xml.XMLTools;
import flash.Lib;

class ParticleBehavior
{
	public var updateRate:Float;
	@:allow(com.problemmachine.particlesystem.internal.ParticleType)
	public var particleType(default, null):ParticleType;
	
	public var source(default, null):ParticleParameter;
	
	public var sourceStarts(default, null):Array<Float>;
	public var sourceEnds(default, null):Array<Float>;
	
	public var operations(default, null):Array<ParticleOp>;	
	public var targets(default, null):Array<ParticleParameter>;
	public var targetParams1(default, null):Array<Float>;
	public var targetParams2(default, null):Array<Float>;
	
	public function new(src:ParticleParameter, updateRate:Float = 0.1)
	{
		this.updateRate = updateRate;
		particleType = null;
		source = src;
		
		sourceStarts = [];
		sourceEnds = [];
		
		operations = [];
		targets = [];
		targetParams1 = [];
		targetParams2 = [];
	}
	
	@:access(com.problemmachine.particlesystem.ParticleSystem.sGlobalParameters)
	public inline function applyBehavior(p:ParticleInstance):Void
	{
		if (source == cast ParticleParameter.COLLISION)
		{
			// todo: collision stuff
		}
		var s:Float = 0;
		if (cast (source, Int) < 0)
		{
			switch(source)
			{
				case ParticleParameter.WORLD_TIME:
					s = Lib.getTimer();
				case ParticleParameter.GLOBAL_0:
					s = ParticleSystem.sGlobalParameters[0];
				case ParticleParameter.GLOBAL_1:
					s = ParticleSystem.sGlobalParameters[1];
				case ParticleParameter.GLOBAL_2:
					s = ParticleSystem.sGlobalParameters[2];
				case ParticleParameter.GLOBAL_3:
					s = ParticleSystem.sGlobalParameters[3];
				case ParticleParameter.GLOBAL_4:
					s = ParticleSystem.sGlobalParameters[4];
				case ParticleParameter.GLOBAL_5:
					s = ParticleSystem.sGlobalParameters[5];
				case ParticleParameter.GLOBAL_6:
					s = ParticleSystem.sGlobalParameters[6];
				case ParticleParameter.GLOBAL_7:
					s = ParticleSystem.sGlobalParameters[7];
				default:
					s = 0;
			}
		}
		else
			s = p.getParam(source);
		var val:Float;
		var t:ParticleParameter;
		for (i in 0...operations.length)
		{
			t = targets[i];
			
			switch(operations[i])
			{
				case ParticleOp.SetVal:
					if (s >= sourceStarts[i])
					{
						if (s >= sourceEnds[i])
							val = targetParams2[i];
						else
							val = targetParams1[i] + 
								(Math.min(1, (s - sourceStarts[i]) / (sourceEnds[i] - sourceStarts[i])) * (targetParams2[i] - targetParams1[i]));
						if (p.getParam(t) != val)
						{
							p.setParam(t, val);
							p.changedParameters[cast t] = true;
						}
					}
					
				case ParticleOp.Add:
					if (s >= sourceStarts[i] && s < sourceEnds[i])
					{
						p.setParam(t, p.getParam(t) + targetParams1[i] * updateRate);
						p.changedParameters[cast t] = true;
					}
					
				case ParticleOp.Multiply:
					if (s >= sourceStarts[i] && s < sourceEnds[i])
					{
						val = p.getParam(t);
						p.setParam(t, val + (val * (targetParams1[i] - 1) * updateRate));
						p.changedParameters[cast t] = true;
					}
					
				case ParticleOp.RandomTrigger:
					if (s >= sourceStarts[i] || s < sourceEnds[i])
						if (targetParams2[i] * updateRate > Math.random()) // almost certainly technically incorrect -- should work for values < 1 second anyway
						{
							p.setParam(t, targetParams1[i]);
							p.changedParameters[cast t] = true;
						}
							
				case ParticleOp.Randomize:
					if (s >= sourceStarts[i] && s < sourceEnds[i])
					{
						p.setParam(t, targetParams1[i] + (Math.random() * (targetParams2[i] - targetParams1[i])));
						p.changedParameters[cast t] = true;
					}
			}
		}
	}
	
	public function addOperation(?op:ParticleOp, srcStart:Float = 0, srcEnd:Float = 0, target:ParticleParameter = LIFE, param1:Float = 0, param2:Float = 0):Void
	{
		if (op == null)
			op = ParticleOp.SetVal;
		operations.push(op);
		sourceStarts.push(srcStart);
		sourceEnds.push(srcEnd);
		targets.push(target);
		targetParams1.push(param1);
		targetParams2.push(param2);
	}
	
	public function deleteOperation(i:Int):Void
	{
		if (i >= 0 && operations.length > i)
		{
			operations.splice(i, 1);
			sourceStarts.splice(i, 1);
			sourceEnds.splice(i, 1);
			targets.splice(i, 1);
			targetParams1.splice(i, 1);
			targetParams2.splice(i, 1);
		}
	}
	
	public function clone():ParticleBehavior
	{
		var b:ParticleBehavior = new ParticleBehavior(source);
		b.mimic(this);
		return b;
	}
	
	public function mimic(target:ParticleBehavior):Void
	{
		source = target.source;
		updateRate = target.updateRate;
		while (operations.length > 0)
		{
			operations.pop();
			sourceStarts.pop();
			sourceEnds.pop();
			targets.pop();
			targetParams1.pop();
			targetParams2.pop();
		}
		for (i in 0...target.operations.length)
			addOperation(operations[i], sourceStarts[i], sourceEnds[i], targets[i], targetParams1[i], targetParams2[i]);
	}
	
	public static function createFromXML(xml:Xml):ParticleBehavior
	{
		var rate:Float = Std.parseFloat(XMLTools.getVal(xml, "updateRate"));
		var source:Int = Std.parseInt(XMLTools.getVal(xml, "source"));
		var pb:ParticleBehavior = new ParticleBehavior(cast source, rate);
		for (x in xml.elementsNamed("operation"))
		{
			var sourceStart:Float = Std.parseFloat(XMLTools.getVal(x, "sourceStart"));
			var sourceEnd:Float = Std.parseFloat(XMLTools.getVal(x, "sourceEnd"));
			var op:ParticleOp = Type.createEnum(ParticleOp, XMLTools.getVal(x, "type"));
			var target:ParticleParameter = cast Std.parseInt(XMLTools.getVal(x, "target"));
			var param1:Float = Std.parseFloat(XMLTools.getVal(x, "parameter1"));
			var param2:Float = Std.parseFloat(XMLTools.getVal(x, "parameter2"));
			pb.addOperation(op, sourceStart, sourceEnd, target, param1, param2);
		}

		return pb;
	}
	
	public function toXML():Xml
	{
		var xml:Xml = Xml.parse(
			"<behavior>\n" +
			"	<updateRate>" + Std.string(updateRate) + "</updateRate>\n" +
			"	<source>" + Std.string(cast (source, Int)) + "</source>\n" +
			"</behavior>").firstElement();
		for (i in 0...operations.length)
		{
			var op:Xml = Xml.parse(
				"<operation>\n" +
				"	<sourceStart>" + Std.string(sourceStarts[i]) + "</sourceStart>\n" +
				"	<sourceEnd>" + Std.string(sourceEnds[i]) + "</sourceEnd>\n" +
				"	<type>" + Std.string(operations[i]) + "</type>\n" +
				"	<target>" + Std.string(cast (targets[i], Int)) + "</target>\n" +
				"	<parameter1>" + Std.string(targetParams1[i]) + "</parameter1>\n" +
				"	<parameter2>" + Std.string(targetParams2[i]) + "</parameter2>\n" +
				"</operation>").firstElement();
			xml.addChild(op);
		}
		return xml;
	}
}