package com.problemmachine.particlesystem;
import com.problemmachine.particlesystem.collision.IParticleCollider;
import com.problemmachine.particlesystem.ParticleBehavior;
import com.problemmachine.particlesystem.internal.ParticleConfiguration;
import com.problemmachine.particlesystem.ParticleSystem;
import com.problemmachine.particlesystem.event.ParticleConfigEvent;
import com.problemmachine.particlesystem.event.ParticleSystemEvent;
import com.problemmachine.particlesystem.internal.ParticleType;
import flash.display.BitmapData;
import flash.events.EventDispatcher;
import flash.geom.Point;
import flash.geom.Rectangle;
#if air
import com.problemmachine.particlesystem.event.ParticleInstanceEvent;
import com.problemmachine.particlesystem.multithreaded.ParticleSystemCommand;
import com.problemmachine.util.multithreading.air.WorkerMessenger;
import com.problemmachine.util.multithreading.event.WorkerMessengerEvent;
import flash.Lib;
import flash.concurrent.Mutex;
import flash.system.Worker;
import flash.utils.ByteArray;
#end

@:access(com.problemmachine.particlesystem.ParticleSystem)
@:access(com.problemmachine.particlesystem.internal.ParticleConfiguration)
@:access(com.problemmachine.particlesystem.internal.ParticleType)
class ParticleInterface
{
	public static inline var MIN_PARTICLE_DISTANCE:Float = -9;
	public static inline var MAX_PARTICLE_DISTANCE:Float = 9;
	
	static private var sConfigurations:Map<String, ParticleConfiguration> = new Map<String, ParticleConfiguration>();
	static private var sPreparedConfigs:Array<String> = [];
	static private var sReadyConfigs:Map<String, Bool> = new Map<String, Bool>();
	static private var sDispatcher:EventDispatcher = new EventDispatcher();
	
	static public var activeConfigName(default, null):String;
	static public var systemReady(default, null):Bool;
	
	#if air
		@:allow(com.problemmachine.particlesystem.ParticleSystem)
		private static inline var MESSAGE_LOCATION:String = "com.problemmachine.particlesystem.worker.ParticleWorkerHandle:MessageLocation";
		static private var sMessenger:WorkerMessenger;
		static private var worker(default, null):Worker;
		static public var progressMutex(default, null):Mutex;
		static public var drawMutex(default, null):Mutex;
		
		static public var autoProcessMessages(default, set):Bool;
	#end
	
	#if air
		static public var layers(get, null):Array<BitmapData>;
	#else
		static public var layers(get, never):Array<BitmapData>;
	#end

	private function new() 
	{}
	
	#if air
	static public function init(worker:Worker, autoProcessMessages:Bool = true):Void
	{
		sMessenger = new WorkerMessenger(worker, MESSAGE_LOCATION, ParticleSystem.MESSAGE_LOCATION);
		if (worker == null)
			worker = Worker.current;
		ParticleInterface.worker = worker;
		drawMutex = new Mutex();
		worker.setSharedProperty(ParticleSystem.DRAW_MUTEX, drawMutex);
		progressMutex = new Mutex();
		worker.setSharedProperty(ParticleSystem.PROGRESS_MUTEX, progressMutex);
		
		ParticleInterface.autoProcessMessages = autoProcessMessages;
		layers = [];
	}
	#end
	
	static public function isConfigReady(configName:String):Bool
	{	
		return sReadyConfigs.get(configName);
	}
	
	static public function isConfigPrepared(configName:String):Bool
	{	
		return sPreparedConfigs.indexOf(configName) >= 0;
	}
	
	static public inline function addEventListener(type:String, listener:Dynamic->Void, ?useCapture:Bool, ?priority:Int, ?useWeakReference:Bool):Void
	{
		sDispatcher.addEventListener(type, listener, useCapture, priority, useWeakReference);
	}
	
	static public inline function removeEventListener(type:String, listener:Dynamic->Void, ?useCapture:Bool):Void
	{
		sDispatcher.removeEventListener(type, listener, useCapture);
	}
	
	static public function registerConfiguration(configName:String):Void
	{
		if (checkConfigExists(configName, "ParticleInterface.registerConfiguration", false))
			return;
		
		var c:ParticleConfiguration = new ParticleConfiguration(configName);
		sConfigurations.set(configName, c);
	}
	
	static public function deleteConfiguration(configName:String):Void
	{
		if (!checkConfigExists(configName, "ParticleInterface.deleteConfiguration"))
			return;
		
		var c:ParticleConfiguration = sConfigurations.get(configName);
		if (isConfigPrepared(c.name))
			retireConfiguration(configName);
		c.dispose();
	}
	
	static public function prepareConfiguration(configName:String):Void
	{
		if (!checkConfigExists(configName, "ParticleInterface.prepareConfiguration"))
			return;
		if (sPreparedConfigs.indexOf(configName) < 0)
		{
			sPreparedConfigs.push(configName);
			sReadyConfigs.set(configName, sConfigurations.get(configName).ready);
		}
		var config:ParticleConfiguration = sConfigurations.get(configName);
		config.addEventListener(ParticleConfigEvent.TYPE_ADDED, configListener);
		config.addEventListener(ParticleConfigEvent.TYPE_DELETED, configListener);
		config.addEventListener(ParticleConfigEvent.TYPE_RENAMED, configListener);
		config.addEventListener(ParticleConfigEvent.CONFIG_RENAMED, configListener);
		#if air
			sendMessage(ParticleSystemCommand.PrepareConfiguration(config.toXML().toString()));			
		#else
			ParticleSystem.system.prepareConfiguration(config);
		#end
	}
	
	static public function retireConfiguration(configName:String):Void
	{
		if (!checkConfigExists(configName, "ParticleInterface.retireConfiguration"))
			return;
		var config:ParticleConfiguration = sConfigurations.get(configName);
		for (s in sPreparedConfigs)
			if (s == configName)
			{
				sPreparedConfigs.remove(s);
				break;
			}
		config.removeEventListener(ParticleConfigEvent.TYPE_ADDED, configListener);
		config.removeEventListener(ParticleConfigEvent.TYPE_DELETED, configListener);
		config.removeEventListener(ParticleConfigEvent.TYPE_RENAMED, configListener);
		config.removeEventListener(ParticleConfigEvent.CONFIG_RENAMED, configListener);
		#if air
			sendMessage(ParticleSystemCommand.RetireConfiguration(configName));		
		#else
			ParticleSystem.system.retireConfiguration(configName);
		#end
	}
	
	static public function getConfigPreAdvanceTime(configName:String):Float
	{
		if (!checkConfigExists(configName, "ParticleInterface.getConfigPreAdvanceTime"))
			return 0;
		return sConfigurations.get(configName).preAdvanceTime;
	}
	static public function setConfigPreAdvanceTime(configName:String, time:Float):Void
	{
		if (!checkConfigExists(configName, "ParticleInterface.setConfigPreAdvanceTime"))
			return;
		sConfigurations.get(configName).preAdvanceTime = time;
		updateConfig(configName);
	}
	
	static public function applyXMLToConfiguration(configName:String, xml:Xml):Void
	{
		if (!checkConfigExists(configName, "ParticleInterface.applyXMLToConfiguration"))
			return;
		sConfigurations.get(configName).setFromXML(xml, shouldBeSleeping(configName), true);
		updateConfig(configName);
	}
	
	static public function getConfigXML(name:String):Xml
	{
		return sConfigurations.get(name).toXML();
	}
	
	static private inline function shouldBeSleeping(name:String):Bool
	{
		if (sPreparedConfigs.indexOf(name) < 0)
			return true;
		#if air
		else if (!Worker.current.isPrimordial)
			return true;
		#end
		else
			return false;
	}
	
	static private inline function checkConfigExists(name:String, calling:String, expected:Bool = true):Bool
	{
		var exists:Bool = sConfigurations.exists(name);
		if (exists != expected)
			trace ("WARNING (" + calling + "): Configuration " + name + (exists ? " already exists " : " not found"));
		return exists;
	}
	
	static public function swapParticleTypeIndices(configName:String, typeName1:String, typeName2:String):Void
	{
		if (!checkConfigExists(configName, "ParticleInterface.swapParticleTypeIndices"))
			return;
		var config:ParticleConfiguration = sConfigurations.get(configName);
		config.swapTypes(config.getTypeByName(typeName1), config.getTypeByName(typeName2));
		updateConfig(configName);
	}
	
	static public function cloneParticleTypes(configName:String, types:Array<String>):Array<String>
	{
		if (!checkConfigExists(configName, "ParticleInterface.cloneParticleTypes"))
			return null;
		var c:ParticleConfiguration = sConfigurations.get(configName);
		var arr:Array<String> = [];
		for (s in types)
			arr.push(c.cloneType(c.getTypeByName(s)).name);
		updateConfig(configName);
		return arr;
	}
	
	static public function getParticleTypes(configName:String, sortOnDistance:Bool):Array<String>
	{
		if (!checkConfigExists(configName, "ParticleInterface.getParticleTypes"))
			return null;
		return sConfigurations.get(configName).getTypeNames(sortOnDistance);
	}
	
	static public function addParticleType(configName:String, ?typeName:String):String
	{
		if (!checkConfigExists(configName, "ParticleInterface.addParticleTypeByXML"))
			return null;
		var newName:String = sConfigurations.get(configName).addType(typeName, null, 0.1, 1, shouldBeSleeping(configName)).name;
		updateConfig(configName);
		return newName;
	}
	
	static public function addParticleTypeFromXML(configName:String, type:Xml):String
	{
		if (!checkConfigExists(configName, "ParticleInterface.addParticleTypeByXML"))
			return null;
		var newName:String = sConfigurations.get(configName).addTypeFromXML(type, shouldBeSleeping(configName)).name;
		updateConfig(configName);
		return newName;
	}
	
	static public function deleteParticleType(configName:String, typeName:String):Void
	{
		if (!checkConfigExists(configName, "ParticleInterface.addParticleTypeByXML"))
			return;
		var c:ParticleConfiguration = sConfigurations.get(configName);
		c.deleteType(c.getTypeByName(typeName));
		updateConfig(configName);
	}
	
	static public function changeParticleTypeName(configName:String, oldTypeName:String, newTypeName:String):Void
	{
		if (!checkConfigExists(configName, "ParticleInterface.changeParticleTypeName"))
			return;
		var c:ParticleConfiguration = sConfigurations.get(configName);
		c.setTypeName(c.getTypeByName(oldTypeName), newTypeName);
		updateConfig(configName);
	}
	
	static public function getParticleTypeInitialCount(configName:String, typeName:String):Int
	{
		if (!checkConfigExists(configName, "ParticleInterface.getParticleTypeInitialCount"))
			return -1;
		return sConfigurations.get(configName).getTypeByName(typeName).initialCount;
	}
	static public function setParticleTypeInitialCount(configName:String, typeName:String, count:Int):Void
	{
		if (!checkConfigExists(configName, "ParticleInterface.setParticleTypeInitialCount"))
			return;
		var t:ParticleType = sConfigurations.get(configName).getTypeByName(typeName);
		if (t.initialCount != count)
		{
			t.initialCount = count;
			updateConfig(configName);
		}
	}
	
	static public function getParticleTypeFacetCount(configName:String, typeName:String):Int
	{
		if (!checkConfigExists(configName, "ParticleInterface.setParticleTypeInitialCount"))
			return -1;
		return sConfigurations.get(configName).getTypeByName(typeName).numberOfFacets;
	}
	static public function removeParticleTypeFacetAt(configName:String, typeName:String, facetIndex:Int):Void
	{
		if (!checkConfigExists(configName, "ParticleInterface.removeParticleTypeFacetAt"))
			return;
		sConfigurations.get(configName).getTypeByName(typeName).removeFacetAt(facetIndex);
		updateConfig(configName);
	}
	static public function addParticleTypeFacet(configName:String, typeName:String):Int
	{
		if (!checkConfigExists(configName, "ParticleInterface.addParticleTypeFacet"))
			return -1;
		var t:ParticleType = sConfigurations.get(configName).getTypeByName(typeName);
		t.addFacet();
		updateConfig(configName);
		return t.numberOfFacets - 1;
	}
	@:access(com.problemmachine.particlesystem.internal.ParticleType.swapFacets)
	static public function swapParticleTypeFacets(configName:String, typeName:String, index1:Int, index2:Int):Void
	{
		if (!checkConfigExists(configName, "ParticleInterface.addParticleTypeFacet"))
			return;
		if (index1 == index2)
			return;
		var t:ParticleType = sConfigurations.get(configName).getTypeByName(typeName);
		t.swapFacetsAt(index1, index2);
		updateConfig(configName);
	}
	static public function getParticleTypeFacetPath(configName:String, typeName:String, index:Int):String
	{
		if (!checkConfigExists(configName, "ParticleInterface.getParticleTypeFacetPath"))
			return null;
		return sConfigurations.get(configName).getTypeByName(typeName).mFacets[index].path;
	}
	static public function setParticleTypeFacetPath(configName:String, typeName:String, index:Int, path:String):Void
	{
		if (!checkConfigExists(configName, "ParticleInterface.addParticleTypeFacet"))
			return;
		var t:ParticleType = sConfigurations.get(configName).getTypeByName(typeName);
		if (path != t.mFacets[index].path)
		{
			t.mFacets[index].setStyle(path);
			updateConfig(configName);
		}
	}
	
	static public function getParticleTypeIsBasicDetail(configName:String, typeName:String):Bool
	{
		if (!checkConfigExists(configName, "ParticleInterface.getParticleTypeIsBasicDetail"))
			return false;
		return sConfigurations.get(configName).getTypeByName(typeName).isBasicDetail();
	}
	
	static public function getParticleTypePerspectiveEnabled(configName:String, typeName:String):Bool
	{
		if (!checkConfigExists(configName, "ParticleInterface.getParticleTypePerspectiveEnabled"))
			return false;
		return sConfigurations.get(configName).getTypeByName(typeName).enablePerspective;
	}
	static public function setParticleTypePerspectiveEnabled(configName:String, typeName:String, enabled:Bool):Void
	{
		if (!checkConfigExists(configName, "ParticleInterface.setParticleTypePerspectiveEnabled"))
			return;
		var t:ParticleType = sConfigurations.get(configName).getTypeByName(typeName);
		if (t.enablePerspective != enabled)
		{
			t.enablePerspective = enabled;
			updateConfig(configName);
		}
		
	}
	static public function getParticleTypeDepthOfFieldEnabled(configName:String, typeName:String):Bool
	{
		if (!checkConfigExists(configName, "ParticleInterface.getParticleTypeDepthOfFieldEnabled"))
			return false;
		return sConfigurations.get(configName).getTypeByName(typeName).enableDepthOfField;
	}
	static public function setParticleTypeDepthOfFieldEnabled(configName:String, typeName:String, enabled:Bool):Void
	{
		if (!checkConfigExists(configName, "ParticleInterface.setParticleTypeDepthOfFieldEnabled"))
			return;
		var t:ParticleType = sConfigurations.get(configName).getTypeByName(typeName);
		if (t.enableDepthOfField != enabled)
		{
			t.enableDepthOfField = enabled;
			updateConfig(configName);
		}
	}
	static public function getParticleTypeSpawnCollisionEnabled(configName:String, typeName:String):Bool
	{
		if (!checkConfigExists(configName, "ParticleInterface.getParticleTypeSpawnCollisionEnabled"))
			return false;
		return sConfigurations.get(configName).getTypeByName(typeName).enableSpawnCollision;
	}
	static public function setParticleTypeSpawnCollisionEnabled(configName:String, typeName:String, enabled:Bool):Void
	{
		if (!checkConfigExists(configName, "ParticleInterface.setParticleTypeSpawnCollisionEnabled"))
			return;
		var t:ParticleType = sConfigurations.get(configName).getTypeByName(typeName);
		if (t.enableSpawnCollision != enabled)
		{
			t.enableSpawnCollision = enabled;
			updateConfig(configName);
		}
	}
	static public function getParticleTypeCollider(configName:String, typeName:String):IParticleCollider
	{
		if (!checkConfigExists(configName, "ParticleInterface.getParticleTypeCollider"))
			return null;
		return sConfigurations.get(configName).getTypeByName(typeName).collider;
	}
	static public function setParticleTypeCollider(configName:String, typeName:String, collider:IParticleCollider):Void
	{
		if (!checkConfigExists(configName, "ParticleInterface.setParticleTypeCollider"))
			return;
		sConfigurations.get(configName).getTypeByName(typeName).collider = collider;
		var t:ParticleType = sConfigurations.get(configName).getTypeByName(typeName);
		t.collider = collider;
		updateConfig(configName);
	}
	
	static public function getParticleTypeBehaviors(configName:String, typeName:String):Array<ParticleBehavior>
	{
		if (!checkConfigExists(configName, "ParticleInterface.getParticleTypeBehaviors"))
			return null;
		return sConfigurations.get(configName).getTypeByName(typeName).mBehaviors.copy();
	}
	static public function addParticleTypeBehavior(configName:String, typeName:String, behavior:ParticleBehavior):Int
	{
		if (!checkConfigExists(configName, "ParticleInterface.addParticleTypeBehavior"))
			return -1;
		var t:ParticleType = sConfigurations.get(configName).getTypeByName(typeName);
		t.addBehavior(behavior);
		updateConfig(configName);
		return t.mBehaviors.length - 1;
	}
	
	static public function getParticleTypeCenter(configName:String, typeName:String):Point
	{
		if (!checkConfigExists(configName, "ParticleInterface.getParticleTypeCenter"))
			return null;
		var t:ParticleType = sConfigurations.get(configName).getTypeByName(typeName);
		return new Point((t.x_start + t.x_end) * 0.5, (t.y_start + t.y_end) * 0.5);
	}
	static public function setParticleTypeCenter(configName:String, typeName:String, center:Point):Void
	{
		if (!checkConfigExists(configName, "ParticleInterface.setParticleTypeCenter"))
			return;
		var t:ParticleType = sConfigurations.get(configName).getTypeByName(typeName);
		var xRange:Float = t.x_end - t.x_start;
		var yRange:Float = t.y_end - t.y_start;
		t.x_start = center.x - xRange * 0.5;
		t.x_end = center.x + xRange * 0.5;
		t.y_start = center.y - yRange * 0.5;
		t.y_end = center.y + yRange * 0.5;
		updateConfig(configName);
	}
	
	static public function getParticleTypeSpawnRect(configName:String, typeName:String):Rectangle
	{
		if (!checkConfigExists(configName, "ParticleInterface.getParticleTypeSpawnRect"))
			return null;
		var t:ParticleType = sConfigurations.get(configName).getTypeByName(typeName);
		var r:Rectangle = new Rectangle();
		r.left = t.x_start;
		r.right = t.x_end;
		r.top = t.y_start;
		r.bottom = t.y_end;
		return r;
	}
	static public function setParticleTypeSpawnRect(configName:String, typeName:String, rect:Rectangle):Void
	{
		if (!checkConfigExists(configName, "ParticleInterface.setParticleTypeSpawnRect"))
			return;
		var t:ParticleType = sConfigurations.get(configName).getTypeByName(typeName);
		t.x_start = rect.left;
		t.x_end = rect.right;
		t.y_start = rect.top;
		t.y_end = rect.bottom;
		updateConfig(configName);
	}
	
	static public function getParticleTypeParameter(configName:String, typeName:String, parameter:ParticleParameter):Array<Float>
	{
		if (!checkConfigExists(configName, "ParticleInterface.getParticleTypeParameter"))
			return null;
		var t:ParticleType = sConfigurations.get(configName).getTypeByName(typeName);
		return [t.paramStart(parameter), t.paramEnd(parameter)];
	}
	static public function setParticleTypeParameter(configName:String, typeName:String, parameter:ParticleParameter, ?start:Float, ?end:Float):Void
	{
		if (!checkConfigExists(configName, "ParticleInterface.setParticleTypeParameter"))
			return;
		var t:ParticleType = sConfigurations.get(configName).getTypeByName(typeName);
		if (start != null)
			t.mSpawnParametersStart[cast parameter] = start;
		if (end != null)
			t.mSpawnParametersEnd[cast parameter] = end;
		updateConfig(configName);
	}
	
	static private inline function updateConfig(configName:String):Void
	{
		if (isConfigPrepared(configName))
		{
			var c:ParticleConfiguration = sConfigurations.get(configName);
			#if air
				sendMessage(ParticleSystemCommand.UpdateConfigParameters(c.toXML().toString()));		
			#else
				ParticleSystem.system.updateConfigurationParameters(c);
			#end
		}
		else
			return;
	}
	
	////////////////////////////////////////////////////////////////////#IF AIR////////////////////////////////////////////////////////////////////
	#if air
	
	static private inline function set_autoProcessMessages(val:Bool):Bool
	{
		if (val != autoProcessMessages)
		{
			if (val)
			{
				sMessenger.addEventListener(WorkerMessengerEvent.RECEIVED_NEW_MESSAGE, receiveMessages);
				sMessenger.enableNotifications = true;
			}
			else
			{
				sMessenger.removeEventListener(WorkerMessengerEvent.RECEIVED_NEW_MESSAGE, receiveMessages);
				sMessenger.enableNotifications = false;
			}
		}
		return autoProcessMessages = val;
	}
	
	static private inline function get_layers():Array<BitmapData>
	{	return layers.copy();	}
	
	static private function sendMessage(m:ParticleSystemCommand):Void
	{
		sMessenger.send(m);
	}
	static public function receiveMessages(?e:WorkerMessengerEvent):Void
	{
		while (sMessenger.numMessages > 0)
		{
			var obj:{event:String, type:String, configName:String, typeName:String, typeCount:Int} = cast sMessenger.receive();
			
			if (obj.event == ParticleSystem.SYSTEM_EVENT)
				processSystemEvent(obj);
			else
				processInstanceEvent(obj);
		}
	}
	static private function processSystemEvent(obj:Dynamic):Void
	{
		var pse:ParticleSystemEvent = new ParticleSystemEvent(obj.type, obj.configName);
		//trace(pse);
		
		if (pse.type == ParticleSystemEvent.PROGRESS_FAILED_BAD_CONFIG)
		{
			systemReady = false;
			if (pse.configName == null)
				activeConfigName = null;
			else
				sReadyConfigs.set(pse.configName, false);
		}
		else if (pse.type == ParticleSystemEvent.UPDATE_START)
		{
			sReadyConfigs.set(pse.configName, true);
			activeConfigName = pse.configName;
		}
		else if (pse.type == ParticleSystemEvent.DRAW_START)
		{
			activeConfigName = pse.configName;
		}
		else if (pse.type == ParticleSystemEvent.DRAW_COMPLETE)
		{
			systemReady = true;
			activeConfigName = pse.configName;
			
			var mutex:Mutex = cast worker.getSharedProperty(ParticleSystem.DRAW_MUTEX);
			var numLayers:Int = cast worker.getSharedProperty(ParticleSystem.NUM_LAYERS);
			for (i in 0...numLayers)
			{
				mutex.lock();
				var bytes:ByteArray = worker.getSharedProperty(ParticleSystem.LAYER_BYTEARRAY + Std.string(i));
				bytes.position = 0;
				layers[i].setPixels(new Rectangle(0, 0, layers[i].width, layers[i].height), bytes);
				mutex.unlock();
			}	
		}
		else if (pse.type == ParticleSystemEvent.CONFIG_ACTIVATED)
		{
			activeConfigName = pse.configName;
		}
		else if (pse.type == ParticleSystemEvent.CONFIG_RETIRED)
		{
			if (activeConfigName == pse.configName)
			{
				activeConfigName = null;
				systemReady = false;
			}
			sReadyConfigs.remove(pse.configName);
		}
		else if (pse.type == ParticleSystemEvent.CONFIG_READY)
		{
			if (activeConfigName == pse.configName)
				systemReady = true;
			sReadyConfigs.set(pse.configName, true);
		}
		else if (pse.type == ParticleSystemEvent.CONFIG_UNREADY)
		{
			if (activeConfigName == pse.configName)
				systemReady = false;
			sReadyConfigs.set(pse.configName, false);
		}
		else if (pse.type == ParticleSystemEvent.LAYERS_MODIFIED)
		{
			var numLayers:Int = cast worker.getSharedProperty(ParticleSystem.NUM_LAYERS);
			var arr:Array<BitmapData> = Reflect.field(ParticleInterface, "layers");
			while (arr.length < numLayers)
				arr.push(new BitmapData(Lib.current.stage.stageWidth, Lib.current.stage.stageHeight, true, 0x00000000));
			while (arr.length > numLayers)
				arr.pop().dispose();
		}
		else if (pse.type == ParticleSystemEvent.READY)
		{
			systemReady = true;
			sReadyConfigs.set(pse.configName, true);
		}
		else if (pse.type == ParticleSystemEvent.UNREADY)
		{
			systemReady = false;
		}
		else if (pse.type == ParticleSystemEvent.CONFIG_ACTIVATE_FAILED)
		{
			trace("Warning(ParticleWorkerHandle): Could not activate particle system " + pse.configName);
			activeConfigName = null;
		}
		/*else if (pwe.type == ParticleSystemEvent.TYPE_FILTER_CHANGED)
		{}
		else if (pwe.type == ParticleSystemEvent.RESET_PARTICLES)
		{}
		else if (pwe.type == ParticleSystemEvent.PARTICLES_SHIFTED)
		{}
		else if (pwe.type == ParticleSystemEvent.CHANNEL_CONNECTED)
		{}
		else if (pse.type == ParticleSystemEvent.CONFIG_PREPARED)
		{}
		else if (pse.type == ParticleSystemEvent.CONFIG_REPLACED)
		{}*/
		
		sDispatcher.dispatchEvent(pse);
	}
	static private function processInstanceEvent(obj:Dynamic):Void
	{
		sDispatcher.dispatchEvent(new ParticleInstanceEvent(obj.type, obj.configName, obj.typeName, obj.typeCount));
	}
	
	static public function progress(time:Float):Void
	{
		sendMessage(ParticleSystemCommand.Progress(time));				
	}
	static public function draw():Void
	{
		sendMessage(ParticleSystemCommand.Draw);						
	}
	
	static public function activateConfiguration(?name:String):Void
	{	
		sendMessage(ParticleSystemCommand.ActivateConfiguration(name));		
	}
	
	static private function configListener(e:ParticleConfigEvent):Void
	{
		var config:ParticleConfiguration = cast e.target;
		if (e.type == ParticleConfigEvent.TYPE_ADDED)
			sendMessage(ParticleSystemCommand.AddType(config.name, config.getTypeByName(e.typeName).toXML().toString()));
		else if (e.type == ParticleConfigEvent.TYPE_DELETED)
			sendMessage(ParticleSystemCommand.DeleteType(config.name, e.typeName));
		else if (e.type == ParticleConfigEvent.TYPE_RENAMED)
		{
			var arr:Array<String> = e.typeName.split("\n");
			sendMessage(ParticleSystemCommand.RenameType(config.name, arr[0], arr[1]));
		}
		else if (e.type == ParticleConfigEvent.CONFIG_RENAMED)
		{
			var arr:Array<String> = e.configName.split("\n");
			sendMessage(ParticleSystemCommand.RenameConfiguration(arr[0], arr[1]));
		}
	}
	
	static public function spawnParticles(names:Array<String>, amount:Int, ?positionOverrides:Array<Point>, ?velocityOverrides:Array<Point>):Void
	{	sendMessage(ParticleSystemCommand.SpawnParticles(names, amount, positionOverrides, velocityOverrides));	 }
	
	static public function setTypeFilter(?filter:Array<String>):Void
	{	sendMessage(ParticleSystemCommand.SetTypeFilter(filter));		}
	
	static public function resetParticles():Void
	{	sendMessage(ParticleSystemCommand.ResetParticles);		}
	
	static public function shiftParticles(x:Float, y:Float):Void
	{	sendMessage(ParticleSystemCommand.ShiftParticles(x, y));		}
	
	static public function setZoom(zoom:Float):Void
	{	sendMessage(ParticleSystemCommand.SetZoom(zoom));						}
	
	static public function setCamera(camera:Point):Void
	{	sendMessage(ParticleSystemCommand.SetCamera(camera.x, camera.y));		}
	
	static public function setAutoProgress(val:Bool):Void
	{	sendMessage(ParticleSystemCommand.SetAutoProgress(val));				}
	
	static public function setAutoDraw(val:Bool):Void
	{	sendMessage(ParticleSystemCommand.SetAutoDraw(val));				}
	
	static public function setLayerSplit(?splits:Array<Float>):Void
	{	sendMessage(ParticleSystemCommand.SetLayerSplit(splits));					}
	
	static public function setDrawRange(minDistance:Float, maxDistance:Float):Void
	{	sendMessage(ParticleSystemCommand.SetDrawRange(minDistance, maxDistance));		}
	
	static public function setGlobalParameter(index:Int, value:Float):Void
	{	sendMessage(ParticleSystemCommand.SetGlobalParameter(index, value));		}
	
	#else
	////////////////////////////////////////////////////////////////////#ELSE////////////////////////////////////////////////////////////////////	
	
	static private function get_layers():Array<BitmapData>
	{
		return ParticleSystem.system.mLayers.copy();
	}
	
	@:allow(com.problemmachine.particlesystem.ParticleSystem.relaySystemEvent)
	static private function dispatchSystemEvent(e:ParticleSystemEvent):Void
	{
		//trace (e.configName + " ----- " + e.type.toUpperCase() + "         (Current Selected Config: " + activeConfigName);
		if (e.type == ParticleSystemEvent.PROGRESS_FAILED_BAD_CONFIG)
		{
			systemReady = false;
			if (e.configName != null)
				sReadyConfigs.set(e.configName, false);
			else
				activeConfigName = null;
		}
		else if (e.type == ParticleSystemEvent.CONFIG_ACTIVATED)
		{
			activeConfigName = e.configName;
		}
		else if (e.type == ParticleSystemEvent.CONFIG_RETIRED)
		{
			if (activeConfigName == e.configName)
				activeConfigName = null;
			sReadyConfigs.remove(e.configName);
		}
		else if (e.type == ParticleSystemEvent.CONFIG_READY)
		{
			sReadyConfigs.set(e.configName, true);
		}
		else if (e.type == ParticleSystemEvent.CONFIG_UNREADY)
		{
			sReadyConfigs.set(e.configName, false);
		}
		else if (e.type == ParticleSystemEvent.READY)
		{
			systemReady = true;
			sReadyConfigs.set(e.configName, true);
		}
		else if (e.type == ParticleSystemEvent.UNREADY)
		{
			systemReady = false;
		}
		else if (e.type == ParticleSystemEvent.CONFIG_ACTIVATE_FAILED)
		{
			trace("Warning(ParticleWorkerHandle): Could not activate particle system " + e.configName);
			activeConfigName = null;
		}
		sDispatcher.dispatchEvent(e);
	}
	
	static private function configListener(e:ParticleConfigEvent):Void
	{
		var config:ParticleConfiguration = cast e.target;
		if (e.type == ParticleConfigEvent.TYPE_DELETED)
			ParticleSystem.system.deleteParticleType(config.name, e.typeName, false);
	}
	
	static public function progress(time:Float):Void
	{	ParticleSystem.system.progress(time);	}
	static public function draw():Void
	{	ParticleSystem.system.draw();	}
	
	static public function activateConfiguration(?name:String):Void
	{	ParticleSystem.system.activateConfiguration(name);	}
	
	static public function spawnParticles(names:Array<String>, amount:Int, ?positionOverrides:Array<Point>, ?velocityOverrides:Array<Point>):Void
	{	ParticleSystem.system.spawnParticles(names, amount, positionOverrides, velocityOverrides);	 }
	
	static public function setTypeFilter(?filter:Array<String>):Void
	{	ParticleSystem.system.setTypeFilter(filter);		}
	
	static public function resetParticles():Void
	{	ParticleSystem.system.resetParticles();				}
	
	static public function shiftParticles(x:Float, y:Float):Void
	{	ParticleSystem.system.shiftParticles(x, y);			}
	
	static public function setZoom(zoom:Float):Void
	{	ParticleSystem.system.mZoom = zoom;					}
	
	static public function setCamera(camera:Point):Void
	{
		ParticleSystem.system.mCamera.x = camera.x;
		ParticleSystem.system.mCamera.y = camera.y;
	}
	
	static public function setAutoProgress(val:Bool):Void
	{	ParticleSystem.system.mAutoProgress = val;			}
	
	static public function setAutoDraw(val:Bool):Void
	{	ParticleSystem.system.mAutoDraw = val;				}
	
	static public function setLayerSplit(?splits:Array<Float>):Void
	{	ParticleSystem.system.setLayerSplit(splits);		}
	
	static public function setDrawRange(minDistance:Float, maxDistance:Float):Void
	{
		ParticleSystem.system.mMinimumDrawDistance = minDistance;
		ParticleSystem.system.mMaximumDrawDistance = maxDistance;
	}
	
	static public function setGlobalParameter(index:Int, value:Float):Void
	{	ParticleSystem.sGlobalParameters[index] = value;		}
	#end
}