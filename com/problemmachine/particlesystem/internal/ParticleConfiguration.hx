package com.problemmachine.particlesystem.internal;

import com.problemmachine.particlesystem.event.ParticleConfigEvent;
import com.problemmachine.particlesystem.event.ParticleTypeEvent;
import com.problemmachine.tools.hash.HashTools;
import com.problemmachine.util.random.RandomNumberGenerator;
import com.problemmachine.tools.xml.XMLTools;
import flash.errors.Error;
import flash.events.Event;
import flash.events.EventDispatcher;

@:allow(com.problemmachine.particlesystem.ParticleSystem)
class ParticleConfiguration extends EventDispatcher
{	
	private var mParticleTypes:Array<ParticleType>;
	
	public var numTypes(get, never):Int;
	public var name(default, set):String;
	public var ready(get, null):Bool;
	
	public var preAdvanceTime(default, default):Float;
	
	private function new(?name:String) 
	{
		super();
		this.preAdvanceTime = 10;
		mParticleTypes = [];
		if (name == null)
			name = "";
		this.name = name;
	}
	
	private function set_name(val:String):String
	{
		if (val == null)
			val = "";
		if (val != name)
		{
			if (val != null && val.indexOf("\n") >= 0)
				throw new Error("ERROR in ParticleConfiguration.set_name(): newline character not allowed in config names (wtf)");
			var namePair:String = name + "\n" + val;
			name = val;
			dispatchEvent(new ParticleConfigEvent(ParticleConfigEvent.CONFIG_RENAMED, namePair, null));
		}
		return val;
	}
	
	private inline function get_numTypes():Int
	{	return mParticleTypes.length;	}
	
	private function get_ready():Bool
	{
		if (ready)
			return true;

		for (type in mParticleTypes)
			if (!type.ready)
				return false;
			
		return ready = true;
	}
	
	private function checkReady(e:Event):Void
	{
		(cast e.target).removeEventListener(ParticleTypeEvent.READY, checkReady);
		if (ready)
			dispatchEvent(new ParticleConfigEvent(ParticleConfigEvent.READY, name, null));
	}
	
	@:allow(com.problemmachine.particlesystem.ParticleSystem)
	private function activate():Void
	{
		for (t in mParticleTypes)
			t.sleeping = false;
	}
	
	public function deactivate():Void
	{
		for (t in mParticleTypes)
			t.sleeping = true;
	}
	
	public function dispose():Void
	{
		for (e in mParticleTypes)
			e.dispose();
		while (mParticleTypes.length > 0)
			mParticleTypes.pop();
	}
	
	private function generateNewTypeName(?baseName:String):String
	{
		if (baseName == null)
			baseName = "Type";
		baseName += " ";
		var number:Int = 0;
		while (getTypeByNameHash(HashTools.hashStringToFloat(baseName + number)) != null)
			++number;
		return baseName + number;
	}
	
	public function setTypeName(type:ParticleType, newName:String):Void
	{
		if (newName.indexOf("\n") >= 0)
			throw new Error("ERROR in ParticleConfiguration.setTypeName(): newline character not allowed in type names (don't be a jackass)");
		var found:Bool = false;
		for (t in mParticleTypes)
			if (t == type)
				found = true;
		if (found)
			checkTypeCollision(type, name);
		if (newName != type.name)
		{
			var names:String = type.name + "\n" + newName;
			type.name = newName;
			type.nameHash = HashTools.hashStringToFloat(newName);
			type.mRandomNumberGenerator = RandomNumberGenerator.fromFloatSeed(type.nameHash);
			dispatchEvent(new ParticleConfigEvent(ParticleConfigEvent.TYPE_RENAMED, this.name, names));
		}
	}
	
	public function getTypeByName(name:String):ParticleType
	{
		for (t in mParticleTypes)
			if (t.name == name)
				return t;
		return null;
	}
	
	private function getTypeByNameHash(hash:Float):ParticleType
	{
		for (t in mParticleTypes)
			if (t.nameHash == hash)
				return t;
		return null;
	}
	
	public function getTypeNames(sortOnDistance:Bool = false):Array<String>
	{
		var types:Array<ParticleType> = mParticleTypes.copy();
		if (sortOnDistance)
			types.sort(function(a:ParticleType, b:ParticleType):Int
				{
					if (a.distance_start + a.distance_end > b.distance_start + b.distance_end)
						return 1;
					else if (a.distance_start + a.distance_end < b.distance_start + b.distance_end)
						return -1;
					else
						return 0;
				});
		else
			types.sort(function(a:ParticleType, b:ParticleType):Int
				{
					if (a.name > b.name)
						return 1;
					else if (a.name < b.name)
						return -1;
					else
						return 0;
				});
		var names:Array<String> = [];
		for (t in types)
			names.push(t.name);
		return names;
	}
	
	private function checkTypeCollision(?type:ParticleType, ?name:String):Void
	{
		if (name == null && type == null)
			return;
		else if (name == null)
			name = type.name;
		var hash:Float = HashTools.hashStringToFloat(name);
		for (t in mParticleTypes)
			if (t != type && HashTools.hashStringToFloat(t.name) == hash)
				throw new Error("ERROR: ParticleSystem.checkTypeCollision() - collision between type name " + t.name + 
					"(hash " + HashTools.hashStringToFloat(t.name) + ") and tested type name " + name + "{hash " + 
					hash + ")");
	}
	
	public inline function addTypeFromXML(xml:Xml, sleeping:Bool):ParticleType
	{
		var type:ParticleType = ParticleType.createFromXML(xml, sleeping);
		if (getTypeByName(type.name) != null)
		{
			var num:Int = 0;
			while (getTypeByName(type.name + " " + (++num)) != null)
				type; //dummy line
			type.name = type.name + " " + num;
			type.nameHash = HashTools.hashStringToFloat(type.name);
			type.mRandomNumberGenerator = RandomNumberGenerator.fromFloatSeed(type.nameHash);
		}
		addRawType(type);
		return type;
	}
	
	public inline function addType(?name:String, ?resourcePaths:Array<String>, updateRate:Float = 0.1, 
		initialCount:Int = 1, sleeping:Bool):ParticleType
	{
		return addTypeAt(name, resourcePaths, updateRate, initialCount, mParticleTypes.length, sleeping);
	}
	
	public function addTypeAt(?name:String, ?resourcePaths:Array<String>, updateRate:Float = 0.1, 
		initialCount:Int = 1, index:Int, sleeping:Bool):ParticleType
	{
		if (name == null)
			name = generateNewTypeName();
		var t:ParticleType = new ParticleType(name, resourcePaths, updateRate, initialCount, sleeping);
		addRawType(t);		
		return t;
	}
	
	private function addRawType(type:ParticleType, ?index:Int):Void
	{
		if (hasType(type))
			return;
		if (type.name == Std.string(null))
			trace ("WARNING: Cannot name particle type \"null\" (reserved)\nRenaming type to " + (type.name = generateNewTypeName()));
		checkTypeCollision(type);
		if (index == null || index >= mParticleTypes.length)
			mParticleTypes.push(type);
		else if (index <= 0)
			mParticleTypes.unshift(type);
		else
			mParticleTypes.insert(index, type);
		if (!type.ready)
		{
			if (ready)
				dispatchEvent(new ParticleConfigEvent(ParticleConfigEvent.UNREADY, name, type.name));
			ready = false;
			type.addEventListener(ParticleTypeEvent.READY, checkReady);
		}
		type.particleConfig = this;
		dispatchEvent(new ParticleConfigEvent(ParticleConfigEvent.TYPE_ADDED, name, type.name));
	}
	
	public function getTypeAt(index:Int):ParticleType
	{
		if (index < 0 || index >= mParticleTypes.length)
			return null;
		return mParticleTypes[index];
	}
	
	public function getTypeIndex(type:ParticleType):Int
	{
		for (i in 0...mParticleTypes.length)
			if (type == mParticleTypes[i])
				return i;
		return -1;
	}
	
	public function deleteType(type:ParticleType):Void
	{
		var length:Int = mParticleTypes.length;
		for (i in 0...length)
			if (mParticleTypes[i] == type)
			{
				deleteTypeAt(i);
				return;
			}
	}
	
	public function deleteTypeAt(index:Int):Void
	{
		var type:ParticleType = null;
		if (index >= 0 && index < mParticleTypes.length)
		{
			type = mParticleTypes[index];
			mParticleTypes.splice(index, 1);
		}
		if (type != null)
		{
			dispatchEvent(new ParticleConfigEvent(ParticleConfigEvent.TYPE_DELETED, name, type.name));
			type.dispose();
		}
	}
	
	@:access(com.problemmachine.particlesystem.internal.ParticleType.clone)
	public function cloneType(type:ParticleType):ParticleType
	{
		var t:ParticleType = type.clone();
		var index:Int = t.name.lastIndexOf("copy");
		if (index < 0)
		{
			index = t.name.length + 1;
			t.name += " copy 1";
		}
		var end:String = t.name.substr(index + 5);
		var validNum:Bool = true;
		for (i in 0...end.length)
			if ("0123456789".indexOf(end.charAt(i)) < 0)
			{
				validNum = false;
				break;
			}
		if (validNum)
		{
			var num:Int = Std.parseInt(end);
			do
			{
				++num;
				try
				{
					checkTypeCollision(null, t.name.substring(0, index + 5) + num);
					t.name = t.name.substring(0, index + 5) + num;
					break;
				}catch(e:Error){}
			}while (true);
		}
		else
			t.name += " copy 1";
		addRawType(t);
		t.nameHash = HashTools.hashStringToFloat(t.name);
		t.mRandomNumberGenerator = RandomNumberGenerator.fromFloatSeed(t.nameHash);
		return t;
	}
	
	public function swapTypes(type1:ParticleType, type2:ParticleType):Void
	{
		if (type1 == type2)
			return;
		var p1:Bool = false;
		var p2:Bool = false;
		for (i in 0...mParticleTypes.length)
		{
			if (!p1 && type1 == mParticleTypes[i])
			{
				mParticleTypes[i] = type2;
				p1 = true;
			}
			else if (!p2 && type2 == mParticleTypes[i])
			{
				mParticleTypes[i] = type1;
				p2 = true;
			}
			else if (p1 && p2)
				return;
		}
	}
	
	public function swapTypesAt(index1:Int, index2:Int):Void
	{
		if (index1 == index2) return;
		var e1:ParticleType = mParticleTypes[index1];
		var e2:ParticleType = mParticleTypes[index2];
		
		mParticleTypes[index1] = e2;
		mParticleTypes[index2] = e1;
	}
	
	public inline function getTypes():Array<ParticleType>
	{	return mParticleTypes.copy();	}
	
	public inline function hasType(type:ParticleType):Bool
	{
		var found:Bool = false;
		for (t in mParticleTypes)
			if (t == type)
			{
				found = true;
				break;
			}
		return found;
	}
	
	static public function fromXML(xml:Xml, sleeping:Bool):ParticleConfiguration
	{
		var pc:ParticleConfiguration = new ParticleConfiguration();
		pc.setFromXML(xml, sleeping, false);
		return pc;
	}
	
	public function setFromXML(xml:Xml, sleeping:Bool, ignoreName:Bool):Void
	{
		dispose();
		mParticleTypes = [];
		preAdvanceTime = Std.parseFloat(XMLTools.getVal(xml, "preAdvanceTime"));
		if (!ignoreName)
		{
			if (xml.elementsNamed("name").hasNext())
				name = XMLTools.getVal(xml, "name");
			else
				name = null;
		}
		for (x in xml.elementsNamed("particleType"))
			addRawType(ParticleType.createFromXML(x, sleeping));
	}
	
	public function toXML():Xml
	{
		var xml:Xml = Xml.parse(
			"<particleSystem>\n" +
			"	<name>" + Std.string(name) + "</name>\n" +	
			"	<preAdvanceTime>" + Std.string(preAdvanceTime) + "</preAdvanceTime>\n" +
			"</particleSystem>").firstElement();
			
		for (t in mParticleTypes)
			xml.addChild(t.toXML());
			
		return xml;
	}
}