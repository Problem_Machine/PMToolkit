package com.problemmachine.particlesystem.internal;

import com.problemmachine.particlesystem.ParticleParameter;
import com.problemmachine.particlesystem.collision.IParticleCollider;
import com.problemmachine.particlesystem.event.ParticleTypeEvent;
import com.problemmachine.particlesystem.internal.ParticleConfiguration;
import com.problemmachine.particlesystem.internal.ParticleFacet;
import com.problemmachine.particlesystem.internal.ParticleOp;
import com.problemmachine.tools.file.FileTools;
import com.problemmachine.tools.hash.HashTools;
import com.problemmachine.tools.xml.XMLTools;
import com.problemmachine.util.random.RandomNumberGenerator;
import flash.display.BlendMode;
import flash.errors.Error;
import flash.events.EventDispatcher;
import flash.events.TimerEvent;
import flash.utils.Timer;
import haxe.ds.Vector.Vector;
import haxe.xml.Fast;

@:allow(com.problemmachine.particlesystem.internal.ParticleConfiguration)
@:allow(com.problemmachine.particlesystem.internal.ParticleInstance)
@:allow(com.problemmachine.particlesystem.ParticleSystem)
class ParticleType extends EventDispatcher
{		
	private var mFacets:Array<ParticleFacet>;
	private var mAllowCaching:Bool;	
	
	private var mBehaviors:Array<ParticleBehavior>;
	private var mSpawnParametersStart:Vector<Float>;
	private var mSpawnParametersEnd:Vector<Float>;
	private var mRandomNumberGenerator:RandomNumberGenerator;
	
	public var name(default, null):String;
	private var nameHash:Float;
	public var particleConfig(default, null):ParticleConfiguration;
	public var sleeping(default, set):Bool;
	public var ready(get, null):Bool;
	public var updateRate(default, default):Float;
	public var enableDepthOfField(default, default):Bool;
	public var enablePerspective(default, default):Bool;
	public var enableBlendFacets(default, default):Bool;
	public var enableSpawnCollision(default, default):Bool;
	public var collider(default, default):IParticleCollider;
	public var initialCount(default, default):Int;
	public var numberOfFacets(get, never):Int;
	
	private function new(name:String, ?resourcePaths:Array<String>, updateRate:Float = 0.1, initialCount:Int = 1, startSleeping:Bool = true)
	{
		super(this);
		particleConfig = null;
		mBehaviors = [];
		
		this.name = name;
		this.nameHash = HashTools.hashStringToFloat(name);
		mRandomNumberGenerator = RandomNumberGenerator.fromFloatSeed(this.nameHash);
		mAllowCaching = true;
		ready = false;
		
		this.updateRate = updateRate;
		enableDepthOfField = false;
		enablePerspective = true;
		enableBlendFacets = false;
		collider = null;
		enableSpawnCollision = false;
		this.initialCount = initialCount;
		
		mFacets = [];
		
		if (resourcePaths != null)
			for (s in resourcePaths)
			{
				var f:ParticleFacet = new ParticleFacet(s);
				mFacets.push(f);
				f.progress(paramStart(ParticleParameter.ANIMATION_OFFSET) + paramRange(ParticleParameter.ANIMATION_OFFSET) * mRandomNumberGenerator.random());
			}
		
		mSpawnParametersStart = new Vector<Float>(cast ParticleParameter.NUMBER_OF_PARAMETERS);
		mSpawnParametersEnd = new Vector<Float>(cast ParticleParameter.NUMBER_OF_PARAMETERS);
		setDefaultSpawnParameters();
		sleeping = startSleeping;
	}
	
	private function set_sleeping(val:Bool):Bool
	{
		if (val != sleeping)
		{
			for (f in mFacets)
				f.sleeping = val;
			if (val)
				ready = false;
		}
		return sleeping = val;
	}
	
	private function get_ready():Bool
	{
		if (sleeping)
			return false;
		else if (ready)
			return true;
		else
		{
			ready = true;
			for (f in mFacets)
				if (!f.ready)
					ready = false;
				
			return ready;
		}
	}
	
	public inline function paramStart(param:ParticleParameter):Float
	{	return mSpawnParametersStart[cast param];	}
	public inline function paramEnd(param:ParticleParameter):Float
	{	return mSpawnParametersEnd[cast param];	}
	public inline function paramRange(param:ParticleParameter):Float
	{	return mSpawnParametersEnd[cast param] - mSpawnParametersStart[cast param];	}
	
	private function isBasicDetail():Bool
	{
		if (mBehaviors.length > 0)
			return false;
		if (initialCount > 1)
			return false;
		for (i in 0...mSpawnParametersStart.length)
			if (mSpawnParametersStart[i] != mSpawnParametersEnd[i])
				return false;
		if (paramStart(ROTATION_SPEED) != 0 || paramStart(X_VELOCITY) != 0 || paramStart(Y_VELOCITY) != 0)
			return false;
		else return true;
	}
	
	private inline function get_numberOfFacets():Int
	{	return mFacets.length;	}
	
	@:allow(com.problemmachine.particlesystem.internal.ParticleInstance)
	private inline function getFacet(index:Int):ParticleFacet
	{	return mFacets[index];	}
	
	private function unready():Void
	{
		ready = false;
		var t:Timer = new Timer(10);
		t.addEventListener(TimerEvent.TIMER, waitReadyListener);
		t.start();
	}
	private function waitReadyListener(e:TimerEvent):Void
	{
		if (ready || sleeping)
		{
			var t:Timer = cast e.target;
			t.removeEventListener(TimerEvent.TIMER, waitReadyListener);
			t.stop();
			if (!sleeping)
				dispatchEvent(new ParticleTypeEvent(ParticleTypeEvent.READY, name));
		}
	}
	
	private function addFacet(?path:String, ?blendMode:BlendMode):Int
	{
		ready = false;
		if (path == Std.string(null))
			path = null;
		if (path != null)
			path = FileTools.formatPath(path);
		var facet:ParticleFacet = new ParticleFacet(path, blendMode);
		facet.progress(paramStart(ANIMATION_OFFSET) + paramRange(ANIMATION_OFFSET) * mRandomNumberGenerator.random());
		if (!facet.ready)
			unready();
		return mFacets.push(facet) - 1;
	}
	
	private function addRawFacet(facet:ParticleFacet):Void
	{
		mFacets.push(facet);
	}
	
	private function getFacetIndex(?path:String):Int
	{
		path = FileTools.formatPath(path);
		for (i in 0...mFacets.length)
			if (mFacets[i].path == path)
				return i;
		return -1;
	}
	
	private function getLastFacetIndex(?path:String):Int
	{
		if (path == Std.string(null))
			path = null;
		if (path != null)
			path = FileTools.formatPath(path);
		for (i in 0...mFacets.length)
			if (mFacets[mFacets.length - 1 - i].path == path)
				return (mFacets.length - 1 - i);
		return -1;
	}
	
	private function insertFacetAt(?path:String, ?blendMode:BlendMode, i:Int):Int
	{
		if (path == Std.string(null))
			path = null;
		if (path != null)
			path = FileTools.formatPath(path);
			
		var facet:ParticleFacet = new ParticleFacet(path, blendMode);
		facet.progress(paramStart(ANIMATION_OFFSET) + paramRange(ANIMATION_OFFSET) * mRandomNumberGenerator.random());
		if (!facet.ready)
			unready();
			
		if (i >= mFacets.length)
			return (mFacets.push(facet) - 1);
		else if (i <= 0)
		{
			mFacets.unshift(facet);
			return 0;
		}
		else
		{
			mFacets.insert(i, facet);
			return i;
		}
	}
	
	private function removeFacetAt(i:Int):String
	{
		if (i < 0 || i >= mFacets.length)
			throw new Error("ParticleType.removeFacetAt() -- ERROR: Index " + i + " out of range " + mFacets.length);
		var pf:ParticleFacet = mFacets[i];
		mFacets.splice(i, 1);
		return pf.path;
	}
	
	private function swapFacetsAt(index1:Int, index2:Int):Void
	{
		if (index1 < 0 || index1 >= mFacets.length)
			throw new Error("ParticleType.swapFacetsAt() -- ERROR: Index1 " + index1 + " out of range " + mFacets.length);
		if (index2 < 0 || index2 >= mFacets.length)
			throw new Error("ParticleType.swapFacetsAt() -- ERROR: Index2 " + index2 + " out of range " + mFacets.length);
		var pf:ParticleFacet = mFacets[index2];
		mFacets[index2] = mFacets[index1];
		mFacets[index1] = pf;
	}
	
	private inline function hasFacet(?path:String):Bool
	{
		return (getFacetIndex(path) >= 0);
	}
	
	private inline function getFacetPaths():Array<String>
	{
		var arr:Array<String> = [];
		for (pf in mFacets)
			arr.push(pf.path);
		return arr;
	}
	
	private function addBehavior(b:ParticleBehavior):Void
	{
		b.particleType = this;
		mBehaviors.push(b);
		if (b.updateRate > 0)
			for (i in 0...b.targets.length)
				switch(b.targets[i])
				{
					case RED, GREEN, BLUE, HUE, SATURATION, BRIGHTNESS, ROTATION, SCALE, DISTANCE:
						if (b.operations[i] != ParticleOp.RandomTrigger)
						{
							for (f in mFacets)
								f.cacheEnabled = false;
						}
					default:
				}
	}
	
	private function removeAllBehaviors():Void
	{
		while (mBehaviors.length > 0)
			mBehaviors.pop().particleType = null;
	}
	
	private function setSpawnParameter(param:Int, start:Float, end:Float):Void
	{
		if (param == cast ParticleParameter.ROTATION_SPEED)
			if (start != 0 || end != 0)
			{
				mAllowCaching = false;
				for (f in mFacets)
					f.cacheEnabled = false;
			}
			
		mSpawnParametersStart[param] = start;
		mSpawnParametersEnd[param] = end;
	}
	
	private inline function setDefaultSpawnParameters():Void
	{
		var start = mSpawnParametersStart;
		var end = mSpawnParametersEnd;
		
		for (i in 0...(cast (ParticleParameter.NUMBER_OF_PARAMETERS, Int)))
		{
			start[i] = end[i] = switch(i)
			{
				case ParticleParameter.TYPE, ParticleParameter.LIFE, ParticleParameter.HUE,
					ParticleParameter.BRIGHTNESS, ParticleParameter.FACET_BLEND, 
					ParticleParameter.ANIMATION_OFFSET,	ParticleParameter.X, ParticleParameter.Y, 
					ParticleParameter.DISTANCE, ParticleParameter.X_VELOCITY, ParticleParameter.Y_VELOCITY, 
					ParticleParameter.ROTATION,	ParticleParameter.ROTATION_SPEED, ParticleParameter.COLLISION, 
					ParticleParameter.AUX_0, ParticleParameter.AUX_1, ParticleParameter.AUX_2, 
					ParticleParameter.AUX_3, ParticleParameter.AUX_4, ParticleParameter.AUX_5, 
					ParticleParameter.AUX_6, ParticleParameter.AUX_7:
						0;
				case ParticleParameter.RED, ParticleParameter.GREEN, ParticleParameter.BLUE,
					ParticleParameter.ALPHA, ParticleParameter.SATURATION, ParticleParameter.ANIMATION_SPEED,
					ParticleParameter.SCALE:
						1;
				default:
						0;
			}
		}
	}
	
	private function dispose():Void
	{
		sleeping = true;
		for (f in mFacets)
			f.dispose();
		mFacets = null;
		mBehaviors = null;
		particleConfig = null;
	}
	
	private function clone():ParticleType
	{
		var t:ParticleType = new ParticleType(name, null, updateRate, initialCount);
		t.mimic(this);
		return t;
	}
	
	private function mimic(target:ParticleType):Void
	{
		mAllowCaching = target.mAllowCaching;
		for (i in 0...target.mBehaviors.length)
			if (mBehaviors[i] != null)
				mBehaviors[i].mimic(target.mBehaviors[i]);
			else
				mBehaviors[i] = target.mBehaviors[i].clone();
		while (mBehaviors.length > target.mBehaviors.length)
			mBehaviors.pop();
		for (i in 0...target.mFacets.length)
			if (mFacets[i] != null)
				mFacets[i].mimic(target.mFacets[i]);
			else
				mFacets[i] = target.mFacets[i].clone();
		while (mFacets.length > target.mFacets.length)
			mFacets.pop().dispose();
		for (i in 0...target.mSpawnParametersStart.length)
			mSpawnParametersStart[i] = target.mSpawnParametersStart[i];
		for (i in 0...target.mSpawnParametersEnd.length)
			mSpawnParametersEnd[i] = target.mSpawnParametersEnd[i];
		
		enableDepthOfField = target.enableDepthOfField;
		enablePerspective = target.enablePerspective;
		enableSpawnCollision = target.enableSpawnCollision;
		if (target.collider != null)
		{
			collider = Type.createInstance(Type.getClass(target.collider), []);
			collider.loadXML(target.collider.toXML());
		}
		sleeping = target.sleeping;
		ready = target.ready;
	}
	
	static private function createFromXML(xml:Xml, startSleeping:Bool = true):ParticleType
	{
		var f:Fast = new Fast(xml);
		var name:String = f.node.name.innerData;
		var updateRate:Float = Std.parseFloat(f.node.updateRate.innerData);
		var initialCount:Int = Std.parseInt(f.node.initialCount.innerData);
		var pt:ParticleType = new ParticleType(name, updateRate, initialCount, startSleeping);
		pt.enableDepthOfField = f.hasNode.enableDepthOfField && Std.string(f.node.enableDepthOfField.innerData) == Std.string(true);
		pt.enablePerspective = f.hasNode.enablePerspective && Std.string(f.node.enablePerspective.innerData) == Std.string(true);
		pt.enableBlendFacets = f.hasNode.enableBlendFacets && Std.string(f.node.enableBlendFacets.innerData) == Std.string(true);
		pt.enableSpawnCollision = f.hasNode.enableSpawnCollision && Std.string(f.node.enableSpawnCollision.innerData) == Std.string(true);
		if (f.hasNode.collider && Std.string(f.node.collider.innerData) != Std.string(null) )
		{
			var cls:Class<IParticleCollider> = cast Type.resolveClass(Std.string(f.node.collider.innerData));
			var col:IParticleCollider = Type.createInstance(cls, []);
			col.loadXML(f.node.colliderData.x.firstElement());
			pt.collider = col;
		}
		#if openfl
			for (x in f.nodes.facet)
				pt.addFacet(x.node.resource.innerData, x.node.blendMode.innerData.toLowerCase());
		#else
			for (x in f.nodes.facet)
				pt.addFacet(x.node.resource.innerData, Type.createEnum(BlendMode, x.node.blendMode.innerData.toUpperCase()));
		#end
		
		pt.mSpawnParametersStart[cast ParticleParameter.TYPE] = 0;
		pt.mSpawnParametersEnd[cast ParticleParameter.TYPE] = 0;
		for (i in 1...(cast ParticleParameter.NUMBER_OF_PARAMETERS))
		{
			var p:ParticleParameter = cast i;
			if (xml.elementsNamed("start_" + p).hasNext())
				pt.mSpawnParametersStart[i] = Std.parseFloat(XMLTools.getVal(xml, "start_" + p));
			if (xml.elementsNamed("end_" + p).hasNext())
				pt.mSpawnParametersEnd[i] = Std.parseFloat(XMLTools.getVal(xml, "end_" + p));
		}
		
		for (x in xml.elementsNamed("behavior"))
			pt.addBehavior(ParticleBehavior.createFromXML(x));

		return pt;
	}
	
	private function toXML():Xml
	{
		var xml:Xml = Xml.parse(
			"<particleType>\n" +
			"	<name>" + name + "</name>\n" +
			"	<updateRate>" + Std.string(updateRate) + "</updateRate>\n" +
			"	<initialCount>" + Std.string(initialCount) + "</initialCount>\n" +
			"	<enableDepthOfField>" + Std.string(enableDepthOfField) + "</enableDepthOfField>\n" +
			"	<enablePerspective>" + Std.string(enablePerspective) + "</enablePerspective>\n" +
			"	<enableBlendFacets>" + Std.string(enableBlendFacets) + "</enableBlendFacets>\n" +
			"	<enableSpawnCollision>" + Std.string(enableSpawnCollision) + "</enableSpawnCollision>\n" +
			"</particleType>").firstElement();
			
		for (i in 1...(cast ParticleParameter.NUMBER_OF_PARAMETERS))
		{
			xml.addChild(Xml.parse("<start_" + cast(i, ParticleParameter) + ">" + Std.string(paramStart(cast i)) + "</start_" + cast(i, ParticleParameter) + ">\n").firstElement());
			xml.addChild(Xml.parse("<end_" + cast(i, ParticleParameter) + ">" + Std.string(paramEnd(cast i)) + "</end_" + cast(i, ParticleParameter) + ">").firstElement());
		}
			
		if (collider == null)
			xml.addChild(Xml.parse("<collider>" + Std.string(null) + "</collider>").firstElement());
		else
		{
			xml.addChild(Xml.parse("<collider>" + Type.getClassName(Type.getClass(collider)) + "</collider>").firstElement());
			var x:Xml = Xml.parse("<colliderData/>").firstElement();
			x.addChild(collider.toXML());
			xml.addChild(x);
		}
			
		for (f in mFacets)
			xml.addChild(Xml.parse(	"<facet>\n" + 
									"	<resource>" + f.path + "</resource>\n" +
									"	<blendMode>" + Std.string(f.blendMode).toUpperCase() + "</blendMode>\n" +
									"</facet>").firstElement());
		
		for (b in mBehaviors)
			xml.addChild(b.toXML());
			
		return xml;
	}
	
	static private inline function findDistanceScale(distance:Float, min:Float = 0.1, max:Float = 10):Float
	{
		if (distance > 0)
			distance = Math.max(min, 1 / (distance + 1));
		else if (distance == 0)
			distance = 1;
		else
			distance = Math.abs(distance) + 1;
		return Math.min(max, distance);
	}
	
	public var x_start(get, set):Float;
	public var x_end(get, set):Float;
	private inline function get_x_start():Float			
	{	return mSpawnParametersStart[cast ParticleParameter.X];		}
	private inline function set_x_start(val:Float):Float	
	{	return mSpawnParametersStart[cast ParticleParameter.X] = val;	}
	private inline function get_x_end():Float			
	{	return mSpawnParametersEnd[cast ParticleParameter.X];		}
	private inline function set_x_end(val:Float):Float	
	{	return mSpawnParametersEnd[cast ParticleParameter.X] = val;	}
	
	public var y_start(get, set):Float;
	public var y_end(get, set):Float;
	private inline function get_y_start():Float			
	{	return mSpawnParametersStart[cast ParticleParameter.Y];		}
	private inline function set_y_start(val:Float):Float	
	{	return mSpawnParametersStart[cast ParticleParameter.Y] = val;	}
	private inline function get_y_end():Float			
	{	return mSpawnParametersEnd[cast ParticleParameter.Y];		}
	private inline function set_y_end(val:Float):Float	
	{	return mSpawnParametersEnd[cast ParticleParameter.Y] = val;	}
	
	public var distance_start(get, set):Float;
	public var distance_end(get, set):Float;
	private inline function get_distance_start():Float			
	{	return mSpawnParametersStart[cast ParticleParameter.DISTANCE];		}
	private inline function set_distance_start(val:Float):Float	
	{	return mSpawnParametersStart[cast ParticleParameter.DISTANCE] = val;	}
	private inline function get_distance_end():Float			
	{	return mSpawnParametersEnd[cast ParticleParameter.DISTANCE];		}
	private inline function set_distance_end(val:Float):Float	
	{	return mSpawnParametersEnd[cast ParticleParameter.DISTANCE] = val;	}
}