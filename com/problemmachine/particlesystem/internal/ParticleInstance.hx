package com.problemmachine.particlesystem.internal;
import com.problemmachine.animation.Animation;
import com.problemmachine.particlesystem.ParticleParameter;
import com.problemmachine.particlesystem.event.ParticleInstanceEvent;
import com.problemmachine.particlesystem.ParticleBehavior;
import com.problemmachine.particlesystem.internal.ParticleFacet;
import com.problemmachine.particlesystem.internal.ParticleType;
import com.problemmachine.util.color.colormatrix.ColorMatrix;
import flash.display.BitmapData;
import flash.display.IGraphicsData;
import flash.display.Sprite;
import flash.errors.Error;
import flash.filters.BlurFilter;
import flash.geom.ColorTransform;
import flash.geom.Point;
import haxe.ds.Vector.Vector;

class ParticleInstance
{
	private static inline var MAX_BLUR:Float = 20;
	
	public var dead(default, set):Bool;
	public var sleeping(default, set):Bool;
	public var changedParameters(default, null):Vector<Bool>;
	public var depthOfField(default, default):Bool;
	public var perspective(default, default):Bool;
	
	private var mParameters(default, null):Vector<Float>;
	private var mBehaviorTimers:Array<Float>;
	private var mTypeTimer:Float;
	
	@:allow(com.problemmachine.particlesystem.internal.ParticleType)
	@:allow(com.problemmachine.particlesystem.ParticleSystem)
	private var mTypeClass:ParticleType;
	private var mBlurFilter:BlurFilter;
	private var mFacets:Vector<ParticleFacet>;
	private var mUpdateRenderQueued:Bool;
	private var mForceCompleteUpdate:Bool;
	
	private function new(type:ParticleType)
	{
		mParameters = new Vector<Float>(cast ParticleParameter.NUMBER_OF_PARAMETERS);
		changedParameters = new Vector<Bool>(cast ParticleParameter.NUMBER_OF_PARAMETERS);
		
		depthOfField = true;
		perspective = true;
		mUpdateRenderQueued = false;
		mForceCompleteUpdate = false;
		
		assign(type);
	}
	
	private inline function set_dead(val:Bool):Bool
	{
		if (!val)
			throw new Error("ERROR in ParticleInstance.set_dead(): Cannot set to false manually");
		if (val != dead)
		{
			mTypeClass.dispatchEvent(new ParticleInstanceEvent(ParticleInstanceEvent.DIED, mTypeClass.particleConfig.name, mTypeClass.name, 1));
			dead = true;
		}
		return val;
	}
	
	private inline function set_sleeping(val:Bool):Bool
	{
		if (val != sleeping)
		{
			if (mFacets != null)
				for (i in 0...mFacets.length)
					mFacets[i].sleeping = val;
		}
		return sleeping = val;
	}
	
	public inline function getParam(param:ParticleParameter):Float
	{	return mParameters[cast param];		}
	public inline function setParam(param:ParticleParameter, val:Float):Void
	{	mParameters[cast param] = val;		}
	
	@:allow(com.problemmachine.particlesystem.ParticleSystem)
	@:access(com.problemmachine.particlesystem.internal.ParticleType)
	private inline function reset():Void
	{
		revive();
		
		for (i in 0...(cast ParticleParameter.NUMBER_OF_PARAMETERS))
		{
			var param:Float = mParameters[i];
			mParameters[i] = mTypeClass.paramStart(cast i) + (mTypeClass.paramRange(cast i) * random());
			if (param != mParameters[i])
				changedParameters[i] = true;
		}
		if (mTypeClass.collider != null && mTypeClass.enableSpawnCollision)
		{
			for (i in 0...100)
			{
				mTypeClass.collider.particleCollision(mTypeClass, [this]);
				if (getParam(COLLISION) == 0)
					break;
				setParam(X, mTypeClass.paramStart(X) + (mTypeClass.paramRange(X) * random()));
				setParam(Y, mTypeClass.paramStart(Y) + (mTypeClass.paramRange(Y) * random()));
			}
		}
		
		for (b in mTypeClass.mBehaviors)
			if (b.updateRate <= 0)
				b.applyBehavior(this);
		
		for (f in mFacets)
			f.progress(getParam(ANIMATION_OFFSET));
				
		mUpdateRenderQueued = true;
		
		setParam(RESPAWN, 0);
	}
	
	@:allow(com.problemmachine.particlesystem.ParticleSystem)
	@:access(com.problemmachine.particlesystem.internal.ParticleType)
	private inline function progress(t:Float):Void
	{
		if (!sleeping && !mTypeClass.sleeping)
		{
			mTypeTimer += t;
			for (i in 0...mBehaviorTimers.length)
				mBehaviorTimers[i] += t;	// direct for loop doesn't work, apparently it doesn't address floats as references
			var rate:Float = mTypeClass.updateRate;
			while (mTypeTimer > rate)
			{
				mParameters[cast ParticleParameter.X] += mParameters[cast ParticleParameter.X_VELOCITY] * rate;
				mParameters[cast ParticleParameter.Y] += mParameters[cast ParticleParameter.Y_VELOCITY] * rate;
				mParameters[cast ParticleParameter.ROTATION] += mParameters[cast ParticleParameter.ROTATION_SPEED] * rate;
				mParameters[cast ParticleParameter.LIFE] += rate;
				
				for (f in mFacets)
					f.progress(rate);
				
				mTypeTimer -= rate;
				
				var behaviors:Array<ParticleBehavior> = mTypeClass.mBehaviors;
				for (i in 0...behaviors.length)
					if (behaviors[i].updateRate > 0)
						while (mBehaviorTimers[i] > behaviors[i].updateRate)
						{
							behaviors[i].applyBehavior(this);
							mBehaviorTimers[i] -= behaviors[i].updateRate;
						}
			}
		}
		mUpdateRenderQueued = true;
	}
	
	@:access(com.problemmachine.particlesystem.internal.ParticleType)
	public function assign(type:ParticleType):Void
	{
		if (type == null || type == mTypeClass)
			return;
		depthOfField = type.enableDepthOfField;
		perspective = type.enablePerspective;
		mUpdateRenderQueued = true;
		mForceCompleteUpdate = true;
		mTypeClass = type;
		if (mFacets != null)
			for (f in mFacets)
				f.dispose();
		
		var facets:Array<ParticleFacet> = type.mFacets;
		mFacets = new Vector<ParticleFacet>(Std.int(Math.max(1, facets.length)));
		for (i in 0...facets.length)
		{
			mFacets[i] = facets[i].clone();
			mFacets[i].sleeping = sleeping;
		}
		if (facets.length == 0)
			mFacets[0] = new ParticleFacet();
		
		mTypeTimer = random() * type.updateRate; // randomize so all particles of same type don't process on the same frame
			
		var behaviors:Array<ParticleBehavior> = type.mBehaviors;
		mBehaviorTimers = [];			
		for (i in 0...behaviors.length)
			mBehaviorTimers.push(random() * behaviors[i].updateRate);
			
		setParam(TYPE, 0);
	}
	
	public inline function revive():Void
	{		
		Reflect.setField(this, "dead", false);	//dead = false;
		sleeping = false;
		setParam(RESPAWN, 0);
		mTypeClass.dispatchEvent(new ParticleInstanceEvent(ParticleInstanceEvent.SPAWNED, mTypeClass.particleConfig.name, mTypeClass.name, 1));
	}
	
	public inline function dispose():Void
	{
		if (mFacets != null)
			for (i in 0...mFacets.length)
				mFacets[i].dispose();
		
		mFacets = null;
		sleeping = true;
	}
	
	public inline function preCache():Void
	{
		for (f in mFacets)
			if (f.getAnimation() != null)
				f.getAnimation().preCacheAllFrames();
	}
	
	private inline function updateRender():Void
	{
		mForceCompleteUpdate = mUpdateRenderQueued = false;
		var P = ParticleParameter;
			
		for (f in mFacets)
			for (i in 0...changedParameters.length)
				if (mForceCompleteUpdate || changedParameters[i])
				{
					changedParameters[i] = false;
					switch(i)
					{
						
						case P.DISTANCE:
							if (depthOfField)
							{
								var blur:Float = findDistanceBlur();
								if (mBlurFilter == null)
								{
									mBlurFilter = new BlurFilter(blur);
									f.setFilters([mBlurFilter]);
								}
								else if (Math.abs(blur - mBlurFilter.blurX) > 1)
								{
									mBlurFilter.blurX = mBlurFilter.blurY = blur;
									f.setFilters([mBlurFilter]);
								}
							}
							else if (mBlurFilter != null)
							{
								f.setFilters([]);
								mBlurFilter = null;
							}
						
						case P.FACET_BLEND:
							setParam(FACET_BLEND, Math.min(mFacets.length - 1, Math.max(0, getParam(FACET_BLEND))));
					}
				}
	}
	
	public inline function generateColorMatrix():ColorMatrix
	{
		var m:ColorMatrix = new ColorMatrix();
		m.setMultipliers(getParam(RED), getParam(GREEN), getParam(BLUE), 1);
		m.adjustSaturation(getParam(SATURATION));
		m.adjustBrightness(getParam(BRIGHTNESS));
		m.adjustHue(getParam(HUE));
		return m;
	}
	
	private static var sOffset:Point = new Point(); //allocating a static variable to get rid of allocation/deallocation on each draw
	public inline function drawParticle(surface:BitmapData, camera:Point, center:Point, zoom:Float = 1):Void
	{		
		if (mUpdateRenderQueued)
			updateRender();
			
		var facetBlend:Float = getParam(FACET_BLEND);
		var i:Int = Math.floor(facetBlend);
		var alpha2:Float = facetBlend - i;
		var alpha1:Float = (1 - alpha2) * getParam(ALPHA);
		
		var blending:Bool = alpha2 != 0;
		alpha2 *= getParam(ALPHA);
		if (!mTypeClass.enableBlendFacets)
		{
			blending = false;
			i = Math.round(facetBlend);
			alpha1 = getParam(ALPHA);
		}
		
		var f:ParticleFacet = mFacets[i];

		var d:Float = perspective ? ParticleType.findDistanceScale(getParam(DISTANCE)) : 1;
		zoom *= d;
		sOffset.x = (getParam(X) - camera.x - f.width * 0.5 - center.x) * zoom + center.x;
		sOffset.y = (getParam(Y) - camera.y - f.height * 0.5 - center.y) * zoom + center.y;
		//sOffset.x = (x - camera.x - f.width * 0.5) * zoom;
		//sOffset.y = (y - camera.y - f.height * 0.5) * zoom;
		zoom *= getParam(SCALE);
		
		f.draw(surface, sOffset, zoom, alpha1, this);
		if (blending) 
		{
			var f2:ParticleFacet = mFacets[i + 1];
			sOffset.x -= (f2.width - f.width) * 0.5 * zoom;
			sOffset.y -= (f2.height - f.height) * 0.5 * zoom;
			f2.draw(surface, sOffset, zoom, alpha2, this);
		}
	}
	
	@:access(com.problemmachine.particlesystem.internal.ParticleType.mRandomNumberGenerator)
	private inline function random():Float
	{
		return mTypeClass.mRandomNumberGenerator.random();
	}
	
	private inline function findDistanceBlur():Float
	{	return ((Math.abs(getParam(DISTANCE)) / ParticleInterface.MAX_PARTICLE_DISTANCE) * MAX_BLUR);	}
	
	static public inline function sortByDistance(particles:Array<ParticleInstance>):Void
	{	particles.sort(compareDistanceDescending);	}
	static private inline function compareDistanceDescending(a:ParticleInstance, b:ParticleInstance):Int
	{
		var diff:Float = (a.getParam(DISTANCE) - b.getParam(DISTANCE));
		if (diff < 0) 
			return 1;
		else if (diff == 0)
			return 0;
		else
			return -1;
	}
}