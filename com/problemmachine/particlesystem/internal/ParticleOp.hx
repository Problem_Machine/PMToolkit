package com.problemmachine.particlesystem.internal;

enum ParticleOp
{
	SetVal;
	Add;
	Multiply;
	RandomTrigger;
	Randomize;
}