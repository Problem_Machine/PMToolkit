package com.problemmachine.particlesystem.internal;

enum ParticleRenderStyle
{
	Animation;
	Graphics;
	Pixel;
}