package com.problemmachine.particlesystem.internal;
import com.problemmachine.animation.Animation;
import com.problemmachine.animation.animationmanager.AnimationManager;
import com.problemmachine.bitmap.bitmapmanager.BitmapManager;
import com.problemmachine.bitmap.bitmapmanager.BitmapManagerQuery;
import com.problemmachine.graphics.graphicsmanager.GraphicsManager;
import com.problemmachine.util.color.Color;
import flash.display.BitmapData;
import flash.display.BlendMode;
import flash.display.IGraphicsData;
import flash.display.Sprite;
import flash.filters.BitmapFilter;
import flash.filters.ColorMatrixFilter;
import flash.geom.ColorTransform;
import flash.geom.Point;

private typedef Gfx = flash.Vector<IGraphicsData>;
class ParticleFacet
{
	public var style(default, null):ParticleRenderStyle;
	public var path:String;
	
	private var mAnimation:Animation;
	
	private var mSprite:Sprite;
	private var mColorTransform:ColorTransform;
	
	public var ready(get, never):Bool;
	public var sleeping(get, set):Bool;
	public var cacheEnabled(get, set):Bool;
	
	public var width(get, never):Float;
	public var height(get, never):Float;
	public var blendMode(default, default):BlendMode;
	
	public function new(?path:String, ?blendMode:BlendMode)
	{
		this.path = "!!!!!!BAD PATH!!!!!!!!!"; // dummy path to ensure setStyle detects a change. Should never be visible.
		setStyle(path, blendMode);
	}	
	
	private inline function get_ready():Bool
	{
		if (style == null)
			return false;
		else if (mAnimation != null)
			return mAnimation.ready;
		else
			return true;
	}
	
	private inline function get_sleeping():Bool
	{
		if (mAnimation != null)
			return mAnimation.sleeping;
		else
			return false;
	}
	private inline function set_sleeping(val:Bool):Bool
	{
		if (mAnimation != null)
			return mAnimation.sleeping = val;
		else
			return val;
	}
	
	private inline function get_cacheEnabled():Bool
	{
		if (mAnimation != null)
			return mAnimation.cacheEnabled;
		else if (mSprite != null)
			return mSprite.cacheAsBitmap;
		else
			return false;
	}
	private inline function set_cacheEnabled (val:Bool):Bool
	{
		if (mAnimation != null)
			return mAnimation.cacheEnabled = val;
		else if (mSprite != null)
			return mSprite.cacheAsBitmap = val;
		else
			return val;
	}
	
	private inline function get_width():Float
	{
		if (mAnimation != null)
			return mAnimation.width;
		else if (mSprite != null)
			return mSprite.width;
		else
			return 0.5;
	}
	private inline function get_height():Float
	{
		if (mAnimation != null)
			return mAnimation.height;
		else if (mSprite != null)
			return mSprite.height;
		else
			return 0.5;
	}
	
	public inline function getAnimation():Animation
	{	return mAnimation;	 }
	public inline function getGraphics():flash.Vector<IGraphicsData>
	{	return mSprite.graphics.readGraphicsData();		 }
	
	public function setStyle(?path:String, ?blendMode:BlendMode):Void
	{
		this.blendMode = blendMode == null ? BlendMode.NORMAL : blendMode;
		if (path == this.path)
			return;
		this.path = path;
		//alpha = 1;
		style = null;
		if (mAnimation != null)
			mAnimation.dispose();
		mAnimation = null;
		mSprite = null;
		mColorTransform = null;
		if (path != null)
		{
			if (BitmapManager.query(path) != BitmapManagerQuery.NotFound)
				mAnimation = Animation.createFromPath(path, false);
			if (mAnimation == null)
				mAnimation = AnimationManager.request(path, false);
			if (mAnimation != null)
				style = Animation;
		}
		if (style == null && path != null)
		{
			// todo: hue/saturation/brightness/contrast for graphics details
			var gfx:Gfx = GraphicsManager.request(path);
			if (gfx != null)
			{
				mSprite = new Sprite();
				//mColorTransform = new ColorTransform();
				mSprite.graphics.drawGraphicsData(gfx);
				style = Graphics;
			}
		}
		if (style == null)
		{
			style = ParticleRenderStyle.Pixel;
		}
	}
	
	public inline function progress(time:Float):Void
	{
		if (mAnimation != null)
			mAnimation.progress(time);
	}
	
	public inline function draw(surface:BitmapData, target:Point, scale:Float, alpha:Float, instance:ParticleInstance):Void
	{
		if (style == null)
			return;
		switch(style)
		{
			case ParticleRenderStyle.Animation:
				mAnimation.blendMode = blendMode;
				mAnimation.scale = scale;
				mAnimation.red = instance.getParam(RED);
				mAnimation.green = instance.getParam(GREEN);
				mAnimation.blue = instance.getParam(BLUE);
				mAnimation.alpha = alpha;
				mAnimation.hue = instance.getParam(HUE);
				mAnimation.saturation = instance.getParam(SATURATION);
				mAnimation.brightness = instance.getParam(BRIGHTNESS);
				mAnimation.rotation = instance.getParam(ROTATION);
				mAnimation.draw(surface, target);
			case ParticleRenderStyle.Graphics:
				drawSprite(surface, target, scale, alpha, instance);
			case ParticleRenderStyle.Pixel:
				drawPixel(surface, Math.floor(target.x), Math.floor(target.y), blendMode, alpha, instance);
		}
	}
	
	private static var sDrawSprite:Sprite = new Sprite();
	private inline function drawSprite(surface:BitmapData, target:Point, scale:Float, alpha:Float, instance:ParticleInstance):Void
	{
		mSprite.x = target.x;
		mSprite.y = target.y;		
		mSprite.rotation = instance.getParam(ROTATION) * 360;
		mSprite.scaleX = mSprite.scaleY = scale;
		sDrawSprite.addChild(mSprite);
		//mColorTransform.alphaMultiplier = instance.alpha;
		sDrawSprite.filters = [new ColorMatrixFilter(instance.generateColorMatrix().matrix)];
		sDrawSprite.alpha = alpha;
		surface.draw(sDrawSprite, null, /*mColorTransform*/null, blendMode);
		sDrawSprite.removeChildren();
	}
	
	private inline function drawPixel(surface:BitmapData, x:Int, y:Int, blendMode:BlendMode, alpha:Float, instance:ParticleInstance):Void
	{
		if (x < 0 || x > surface.width || y < 0 || y > surface.height)
			return;
		else
		{
			var dest:Color = Color.createFromRaw(0xFF000000 | surface.getPixel(x, y));
			dest.blend(Color.createFromFloatRGB(instance.getParam(RED), instance.getParam(GREEN), instance.getParam(BLUE), alpha), blendMode, false, true);
			surface.setPixel(x, y, dest.raw);
			return;
		}
	}
	
	public inline function setFilters(filters:Array<BitmapFilter>):Void
	{
		if (mAnimation != null)
			mAnimation.setFilters(filters);
		else if (mSprite != null)
			mSprite.filters = filters.copy();
	}
	
	public function clone():ParticleFacet
	{
		var f:ParticleFacet = new ParticleFacet();
		f.mimic(this);
		return f;
	}
	
	public function mimic(target:ParticleFacet):Void
	{
		style = target.style;
		if (target.mSprite != null)
		{
			mSprite = new Sprite();
			mSprite.graphics.copyFrom(target.mSprite.graphics);
			mColorTransform = new ColorTransform();
		}
		else if (target.mAnimation != null && mAnimation != target.mAnimation)
		{
			mAnimation = target.mAnimation.clone();
			cacheEnabled = target.cacheEnabled;
			sleeping = target.sleeping;
		}
			
		path = target.path;
		blendMode = target.blendMode;
	}
	
	public function dispose():Void
	{
		if (mAnimation != null)
			mAnimation.dispose();
		mAnimation = null;
		mSprite = null;
		path = null;
	}
}