package com.problemmachine.particlesystem.event;
import flash.events.Event;

class ParticleSystemEvent extends Event
{
	public static inline var READY:String = "Ready (ParticleSystemEvent)";
	public static inline var UNREADY:String = "Unready (ParticleSystemEvent)";
	
	public static inline var CHANNEL_CONNECTED:String = "channelConnected (ParticleSystemEvent)";
	
	public static inline var UPDATE_START:String = "updateStart (ParticleSystemEvent)";
	public static inline var UPDATE_COMPLETE:String = "updateComplete (ParticleSystemEvent)";
	public static inline var DRAW_START:String = "drawStart (ParticleSystemEvent)";
	public static inline var DRAW_COMPLETE:String = "drawComplete (ParticleSystemEvent)";
	public static inline var PROGRESS_FAILED_BAD_CONFIG:String = "ProgressFailedBadConfig (ParticleSystemEvent)";
	
	public static inline var CONFIG_PREPARED:String = "configPrepared (ParticleSystemEvent)";
	public static inline var CONFIG_ACTIVATED:String = "configActivated (ParticleSystemEvent)";
	public static inline var CONFIG_ACTIVATE_FAILED:String = "configActivateFailed (ParticleSystemEvent)";
	public static inline var CONFIG_RETIRED:String = "configRetired (ParticleSystemEvent)";
	public static inline var CONFIG_READY:String = "configReady (ParticleSystemEvent)";
	public static inline var CONFIG_UNREADY:String = "configUnready (ParticleSystemEvent)";
	
	public static inline var TYPE_FILTER_CHANGED:String = "typeFilterChanged (ParticleSystemEvent)";
	public static inline var RESET_PARTICLES:String = "resetParticles (ParticleSystemEvent)";
	public static inline var PARTICLES_SHIFTED:String = "particlesShifted (ParticleSystemEvent)";
	
	public static inline var LAYERS_MODIFIED:String = "layersModified (ParticleSystemEvent)";
	
	public var configName(default, null):String;
	
	public function new(type:String, configName:String) 
	{
		super(type, false, false);
		this.configName = configName;
	}
	
	override public function toString():String
	{
		return ("ParticleSystemEvent-" + type + " (" + configName + ")");
	}
}