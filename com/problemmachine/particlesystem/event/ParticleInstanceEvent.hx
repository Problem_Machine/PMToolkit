package com.problemmachine.particlesystem.event;

import flash.events.Event;

class ParticleInstanceEvent extends Event
{
	public static inline var DIED:String = "Died (ParticleInstanceEvent)";
	public static inline var SPAWNED:String = "Spawned (ParticleInstanceEvent)";
	
	public var configName(default, null):String;
	public var typeName(default, null):String;
	public var typeCount(default, null):Int;

	public function new(type:String, configName:String, typeName:String, typeCount:Int) 
	{
		super(type, false, false);
		this.configName = configName;
		this.typeName = typeName;
		this.typeCount = typeCount;
	}
	
}