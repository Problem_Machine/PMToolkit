package com.problemmachine.particlesystem.event;

import flash.events.Event;

class ParticleTypeEvent extends Event
{
	public static inline var READY:String = "Ready (ParticleTypeEvent)";
	public static inline var UNREADY:String = "Unready (ParticleTypeEvent)";
	
	public var typeName(default, null):String;

	public function new(type:String, typeName:String) 
	{
		super(type, false, false);
		this.typeName = typeName;
	}
	
}