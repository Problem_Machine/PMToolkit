package com.problemmachine.particlesystem.event;

import flash.events.Event;

class ParticleConfigEvent extends Event
{
	public static inline var READY:String = "Ready (ParticleConfigEvent)";
	public static inline var UNREADY:String = "Unready (ParticleConfigEvent)";
	public static inline var CONFIG_RENAMED:String = "ConfigRenamed (ParticleConfigEvent)";
	
	public static inline var TYPE_ADDED:String = "TypeAdded (ParticleConfigEvent)";
	public static inline var TYPE_DELETED:String = "TypeDeleted (ParticleConfigEvent)";
	public static inline var TYPE_RENAMED:String = "TypeRenamed (ParticleConfigEvent)";
	
	public var configName(default, null):String;
	public var typeName(default, null):String;

	public function new(type:String, configName:String, typeName:String) 
	{
		super(type, false, false);
		this.configName = configName;
		this.typeName = typeName;
	}
	
}