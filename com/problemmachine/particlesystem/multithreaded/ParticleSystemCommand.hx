package com.problemmachine.particlesystem.multithreaded;

enum ParticleSystemCommand
{
	Progress(time:Float);
	Draw();
	
	PrepareConfiguration(configXML:String);
	RetireConfiguration(name:String);
	ActivateConfiguration(name:String);
	RenameConfiguration(oldName:String, newName:String);
	
	UpdateConfigParameters(configXML:String);

	SetTypeFilter(filter:Array<String>);
	ResetParticles;
	ShiftParticles(x:Float, y:Float);
	SpawnParticles(names:Array<String>, number:Int, positionOverrides:Array<Dynamic>, velocityOverrides:Array<Dynamic>);
	
	DeleteType(configName:String, typeName:String);
	AddType(configName:String, typeXML:String);
	RenameType(configName:String, oldTypeName:String, newTypeName:String);
	
	SetZoom(zoom:Float);
	SetCamera(x:Float, y:Float);
	SetAutoProgress(val:Bool);
	SetAutoDraw(val:Bool);
	SetLayerSplit(splits:Array<Float>);
	SetDrawRange(min:Float, max:Float);
	SetGlobalParameter(index:Int, value:Float);
}