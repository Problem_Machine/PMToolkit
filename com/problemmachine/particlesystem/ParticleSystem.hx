package com.problemmachine.particlesystem;

import com.problemmachine.particlesystem.event.ParticleConfigEvent;
import com.problemmachine.particlesystem.event.ParticleInstanceEvent;
import com.problemmachine.particlesystem.event.ParticleSystemEvent;
import com.problemmachine.particlesystem.ParticleInterface;
import com.problemmachine.particlesystem.internal.ParticleConfiguration;
import com.problemmachine.particlesystem.internal.ParticleInstance;
import com.problemmachine.particlesystem.internal.ParticleType;
import com.problemmachine.tools.math.IntMath;
import flash.Lib;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.PixelSnapping;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.TimerEvent;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.utils.Timer;
#if air
import flash.concurrent.Mutex;
import flash.errors.Error;
import flash.system.Worker;
import flash.utils.ByteArray;
import com.problemmachine.particlesystem.multithreaded.ParticleSystemCommand;
import com.problemmachine.util.multithreading.air.WorkerMessenger;
import com.problemmachine.util.multithreading.event.WorkerMessengerEvent;
#end
import haxe.CallStack;

@:allow(com.problemmachine.particlesystem.ParticleInterface)
class ParticleSystem extends Sprite
{
	#if air
	@:allow(com.problemmachine.particlesystem.ParticleInterface)
	private static inline var MESSAGE_LOCATION:String = "com.problemmachine.particlesystem.ParticleSystem:MessageLocation";
	@:allow(com.problemmachine.particlesystem.ParticleInterface)
	private static inline var PROGRESS_MUTEX:String = "com.problemmachine.particlesystem.ParticleSystem:ProgressMutex";
	@:allow(com.problemmachine.particlesystem.ParticleInterface)
	private static inline var DRAW_MUTEX:String = "com.problemmachine.particlesystem.ParticleSystem:DrawMutex";
	@:allow(com.problemmachine.particlesystem.ParticleInterface)
	private static inline var SYSTEM_EVENT:String = "com.problemmachine.particlesystem.ParticleSystem:SystemEvent";
	@:allow(com.problemmachine.particlesystem.ParticleInterface)
	private static inline var INSTANCE_EVENT:String = "com.problemmachine.particlesystem.ParticleSystem:InstanceEvent";
	
	public static inline var LAYER_BYTEARRAY:String = "com.problemmachine.particlesystem.ParticleSystem:LayerByteArray";
	public static inline var PARTICLE_SYSTEM_CREATED:String = "com.problemmachine.particlesystem.ParticleSystem:ParticleSystemCreated";
	public static inline var NUM_LAYERS:String = "com.problemmachine.particlesystem.ParticleSystem:NumLayers";
	
	private var mMessenger:WorkerMessenger;
	private var mLayerByteArrays:Array<ByteArray>;
	#end
	
	private static var system(default, null):ParticleSystem;
	private static var sGlobalParameters:Array<Float> = [0, 0, 0, 0, 0, 0, 0, 0];
	
	private var mPreparedConfigurations:Array<ParticleConfiguration>;
	
	private var mParticles:Array<ParticleInstance>;
	private var mDeadParticles:Array<ParticleInstance>;
	
	private var mSplits:Array<Float>;
	private var mLayerContainers:Array<Bitmap>;
	private var mLayers:Array<BitmapData>;
	
	private var mTypeFilter:Array<String>;
	
	private var mSpawnedMessages:Map<String, Int>;
	private var mDiedMessages:Map<String, Int>;
	
	private var mLastUpdateTime:Int;
	private var mTimer:Timer;
	
	private var configuration(default, set):ParticleConfiguration;
	private var numLayers(get, never):Int;
	private var targetFramerate(default, set):Float;
	
	private var mCamera:Point;
	private var mZoom:Float;
	private var mMinimumDrawDistance:Float;
	private var mMaximumDrawDistance:Float;
	private var mAutoProgress:Bool;
	private var mAutoDraw:Bool;
	
	private var mReady:Bool;
		
	private function new(?targetFramerate:Null<Float>) 
	{
		super();
		mParticles = [];
		mDeadParticles = [];
		
		mPreparedConfigurations = [];
		mTypeFilter = [];
		mSplits = [];
		mLayers = [new BitmapData(Lib.current.stage.stageWidth, Lib.current.stage.stageHeight, true, 0x00000000)];
		mLayerContainers = [new Bitmap(mLayers[0], PixelSnapping.ALWAYS, false)];
		addChild(mLayerContainers[0]);
		
		#if air
			mLayerByteArrays = [new ByteArray()];
			mLayerByteArrays[0].length = mLayers[0].width * mLayers[0].height * 4;
			mLayerByteArrays[0].shareable = true;
			Worker.current.setSharedProperty(LAYER_BYTEARRAY + Std.string(0), mLayerByteArrays[0]);
			Worker.current.setSharedProperty(NUM_LAYERS, 1);
			if (Worker.current.getSharedProperty(DRAW_MUTEX) == null)
				Worker.current.setSharedProperty(DRAW_MUTEX, new Mutex());
			if (Worker.current.getSharedProperty(PROGRESS_MUTEX) == null)
				Worker.current.setSharedProperty(PROGRESS_MUTEX, new Mutex());
				
			mMessenger = new WorkerMessenger(null, MESSAGE_LOCATION, ParticleInterface.MESSAGE_LOCATION);
			mMessenger.enableNotifications = true;
			mMessenger.addEventListener(WorkerMessengerEvent.RECEIVED_NEW_MESSAGE, processMessages);
		#end
		
		configuration = null;
		mCamera = new Point();
		mZoom = 1;
		mMinimumDrawDistance = ParticleInterface.MIN_PARTICLE_DISTANCE;
		mMaximumDrawDistance = ParticleInterface.MAX_PARTICLE_DISTANCE;	
		mAutoDraw = false;
		mAutoProgress = false;
		mReady = false;
		
		mLastUpdateTime = Lib.getTimer();
		if (targetFramerate == null)
			targetFramerate = Lib.current.stage.frameRate;
		mTimer = new Timer(100);
		mTimer.addEventListener(TimerEvent.TIMER, updateListener);
		mTimer.start();
		this.targetFramerate = targetFramerate;
	}
	
	public static function init():Void
	{
		#if air
			if (system == null && Worker.current.getSharedProperty(PARTICLE_SYSTEM_CREATED) == null)
				system = new ParticleSystem();
			else if (system == null)
				throw new Error("ERROR in ParticleSystem.init(): Tried to init particle system in two separate workers. Only use ParticleSystem in one worker at a time!");
			Worker.current.setSharedProperty(PARTICLE_SYSTEM_CREATED, true);
		#else
			if (system == null)
				system = new ParticleSystem();
		#end
	}
	
	private inline function get_numLayers():Int
	{
		return mLayers.length;
	}
	
	private inline function set_targetFramerate(val:Float):Float
	{
		if (val != targetFramerate)
		{
			val = targetFramerate;
			mTimer.delay = Math.floor(1000 / targetFramerate);
		}
		return val;
	}
	
	private function set_configuration(val:ParticleConfiguration):ParticleConfiguration
	{
		if (val != configuration)
		{
			if (configuration != null)
			{
				if (mPreparedConfigurations.indexOf(configuration) < 0)
				{
					#if air
						if (!Worker.current.isPrimordial)
							configuration.dispose();
					#end
				}
				configuration.removeEventListener(ParticleInstanceEvent.DIED, particleListener);
				configuration.removeEventListener(ParticleInstanceEvent.SPAWNED, particleListener);
			}
			configuration = val;
			if (configuration != null)
			{
				configuration.activate();
				configuration.addEventListener(ParticleInstanceEvent.DIED, particleListener);
				configuration.addEventListener(ParticleInstanceEvent.SPAWNED, particleListener);
			}
			else
				killParticles();
			if (configuration != null && configuration.ready) 
				readySystem(); 
			else 
				unreadySystem();
			mSpawnedMessages = new Map<String, Int>();
			mDiedMessages = new Map<String, Int>();
			mTypeFilter = [];
			resetParticles();
			relaySystemEvent(new ParticleSystemEvent(ParticleSystemEvent.CONFIG_ACTIVATED, val != null ? val.name : null));
		}
		return val;
	}
	
	private function updateListener(e:TimerEvent):Void
	{
		#if air
			processMessages();
		#end
		
		if (mAutoProgress)
		{
			var time:Float = (Lib.getTimer() - mLastUpdateTime) / 1000;
				mLastUpdateTime = Lib.getTimer();
			progress(time);
		}
		
		if (mAutoDraw)
			draw();
	}
	
	private function readySystem():Void
	{
		if (mReady != true)
			relaySystemEvent(new ParticleSystemEvent(ParticleSystemEvent.READY, configuration.name));
		mReady = true;
	}
	private function unreadySystem():Void
	{
		if (mReady == true)
			relaySystemEvent(new ParticleSystemEvent(ParticleSystemEvent.UNREADY, configuration != null ? configuration.name : null));
		mReady = false;
	}
	
	#if air
	private function progress(time:Float, processMessages:Bool = true):Void
	#else
	private function progress(time:Float):Void
	#end
	{
		#if air
			if (processMessages)
				this.processMessages();
		#end
		if (configuration == null || !configuration.ready)
		{
			relaySystemEvent(new ParticleSystemEvent(ParticleSystemEvent.PROGRESS_FAILED_BAD_CONFIG, configuration == null ? null : configuration.name));
			unreadySystem();
		}
		else if (!mReady)
		{
			if (configuration != null && configuration.ready)
				readySystem();
		}
		if (!mReady)
			return;
			
		#if air
			var mutex:Mutex = Worker.current.getSharedProperty(PROGRESS_MUTEX);
			mutex.lock();
		#end
		relaySystemEvent(new ParticleSystemEvent(ParticleSystemEvent.UPDATE_START, configuration.name));
		var allTypes:Bool = (mTypeFilter == null || mTypeFilter.length <= 0);
		
		var deadCount:Int = 0;
		var reorder:Bool = false;
		for (p in mParticles)
		{
			if (p.dead)
			{
				++deadCount;
				continue;
			}
			if (!allTypes && mTypeFilter.indexOf(p.mTypeClass.name) < 0)
				continue;
			
			var dist:Float = p.getParam(DISTANCE);
			p.progress(time);
			if (p.getParam(TYPE) != 0)
				p.assign(configuration.getTypeByNameHash(p.getParam(TYPE)));
			if (p.getParam(RESPAWN) != 0)
			{				
				if (p.getParam(RESPAWN) > 0)
					p.reset();
				else
				{
					killParticle(p);
					++deadCount;
					continue;
				}
			}
			if (!reorder && dist != p.getParam(DISTANCE))
				reorder = true;
		}
		if (deadCount > mParticles.length * 0.1)
			clean();
		if (reorder)
			ParticleInstance.sortByDistance(mParticles);
		#if air
			mutex.unlock();
		#end
		for (s in mSpawnedMessages.keys())
			relayInstanceEvent(new ParticleInstanceEvent(ParticleInstanceEvent.SPAWNED, configuration.name, s, mSpawnedMessages.get(s)));
		for (s in mDiedMessages.keys())
			relayInstanceEvent(new ParticleInstanceEvent(ParticleInstanceEvent.DIED, configuration.name, s, mSpawnedMessages.get(s)));
		mSpawnedMessages = new Map<String, Int>();
		mDiedMessages = new Map<String, Int>();
		
		relaySystemEvent(new ParticleSystemEvent(ParticleSystemEvent.UPDATE_COMPLETE, configuration.name));
	}
	
	#if air
	private function draw(processMessages:Bool = true):Void
	#else
	private function draw():Void
	#end
	{
		if (!mReady)
			return;
		#if air
			if (processMessages)
				this.processMessages();
		#end
		relaySystemEvent(new ParticleSystemEvent(ParticleSystemEvent.DRAW_START, configuration != null ? configuration.name : null));
		for (b in mLayers)
		{
			b.lock();
			b.fillRect(new Rectangle(0, 0, b.width, b.height), 0x00000000);
		}
		
		var i:Int = mSplits.length;
		var center:Point = new Point(Lib.current.stage.stageWidth * 0.5, Lib.current.stage.stageHeight * 0.5);
		var allTypes:Bool = (mTypeFilter == null || mTypeFilter.length <= 0);
		
		for (pi in mParticles)
			if (!pi.sleeping && (allTypes || mTypeFilter.indexOf(pi.mTypeClass.name) >= 0))
			{
				if (pi.getParam(DISTANCE) > mMaximumDrawDistance)
					continue;
				if (pi.getParam(DISTANCE) < mMinimumDrawDistance)
					break;
				if (i > 0 && pi.getParam(DISTANCE) <= mSplits[i - 1])
					--i;
				pi.drawParticle(mLayers[mLayers.length - 1 - i], mCamera, center, mZoom);
			}
		#if air
			var mutex:Mutex = cast Worker.current.getSharedProperty(DRAW_MUTEX);
			for (j in 0...mLayers.length)
			{
				mutex.lock();
				mLayerByteArrays[j].position = 0;
				mLayers[j].copyPixelsToByteArray(new Rectangle(0, 0, mLayers[j].width, mLayers[j].height), mLayerByteArrays[j]);
				mLayers[j].unlock();
				mutex.unlock();
			}
		#end
		relaySystemEvent(new ParticleSystemEvent(ParticleSystemEvent.DRAW_COMPLETE, configuration != null ? configuration.name : null));
	}
	
	private function setLayerSplit(?splits:Array<Float>):Void
	{
		if (splits == null)
			splits = [];
		if (mSplits.length == splits.length)
		{
			var equals:Bool = true;
			for (i in 0...mSplits.length)
				if (splits[i] != mSplits[i])
				{
					equals = false;
					break;
				}
			if (equals)
				return;
		}
		mSplits = splits;
		#if air
			var mutex:Mutex = cast Worker.current.getSharedProperty(DRAW_MUTEX);
			mutex.lock();
			while (mLayers.length < mSplits.length + 1)
			{
				mLayers.push(new BitmapData(Lib.current.stage.stageWidth, Lib.current.stage.stageHeight, true, 0x00000000));
				mLayerContainers.push(new Bitmap(mLayers[mLayers.length - 1], PixelSnapping.ALWAYS, false));
				addChild(mLayerContainers[mLayerContainers.length - 1]);
				mLayerByteArrays.push(new ByteArray());
				mLayerByteArrays[mLayers.length - 1].length = mLayers[0].width * mLayers[0].height * 4;
				mLayerByteArrays[mLayers.length - 1].shareable = true;
				Worker.current.setSharedProperty(LAYER_BYTEARRAY + Std.string(mLayers.length - 1), mLayerByteArrays[mLayers.length - 1]);
			}
			while (mLayers.length > mSplits.length + 1)
			{
				mLayers.pop().dispose();
				mLayerContainers[mLayers.length].parent.removeChild(mLayerContainers.pop());
				Worker.current.setSharedProperty(LAYER_BYTEARRAY + Std.string(mLayers.length), null);
				mLayerByteArrays[mLayers.length].shareable = false;
				mLayerByteArrays.pop();
			}
			Worker.current.setSharedProperty(NUM_LAYERS, numLayers);
			mutex.unlock();
		#else
			while (mLayers.length < mSplits.length + 1)
			{
				mLayers.push(new BitmapData(Lib.current.stage.stageWidth, Lib.current.stage.stageHeight, true, 0x00000000));
				mLayerContainers.push(new Bitmap(mLayers[mLayers.length - 1], PixelSnapping.ALWAYS, false));
				addChild(mLayerContainers[mLayerContainers.length - 1]);
			}
			while (mLayers.length > mSplits.length + 1)
			{
				mLayers.pop().dispose();
				mLayerContainers[mLayers.length].parent.removeChild(mLayerContainers.pop());
			}
		#end
		relaySystemEvent(new ParticleSystemEvent(ParticleSystemEvent.LAYERS_MODIFIED, null));
	}
	private function getLayerSplit():Array<Float>
	{
		return mSplits.copy();
	}
	
	private function shiftParticles(x:Float, y:Float):Void
	{
		if (configuration == null)
			return;
		var allTypes:Bool = (mTypeFilter == null || mTypeFilter.length <= 0);
		for (pi in mParticles)
			if (allTypes || mTypeFilter.indexOf(pi.mTypeClass.name) >= 0)
			{
				pi.setParam(X, pi.getParam(X) + x);
				pi.setParam(Y, pi.getParam(Y) + y);
			}
		relaySystemEvent(new ParticleSystemEvent(ParticleSystemEvent.PARTICLES_SHIFTED, configuration.name));
	}
	
	@:access(com.problemmachine.particlesystem.internal.ParticleInstance)
	private function getParticle(type:ParticleType):ParticleInstance
	{
		for (p in mDeadParticles)
			if (p.mTypeClass == type)
			{
				mDeadParticles.remove(p);
				p.revive();
				return p;
			}
		
		if (mDeadParticles.length > 0)
		{
			var p:ParticleInstance = mDeadParticles.pop();
			p.assign(type);
			return p;
		}
		else
		{
			var p:ParticleInstance = new ParticleInstance(type);
			mParticles.push(p);
			return p;
		}
	}
	
	private inline function killParticle(p:ParticleInstance):Void
	{
		if (!p.dead)
			mDeadParticles.push(p);
		p.dead = true;
	}
	
	private function clean():Void
	{
		var length:Int = mParticles.length;
		for (i in 0...length)
			if (mParticles[length - 1 - i].dead)
			{
				mParticles[length - 1 - i].dispose();
				mParticles.splice(length - 1 - i, 1);
			}
		mDeadParticles = [];
	}
	
	private function prepareConfiguration(config:ParticleConfiguration):Void
	{
		var exists:Bool = false;
		for (i in 0...mPreparedConfigurations.length)
			if (mPreparedConfigurations[i].name == config.name)
			{
				exists = true;
				updateConfigurationParameters(config);
				break;
			}
		if (!exists)
		{
			mPreparedConfigurations.push(config);
			config.addEventListener(ParticleConfigEvent.READY, configListener);
			config.addEventListener(ParticleConfigEvent.UNREADY, configListener);
		}
		relaySystemEvent(new ParticleSystemEvent(ParticleSystemEvent.CONFIG_PREPARED, config.name));
		if (config.ready)
		{
			if (configuration != null && config.name == configuration.name)
				readySystem();
		}
		else if (configuration != null && config.name == configuration.name)
			unreadySystem();
	}
	
	private function retireConfiguration(name:String):Void
	{
		for (c in mPreparedConfigurations)
			if (c.name == name)
			{
				c.removeEventListener(ParticleConfigEvent.READY, configListener);
				c.removeEventListener(ParticleConfigEvent.UNREADY, configListener);
				if (configuration == c)
					configuration = null;
				#if air
					if (!Worker.current.isPrimordial)
						c.dispose();
					else
				#else
						c.deactivate();
				#end
				
				mPreparedConfigurations.remove(c);
				relaySystemEvent(new ParticleSystemEvent(ParticleSystemEvent.CONFIG_RETIRED, name));
				return;
			}
	}
	
	private function activateConfiguration(?name:String):Void
	{
		var config:ParticleConfiguration = null;
		for (c in mPreparedConfigurations)
			if (c.name == name)
			{
				config = c;
				break;
			}
		if (config != configuration)
		{
			setTypeFilter();
			if (config != null || name == null)
				configuration = config;
			else
				relaySystemEvent(new ParticleSystemEvent(ParticleSystemEvent.CONFIG_ACTIVATE_FAILED, name));
			if (configuration != null)
			{
				if (configuration.ready)
					readySystem();
				else
					unreadySystem();
			}
		}
	}
	
	@:access(com.problemmachine.particlesystem.internal.ParticleType.mimic)
	@:access(com.problemmachine.particlesystem.internal.ParticleType.clone)
	private function updateConfigurationParameters(config:ParticleConfiguration):Void
	{
		var old:ParticleConfiguration = getConfig(config.name);
		if (old == null)
			return;
		for (i in 0...config.mParticleTypes.length)
			if (old.mParticleTypes[i] != null)
				old.mParticleTypes[i].mimic(config.mParticleTypes[i]);
			else
				old.mParticleTypes[i] = (config.mParticleTypes[i].clone());
		while (old.mParticleTypes.length > config.mParticleTypes.length)
			old.mParticleTypes.pop().dispose();
			
	}
	
	private function setTypeFilter(?filter:Array<String>):Void
	{
		if (filter == null)
			filter = [];
		mTypeFilter = filter;
		relaySystemEvent(new ParticleSystemEvent(ParticleSystemEvent.TYPE_FILTER_CHANGED, configuration != null ? configuration.name : null));
	}
	
	private function resetParticles():Void
	{
		if (configuration == null)
			return;
		var allTypes:Bool = (mTypeFilter == null || mTypeFilter.length <= 0);
		for (p in mParticles)
			if (allTypes || mTypeFilter.indexOf(p.mTypeClass.name) >= 0)
				killParticle(p);
		if (allTypes)
			mTypeFilter = configuration.getTypeNames();
		clean();
		if (configuration == null)
		{
			relaySystemEvent(new ParticleSystemEvent(ParticleSystemEvent.RESET_PARTICLES, null));
			return;
		}
		for (s in mTypeFilter)
		{
			var t:ParticleType = configuration.getTypeByName(s);
			for (i in 0...t.initialCount)
			{
				var p:ParticleInstance = getParticle(t);
				p.reset();
				if (t.isBasicDetail())
					p.preCache();
			}
		}
		if (allTypes)
			mTypeFilter = [];
		#if air
			progress(configuration.preAdvanceTime, false);
		#else
			progress(configuration.preAdvanceTime);
		#end
		ParticleInstance.sortByDistance(mParticles);
		relaySystemEvent(new ParticleSystemEvent(ParticleSystemEvent.RESET_PARTICLES, configuration.name));
	}
	
	private function killParticles():Void
	{
		var allTypes:Bool = (mTypeFilter == null || mTypeFilter.length <= 0);
		for (p in mParticles)
			if (allTypes || mTypeFilter.indexOf(p.mTypeClass.name) >= 0)
				killParticle(p);
	}
	
	private function spawnParticles(names:Array<String>, number:Int, ?positionOverrides:Array<Point>, ?velocityOverrides:Array<Point>):Void
	{
		names = names.copy();
		for (i in 0...number)
		{
			var type:ParticleType = configuration.getTypeByName(names[Std.random(names.length)]);
			if (type == null)
				continue;
			var p:ParticleInstance = getParticle(type);
			p.reset();
			if (positionOverrides != null)
			{
				p.setParam(X, positionOverrides[IntMath.min(i, positionOverrides.length - 1)].x);
				p.setParam(Y, positionOverrides[IntMath.min(i, positionOverrides.length - 1)].y);
			}
			if (velocityOverrides != null)
			{
				p.setParam(X_VELOCITY, velocityOverrides[IntMath.min(i, velocityOverrides.length - 1)].x);
				p.setParam(Y_VELOCITY, velocityOverrides[IntMath.min(i, velocityOverrides.length - 1)].y);
			}
		}
	}
	
	#if air
	private function processMessages(?e:WorkerMessengerEvent):Void
	{
		while (mMessenger.numMessages > 0)
		{
			var obj:Dynamic = mMessenger.receive();
			var com:ParticleSystemCommand = Type.createEnum(ParticleSystemCommand, obj.tag, obj.params);
			switch(com)
			{
				case ParticleSystemCommand.Progress(time):
					progress(time, false);
					
				case ParticleSystemCommand.Draw:
					draw(false);
				
				case ParticleSystemCommand.PrepareConfiguration(configXML):
					prepareConfiguration(ParticleConfiguration.fromXML(Xml.parse(configXML).firstElement(), false));
					
				case ParticleSystemCommand.RetireConfiguration(name):
					retireConfiguration(name);
					
				case ParticleSystemCommand.ActivateConfiguration(name):
					activateConfiguration(name);
					
				case ParticleSystemCommand.UpdateConfigParameters(configXML):
					updateConfigurationParameters(ParticleConfiguration.fromXML(Xml.parse(configXML).firstElement(), false));
				
				case ParticleSystemCommand.SetTypeFilter(filter):
					setTypeFilter(filter);
					
				case ParticleSystemCommand.ResetParticles:
					resetParticles();
					
				case ParticleSystemCommand.ShiftParticles(x, y):
					shiftParticles(x, y);
					
				case ParticleSystemCommand.SpawnParticles(names, number, positionOverrides, velocityOverrides):
					// convert generic objects into point objects
					if (positionOverrides != null)
						for (i in 0...positionOverrides.length)
							positionOverrides[i] = new Point(positionOverrides[i].x, positionOverrides[i].y);
					if (velocityOverrides != null)
						for (i in 0...velocityOverrides.length)
							velocityOverrides[i] = new Point(velocityOverrides[i].x, velocityOverrides[i].y);
					spawnParticles(names, number, cast positionOverrides, cast velocityOverrides);
				
				case ParticleSystemCommand.SetDrawRange(min, max):
					mMinimumDrawDistance = min;
					mMaximumDrawDistance = max;
				case ParticleSystemCommand.SetGlobalParameter(index, value):
					sGlobalParameters[index] = value;
				case ParticleSystemCommand.SetAutoDraw(val):
					mAutoDraw = val;
				case ParticleSystemCommand.SetAutoProgress(val):
					mAutoProgress = val;
				case ParticleSystemCommand.SetZoom(zoom):
					mZoom = zoom;
				case ParticleSystemCommand.SetCamera(x, y):
					mCamera.x = x;
					mCamera.y = y;
				case ParticleSystemCommand.SetLayerSplit(splits):
					setLayerSplit(splits);
				
				case ParticleSystemCommand.AddType(configName, typeXML):
					addParticleTypeToConfig(configName, ParticleType.createFromXML(Xml.parse(typeXML).firstElement(), false));
					
				case ParticleSystemCommand.DeleteType(configName, typeName):
					deleteParticleType(configName, typeName, true);
					
				case ParticleSystemCommand.RenameType(configName, oldTypeName, newTypeName):
					renameParticleType(configName, oldTypeName, newTypeName);
					
				case ParticleSystemCommand.RenameConfiguration(oldName, newName):
					renameConfiguration(oldName, newName);
			}
		}
	}
	#end
	
	private function addParticleTypeToConfig(configName:String, type:ParticleType):Void
	{
		var target:ParticleConfiguration = getConfig(configName);
		type.sleeping = (target != configuration);
		target.addRawType(type);
	}
	
	private function deleteParticleType(configName:String, typeName:String, deleteFromConfig:Bool = true):Void
	{
		var target:ParticleConfiguration = getConfig(configName);
		for (p in mParticles)
			if (p.mTypeClass.name == typeName)
				p.dead = true;
		if (deleteFromConfig)
			target.deleteType(target.getTypeByName(typeName));
	}
	
	private function renameParticleType(configName:String, oldTypeName:String, newTypeName:String):Void
	{
		var target:ParticleConfiguration = getConfig(configName);
		target.setTypeName(target.getTypeByName(oldTypeName), newTypeName);
	}
	
	private function renameConfiguration(oldConfigName:String, newConfigName:String):Void
	{
		getConfig(oldConfigName).name = newConfigName;
	}
	
	private function getConfig(configName:String):ParticleConfiguration
	{
		for (c in mPreparedConfigurations)
			if (c.name == configName)
				return c;
		return null;
	}
	
	private function configListener(e:ParticleConfigEvent):Void
	{
		var c:ParticleConfiguration = e.target;
		if (e.type == ParticleConfigEvent.READY)
		{
			relaySystemEvent(new ParticleSystemEvent(ParticleSystemEvent.CONFIG_READY, c.name));
			if (c == configuration)
				readySystem();
		}
		else if (e.type == ParticleConfigEvent.UNREADY)
		{
			relaySystemEvent(new ParticleSystemEvent(ParticleSystemEvent.CONFIG_UNREADY, c.name));
			if (c == configuration)
				unreadySystem();
		}
	}
	
	private function particleListener(e:ParticleInstanceEvent):Void
	{
		if (e.type == ParticleInstanceEvent.SPAWNED)
			mSpawnedMessages.set(e.typeName, mSpawnedMessages.get(e.typeName) + 1);
		else if (e.type == ParticleInstanceEvent.DIED)
			mDiedMessages.set(e.typeName, mDiedMessages.get(e.typeName) + 1);
	}
	
	private function relaySystemEvent(event:ParticleSystemEvent):Void
	{
		#if air
			mMessenger.send({event:SYSTEM_EVENT, type:event.type, configName:event.configName, typeName:null, typeCount:null});
		#else
			ParticleInterface.dispatchSystemEvent(event);
		#end
		dispatchEvent(event);
	}
	
	@:access(com.problemmachine.particlesystem.ParticleInterface.sDispatcher)
	private function relayInstanceEvent(event:ParticleInstanceEvent):Void
	{
		#if air
			mMessenger.send({event:INSTANCE_EVENT, type:event.type, configName:event.configName, typeName:event.typeName, typeCount:event.typeCount});
		#else
			ParticleInterface.sDispatcher.dispatchEvent(event);
		#end
		dispatchEvent(event);
	}
}