package com.problemmachine.particlesystem;

@:enum abstract ParticleParameter(Null<Int>) 
{
	public var TYPE				= 0;
	public var LIFE 			= 1;
	public var RED 				= 2;
	public var GREEN	 		= 3;
	public var BLUE 			= 4;
	public var ALPHA 			= 5;
	public var HUE 				= 6;
	public var SATURATION		= 7;
	public var BRIGHTNESS		= 8;
	public var FACET_BLEND 		= 9;
	public var ANIMATION_SPEED 	= 10;
	public var ANIMATION_OFFSET = 11;
	public var RESPAWN 			= 12;
	public var X 				= 13;
	public var Y 				= 14;
	public var DISTANCE 		= 15;
	public var SCALE 			= 16;
	public var X_VELOCITY 		= 17;
	public var Y_VELOCITY 		= 18;
	public var ROTATION 		= 19;
	public var ROTATION_SPEED 	= 20;
	public var COLLISION 		= 21;
	public var AUX_0 			= 22;
	public var AUX_1 			= 23;
	public var AUX_2 			= 24;
	public var AUX_3 			= 25;
	public var AUX_4 			= 26;
	public var AUX_5 			= 27;
	public var AUX_6 			= 28;
	public var AUX_7 			= 29;
	
	public var NUMBER_OF_PARAMETERS = 30;
	
	public var WORLD_TIME		= -1;
	public var GLOBAL_0 		= -2;
	public var GLOBAL_1 		= -3;
	public var GLOBAL_2 		= -4;
	public var GLOBAL_3 		= -5;
	public var GLOBAL_4 		= -6;
	public var GLOBAL_5 		= -7;
	public var GLOBAL_6 		= -8;
	public var GLOBAL_7 		= -9;
	
	@:from private static function fromString (val:String):ParticleParameter 
	{		
		return switch (val) 
		{
			case "type": TYPE;
			case "life": LIFE;
			case "red": RED;
			case "green": GREEN;
			case "blue": BLUE;
			case "alpha": ALPHA;
			case "hue": HUE;
			case "saturation": SATURATION;
			case "brightness": BRIGHTNESS;
			case "facetBlend": FACET_BLEND;
			case "animationSpeed": ANIMATION_SPEED;
			case "animationOffset": ANIMATION_OFFSET;
			case "respawn": RESPAWN;
			case "x": X;
			case "y": Y;
			case "distance": DISTANCE;
			case "scale": SCALE;
			case "xVelocity": X_VELOCITY;
			case "yVelocity": Y_VELOCITY;
			case "rotation": ROTATION;
			case "rotationSpeed": ROTATION_SPEED;
			case "collision": COLLISION;
			case "aux0": AUX_0;
			case "aux1": AUX_1;
			case "aux2": AUX_2;
			case "aux3": AUX_3;
			case "aux4": AUX_4;
			case "aux5": AUX_5;
			case "aux6": AUX_6;
			case "aux7": AUX_7;
			
			case "worldTime": WORLD_TIME;
			case "global0": GLOBAL_0;
			case "global1": GLOBAL_1;
			case "global2": GLOBAL_2;
			case "global3": GLOBAL_3;
			case "global4": GLOBAL_4;
			case "global5": GLOBAL_5;
			case "global6": GLOBAL_6;
			case "global7": GLOBAL_7;
			
			default: null;
		}
	}
	
	@:to private static function toString (val:Int):String 
	{		
		return switch (val) 
		{
			case TYPE: "type";
			case LIFE: "life";
			case RED: "red";
			case GREEN: "green";
			case BLUE: "blue";
			case ALPHA: "alpha";
			case HUE: "hue";
			case SATURATION: "saturation"; 
			case BRIGHTNESS: "brightness";
			case FACET_BLEND: "facetBlend";
			case ANIMATION_SPEED: "animationSpeed";
			case ANIMATION_OFFSET: "animationOffset";
			case RESPAWN: "respawn";
			case X: "x"; 
			case Y: "y";
			case DISTANCE: "distance";
			case SCALE: "scale";
			case X_VELOCITY: "xVelocity"; 
			case Y_VELOCITY: "yVelocity";
			case ROTATION: "rotation";
			case ROTATION_SPEED: "rotationSpeed";
			case COLLISION: "collision";
			case AUX_0: "aux0"; 
			case AUX_1: "aux1"; 
			case AUX_2: "aux2"; 
			case AUX_3: "aux3"; 
			case AUX_4: "aux4"; 
			case AUX_5: "aux5"; 
			case AUX_6: "aux6"; 
			case AUX_7: "aux7";
			
			case WORLD_TIME: "worldTime";
			case GLOBAL_0: "global0"; 
			case GLOBAL_1: "global1"; 
			case GLOBAL_2: "global2"; 
			case GLOBAL_3: "global3"; 
			case GLOBAL_4: "global4"; 
			case GLOBAL_5: "global5"; 
			case GLOBAL_6: "global6"; 
			case GLOBAL_7: "global7";
			default: null;
		}
	}
}