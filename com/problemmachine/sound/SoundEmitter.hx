package com.problemmachine.sound;
import flash.events.Event;
import flash.geom.Point;
import flash.media.Sound;
import flash.media.SoundChannel;
import flash.media.SoundTransform;

class SoundEmitter
{
	static private var sSoundEmitters:Array<SoundEmitter> = new Array<SoundEmitter>();
	
	static private var sMaxPan:Float = 0.9;
	static private var sMaxPanDist:Float = 1200;
	static private var sMaxDistance:Float = 2000;
	static private var sFalloffDistance:Float = 250;
	
	private var mTransform:SoundTransform;
	private var mActiveSounds:Array<SoundData>;
	private var mSounds:Array<String>;
	private var mSoundCompletionListeners:Map<String, Array<Event->Void>>;
	
	
	public var enablePositionalPanning(default, default):Bool;
	public var enablePositionalVolume(default, default):Bool;
	
	public var ready(get, null):Bool;	
	public var position(default, null):Point;
	public var level(get, never):Float;
	
	public function new(?position:Point, enablePositionalPanning:Bool = true, enablePositionalVolume = true) 
	{
		sSoundEmitters.push(this);
		
		if (position != null)
			this.position = position;
		else
			this.position = new Point();
			
		mTransform = new SoundTransform(SoundManager.volume, 0);
		mSounds = new Array<String>();
		mActiveSounds = new Array<SoundData>();
		mSoundCompletionListeners = new Map<String, Array<Event->Void>>();
		this.enablePositionalPanning = enablePositionalPanning;
		this.enablePositionalVolume = enablePositionalVolume;
		ready = false;
	}
	
	@:allow(com.problemmachine.sound.SoundManager)
	static private inline function updateVolume(newVolume:Float):Void
	{
		for (se in sSoundEmitters)
			for (sd in se.mActiveSounds)
				sd.channel.soundTransform.volume = 
					(sd.channel.soundTransform.volume / SoundManager.volume) * newVolume;
	}
	
	static public function updateAll(listenerLocation:Point):Void
	{
		for (se in sSoundEmitters)
			se.update(listenerLocation);
	}
	
	public function update(listenerLocation:Point):Void
	{
		var dist:Float = 0;
		if (enablePositionalVolume)
		{
			dist = listenerLocation.subtract(position).length;
			dist = (dist - sFalloffDistance) / (sMaxDistance - sFalloffDistance);
			dist = Math.max(0, Math.min(1, dist));
		}
		mTransform.volume = SoundManager.volume * (1 - dist);
		var pan:Float = 0;
		if (enablePositionalPanning)
		{
			pan = ((listenerLocation.x - position.x) / sMaxPanDist) * sMaxPan;
			pan = Math.max( -sMaxPan, Math.min(pan, sMaxPan));
		}
		mTransform.pan = pan;
	}
	
	public function getSounds():Array<String>
	{
		return mSounds.copy();
	}
	
	public function loadSound(fileName:String):Void
	{
		for (s in mSounds)
			if (s == fileName)
				return;
		ready = false;
		mSounds.push(fileName);
		SoundManager.lockSound(fileName, this);
	}
	
	public function unloadSound(fileName:String):Void
	{
		for (i in 0...mSounds.length)
			if (mSounds[i] == fileName)
			{
				SoundManager.unlockSound(fileName, this);
				mSounds.splice(i, 1);
			}
	}
	
	public function playSound(fileName:String, loop:Bool = false, singleInstance:Bool = false):Void
	{
		var s:Sound = SoundManager.getSound(fileName);
		if (singleInstance)
			for (sd in mActiveSounds)
				if (sd.path == fileName)
					return;
		var sd:SoundData = new SoundData(s, s.play(0, loop ? 0xFFFFFF:0, mTransform), fileName);
		sd.channel.addEventListener(Event.SOUND_COMPLETE, soundCompleteListener);
		mActiveSounds.push(sd);
	}
	
	public function getAllPlayingSounds():Array<String>
	{
		var sounds:Array<String> = new Array<String>();
		for (sc in mActiveSounds)
		{
			var dupe:Bool = false;
			for (s in sounds)
				if (sc.path == s)
				{
					dupe = true;
					break;
				}
			if (!dupe)
				sounds.push(sc.path);
		}
		return sounds;
	}
	
	public inline function getSoundIsReady(fileName:String):Bool
	{		
		return SoundManager.getSoundIsReady(fileName);
	}
	
	public inline function getSoundPosition(filename:String):Float
	{
		var index:Int = -1;
		for (i in 0...mActiveSounds.length )
			if (mActiveSounds[i].path == filename)
				index = i;
		if (index < 0)
			return getSoundLength(filename);
		else
			return mActiveSounds[index].channel.position;
	}
	
	public inline function getSoundLength(filename:String):Float
	{
		return SoundManager.getSound(filename).length;
	}
	
	public function startListeningForSoundCompletion(filename:String, listener:Event->Void):Void
	{
		if (!mSoundCompletionListeners.exists(filename))
			mSoundCompletionListeners.set(filename, [listener]);
		else
			mSoundCompletionListeners.get(filename).push(listener);
	}
	
	public function stopListeningForSoundCompletion(filename:String, listener:Event->Void):Void
	{
		var arr:Array<Event->Void> = mSoundCompletionListeners.get(filename);
		if (arr != null)
		{
			arr.remove(listener);
			if (arr.length == 0)
				mSoundCompletionListeners.remove(filename);
		}
	}
	
	private function soundCompleteListener(e:Event):Void
	{
		var c:SoundChannel = cast(e.target, SoundChannel);
		c.removeEventListener(Event.SOUND_COMPLETE, soundCompleteListener);
		for (i in 0...mActiveSounds.length)
			if (mActiveSounds[i].channel == c)
			{
				var arr:Array<Event->Void> = mSoundCompletionListeners.get(mActiveSounds[i].path);
				if (arr != null)
					for (l in arr)
						l(e);
				mActiveSounds.splice(i, 1);
				return;
			}			
	}
	
	public function stopSound(fileName:String):Void
	{
		var remove:Array<Int> = new Array<Int>();
		for (i in 0...mActiveSounds.length)
			if (mActiveSounds[i].path == fileName)
			{
				mActiveSounds[i].channel.stop();
				remove.push(i);
			}
		remove.sort(function(a:Int, b:Int) {	return b - a;	} );
		for (i in remove)
			mActiveSounds.splice(i, 1);
	}
	
	public function silence():Void
	{
		var s:SoundData;
		while ((s = mActiveSounds.pop()) != null)
			s.channel.stop();
	}
	
	public function dispose():Void
	{
		silence();
		for (s in mSounds)
			SoundManager.unlockSound(s, this);
		mSounds = null;
		for (i in 0...sSoundEmitters.length)
			if (sSoundEmitters[i] == this)
			{
				sSoundEmitters.splice(i, 1);
				return;
			}
	}
	
	public function clone():SoundEmitter
	{
		var se:SoundEmitter = new SoundEmitter(position.clone());
		se.enablePositionalPanning = enablePositionalPanning;
		se.enablePositionalVolume = enablePositionalVolume;
		se.mSounds = mSounds.copy();
		se.mTransform.pan = mTransform.pan;
		se.mTransform.volume = mTransform.volume;
		return se;
	}
	
	private function get_ready():Bool
	{
		if (ready)
			return true;
		for (s in mSounds)
			if (!SoundManager.getSoundIsReady(s))
				return false;
		return (ready = true);
	}
	
	private function get_level():Float
	{
		var l:Float = 0;
		for (s in mActiveSounds)
			l = Math.max(l, Math.max(s.channel.leftPeak, s.channel.rightPeak));
		return l;
	}
}

private class SoundData
{
	public var sound(default, null):Sound;
	public var channel(default, null):SoundChannel;
	public var path(default, null):String;
	
	public function new(sound:Sound, channel:SoundChannel, path:String)
	{
		this.sound = sound;
		this.channel = channel;
		this.path = path;
	}
}