package com.problemmachine.sound;

import com.problemmachine.util.lock.Lock.Lock;
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.media.Sound;
import flash.media.SoundChannel;
import flash.media.SoundTransform;
import flash.net.URLRequest;

class SoundManager 
{
	private static var sMaximumBytes:Int = 60 * 1024 * 1024;
	private static var sCurrentBytes:Int = 0;
	
	private static var sVolume:Float = 1;
	public static var volume(get, set):Float;
	public static var mute(default, default):Bool;
	
	private static var sSoundBank:Map<String, Sound> = new Map<String, Sound>();
	private static var sSoundLocks:Map<String, Lock<SoundEmitter>> = new Map<String, Lock<SoundEmitter>>();
	private static var sSoundListeners:Map<Sound, Dynamic->Void> = new Map<Sound, Dynamic->Void>();
	
	private function new()
	{	
	}
	
	@:allow(com.problemmachine.sound.SoundEmitter)
	static private function lockSound(fileName:String, source:SoundEmitter, ?loadListener:Dynamic->Void):Void
	{
		if (!sSoundBank.exists(fileName))
		{
			var s:Sound = new Sound(new URLRequest(fileName));
			sSoundBank.set(fileName, s);
			if (loadListener != null)
			{
				sSoundListeners.set(s, loadListener);
				s.addEventListener(Event.COMPLETE, finishLoadListener);
			}
			s.addEventListener(IOErrorEvent.IO_ERROR, errorListener);
		}
		else if (loadListener != null)
			loadListener(null);
		if (!sSoundLocks.exists(fileName))
			sSoundLocks.set(fileName, new Lock<SoundEmitter>());
		sSoundLocks.get(fileName).lock(source);
	}
	
	static private function finishLoadListener(e:Event):Void
	{
		var s:Sound = cast(e.target, Sound);
		s.removeEventListener(Event.COMPLETE, finishLoadListener);
		s.removeEventListener(IOErrorEvent.IO_ERROR, errorListener);
		sCurrentBytes += s.bytesTotal;
		if (sSoundListeners.exists(s))
		{
			var f:Dynamic->Void = sSoundListeners.get(s);
			f(e);
			sSoundListeners.remove(s);
		}
		if (sCurrentBytes >= sMaximumBytes)
			freeSounds();
	}
	static private function errorListener(e:Event):Void
	{
		var s:Sound = cast(e.target, Sound);
		s.removeEventListener(Event.COMPLETE, finishLoadListener);
		s.removeEventListener(IOErrorEvent.IO_ERROR, errorListener);
		trace("IO ERROR: Could not load sound \"" + s.url + "\"");
	}
	
	@:allow(com.problemmachine.sound.SoundEmitter)
	static private function unlockSound(fileName:String, source:SoundEmitter):Void
	{
		if (sSoundLocks.exists(fileName))
			sSoundLocks.get(fileName).unlock(source);
	}
	
	@:allow(com.problemmachine.sound.SoundEmitter)
	static private function getSound(fileName:String):Sound
	{
		if (sSoundBank.exists(fileName))
		{
			return sSoundBank.get(fileName);
		}
		else 
		{
			trace ("WARNING: Sound " + fileName + " not found or not ready.");
			return null;
		}
	}
	
	static private function freeSounds():Void
	{
		var remove:Array<String> = new Array<String>();
		for (key in sSoundBank.keys())
			if (!sSoundLocks.exists(key))
				if (!sSoundLocks.get(key).isLocked)
					remove.push(key);
		for (key in remove)
		{
			sCurrentBytes -= sSoundBank.get(key).bytesTotal;
			sSoundBank.remove(key);
		}
		if (sCurrentBytes > sMaximumBytes)
			trace("WARNING: Maximum size of sound bank exceeded: " + sCurrentBytes + "/" + sMaximumBytes + " bytes");
	}
	
	@:allow(com.problemmachine.sound.SoundEmitter)
	static private function getSoundIsReady(fileName:String):Bool
	{
		if (sSoundBank.exists(fileName))
		{
			var s:Sound = sSoundBank.get(fileName);
			if (cast(s.bytesLoaded, Int) == s.bytesTotal)
				return true;
		}
		return false;
	}
	
	static private function get_volume():Float
	{
		return (mute ? 0 : sVolume);
	}
	
	static private function set_volume(val:Float):Float
	{
		val = Math.max(0, Math.min(1, val));
		if (val != volume)
			SoundEmitter.updateVolume(val);
		return sVolume = val;
	}
}