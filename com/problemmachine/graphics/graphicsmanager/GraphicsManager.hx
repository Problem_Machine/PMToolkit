package com.problemmachine.graphics.graphicsmanager;

import com.problemmachine.tools.file.FileTools;
import com.problemmachine.tools.file.FileToolsEvent;
import com.problemmachine.tools.graphics.GraphicsTools;
import flash.display.IGraphicsData;
import flash.errors.Error;
import flash.utils.ByteArray;
import flash.Vector;
#if air
import flash.filesystem.File;
#end

typedef Gfx = flash.Vector<IGraphicsData>;
class GraphicsManager 
{
	public static var rootDirectory(default, set):String = FileTools.rootDirectory;
	
	private static var sGraphics:Map<String, Gfx> = new Map<String, Gfx>();
	
	private static function set_rootDirectory(val:String):String
	{
		FileTools.formatPath(val);
		if (val.charAt(val.length - 1) != "\\")
			val += "\\";
		if (val != FileTools.formatPath(rootDirectory))
			for (s in sGraphics.keys())
			{
				var newPath:String = FileTools.getRelativePathFromAbsolutePath(val, rootDirectory + s);
				var g:Gfx = sGraphics.get(s);
				sGraphics.remove(s);
				sGraphics.set(s, g);
			}
		rootDirectory = val;
	
		return val;
	}
	
	public static inline function isRecognizedType(ext:String):Bool
	{
		return (ext.toLowerCase() == "gfx");
	}
	
	public static function addAllResourcesInDirectory(path:String, includeSubDirectories:Bool = true):Void
	{		
		var files:Array<String> = FileTools.getAllFilesIn(rootDirectory, true);
		for (s in files)
			if (isRecognizedType(FileTools.extensionOf(s)))
				load(FileTools.getRelativePathFromAbsolutePath(rootDirectory, s), true);
	}
	
	public static inline function getAllKnownResources():Array<String>
	{
		var arr = new Array<String>();
		for (s in sGraphics.keys())
			arr.push(s);
		return arr;
	}
	
	public static function load(fileName:String, useRootDirectory:Bool = true):Void
	{
		if (!useRootDirectory)
			fileName = FileTools.getRelativePathFromAbsolutePath(rootDirectory, fileName);
		fileName = FileTools.formatPath(fileName);
		
		for (s in sGraphics.keys())
			if (FileTools.comparePaths(s, fileName))
				return;
		var path:String = FileTools.getAbsolutePathFromRelativePath(rootDirectory, fileName);
		var xml:Xml = Xml.parse(cast FileTools.immediateReadFrom(path)).firstElement();
		sGraphics.set(fileName, cast GraphicsTools.createFromXML(xml));
	}
	
	public static function request(fileName:String, useRootDirectory:Bool = true):flash.Vector<IGraphicsData>
	{
		if (!useRootDirectory)
			fileName = FileTools.getRelativePathFromAbsolutePath(rootDirectory, fileName);
		fileName = FileTools.formatPath(fileName);
		for (s in sGraphics.keys())
			if (FileTools.comparePaths(s, fileName))
				return GraphicsTools.cloneGraphics(sGraphics.get(s));
		return null;
	}
	
	private static function loadError(e:FileToolsEvent):Void
	{
		trace("WARNING: Could not load graphics " + e.path);
	}
}