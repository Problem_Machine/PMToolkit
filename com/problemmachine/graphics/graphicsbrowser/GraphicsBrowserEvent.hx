package com.problemmachine.graphics.graphicsbrowser;
import flash.display.IGraphicsData;
import flash.events.Event;
import flash.Vector;

/**
 * ...
 * @author 
 */
class GraphicsBrowserEvent extends Event
{
	public static inline var CANCEL:String = "Cancel Gfx Select";
	public static inline var CONFIRM:String = "Confirm Gfx Select";
	
	public var path(default, null):String;
	public var graphics(default, null):flash.Vector<IGraphicsData>;

	public function new(path:String, ?graphics:flash.Vector<IGraphicsData>, type:String, bubbles:Bool = false, cancelable:Bool = false) 
	{
		super(type, bubbles, cancelable);
		this.graphics = graphics;
		this.path = path;
	}
	
}