﻿package com.problemmachine.animation;

import com.problemmachine.animation.event.AnimationEvent;
import com.problemmachine.bitmap.bitmapmanager.BitmapManager;
import com.problemmachine.bitmap.bitmapmanager.BitmapManagerEvent;
import com.problemmachine.bitmap.bitmapmanager.BitmapManagerQuery;
import com.problemmachine.tools.file.FileTools;
import com.problemmachine.tools.math.IntMath;
import com.problemmachine.util.color.colormatrix.ColorMatrix;
import flash.display.BitmapData;
import flash.display.BlendMode;
import flash.events.EventDispatcher;
import flash.filters.BitmapFilter;
import flash.filters.ColorMatrixFilter;
import flash.geom.ColorTransform;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.filters.DisplacementMapFilter;

class Frame extends EventDispatcher
{
	private static var sResourceTracker:Map<String, Array<Frame>> = new Map<String, Array<Frame>>();
	
	static private var sTempBitmap:BitmapData;
	static private var sTempColorMatrix:ColorMatrix;
	static private var sTempColorTransform:ColorTransform;
	static private var sTempMatrix:Matrix;
	static private var sTempRect:Rectangle;
	static private var sTempPoint:Point;
	
	private var mCacheWhenReady:Bool;
	private var mCache:BitmapData;
	private var mCacheValid:Bool;
	private var mFilterOffset:Point;
	private var mAsset:BitmapData;
	private var mRegistrationPoints:Array<Point>;
	private var mClipRect:Rectangle;
	
	public var resource(default, set):String;
	public var sleeping(default, set):Bool;
	public var cacheable(default, set):Bool;
	public var parent(default, set):Animation;
	public var delay(default, default):Float;
	public var rotation(default, set):Float;
	public var scale(default, set):Float;
	public var offset(default, set):Point;
	public var clipRect(get, set):Rectangle;
	public var flipHorizontal(default, set):Bool;
	public var flipVertical(default, set):Bool;
	public var ready(get, null):Bool;
	
	public var width(get, never):Int;
	public var height(get, never):Int;
	public var rotationDegrees(get, set):Float;
	public var rotationRadians(get, set):Float;
	public var red(default, set):Float;
	public var green(default, set):Float;
	public var blue(default, set):Float;
	public var alpha(default, set):Float;
	public var hue(default, set):Float;
	public var saturation(default, set):Float;
	public var brightness(default, set):Float;
	
	public function new(resource:String, delay:Float, ?offset:Point, ?clipRect:Rectangle, 
		?parent:Animation, startAsleep:Bool = true)
	{		
		super(this);
		
		mCache = null;
		mCacheValid = false;
		mCacheWhenReady = false;
		mFilterOffset = null;
		mAsset = null;
		mRegistrationPoints = [];
		if (sTempColorMatrix == null)
			sTempColorMatrix = new ColorMatrix();
		if (sTempColorTransform == null)
			sTempColorTransform = new ColorTransform();
		if (sTempMatrix == null)
			sTempMatrix = new Matrix();
		if (sTempPoint == null)
			sTempPoint = new Point();
		if (sTempRect == null)
			sTempRect = new Rectangle();
		
		ready = false;
		if (offset != null)
			this.offset = offset.clone();
		else
			this.offset = new Point();
		this.resource = resource;
		this.cacheable = true;
		this.parent = parent;
		this.delay = delay;
		this.sleeping = startAsleep;
		this.rotation = 0;
		this.scale = 1;
		this.offset = (offset == null ? new Point() : offset.clone());
		if (clipRect != null)
			this.clipRect = clipRect.clone();
		this.flipHorizontal = false;
		this.flipVertical = false;
		this.alpha = 1;
		this.red = this.green = this.blue = 1;
		this.hue = this.brightness = 0;
		this.saturation = 1;
	}
	
	private inline function get_ready():Bool
	{
		if (ready)
			return true;
		if (!sleeping && mAsset != null)
			return ready = true;
		else
			return false;
	}
	
	public function getCachedBitmap():BitmapData
	{
		if (cacheable)
			return mCache;
		else
			return mAsset;
	}
	
	public function setRegistrationPoint(index:Int, point:Point):Void
	{
		if (index < 0)
			return;
		while (mRegistrationPoints.length + 1 < index)
			mRegistrationPoints.push(null);
		if (mRegistrationPoints[index] == null)
			mRegistrationPoints[index] = new Point();
		mRegistrationPoints[index].x = point.x;
		mRegistrationPoints[index].y = point.y;
	}
	
	public function getRegistrationPoint(index:Int, transform:Bool):Point
	{
		if (index < 0 || index >= mRegistrationPoints.length)
			return new Point();
		if (mRegistrationPoints[index] == null)
			return new Point();
		var p:Point = mRegistrationPoints[index].clone();
		if (transform)
		{
			var s:Float = scale * (parent != null ? parent.scale : 1);
			var r:Float = rotation + (parent != null ? parent.rotation : 0);
			p.x += clipRect.width * 0.5;
			p.y += clipRect.height * 0.5;
			if (r != 0)
			{
				r *= 2 * Math.PI;
				var sin:Float = Math.sin(r);
				var cos:Float = Math.cos(r);
				p.x = (p.x * cos) - (p.y * sin);
				p.y = (p.y * cos) + (p.x * sin);
			}
			p.x -= clipRect.width * 0.5 * s;
			p.y -= clipRect.height * 0.5 * s;
		}
		return p;
	}
	
	public inline function draw(surface:BitmapData, offset:Point, ?filters:Array<BitmapFilter>, ?blendMode:BlendMode, smoothing:Bool = false, 
		?stencilImage:BitmapData, ?stencilOffset:Point):Void
	{	
		if (blendMode == null)
			blendMode = NORMAL;
		if (resource == "")
		{
			trace ("Frame.draw(): Could not draw null/empty resource");
			return;
		}
		if (sleeping || mAsset == null)
		{
			if (sleeping) trace ("Frame.draw(): Could not draw resource(" + resource + ") because frame is sleeping");
			else trace ("Frame.draw(): Could not draw resource(" + resource + ") because resource is not ready");
			return;
		}
		var s:Float = scale * (parent != null ? parent.scale : 1);
		if (s == 0)
			return;
		var r:Float = rotation + (parent != null ? parent.rotation : 0);
		var canCopyPixels:Bool = (filters == null && blendMode == NORMAL && smoothing == false) 
			&& (cacheable || 
				(r == 0 && red == 1 && green == 1 && blue == 1 && hue == 0 && saturation == 1 && brightness == 0 && s == 1 && 
					(parent == null || (parent.red == 1 && parent.green == 1 && parent.blue == 1))));
		
		offset = offset.clone();
		var minX:Float = clipRect.left;
		var maxX:Float = clipRect.right;
		var minY:Float = clipRect.top;
		var maxY:Float = clipRect.bottom;
		if (r != 0)
		{
			var sin:Float = Math.sin(r * 2 * Math.PI);
			var cos:Float = Math.cos(r * 2 * Math.PI);

			sTempRect.setTo(this.offset.x * s, this.offset.y * s, clipRect.width * s, clipRect.height * s);
			var rect:Rectangle = sTempRect;
			minX =  Math.min(rect.left * cos - rect.top * sin,
					Math.min(rect.right * cos - rect.top * sin, 
					Math.min(rect.right * cos - rect.bottom * sin, 
					 rect.left * cos - rect.bottom * sin)));
			maxX =  Math.max(rect.left * cos - rect.top * sin,
					Math.max(rect.right * cos - rect.top * sin, 
					Math.max(rect.right * cos - rect.bottom * sin, 
					 rect.left * cos - rect.bottom * sin)));
			minY =  Math.min(rect.left * sin + rect.top * cos,
					Math.min(rect.right * sin + rect.top * cos, 
					Math.min(rect.right * sin + rect.bottom * cos, 
					 rect.left * sin + rect.bottom * cos)));
			maxY =  Math.max(rect.left * sin + rect.top * cos,
					Math.max(rect.right * sin + rect.top * cos, 
					Math.max(rect.right * sin + rect.bottom * cos, 
					 rect.left * sin + rect.bottom * cos)));
			offset.x += minX;
			offset.y += minY;
		}
		else
		{
			offset.x += this.offset.x * s;
			offset.y += this.offset.y * s;
		}
		if (mFilterOffset != null)
		{
			offset.x += mFilterOffset.x;
			offset.y += mFilterOffset.y;
		}
		
		if (offset.x > surface.width || offset.y > surface.height || offset.x + maxX - minX < 0 || offset.y + maxY - minY < 0)
			return;
		if (cacheable && !mCacheValid)
			updateCache();
		
		if (!canCopyPixels)
			drawComplex(surface, offset, filters, blendMode, smoothing, stencilImage, stencilOffset);
		else
			drawSimple(surface, offset, stencilImage, stencilOffset);
	}
	
	private function drawComplex(surface:BitmapData, offset:Point, filters:Array<BitmapFilter>, blendMode:BlendMode, smoothing:Bool, 
		?stencilImage:BitmapData, stencilOffset:Point):Void
	{
		if (stencilOffset != null)
			stencilOffset = stencilOffset.clone();
		sTempPoint.setTo(0, 0);
		var origin:Point = sTempPoint;
		sTempMatrix.identity();
		var m:Matrix = sTempMatrix;
		if (cacheable)
		{
			sTempColorTransform.redMultiplier = sTempColorTransform.greenMultiplier = sTempColorTransform.blueMultiplier = 1;
			sTempColorTransform.alphaMultiplier = alpha * (parent != null ? parent.alpha : 1);
			if (stencilImage != null && stencilOffset != null)
			{
				sTempBitmap.fillRect(new Rectangle(0, 0, sTempBitmap.width, sTempBitmap.height), 0x00000000);
				sTempBitmap.draw(mCache, m, sTempColorTransform);
				sTempRect.setTo(stencilOffset.x - offset.x, stencilOffset.y - offset.y, stencilImage.width, stencilImage.height);
				sTempBitmap.copyPixels(stencilImage, sTempRect, origin, sTempBitmap, new Point(offset.x - stencilOffset.x, offset.y - stencilOffset.y), false);
				m.translate(offset.x, offset.y);
				surface.draw(sTempBitmap, m, null, blendMode, null, smoothing);
			}
			else
			{
				m.translate(offset.x, offset.y);
				surface.draw(mCache, m, sTempColorTransform, blendMode, null, smoothing);
			}
			return;
		}
		else
		{
			var s:Float = scale;
			var r:Float = rotation;
			var a:Float = alpha;
			var hFlip:Bool = flipHorizontal;
			var vFlip:Bool = flipVertical;
			if (parent != null)
			{
				s *= parent.scale;
				r += parent.rotation;
				a *= parent.alpha;
				hFlip = hFlip != parent.flipHorizontal;
				vFlip = vFlip != parent.flipVertical;
			}
			
			var cm:ColorMatrix = null;
			var ct:ColorTransform = null;
			if (hue != 0 || saturation != 1 || brightness != 0 ||  
				(parent != null && (parent.hue != 0 || parent.saturation != 1 || parent.brightness != 0)))
			{			
				sTempColorMatrix.reset();
				cm = sTempColorMatrix;
				if (parent != null)
				{
					cm.setMultipliers(red * parent.red, green * parent.green, blue * parent.blue, a);
					cm.adjustHue(hue + parent.hue);
					cm.adjustSaturation(saturation * parent.saturation);
					cm.adjustBrightness(brightness + parent.brightness);
				}
				else
				{
					cm.setMultipliers(red, green, blue, a);
					cm.adjustHue(hue);
					cm.adjustSaturation(saturation);
					cm.adjustBrightness(brightness);
				}
			}
			else if (red != 1 || green != 1 || blue != 1 || a != 1 || 
				(parent != null && (parent.red != 1 || parent.green != 1 || parent.blue != 1)))
			{			
				sTempColorTransform.redMultiplier = red * (parent == null ? 1 : parent.red);
				sTempColorTransform.greenMultiplier = green * (parent == null ? 1 : parent.green);
				sTempColorTransform.blueMultiplier = blue * (parent == null ? 1 : parent.blue);
				sTempColorTransform.alphaMultiplier = a;
				ct = sTempColorTransform;
			}
			
			sTempRect.setTo(0, 0, 0, 0);
			var rect:Rectangle = sTempRect;
			if (filters == null)
				filters = [];
			else
				filters = filters.copy();
			if (cm != null)
				filters.push(new ColorMatrixFilter(cm.matrix));
			var temp:BitmapData = mCache;
			if (temp == null)
				mCache = temp = new BitmapData(Math.ceil(clipRect.width), Math.ceil(clipRect.height), true, 0x00);
			else if (temp.width < clipRect.width || temp.height < clipRect.height)
			{
				var w:Int = temp.width;
				var h:Int = temp.height;
				temp.dispose();
				mCache = temp = new BitmapData(IntMath.max(w, Math.ceil(clipRect.width)), IntMath.max(h, Math.ceil(clipRect.height)), true, 0x00);
			}
			else
			{
				rect.setTo(0, 0, temp.width, temp.height);
				temp.fillRect(rect, 0x00000000);
			}
			if (stencilImage != null && stencilOffset != null)
			{
				stencilOffset.x -= offset.x;
				stencilOffset.y -= offset.y;
				sTempRect.setTo(stencilOffset.x, stencilOffset.y, stencilImage.width, stencilImage.height);
				temp.copyPixels(stencilImage, sTempRect, stencilOffset, mAsset, new Point(-clipRect.x, -clipRect.y), false);
			}
			else
				temp.copyPixels(mAsset, clipRect, origin);
			var p:Point = sTempPoint;
			rect.setTo(0, 0, clipRect.width, clipRect.height);
			if (filters.length > 0)
			{
				var filtRect:Rectangle = new Rectangle(0, 0, clipRect.width, clipRect.height);
				for (f in filters)
				{
					var tempFiltRect:Rectangle = temp.generateFilterRect(rect, f);
					if (Std.is(f, DisplacementMapFilter))
					{
						var d:DisplacementMapFilter = cast f;
						tempFiltRect.setTo( -Math.floor(d.scaleX * 0.5), -Math.floor(d.scaleY * 0.5), clipRect.width + Math.floor(d.scaleX * 0.5) * 2, clipRect.height + Math.floor(d.scaleY * 0.5) * 2);
					}
					filtRect.setTo(filtRect.x + tempFiltRect.x, filtRect.y + tempFiltRect.y, 
						filtRect.width + (tempFiltRect.width - clipRect.width), filtRect.height + (tempFiltRect.height - clipRect.height));
				}
				p.x += filtRect.x;
				p.y += filtRect.y;
				
				for (f in filters)
				{				
					var second:BitmapData = sTempBitmap;
					if (second == null)
						sTempBitmap = second = new BitmapData(Math.ceil(filtRect.width), Math.ceil(filtRect.height), true, 0x00000000);
					else if (clipRect.width < filtRect.width || clipRect.height < filtRect.height)
					{
						var w:Int = second.width;
						var h:Int = second.height;
						second.dispose();
						sTempBitmap = second = new BitmapData(IntMath.max(Math.ceil(filtRect.width), w), IntMath.max(Math.ceil(filtRect.height), h), true, 0x00000000);
					}
					else
						second.fillRect(new Rectangle(0, 0, second.width, second.height), 0x00000000);
					second.applyFilter(temp, rect, new Point(), f);
				
					temp = second;
					sTempBitmap = mCache;
					mCache = temp;
				}
				rect.setTo(0, 0, temp.width, temp.height);
			}
			if (hFlip || vFlip)
			{
				m.translate(hFlip ? -clipRect.width * 0.5 : 0, vFlip ? -clipRect.height * 0.5 : 0);
				m.scale(hFlip ? -1 : 1, vFlip ? -1 : 1);
				m.translate(hFlip ? clipRect.width * 0.5 : 0, vFlip ? clipRect.height * 0.5 : 0);
			}
			m.translate(this.offset.x, this.offset.y);
			m.rotate(r * 2 * Math.PI);
			m.scale(s, s);
			m.translate(offset.x - this.offset.x * s, offset.y - this.offset.y * s);
			
			surface.draw(temp, m, ct, blendMode, null, smoothing);
		}
	}
	
	private inline function drawSimple(surface:BitmapData, offset:Point, ?stencilImage:BitmapData, ?stencilOffset:Point):Void
	{		
		if (cacheable)
			sTempRect.setTo(0, 0, mCache.width, mCache.height);
		else
			sTempRect.setTo(0, 0, clipRect.width, clipRect.height);
		if (alpha == 0 || (parent != null && parent.alpha == 0))
			return;
		else
		{
			offset.x = Math.round(offset.x);
			offset.y = Math.round(offset.y);
			
			var bmp:BitmapData = cacheable ? mCache : mAsset;
			if (stencilImage != null && stencilOffset != null)
			{
				if (!cacheable)
				{
					sTempBitmap.fillRect(new Rectangle(0, 0, sTempBitmap.width, sTempBitmap.height), 0x00000000);
					sTempBitmap.copyPixels(bmp, sTempRect, new Point(), null, null, true);
					bmp = sTempBitmap;
				}
				sTempRect.setTo(0, 0, stencilImage.width, stencilImage.height);
				surface.copyPixels(stencilImage, sTempRect, stencilOffset, bmp, offset, true);
			}
			else
				surface.copyPixels(bmp, sTempRect, offset, null, null, true);
			return;
		}
	}
	
	public function clone(?startSleeping:Bool):Frame
	{
		var f:Frame = new Frame(resource, delay, offset, clipRect, null, true);
		f.ready = this.ready;
		f.alpha = this.alpha;
		f.red = this.red;
		f.green = this.green;
		f.blue = this.blue;
		f.hue = this.hue;
		f.saturation = this.saturation;
		f.brightness = this.brightness;
		f.cacheable = this.cacheable;
		f.mCacheValid = this.mCacheValid;
		if (this.mCache != null && this.mCacheValid)
			f.mCache = this.mCache.clone();
		f.mFilterOffset = this.mFilterOffset;
		f.flipHorizontal = this.flipHorizontal;
		f.flipVertical = this.flipVertical;
		f.rotation = this.rotation;
		f.scale = this.scale;
		f.mRegistrationPoints = mRegistrationPoints.copy();
		for (i in 0...f.mRegistrationPoints.length)
			if (f.mRegistrationPoints[i] != null)
				f.mRegistrationPoints[i] = f.mRegistrationPoints[i].clone();
		if (startSleeping != null)
			f.sleeping = startSleeping;
		else
			f.sleeping = this.sleeping;
		return f;
	}
	
	public inline function dispose():Void
	{
		mAsset = null;
		parent = null;
		BitmapManager.unlock(resource, this);
		if (mCache != null)
			mCache.dispose();
		ready = false;
	}
	
	public inline function invalidateCache():Void
	{	
		mCacheValid = false;
	}
	
	private function updateCache():Void
	{
		if (resource == "")
		{
			if (mCache != null)
				mCache.dispose();
			mCache = null;
			return;
		}
		if (mAsset == null)
		{
			mCacheWhenReady = true;
			return;
		}
		var m:Matrix = new Matrix();
		var s:Float = scale;
		var r:Float = rotation;
		var a:Float = alpha;
		var origin:Point = new Point();
		var hFlip:Bool = flipHorizontal;
		var vFlip:Bool = flipVertical;
		if (parent != null)
		{
			s *= parent.scale;
			r += parent.rotation;
			a *= parent.alpha;
			hFlip = hFlip != parent.flipHorizontal;
			vFlip = vFlip != parent.flipVertical;
		}
		
		var cm:ColorMatrix = null;
		var ct:ColorTransform = null;
		if (hue != 0 || saturation != 1 || brightness != 0 || 
			(parent != null && (parent.hue != 0 || parent.saturation != 1 || parent.brightness != 0)))
		{			
			cm = new ColorMatrix();
			if (parent != null)
			{
				cm.setMultipliers(red * parent.red, green * parent.green, blue * parent.blue, a);
				cm.adjustHue(hue + parent.hue);
				cm.adjustSaturation(saturation * parent.saturation);
				cm.adjustBrightness(brightness + parent.brightness);
			}
			else
			{
				cm.setMultipliers(red, green, blue, a);
				cm.adjustHue(hue);
				cm.adjustSaturation(saturation);
				cm.adjustBrightness(brightness);
			}
		}
		else if (red != 1 || green != 1 || blue != 1 || a != 1 || 
			(parent != null && (parent.red != 1 || parent.green != 1 || parent.blue != 1)))
		{			
			if (parent != null)
				ct = new ColorTransform(red * parent.red, green * parent.green, blue * parent.blue, a);
			else
				ct = new ColorTransform(red, green, blue, a);
		}
		
		var temp:BitmapData;
		temp = new BitmapData(Math.ceil(clipRect.width), Math.ceil(clipRect.height), true, 0x00);
		temp.copyPixels(mAsset, clipRect, origin);
		
		var sin:Float = Math.sin(r * 2 * Math.PI);
		var cos:Float = Math.cos(r * 2 * Math.PI);
		var rw:Int = Math.ceil(Math.max(Math.abs((-clipRect.width * 0.5 * cos) - (-clipRect.height * 0.5 * sin)),
					Math.abs((clipRect.width * 0.5 * cos) - ( -clipRect.height * 0.5 * sin))) * 2 * s);
		var rh:Int = Math.ceil(Math.max(Math.abs((-clipRect.width * 0.5 * sin) + (-clipRect.height * 0.5 * cos)),
					Math.abs((clipRect.width * 0.5 * sin) + ( -clipRect.height * 0.5 * cos))) * 2 * s);

		m.translate( -temp.width * 0.5, -temp.height * 0.5);
		m.rotate(r * 2 * Math.PI);
		m.scale(hFlip ? -s : s, vFlip ? -s : s);
		m.translate(rw * 0.5, rh * 0.5);
		
		if (mCache == null || mCache.width != rw || mCache.height != rh)
		{
			if (mCache != null)
				mCache.dispose();
			mCache = new BitmapData(rw, rh, true, 0x00000000);
		}
		else if (mCache != null)
			mCache.fillRect(new Rectangle(0, 0, mCache.width, mCache.height), 0x00000000);
			
		mCache.draw(temp, m, ct, null, null, false);
		temp.dispose();
		if (cm != null)
		{
			temp = mCache.clone();
			temp.fillRect(new Rectangle(0, 0, temp.width, temp.height), 0x00000000);
			temp.applyFilter(mCache, new Rectangle(0, 0, mCache.width, mCache.height), new Point(0, 0), new ColorMatrixFilter(cm.matrix));
			mCache.dispose();
			mCache = temp;
		}
		else
			temp = mCache;
		
		var rect:Rectangle = sTempRect; 
		rect.setTo(0, 0, clipRect.width, clipRect.height);
		if (parent != null)
		{
			var filters:Array<BitmapFilter> = parent.queryFilters();
			if (filters != null && filters.length > 0)
			{
				var filtRect:Rectangle = new Rectangle(0, 0, clipRect.width, clipRect.height);
				for (f in filters)
				{
					var tempFiltRect:Rectangle = temp.generateFilterRect(rect, f);
					if (Std.is(f, DisplacementMapFilter))
					{
						var d:DisplacementMapFilter = cast f;
						tempFiltRect.setTo( -Math.floor(d.scaleX * 0.5), -Math.floor(d.scaleY * 0.5), clipRect.width + Math.floor(d.scaleX * 0.5) * 2, clipRect.height + Math.floor(d.scaleY * 0.5) * 2);
					}
					filtRect.setTo(filtRect.x + tempFiltRect.x, filtRect.y + tempFiltRect.y, 
						filtRect.width + (tempFiltRect.width - clipRect.width), filtRect.height + (tempFiltRect.height - clipRect.height));
				}
				var second:BitmapData = new BitmapData(Math.ceil(filtRect.width), Math.ceil(filtRect.height), true, 0x00000000);
				for (f in filters)
				{
					rect.width = temp.width;
					rect.height = temp.height;
					if (second.width < filtRect.width || second.height < filtRect.height)
					{
						second.dispose();
						second = new BitmapData(Math.ceil(filtRect.width), Math.ceil(filtRect.height), true, 0x00000000);
					}
					
					second.applyFilter(temp, rect, new Point(), f);
				
					var tmpbmp:BitmapData = temp;
					temp = second;
					second = tmpbmp;
				}
				second.dispose();
			}
		}
		
		mCache = temp;
		
		mCacheValid = true;
	}
	
	private static function requestAsset(path:String, requester:Frame):Void
	{
		var arr:Array<Frame> = sResourceTracker.get(path);
		if (arr == null)
		{
			arr = [requester];
			sResourceTracker.set(path, arr);
			switch (BitmapManager.query(path))
			{
				case BitmapManagerQuery.Ready:
					BitmapManager.lock(path, sResourceTracker);
					requester.mAsset = BitmapManager.request(path);
					requester.ready = !requester.sleeping;
				case BitmapManagerQuery.NotReady:
					BitmapManager.startListeningForCompletion(path, bitmapListener); 
					BitmapManager.lock(path, sResourceTracker);
					requester.ready = false;
				case BitmapManagerQuery.NotActive, BitmapManagerQuery.NotFound:
					BitmapManager.startListeningForCompletion(path, bitmapListener); 
					BitmapManager.loadAndLock(path, sResourceTracker);
					requester.ready = false;
			}
		}
		else if (arr.indexOf(requester) < 0)
			arr.push(requester);
		requester.mAsset = arr[0].mAsset;
	}
	
	private static function bitmapListener(e:BitmapManagerEvent):Void
	{
		BitmapManager.stopListeningForCompletion(e.type, bitmapListener);
		var arr:Array<Frame> = sResourceTracker.get(e.type);
		for (f in arr)
		{
			f.mAsset = e.data;
			f.dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_READY));
			if (f.mCacheWhenReady)
			{
				f.mCacheWhenReady = false;
				f.updateCache();
			}
		}
	}
	
	private static function releaseAsset(path:String, releaser:Frame):Void
	{
		var arr:Array<Frame> = sResourceTracker.get(path);
		arr.remove(releaser);
		if (arr.length == 0)
		{
			BitmapManager.unlock(path, sResourceTracker);
			BitmapManager.stopListeningForCompletion(path, bitmapListener);
			sResourceTracker.remove(path);
		}
	}
	
	@:access(com.problemmachine.animation.Animation.ready)
	private inline function set_resource(val:String):String
	{
		if (val == null || FileTools.formatPath(val.toLowerCase()) != this.resource)
		{
			if (this.resource != null && this.resource != "")
				releaseAsset(FileTools.formatPath(resource.toLowerCase()), this);
			mAsset = null;
			mCacheValid = false;
			this.resource = val;
			if (this.resource == null)
				this.resource = "";
			else 
				this.resource = FileTools.formatPath(this.resource.toLowerCase());
				
			if (resource != "")
				requestAsset(resource, this);
				
			if (parent != null && mAsset == null)
				parent.ready = false;
		}
		return val;
	}
	
	private inline function set_sleeping(val:Bool):Bool
	{
		if (val != sleeping)
		{
			if (val)
			{
				releaseAsset(resource, this);
				if (mCache != null)
					mCache.dispose();
				mCache = null;
				mCacheValid = false;
				ready = false;
			}
			else if (resource != "")
				requestAsset(resource, this);
		}
		return sleeping = val;
	}
	
	private inline function set_cacheable(c:Bool):Bool
	{
		if (cacheable && !c)
		{
			mFilterOffset = null;
			if (mCache != null)
				mCache.dispose();
			mCache = null;
			mCacheValid = false;
		}
		return cacheable = c;
	}
	
	private inline function set_parent(p:Animation):Animation
	{	
		if (p != parent)
			mCacheValid = false;
		return parent = p;
	}
	
	private inline function set_rotation(val:Float):Float
	{	
		while (val < 0)
			++val;
		while (val > 1)
			--val;
		if (rotation != val && cacheable)
			mCacheValid = false;
		
		return rotation = val;			
	}
	
	private inline function set_scale(s:Float):Float
	{
		if (s != scale)
			mCacheValid = false;
		return scale = s;
	}
	
	private inline function set_offset(p:Point):Point
	{	return (offset = p.clone());	}
		
	
	private function get_clipRect():Rectangle
	{
		if (mClipRect == null)
		{
			if (mAsset == null)
				return null;
			mClipRect = new Rectangle(0, 0, mAsset.width, mAsset.height);
		}
		return mClipRect;
	}
	private function set_clipRect(r:Rectangle):Rectangle
	{	
		if (mClipRect != null)
			if (mClipRect.x != r.x || mClipRect.y != r.y || mClipRect.width != r.width || mClipRect.height != r.height)
				mCacheValid = false;
		return mClipRect = r;
	}
	
	private inline function set_flipHorizontal(val:Bool):Bool
	{
		if (val != flipHorizontal)
			mCacheValid = false;
		return flipHorizontal = val;
	}
	
	private inline function set_flipVertical(val:Bool):Bool
	{
		if (val != flipVertical)
			mCacheValid = false;
		return flipVertical = val;
	}
	
	private inline function set_rotationDegrees(val:Float):Float
	{	
		rotation = (val / 360);		
		return val;		
	}
	private inline function get_rotationDegrees():Float
	{	return rotation * 360;			}
	
	private inline function set_rotationRadians(val:Float):Float
	{	
		rotation = val / (Math.PI * 2);	
		return val; 
	}
	private inline function get_rotationRadians():Float
	{	return rotation * Math.PI * 2;	}
	
	private inline function set_red(val:Float):Float
	{
		if (Math.max(0, val) != red)
		{
			red = Math.max(0, val);
			mCacheValid = false;
		}
		return val;
	}	
	private inline function set_green(val:Float):Float
	{
		if (Math.max(0, val) != green)
		{
			green = Math.max(0, val);
			mCacheValid = false;
		}
		return val;
	}
	private inline function set_blue(val:Float):Float
	{
		if (Math.max(0, val) != blue)
		{
			blue = Math.max(0, val);
			mCacheValid = false;
		}
		return val;
	}
	private inline function set_alpha(val:Float):Float
	{
		if (Math.max(0, val) != alpha)
		{
			alpha = Math.max(0, val);
			mCacheValid = false;
		}
		return val;
	}
	
	private inline function set_hue(val:Float):Float
	{
		if (val != hue)
		{
			hue = val;
			mCacheValid = false;
		}
		return val;
	}
	private inline function set_saturation(val:Float):Float
	{
		if (val != saturation)
		{
			saturation = val;
			mCacheValid = false;
		}
		return val;
	}
	private inline function set_brightness(val:Float):Float
	{
		if (val != brightness)
		{
			brightness = val;
			mCacheValid = false;
		}
		return val;
	}
	
	private function get_width():Int
	{
		if (mCache != null)
			return mCache.width;
		else
		{
			if (clipRect == null)
				return 0;
			var s:Float = scale * (parent != null ? parent.scale : 1);
			var r:Float = rotation + (parent != null ? parent.rotation : 0);
			if (s == 1 && r == 0)
				return Math.ceil(clipRect.width);
			var cos:Float = Math.cos(r * 2 * Math.PI);
			var sin:Float = Math.sin(r * 2 * Math.PI);
			var maxX:Float = 
				Math.max(Math.abs((-clipRect.width * 0.5 * cos) - (-clipRect.height * 0.5 * sin)),
						Math.abs((clipRect.width * 0.5 * cos) - (-clipRect.height * 0.5 * sin)));
			//	because all rotations are rectangular and symmetric we only have to test half the points
			// 		and we can calculate width as maximum x offset * 2
			
			return Math.ceil(maxX * 2 * s);
		}
	}
	
	private function get_height():Int
	{
		if (mCache != null)
			return mCache.height;
		else
		{
			if (clipRect == null)
				return 0;
			var s:Float = scale * (parent != null ? parent.scale : 1);
			var r:Float = rotation + (parent != null ? parent.rotation : 0);
			if (s == 1 && r == 0)
				return Math.ceil(clipRect.height);
			var cos:Float = Math.cos(r * 2 * Math.PI);
			var sin:Float = Math.sin(r * 2 * Math.PI);
			var maxY:Float = 
				Math.max(Math.abs((-clipRect.height * 0.5 * cos) + (-clipRect.width * 0.5 * sin)),
						Math.abs((-clipRect.height * 0.5 * cos) + (clipRect.width * 0.5 * sin)));
			//	because all rotations are rectangular and symmetric we only have to test half the points
			// 		and we can calculate height as maximum y offset * 2
			
			return Math.ceil(maxY * 2 * s);
		}
	}
}