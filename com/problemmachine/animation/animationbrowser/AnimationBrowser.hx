package com.problemmachine.animation.animationbrowser;
import com.problemmachine.animation.Animation;
import com.problemmachine.animation.animationmanager.AnimationManager;
import com.problemmachine.ui.scrollwindow.ScrollWindow;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.BlendMode;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Rectangle;
import flash.Lib;
import flash.text.TextField;
import flash.text.TextFieldType;
import flash.text.TextFormat;
import flash.text.TextFormatAlign;

class AnimationBrowser extends Sprite
{
	private var mWindow:ScrollWindow;
	private var mVerticalAlign:Bool;
	private var mNodes:Array<AnimationBrowserNode>;
	private var mNodeWidth:Float;
	private var mNodeHeight:Float;
	private var mTextSize:Float;
	
	private var mFilterTextBoxBack:Sprite;
	private var mFilterTextField:TextField;
	private var mFilterTextInputField:TextField;
	
	private var mSelectedAnimationDisplay:Sprite;
	private var mSelectedAnimationSurface:Bitmap;
	private var mSelectedAnimationMask:Sprite;
	private var mSelectedAnimation:Animation;
	private var mSelectedAnimationPath:String;
	private var mSelectedNodeHighlight:Sprite;
	
	private var mConfirmButton:Sprite;
	private var mConfirmText:TextField;
	private var mCancelButton:Sprite;
	private var mCancelText:TextField;
	private var mLastUpdateTime:Int;
	
	public var passiveFilter(default, default):String = "";

	public function new(width:Float, height:Float, color1:UInt, color2:UInt, nodeWidth:Float = 200, nodeHeight:Float = 150, textSize:Int = 16, barThickness:Float = 30, alignVertically:Bool = true) 
	{
		super();
		mVerticalAlign = alignVertically;
		mNodeWidth = nodeWidth;
		mNodeHeight = nodeHeight;
		mTextSize = textSize;
		graphics.beginFill(color1);
		graphics.drawRoundRect(0, 0, width, height, 5);
		
		mSelectedAnimationDisplay = new Sprite();
		mSelectedAnimationDisplay.graphics.lineStyle(5, color1);
		mSelectedAnimationDisplay.graphics.beginFill(color2);
		mSelectedAnimationDisplay.graphics.drawRoundRect(0, 0, width * 0.25, height - 100, 5);
		mSelectedAnimationDisplay.x = width - mSelectedAnimationDisplay.width;
		addChild(mSelectedAnimationDisplay);
		
		mSelectedAnimationSurface = new Bitmap(
			new BitmapData(Math.floor(mSelectedAnimationDisplay.width * 0.9), 
			Math.floor(mSelectedAnimationDisplay.height * 0.9), true, 0x00000000));
		mSelectedAnimationSurface.alpha = 0.8;
		mSelectedAnimationDisplay.addChild(mSelectedAnimationSurface);
		
		mSelectedAnimationMask = new Sprite();
		mSelectedAnimationMask.graphics.beginFill(0xFFFFFF);
		mSelectedAnimationMask.graphics.drawRoundRect(2.5, 2.5, width * 0.25 - 5, height - 105, 5);
		mSelectedAnimationDisplay.addChild(mSelectedAnimationMask);
		mSelectedAnimationSurface.mask = mSelectedAnimationMask;
		mSelectedAnimation = null;
		
		mSelectedNodeHighlight = new Sprite();
		mSelectedNodeHighlight.graphics.lineStyle(20, 0xFFFFFF);
		mSelectedNodeHighlight.graphics.beginFill(color1, 0.5);
		mSelectedNodeHighlight.graphics.drawRoundRect(-10, -10, nodeWidth + 20, nodeHeight + 20, 5);
		mSelectedNodeHighlight.blendMode = BlendMode.ADD;
		
		mFilterTextBoxBack = new Sprite();
		mFilterTextBoxBack.graphics.lineStyle(5, color1);
		mFilterTextBoxBack.graphics.beginFill(color2);
		mFilterTextBoxBack.graphics.drawRoundRect(0, 0, width - mSelectedAnimationDisplay.width, 30, 5);
		addChild(mFilterTextBoxBack);
		
		mFilterTextField = new TextField();
		mFilterTextField.defaultTextFormat = new TextFormat(null, textSize, color1);
		mFilterTextField.text = "Filters: ";
		mFilterTextField.x = 5;
		mFilterTextField.y = 5;
		mFilterTextField.height = 20;
		mFilterTextField.width = mFilterTextField.textWidth + 4;
		mFilterTextBoxBack.addChild(mFilterTextField);
		
		mFilterTextInputField = new TextField();
		mFilterTextInputField.defaultTextFormat = new TextFormat(null, textSize, color1);
		mFilterTextInputField.border = true;
		mFilterTextInputField.borderColor = color1;
		mFilterTextInputField.type = TextFieldType.INPUT;
		mFilterTextInputField.x = mFilterTextField.width + mFilterTextField.x + 5;
		mFilterTextInputField.y = 5;
		mFilterTextInputField.height = 20;
		mFilterTextInputField.width = mFilterTextBoxBack.width - mFilterTextField.width - 20;
		mFilterTextInputField.addEventListener(Event.CHANGE, filterChangeListener);
		mFilterTextBoxBack.addChild(mFilterTextInputField);
		
		mWindow = new ScrollWindow(width - mSelectedAnimationDisplay.width, height - mFilterTextBoxBack.height, color1, color2, barThickness);
		mWindow.enableMovement = false;
		mWindow.enableHorizontalScroll = !alignVertically;
		mWindow.enableVerticalScroll = alignVertically;
		mWindow.panel.y = mFilterTextBoxBack.height;
		addChild(mWindow.panel);
		
		mNodes = new Array<AnimationBrowserNode>();
		addEventListener(Event.ADDED_TO_STAGE, stageListener);
		addEventListener(Event.REMOVED_FROM_STAGE, stageListener);
		
		mCancelButton = new Sprite();
		mCancelButton.graphics.lineStyle(5, color2);
		mCancelButton.graphics.beginFill(color1);
		mCancelButton.graphics.drawRect(2.5, 2.5, mSelectedAnimationDisplay.width - 5, 45);
		mCancelButton.graphics.endFill();
		mCancelButton.x = mSelectedAnimationDisplay.x;
		mCancelButton.y = height - 100;
		mCancelButton.buttonMode = mCancelButton.useHandCursor = true;
		addChild(mCancelButton);
		
		mCancelText = new TextField();
		mCancelText.defaultTextFormat = new TextFormat(null, textSize * 2, color2, null, null, null, null, null, TextFormatAlign.CENTER);
		mCancelText.text = "Cancel";
		mCancelText.x = 5;
		mCancelText.width = mCancelButton.width - 10;
		mCancelText.height = 44;
		mCancelText.mouseEnabled = false;
		mCancelButton.addChild(mCancelText);
		mCancelButton.addEventListener(MouseEvent.CLICK, function(e:MouseEvent):Void
			{	
				dispatchEvent(new AnimationBrowserEvent("", null,  AnimationBrowserEvent.CANCEL));		
				parent.removeChild(this);	
			} );
		
		mConfirmButton = new Sprite();
		mConfirmText = new TextField();
		drawConfirmButton(false);
		mConfirmButton.x = mSelectedAnimationDisplay.x;
		mConfirmButton.y = height - 50;
		addChild(mConfirmButton);
		
		mConfirmText.defaultTextFormat = new TextFormat(null, textSize * 2, 0x444444, null, null, null, null, null, TextFormatAlign.CENTER);
		mConfirmText.text = "Confirm";
		mConfirmText.x = 5;
		mConfirmText.width = mCancelButton.width - 10;
		mConfirmText.height = 44;
		mConfirmText.mouseEnabled = false;
		mConfirmButton.addChild(mConfirmText);
		mConfirmButton.addEventListener(MouseEvent.CLICK, function(e:MouseEvent):Void
			{	
				dispatchEvent(new AnimationBrowserEvent(mSelectedAnimationPath, 
					mSelectedAnimation.clone(true), AnimationBrowserEvent.CONFIRM));	
				parent.removeChild(this);	
			} );
			
		mLastUpdateTime = Lib.getTimer();
	}
	
	public function selectAnimation(path:String):Void
	{
		for (n in mNodes)
			if (n.path == path)
			{
				selectNode(n);
				return;
			}
	}
	
	private function stageListener(e:Event):Void
	{
		if (e.type == Event.ADDED_TO_STAGE)
		{
			populate();
			displayAnimations();
			addEventListener(Event.ENTER_FRAME, updateListener);
			mLastUpdateTime = Lib.getTimer();
		}
		else
		{
			mSelectedAnimationPath = "";
			if (mSelectedAnimation != null)
			{
				mSelectedAnimation.dispose();
				mSelectedAnimation = null;
			}
			removeEventListener(Event.ENTER_FRAME, updateListener);
			if (mSelectedNodeHighlight.parent != null)
				mSelectedNodeHighlight.parent.removeChild(mSelectedNodeHighlight);
			while (mNodes.length > 0)
			{
				var n:AnimationBrowserNode = mNodes.pop();
				n.removeEventListener(MouseEvent.CLICK, selectListener);
				if (n.parent != null)
					n.parent.removeChild(n);
				n.dispose();
			}
		}
	}
	
	private function updateListener(e:Event):Void
	{
		if (mSelectedAnimation != null)
		{
			var t:Float = (Lib.getTimer() - mLastUpdateTime) * 0.001;
			mLastUpdateTime = Lib.getTimer();
			mSelectedAnimationSurface.bitmapData.fillRect(
				new Rectangle(0, 0, mSelectedAnimationSurface.bitmapData.width, 
				mSelectedAnimationSurface.bitmapData.height), 0x00000000);
			mSelectedAnimation.progressAndDraw(t, mSelectedAnimationSurface.bitmapData);
		}
		for (n in mNodes)
			if (n.parent != null)
				n.ready = mWindow.objectCurrentlyVisible(n);
	}
	
	private function filterChangeListener(e:Event):Void
	{
		displayAnimations((cast e.target).text);
		if (mSelectedNodeHighlight.parent != null)
			mSelectedNodeHighlight.parent.removeChild(mSelectedNodeHighlight);
		
		for (n in mNodes)
			if (n.path == mSelectedAnimationPath)
			{
				if (n.parent != null)
				{
					mSelectedNodeHighlight.x = n.x;
					mSelectedNodeHighlight.y = n.y;
					mWindow.addChild(mSelectedNodeHighlight);
				}
				break;
			}
	}
	
	public function populate():Void
	{
		while (mNodes.length > 0)
		{
			var n = mNodes.pop();
			n.removeEventListener(MouseEvent.CLICK, selectListener);
			if (n.parent != null)
				n.parent.removeChild(n);
			n.dispose();
		}
		var arr:Array<String> = AnimationManager.getAllKnownResources();
		for (s in arr)
		{
			var n:AnimationBrowserNode = new AnimationBrowserNode(s, mNodeWidth, mNodeHeight, mWindow.color2, mWindow.color1, Math.ceil(mTextSize * (2/3)));
			n.addEventListener(MouseEvent.CLICK, selectListener);
			mNodes.push(n);
		}
	}
	
	private function displayAnimations(filter:String = ""):Void
	{
		filter += " " + passiveFilter;
		
		var filters:Array<String> = filter.split(" ");
		for (i in 0...filters.length)
			if (filters[filters.length - 1 - i] == "")
				filters.splice(filters.length - 1 - i, 1);
		if (filters.length > 0)
		{
			for (n in mNodes)
			{
				var valid:Bool = true;
				var p:String = n.path.toLowerCase();
				for (f in filters)
					if (p.indexOf(f.toLowerCase()) == -1)
					{
						valid = false;
						break;
					}
				if (valid)
				{
					if (n.parent == null)
						mWindow.addChild(n);
				}
				else if (n.parent != null)					
					n.parent.removeChild(n);
			}
		}
		else for (n in mNodes)
			if (n.parent == null)
				mWindow.addChild(n);
		
		arrangeNodes();
	}
	
	private function selectListener(e:MouseEvent):Void
	{
		selectNode(cast e.target);
	}
	
	private function selectNode(node:AnimationBrowserNode):Void
	{
		if (mSelectedAnimation != null)
			mSelectedAnimation.dispose();
		mSelectedAnimationPath = node.path;
		mSelectedAnimation = node.animation.clone(false);
		mSelectedAnimation.sleeping = false;
		
		if (mSelectedAnimation.width != 0 && mSelectedAnimation.height != 0)
		{
			var ratio:Float = mSelectedAnimation.width / mSelectedAnimation.height;
			if (ratio > width / height)
				mSelectedAnimation.scale = Math.min(1, (width * 0.9) / mSelectedAnimation.width);
			else
				mSelectedAnimation.scale = Math.min(1, (height * 0.9) / mSelectedAnimation.height);
		}
		mSelectedNodeHighlight.x = node.x;
		mSelectedNodeHighlight.y = node.y;
		mWindow.addChild(mSelectedNodeHighlight);
		drawConfirmButton(true);
	}
	
	private function drawConfirmButton(enabled:Bool):Void
	{
		if (enabled)
		{
			mConfirmButton.graphics.lineStyle(5, mWindow.color2);
			mConfirmButton.graphics.beginFill(mWindow.color1);
			mConfirmText.textColor = mWindow.color2;
		}
		else
		{
			mConfirmButton.graphics.lineStyle(5, 0x444444);
			mConfirmButton.graphics.beginFill(0x666666);
			mConfirmText.textColor = 0x444444;
		}
		mConfirmButton.graphics.drawRect(2.5, 2.5, mSelectedAnimationDisplay.width - 5, 45);
		mConfirmButton.graphics.endFill();
		mConfirmButton.mouseEnabled = mConfirmButton.buttonMode = mConfirmButton.useHandCursor = enabled;
	}
	
	private function arrangeNodes():Void
	{
		var tempX:Float = 10;
		var tempY:Float = 10;
		var biggest:Float = 0;
		for (n in mNodes)
		{
			if (n.parent == null)
				continue;
			if (mVerticalAlign)
			{
				if (tempX + n.width > mWindow.width)
				{
					tempX = 10;
					tempY += biggest;
					biggest = 0;
				}
				if (n.height > biggest)
					biggest = n.height;
			}
			else
			{
				if (tempY + n.height > mWindow.height)
				{
					tempY = 10;
					tempX += biggest;
					biggest = 0;
				}
				if (n.width > biggest)
					biggest = n.width;
			}	
					
						
			n.x = tempX;
			n.y = tempY;
			
			if (mVerticalAlign)
				tempX += n.width;
			else
				tempY += n.height;
		}
	}
}

private class AnimationBrowserNode extends Sprite
{
	public var path(default, null):String;
	public var ready(get, set):Bool;
	public var animation(default, null):Animation;
	
	private var mLocked:Bool;
	private var mPathDisplay:TextField;
	private var mImageDisplay:Bitmap;
	private var mMask:Sprite;
	private var mLastUpdateTime:Int;
	
	public function new(path:String, width:Float, height:Float, backColor:UInt, textColor:UInt, textSize:Int)
	{
		super();
		this.path = path;
		mLocked = false;
		buttonMode = true;
		useHandCursor = true;
		
		graphics.beginFill(backColor);
		graphics.lineStyle(5, textColor, 0.5);
		graphics.drawRoundRect(0, 0, width, height, Math.min(width, height) * 0.05);
		graphics.endFill();
		mMask = new Sprite();
		mMask.graphics.beginFill(0xFFFFFF);
		mMask.graphics.drawRoundRect(0, 0, width, height, Math.min(width, height) * 0.05);
		mMask.graphics.endFill();
		addChild(mMask);
			
		animation = AnimationManager.request(path);
		
		mImageDisplay = new Bitmap();
		mImageDisplay.bitmapData = new BitmapData(Math.floor(width * 0.9), Math.floor(height * 0.9), true, 0x00000000);
		mImageDisplay.x = width * 0.05;
		mImageDisplay.y = height * 0.05;
		mImageDisplay.alpha = 0.5;
		mImageDisplay.mask = mMask;
		
		mPathDisplay = new TextField();
		mPathDisplay.width = width - 10;
		mPathDisplay.x = 5;
		mPathDisplay.height = height * 0.98;
		mPathDisplay.y = height * 0.01;
		mPathDisplay.mouseEnabled = false;
		mPathDisplay.multiline = true;
		mPathDisplay.defaultTextFormat = new TextFormat("Consolas", textSize, textColor);
		{
			var txt:String = path;
			var arr:Array<String> = txt.split("\\");
			
			for (i in 0...arr.length)
			{
				if (i < arr.length - 1)
				{
					mPathDisplay.text += arr[i];
					mPathDisplay.text += "\\\r";
					for (j in 0...(i + 1))
						mPathDisplay.text += "-> ";
				}
				else
				{
					var arr2:Array<String> = arr[i].split(".");
					for (j in 0...arr2.length)
					{
						if (j < arr2.length - 2)
						{
							mPathDisplay.text += arr2[j];
							mPathDisplay.text += ".\r";
							for (k in 0...i)
								mPathDisplay.text += "   ";
						}
						else
						{
							mPathDisplay.text += arr2[j];
							if (j == arr2.length - 2)
								mPathDisplay.text += ".";
						}
					}
				}
			}
		}
		addChild(mPathDisplay);
	}
	
	private function update(e:Event):Void
	{
		var t:Float = (Lib.getTimer() - mLastUpdateTime) * 0.001;
		mLastUpdateTime = Lib.getTimer();
		if (animation.scale == 1 && animation.width != 0 && animation.height != 0)
		{
			var ratio:Float = animation.width / animation.height;
			if (ratio > width / height)
				animation.scale = Math.min(1, (width * 0.9) / animation.width);
			else
				animation.scale = Math.min(1, (height * 0.9) / animation.height);
		}
		mImageDisplay.bitmapData.fillRect(
				new Rectangle(0, 0, mImageDisplay.bitmapData.width, 
				mImageDisplay.bitmapData.height), 0x00000000);
		animation.progressAndDraw(t, mImageDisplay.bitmapData);
	}
	
	public function dispose():Void
	{
		ready = false;
		mPathDisplay = null;
		animation.dispose();
		animation = null;
	}
	
	private function get_ready():Bool
	{
		return (mLocked && animation.ready);
	}
	
	private function set_ready(val:Bool):Bool
	{
		if (val == animation.sleeping)
		{
			animation.sleeping = !val;
			if (val)
			{
				addEventListener(Event.ENTER_FRAME, update);
				mLastUpdateTime = Lib.getTimer();
				mImageDisplay.bitmapData = new BitmapData(Math.floor(width * 0.9), Math.floor(height * 0.9), true, 0x00000000);
				animation.scale = 1;
			}
			else
			{
				removeEventListener(Event.ENTER_FRAME, update);
				mImageDisplay.bitmapData.dispose();
				mImageDisplay.bitmapData = null;
			}
		}
		return val;
	}
}