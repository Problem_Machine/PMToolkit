package com.problemmachine.animation.animationbrowser;
import com.problemmachine.animation.Animation;
import flash.events.Event;

/**
 * ...
 * @author 
 */
class AnimationBrowserEvent extends Event
{
	public static inline var CANCEL:String = "Cancel Image Select";
	public static inline var CONFIRM:String = "Confirm Image Select";
	
	public var path(default, null):String;
	public var animation(default, null):Animation;

	public function new(path:String, ?animation:Animation, type:String, bubbles:Bool = false, cancelable:Bool = false) 
	{
		super(type, bubbles, cancelable);
		this.animation = animation;
		this.path = path;
	}
	
}