package com.problemmachine.animation.animationmanager;

import com.problemmachine.animation.Animation;
import com.problemmachine.tools.file.FileTools;
import com.problemmachine.tools.file.FileToolsEvent;
import flash.errors.Error;
#if air
import flash.filesystem.File;
#end

class AnimationManager 
{
	public static var rootDirectory(default, set):String = FileTools.rootDirectory;
	
	private static var sAnimations:Map<String, Animation> = new Map<String, Animation>();
	
	private static function set_rootDirectory(val:String):String
	{
		FileTools.formatPath(val);
		if (val.charAt(val.length - 1) != "\\")
			val += "\\";
		if (val != FileTools.formatPath(rootDirectory))
			for (s in sAnimations.keys())
			{
				var newPath:String = FileTools.getRelativePathFromAbsolutePath(val, rootDirectory + s);
				var a:Animation = sAnimations.get(s);
				sAnimations.remove(s);
				sAnimations.set(s, a);
			}
		rootDirectory = val;
	
		return val;
	}
	
	public static inline function isRecognizedType(ext:String):Bool
	{
		return (ext.toLowerCase() == "anx");
	}
	
	public static function addAllResourcesInDirectory(path:String, includeSubDirectories:Bool = true):Void
	{		
		var files:Array<String> = FileTools.getAllFilesIn(rootDirectory, true);
		for (s in files)
			if (isRecognizedType(FileTools.extensionOf(s)))
				load(FileTools.getRelativePathFromAbsolutePath(rootDirectory, s));
	}
	
	public static inline function getAllKnownResources():Array<String>
	{
		var arr = new Array<String>();
		for (s in sAnimations.keys())
			arr.push(s);
		return arr;
	}
	
	public static function load(fileName:String):Void
	{
		fileName = FileTools.formatPath(fileName);
		var path:String = (fileName.indexOf(":") < 0) ? rootDirectory + fileName : fileName;
		
		for (s in sAnimations.keys())
			if (s == fileName)
				return;
				
		var a:Animation = Animation.createFromPath(path, true);
		sAnimations.set(fileName, a);
	}
	
	public static function request(fileName:String, ?startSleeping:Bool):Animation
	{
		fileName = FileTools.formatPath(fileName);
		if (sAnimations.exists(fileName))
			return sAnimations.get(fileName).clone(startSleeping);
		else
			return null;
	}
	
	private static function loadError(e:FileToolsEvent):Void
	{
		trace("WARNING: Could not load animation " + e.path);
	}
}