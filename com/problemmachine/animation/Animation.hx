﻿package com.problemmachine.animation;

import com.problemmachine.animation.event.AnimationEvent;
import com.problemmachine.bitmap.bitmapmanager.BitmapManager;
import com.problemmachine.tools.file.FileTools;
import flash.display.BitmapData;
import flash.display.BlendMode;
import flash.errors.Error;
import flash.events.EventDispatcher;
import flash.filters.BitmapFilter;
import flash.geom.Point;
import flash.geom.Rectangle;
import haxe.xml.Fast;

class Animation extends EventDispatcher
{
	static public var verbose:Bool = false;
	
	private var mFrames:Array<Frame>;
	private var mFilters:Array<BitmapFilter>;
	
	public var blendMode(default, default):BlendMode;
	public var sleeping(default, set):Bool;	
	public var ready(get, null):Bool;
	public var complete(default, null):Bool;
	public var currentFrame(default, set):Int;
	public var animationSpeed(default, default):Float;
	public var animationMode(default, default):AnimationMode;
	public var red(default, set):Float;
	public var green(default, set):Float;
	public var blue(default, set):Float;
	public var alpha(default, set):Float;
	public var hue(default, set):Float;
	public var saturation(default, set):Float;
	public var brightness(default, set):Float;
	public var scale(default, set):Float;
	public var rotation(default, set):Float;
	public var flipHorizontal(default, set):Bool;
	public var flipVertical(default, set):Bool;
	public var cacheEnabled(default, set):Bool;
	
	public var width(get, never):Float;
	public var height(get, never):Float;
	public var numFrames(get, never):Int;
	public var rotationDegrees(get, set):Float;
	public var rotationRadians(get, set):Float;
	
	private var mCurrentDelay:Float;
	private var mEnteringFrame:Bool;
	
	public function new(startSleeping:Bool = true, ?startMode:AnimationMode, cacheEnabled:Bool = true) 
	{			
		super();
		mFrames = new Array<Frame>();
		mFilters = new Array<BitmapFilter>();
		this.cacheEnabled = cacheEnabled;
		blendMode = BlendMode.NORMAL;
		sleeping = startSleeping;
		currentFrame = 0;
		animationSpeed = 1;
		animationMode = startMode != null ? startMode : AnimationMode.Stop;
		red = green = blue = alpha = scale = 1;
		hue = 0;
		saturation = 1;
		brightness = 0;
		rotation = 0;
		flipHorizontal = false;
		flipVertical = false;
		mCurrentDelay = 0;
		ready = false;
		complete = false;
		mEnteringFrame = false;
	}
	
	public static function createFromXML(xml:Xml, startSleeping:Bool = true):Animation
	{
		var ba:Animation = new Animation(startSleeping);
		ba.loadInformationFromXML(xml);
		
		return ba;
	}
	
	@:access(com.problemmachine.animation.Frame.mAsset)
	@:access(com.problemmachine.animation.Frame.mRegistrationPoints)
	public function toXML():Xml
	{
		var xml:Xml = Xml.parse(
			"<animation>\n" +
			"	<alpha>" + alpha + "</alpha>\n" +
			"	<red>" + red + "</red>\n" +
			"	<green>" + green + "</green>\n" +
			"	<blue>" + blue + "</blue>\n" +
			"	<hue>" + hue + "</hue>\n" +
			"	<saturation>" + saturation + "</saturation>\n" +
			"	<brightness>" + brightness + "</brightness>\n" +
			"	<scale>" + scale + "</scale>\n" +
			"	<rotation>" + rotation + "</rotation>\n" +
			"	<animationSpeed>" + animationSpeed + "</animationSpeed>\n" +
			"	<animationMode>" + Std.string(animationMode) + "</animationMode>\n" +
			"	<blendMode>" + Std.string(blendMode).toUpperCase() + "</blendMode>\n" + 
			//For some reason blendMode strings out in lower case even though it needs upper to construct the enum
			"	<cacheEnabled>" + cacheEnabled + "</cacheEnabled>\n" +
			"	<flipHorizontal>" + flipHorizontal + "</flipHorizontal>\n" +
			"	<flipVertical>" + flipVertical + "</flipVertical>\n" +
			"</animation>").firstElement();
		for (f in mFrames)
		{
			var x:Xml = Xml.parse(
				"<frame>\n" +
				"	<resource>" + f.resource + "</resource>\n" +
				"	<delay>" + f.delay + "</delay>\n" +
				"</frame>").firstElement();
			for (i in 0...f.mRegistrationPoints.length)
			{
				if (f.mRegistrationPoints[i] != null)
					x.addChild(Xml.parse(
						"<point>\n" + 
						"	<index>" + Std.string(i) + "</index>\n" +
						"	<x>" + Std.string(f.mRegistrationPoints[i].x) + "</x>\n" +
						"	<y>" + Std.string(f.mRegistrationPoints[i].y) + "</y>\n" +
						"</point>").firstElement());
			}
			if (f.offset.x != 0)
				x.addChild(Xml.parse("<offsetX>" + f.offset.x + "</offsetX>").firstElement());
			if (f.offset.y != 0)
				x.addChild(Xml.parse("<offsetY>" + f.offset.y + "</offsetY>").firstElement());
			// if the asset's null we don't know whether clipRect is non-default or not so always save it
			if (f.resource != "")
				if (f.clipRect != null && (f.clipRect.x != 0 || f.clipRect.y != 0 || f.mAsset == null || f.clipRect.width != f.mAsset.width || f.clipRect.height != f.mAsset.height))
				{
					x.addChild(Xml.parse("<clipRectX>" + f.clipRect.x + "</clipRectX>").firstElement());
					x.addChild(Xml.parse("<clipRectY>" + f.clipRect.y + "</clipRectY>").firstElement());
					x.addChild(Xml.parse("<clipRectWidth>" + f.clipRect.width + "</clipRectWidth>").firstElement());
					x.addChild(Xml.parse("<clipRectHeight>" + f.clipRect.height + "</clipRectHeight>").firstElement());
				}
				
			xml.addChild(x);
		}
		return xml;
	}
	
	public static function createFromPath(path:String, startSleeping:Bool = true):Animation
	{
		var ext:String = path.substr(path.length - 3).toLowerCase();
		if (BitmapManager.isRecognizedType(ext))
		{
			var a:Animation = new Animation(startSleeping, AnimationMode.Stop, true);
			a.addFrame(path, 1);
			return a;
		}
		else
		{
			var a:Animation = new Animation(startSleeping);
			a.loadInformationFromPath(path);
			return a;
		}		
	}
	
	public static function isValidXML(xml:Xml):Bool
	{
		while (xml != null && xml.nodeName != "animation")
			xml = xml.firstElement();
		return (xml != null);
	}
	
	private inline function loadInformationFromPath(path:String):Void
	{	
		var xml:Xml = Xml.parse(cast FileTools.immediateReadFrom(path)).firstElement();
		while (xml != null && xml.nodeName != "animation")
			xml = xml.firstElement();
		if (xml == null)
			throw new Error("Animation.XMLListener() -- ERROR: Bad XML Data");
		loadInformationFromXML(xml); 	
	}

	private function loadInformationFromXML(xml:Xml):Void
	{
		if (mFrames == null) // If the animation is disposed before the load operation completes
			return; 
		var fxml:Fast = new Fast(xml);
		
		if (fxml.hasNode.alpha)
			alpha = Std.parseFloat(fxml.node.alpha.innerData);
		if (fxml.hasNode.red)
			red = Std.parseFloat(fxml.node.red.innerData);
		if (fxml.hasNode.green)
			green = Std.parseFloat(fxml.node.green.innerData);
		if (fxml.hasNode.blue)
			blue = Std.parseFloat(fxml.node.blue.innerData);
			
		if (fxml.hasNode.hue)
			hue = Std.parseFloat(fxml.node.hue.innerData);
		if (fxml.hasNode.saturation)
			saturation = Std.parseFloat(fxml.node.saturation.innerData);
		if (fxml.hasNode.brightness)
			brightness = Std.parseFloat(fxml.node.brightness.innerData);
		
		if (fxml.hasNode.scale)
			scale = Std.parseFloat(fxml.node.scale.innerData);
		if (fxml.hasNode.rotation)
			rotation = Std.parseFloat(fxml.node.rotation.innerData);
		if (fxml.hasNode.animationSpeed)
			animationSpeed = Std.parseFloat(fxml.node.animationSpeed.innerData);
		if (fxml.hasNode.animationMode)
			animationMode = Type.createEnum(AnimationMode, fxml.node.animationMode.innerData);
		
		#if openfl
			if (fxml.hasNode.blendMode)
				blendMode = fxml.node.blendMode.innerData.toLowerCase();
		#else
			if (fxml.hasNode.blendMode)
				blendMode = Type.createEnum(BlendMode, fxml.node.blendMode.innerData.toUpperCase());
		#end
		if (fxml.hasNode.cacheEnabled)
			cacheEnabled = (fxml.node.cacheEnabled.innerData == Std.string(true));
		if (fxml.hasNode.flipHorizontal)
			flipHorizontal = (fxml.node.flipHorizontal.innerData == Std.string(true));
		if (fxml.hasNode.flipVertical)
			flipVertical = (fxml.node.flipVertical.innerData == Std.string(true));
			
		for (f in fxml.nodes.frame)
		{
			if (!f.hasNode.resource || f.node.resource.x.firstChild().nodeValue == "")
				continue;
			
			var delay:Float = Std.parseFloat(f.node.delay.innerData);
			if (delay <= 0) 
				delay = 0.1;
			
			var offsetX:Float = f.hasNode.offsetX ? Std.parseFloat(f.node.offsetX.innerData) : 0;
			var offsetY:Float = f.hasNode.offsetY ? Std.parseFloat(f.node.offsetY.innerData) : 0;
			
			var clipRectX:Float = f.hasNode.clipRectX ? Std.parseFloat(f.node.clipRectX.innerData) : 0;
			var clipRectY:Float = f.hasNode.clipRectY ? Std.parseFloat(f.node.clipRectY.innerData) : 0;
			var clipRectWidth:Float = f.hasNode.clipRectWidth ? Std.parseFloat(f.node.clipRectWidth.innerData) : 0;
			var clipRectHeight:Float = f.hasNode.clipRectHeight ? Std.parseFloat(f.node.clipRectHeight.innerData) : 0;
			var nullClipRect:Bool = (clipRectWidth == 0 || clipRectHeight == 0);
			
			addFrame(f.node.resource.innerData, delay, new Point(offsetX, offsetY), 
				nullClipRect ? null : new Rectangle(clipRectX, clipRectY, clipRectWidth, clipRectHeight));
			for (x in f.nodes.point)
				mFrames[mFrames.length - 1].setRegistrationPoint(Std.parseInt(x.node.index.innerData), 
					new Point(Std.parseFloat(x.node.x.innerData), Std.parseFloat(x.node.y.innerData)));
		}
	}
	
	public function clone(?startSleeping:Bool):Animation
	{
		var a:Animation = new Animation();
		if (startSleeping != null)
			a.sleeping = startSleeping;
		else
			a.sleeping = sleeping;
		a.cacheEnabled = this.cacheEnabled;
		a.mFilters = this.mFilters.copy();
		a.currentFrame = this.currentFrame;
		a.mCurrentDelay = this.mCurrentDelay;
		a.animationSpeed = this.animationSpeed;
		a.animationMode = this.animationMode;
		a.red = this.red;
		a.green = this.green;
		a.blue = this.blue;
		a.alpha = this.alpha;
		a.hue = this.hue;
		a.saturation = this.saturation;
		a.brightness = this.brightness;
		a.scale = this.scale;
		a.rotation = this.rotation;
		a.blendMode = this.blendMode;
		a.flipHorizontal = this.flipHorizontal;
		a.flipVertical = this.flipVertical;
		for (f in mFrames)
		{
			f = f.clone(a.sleeping);
			f.parent = a;
			a.mFrames.push(f);
		}
			
		return a;
	}
	
	public function progress(time:Float):Void
	{
		var mode:AnimationMode = animationMode;
		if (animationSpeed < 0)
		{
			mode = reverseMode(mode);
			animationSpeed = -animationSpeed;
		}
		
		mCurrentDelay += Math.abs(animationSpeed) * time;
		
		if (mFrames.length == 0)
			return;
		var delay:Float = getActiveFrame().delay;
		
			
		while (mCurrentDelay >= delay)
		{
			delay = getActiveFrame().delay;
			mCurrentDelay -= delay;
			mEnteringFrame = true;
			switch (mode)
			{
				case AnimationMode.ForwardLoop:
					currentFrame = (currentFrame + 1) % mFrames.length;
					if (currentFrame == 0)
						dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_LOOP));
					
				case AnimationMode.ReverseLoop:
					if (--currentFrame < 0)
					{
						currentFrame = mFrames.length - 1;
						dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_LOOP));
					}
					
				case AnimationMode.ForwardPingpong:
					if (++currentFrame >= mFrames.length)
					{
						animationMode = reverseMode(animationMode);
						dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_BOUNCE));
					}
					
				case AnimationMode.ReversePingpong:
					if (--currentFrame <= 0)
					{
						animationMode = reverseMode(animationMode);
						dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_BOUNCE));
					}
					
				case AnimationMode.ForwardStop:
					if (++currentFrame >= mFrames.length - 1)
					{
						currentFrame = mFrames.length - 1;
						mCurrentDelay = 0;
						if (!complete)
						{
							complete = true;
							dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_FINISH));
						}
					}
					
				case AnimationMode.ReverseStop:
					if (--currentFrame <= 0)
					{
						currentFrame = 0;
						mCurrentDelay = 0;
						if (!complete)
						{
							complete = true;
							dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_FINISH));
						}
					}
					
				case AnimationMode.Random:
					currentFrame = Std.random(mFrames.length);
				case AnimationMode.Stop:
					//nothing
			}
			mode = animationMode;
		}
	}		
	
	private static var ORIGIN:Point = new Point();
	public inline function draw(surface:BitmapData, ?offset:Point, smoothing:Bool = false, ?stencilImage:BitmapData, ?stencilOffset:Point):Void
	{
		if (mFrames.length == 0)
		{
			if (verbose) trace ("Warning: Tried to draw Animation with no frames");
			return;
		}
		else if (sleeping)
		{
			if (verbose) trace ("Warning: Tried to draw sleeping Animation");
			return;
		}
		else
		{
			if (offset == null)
				offset = ORIGIN;
		
			if (mEnteringFrame)
				dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_FRAME));
			mEnteringFrame = false;
			
			
			getActiveFrame().draw(surface, offset, (!cacheEnabled && mFilters.length > 0) ? mFilters : null, blendMode, smoothing, stencilImage, stencilOffset);	
			return;
		}
	}
	
	public inline function progressAndDraw(time:Float, surface:BitmapData, ?offset:Point, smoothing:Bool = false):Void
	{
		progress(time);
		draw(surface, offset, smoothing);
	}
	
	public inline function preCacheAllFrames():Void
	{
		if (cacheEnabled)
			for (i in 0...mFrames.length)
				preCacheFrame(i);
	}
	
	@:access(com.problemmachine.animation.Frame.updateCache)
	public inline function preCacheFrame(i:Int):Void
	{
		if (cacheEnabled)
		{
			var f:Frame = getFrame(i);
			if (f != null)
				f.updateCache();
		}
	}
	
	public inline function enableCache():Void
	{
		cacheEnabled = true;
	}
	
	public inline function disableCache():Void
	{
		cacheEnabled = false;
	}
	
	private inline function getActiveFrame():Frame
	{
		return mFrames[currentFrame];
	}
	
	private inline function invalidateCache():Void
	{
		if (cacheEnabled)
			for (f in mFrames)
				f.invalidateCache();
	}	
	
	public inline function dispose():Void
	{
		sleeping = true;
		while (mFrames.length > 0)
			mFrames.pop().dispose();
		mFrames = null;
	}
	
	public function setRegistrationPoint(frame:Int, index:Int, point:Point):Void
	{
		mFrames[frame].setRegistrationPoint(index, point);
	}
	
	public inline function getRegistrationPoint(frame:Int, index:Int, transform:Bool):Point
	{
		return mFrames[frame].getRegistrationPoint(index, transform);
	}
	
	public inline function getCurrentRegistrationPoint(index:Int, transform:Bool):Point
	{
		return getRegistrationPoint(currentFrame, index, transform);
	}
	
	public inline function setFilters(filters:Array<BitmapFilter>):Void
	{
		mFilters = filters.copy();
		invalidateCache();
	}
	
	public function addFilter(filter:BitmapFilter, index:Int = 0xFFFFFF):Int
	{
		for (b in mFilters)
			if (b == filter)
				return -1;
		
		invalidateCache();
		if (index > mFilters.length)
			index = mFilters.length;
		if (index < 0)
			index = 0;
		
		mFilters.insert(index, filter);
		return index;
	}
	
	public function removeFilter(filter:BitmapFilter):Void
	{
		var length:Int = mFilters.length;
		for (i in 1...length + 1)
			if (mFilters[length - i] == filter)
			{
					mFilters.splice(length - i, 1);
					invalidateCache();
					return;
			}
	}
	
	public inline function removeFilterByIndex(index:Int):BitmapFilter
	{
		if (index >= mFilters.length)
			return null;
		else
		{
			invalidateCache();
			var f:BitmapFilter = mFilters[index];
			mFilters.splice(index, 1);
			return f;
		}
	}
	
	public inline function removeAllFilters():Void
	{
		if (mFilters.length != 0)
		{
			invalidateCache();
			mFilters = [];
		}
	}
	
	public inline function pushFilter(filter:BitmapFilter):Int
	{
		var index:Int = -1;
		for (i in 0...mFilters.length)
			if (mFilters[i] == filter)
			{
				index = i;
				break;
			}
		if (index >= 0)
			return index;
		else
		{
			invalidateCache();
			return (mFilters.push(filter) - 1);
		}
	}
	
	public inline function popFilter():BitmapFilter
	{
		if (mFilters.length == 0)
			return null;
		else
		{
			invalidateCache();
			return mFilters.pop();
		}
	}
	
	public inline function queryFilters():Array<BitmapFilter>
	{
		return mFilters.copy();
	}
	
	public inline function getFilterByIndex(index:Int):BitmapFilter
	{
		return mFilters[index];
	}
	
	public inline function getFilterByType(type:Class<BitmapFilter>):BitmapFilter
	{
		for (f in mFilters)
			if (Std.is(f, type))
				return f;
		return null;
	}
	
	public function reverse():Void
	{
		animationMode = reverseMode(animationMode);
	}
	
	private inline function reverseMode(mode:AnimationMode):AnimationMode
	{
		return switch(mode)
		{
			case AnimationMode.ForwardLoop: 		AnimationMode.ReverseLoop;
			case AnimationMode.ForwardStop: 		AnimationMode.ReverseStop;
			case AnimationMode.ForwardPingpong: 	AnimationMode.ReversePingpong;
			case AnimationMode.ReverseLoop: 		AnimationMode.ForwardLoop;
			case AnimationMode.ReverseStop: 		AnimationMode.ForwardStop;
			case AnimationMode.ReversePingpong: 	AnimationMode.ForwardPingpong;
			default: mode;
		}
	}
	
	public function reset():Void
	{
		mCurrentDelay = 0;
		complete = false;
		currentFrame = switch(animationMode)
		{
			case AnimationMode.ForwardLoop, AnimationMode.ForwardPingpong, AnimationMode.ForwardStop:
				0;
			case AnimationMode.ReverseLoop, AnimationMode.ReversePingpong, AnimationMode.ReverseStop:
				mFrames.length - 1;
			case AnimationMode.Random:
				Std.random(mFrames.length);
			case AnimationMode.Stop:
				currentFrame;
		}
	}
	
	public function getTime():Float
	{
		var time:Float = 0;
		switch(animationMode)
		{
			case AnimationMode.ForwardLoop, AnimationMode.ForwardPingpong, AnimationMode.ForwardStop:
				for (i in 0...currentFrame)
					time += mFrames[i].delay;
				time += mCurrentDelay;
				
			case AnimationMode.ReverseLoop, AnimationMode.ReversePingpong, AnimationMode.ReverseStop:
				for (i in 1...(mFrames.length - currentFrame))
					time += mFrames[mFrames.length - i].delay;
				time += mCurrentDelay;
				
			case AnimationMode.Stop, AnimationMode.Random:
				// also nothing
		}
		return time;
	}
	
	public function gotoTime(time:Float):Void
	{
		reset();
		progress(time);
	}
	
	public function gotoFirst():Void
	{
		currentFrame = 0;
		mCurrentDelay = 0;
		complete = false;
	}
	
	public function gotoLast():Void
	{
		currentFrame = Std.int(Math.max(0, mFrames.length - 1));
		mCurrentDelay = 0;
	}
	
	public function gotoFrame(i:Int):Void
	{
		currentFrame = Std.int(Math.max(0, Math.min(mFrames.length - 1, i)));
		mCurrentDelay = 0;
		if (currentFrame < mFrames.length - 1)
			complete = false;
	}
	
	public function addFrame(resource:String, delay:Float, ?offset:Point, ?clipRect:Rectangle):Void
	{	insertFrame(0xFFFFFF, resource, delay, offset, clipRect);					 }
	
	public function insertFrame(i:Int, resource:String, delay:Float, ?offset:Point, ?clipRect:Rectangle):Void
	{	insertRawFrame(i, new Frame(resource, delay, offset, clipRect, this, sleeping));	}
	public function addRawFrame(frame:Frame):Void
	{	insertRawFrame(0xFFFFFF, frame);					 }
	public function insertRawFrame(i:Int, frame:Frame):Void
	{
		frame.parent = this;
		frame.sleeping = sleeping;
		if (!sleeping && !frame.ready)
		{
			ready = false;
			frame.addEventListener(AnimationEvent.ANIM_READY, checkReady);
		}
		frame.cacheable = cacheEnabled;
		if (i < mFrames.length)
			mFrames.insert(i, frame);
		else
			mFrames.push(frame);
		dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_ADD_FRAME));
	}
	private function checkReady(e:AnimationEvent)
	{
		cast (e.target, EventDispatcher).removeEventListener(AnimationEvent.ANIM_READY, checkReady);
		if (ready)
			dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_READY));
	}
	
	public function removeLastFrame():Void
	{	
		mFrames.pop().dispose();		
		dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_REMOVE_FRAME));
	}
	
	public function removeFrame(i:Int):Void
	{
		if (i >= mFrames.length)
			throw new Error("Animation.removeFrame(): Index " + i + " out of range " + mFrames.length);
		mFrames[i].dispose();
		mFrames.splice(i, 1);
		if (i <= currentFrame)
			--currentFrame;
		dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_REMOVE_FRAME));
	}
	
	public function replaceFrame(i:Int, resource:String, delay:Float, offset:Point, clipRect:Rectangle = null):Void
	{
		if (i >= mFrames.length)
			throw new Error("BitmapFrameSet.replaceFrame(): Index " + i + " out of range " + mFrames.length);
		var f:Frame = new Frame(resource, delay, offset, clipRect, this, sleeping); 
		mFrames[i].dispose();
		mFrames[i] = f;
		if (!sleeping && !f.ready)
		{
			ready = false;
			f.addEventListener(AnimationEvent.ANIM_READY, checkReady);
		}
		dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_REPLACE_FRAME));
	}				
	
	public function getFrame(i:Int):Frame
	{	return mFrames[i];				}
	
	public function getAllFrames():Array<Frame>
	{	return mFrames.copy();		}
	
	private function get_ready():Bool
	{
		if (ready)
			return true;
		if (sleeping)
			return false;
			
		for (f in mFrames)
			if (!f.ready)
				return false;
		return ready = true;
	}
	
	private function set_sleeping(val:Bool):Bool
	{
		if (val != sleeping)
		{
			if (val)
				ready = false;
			for (f in mFrames)
				f.sleeping = val;
		}
		return sleeping = val;
	}
	
	private inline function set_currentFrame(val:Int):Int
	{	return currentFrame = Std.int(Math.max(0, Math.min(mFrames.length - 1, val)));			}
	
	private inline function set_red(val:Float):Float
	{	
		val = Math.max(0, val);
		if (val != red)
			invalidateCache();
		return red = val;			
	}
	private inline function set_green(val:Float):Float
	{	
		val = Math.max(0, val);
		if (val != green)
			invalidateCache();
		return green = val;			
	}
	private inline function set_blue(val:Float):Float
	{	
		val = Math.max(0, val);
		if (val != blue)
			invalidateCache();
		return blue = val;			
	}
	private inline function set_alpha(val:Float):Float
	{	
		if (Math.max(0, val) != alpha)
			invalidateCache();
		alpha = Math.max(0, val);
		return val;			
	}
	
	private inline function set_hue(val:Float):Float
	{	
		if (val != hue)
			invalidateCache();
		return hue = val;			
	}
	private inline function set_saturation(val:Float):Float
	{	
		if (val != saturation)
			invalidateCache();
		return saturation = val;		
	}
	private inline function set_brightness(val:Float):Float
	{	
		if (val != brightness)
			invalidateCache();
		return brightness = val;			
	}
	
	private inline function set_rotation(val:Float):Float
	{	
		while (val < 0)
			++val;
		while (val > 1)
			--val;
		if (rotation != val)
			invalidateCache();
		return rotation = val;
	}
	
	private inline function set_scale(s:Float):Float
	{	
		if (s != scale)
			invalidateCache();
		return scale = s;			
	}
	
	private inline function set_flipHorizontal(val:Bool):Bool
	{
		if (val != flipHorizontal)
			invalidateCache();
		return flipHorizontal = val;
	}
	
	private inline function set_flipVertical(val:Bool):Bool
	{
		if (val != flipVertical)
			invalidateCache();
		return flipVertical = val;
	}
	
	private inline function set_cacheEnabled(val:Bool):Bool
	{
		if (val != cacheEnabled)
		{
			if (val)
				for (f in mFrames)
					f.cacheable = true;
			else
				for (f in mFrames)
					f.cacheable = false;
		}

		return cacheEnabled = val;
	}
	
	private function get_width():Float
	{	
		if (mFrames.length > 0)
			return mFrames[currentFrame].width;		
		else
			return 0;
	}
	
	private function get_height():Float
	{
		if (mFrames.length > 0)
			return mFrames[currentFrame].height;		
		else
			return 0;
	}
	
	private inline function get_numFrames():Int
	{	return mFrames.length;			}
	
	private inline function get_rotationDegrees():Float
	{	return rotation * 360;			}
	private inline function set_rotationDegrees(val:Float):Float
	{	
		rotation = val / 360;				
		return val;	
	}
	
	private inline function get_rotationRadians():Float
	{	return rotation * 2 * Math.PI;	}	
	private inline function set_rotationRadians(val:Float):Float
	{	
		rotation = val / (Math.PI * 2);	
		return val;	
	}
}