package com.problemmachine.animation;
import flash.display.Bitmap;
import flash.display.Sprite;
import flash.events.Event;
import flash.utils.Timer;

class AnimationContainer extends Sprite
{
	public var animation:Animation;
	private var mBitmap:Bitmap;
	private var mMask:Bitmap;
	private var mTimer:Timer;
	private var mLastTime:Int;
	
	public var framerate(default, set):Float;

	public function new() 
	{
		super();
		mTimer = new Timer(1000 / 60);
		framerate = 60;
		mBitmap = new Bitmap();
	}
	
	private function update(e:Event):Void
	{
		
	}
	
	private inline function set_framerate(val:Float):Float
	{
		if (val != framerate)
		{
			framerate = val;
			mTimer.delay = 1000 / val;
		}
		return val;
	}
	
}