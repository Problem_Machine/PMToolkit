package com.problemmachine.util.lock;

class Lock<T>
{
	private var mSources:Array<T>;

	public var isLocked(get, never):Bool;
	
	public function new() 
	{	mSources = new Array<T>();	}
	
	public inline function lock(source:T):Void
	{
		var add:Bool = true;
		for (s in mSources)
			if (s == source)
			{
				add = false;
				break;
			}
		if (add)
			mSources.push(source);
	}
	
	public inline function unlock(source:T):Void
	{
		for (i in 0...mSources.length)
			if (mSources[i] == source)
				mSources.splice(i, 1);
	}
	
	public inline function forceUnlock():Void
	{	mSources = new Array<T>();	}
	
	private function get_isLocked():Bool
	{	return (mSources.length > 0);	}
	
}