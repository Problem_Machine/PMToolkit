package com.problemmachine.util.color.colormatrix;

enum ColorMatrixDefinition 
{
	Protanopia;
	Protanomaly;
	Deuteranopia;
	Deuteranomaly;
	Tritanopia;
	Tritanomaly;
	Achromatopsia;
	Achromatomaly;
	Inversion;
	Sepia;
	BlackAndWhite;
	GrayScale;
	Polaroid;
	WhiteToAlpha;
}