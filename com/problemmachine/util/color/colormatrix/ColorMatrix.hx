package com.problemmachine.util.color.colormatrix;

// built using http://www.quasimondo.com/colormatrix/ColorMatrix.as as basis
// special thanks Mario Klingemann 
class ColorMatrix
{
	// RGB to Luminance conversion constants as found on
	// Charles A. Poynton's colorspace-faq:
	// http://www.faqs.org/faqs/graphics/colorspace-faq/
	
	private static inline var LUMA_R:Float = 0.212671;
	private static inline var LUMA_G:Float = 0.71516;
	private static inline var LUMA_B:Float = 0.072169;
	
	
	// There seem different standards for converting RGB
	// values to Luminance. This is the one by Paul Haeberli:
	
	private static inline var LUMA_R2:Float = 0.3086;
	private static inline var LUMA_G2:Float = 0.6094;
	private static inline var LUMA_B2:Float = 0.0820;
	
	private static inline var ONE_THIRD:Float = 0.3333333333333333333333333333333333333333;
	
	private static var IDENTITY(get, never):Array<Float>;
	
	public var matrix:Array<Float>;
	
	public function new(?initMatrix:Array<Float>) 
	{
		if (initMatrix != null)
			matrix = initMatrix.copy();
		else
			reset();
		while (matrix.length > 4 * 5)
			matrix.pop();
		while (matrix.length < 4 * 5)
			matrix.push(IDENTITY[matrix.length]);
		preHue = postHue = null;
	}
	
	static private inline function get_IDENTITY():Array<Float>
	{
		return [1, 0, 0, 0, 0,
				0, 1, 0, 0, 0,
				0, 0, 1, 0, 0,
				0, 0, 0, 1, 0];
	}

	public inline function reset():Void
	{
		matrix = IDENTITY;
		preHue = postHue = null;
	}
	
	public function isIdentity():Bool
	{
		for (y in 0...4)
			for (x in 0...5)
				if (matrix[y * 5 + x] != (x == y ? 1 : 0))
					return false;
		return true;
	}
		
	public inline function clone():ColorMatrix
	{
		return new ColorMatrix(matrix);
	}
		
	public inline function invert():Void
	{
		fastConcat( -1 ,  0,  0, 0, 255,
				  0 , -1,  0, 0, 255,
				  0 ,  0, -1, 0, 255,
				  0,   0,  0, 1,   0);
	}
		
	public inline function adjustSaturation(sat:Float):Void
	{
		var invSat:Float = (1 - sat);
		var rLum:Float = (invSat * LUMA_R);
		var gLum:Float = (invSat * LUMA_G);
		var bLum:Float = (invSat * LUMA_B);
		
		fastConcat((rLum + sat), gLum, bLum, 0, 0, 
				rLum, (gLum + sat), bLum, 0, 0, 
				rLum, gLum, (bLum + sat), 0, 0, 
				0, 0, 0, 1, 0);
	
	}
	
	public inline function adjustContrast(r:Float, ?g:Float, ?b:Float):Void
	{
		if (g == null) g = r;
		if (b == null) b = r;
		r += 1;
		g += 1;
		b += 1;
		
		fastConcat(r, 0, 0, 0, (128 * (1 - r)), 
				0, g, 0, 0, (128 * (1 - g)), 
				0, 0, b, 0, (128 * (1 - b)), 
				0, 0, 0, 1, 0);
	}
	  
	
	public inline function adjustBrightness(r:Float, ?g:Float, ?b:Float):Void
	{
		if (g == null) g = r;
		if (b == null) b = r;
		r *= 256;
		g *= 256;
		b *= 256;
		
		fastConcat(1, 0, 0, 0, r, 
				0, 1, 0, 0, g, 
				0, 0, 1, 0, b, 
				0, 0, 0, 1, 0);
	}
	
	public inline function adjustHue(rotation:Float):Void
	{
		adjustHueRadians(rotation * Math.PI);
	}
	public inline function adjustHueDegrees(degrees:Float):Void
	{
		adjustHueRadians((degrees / 180) * Math.PI);
	}  
	public inline function adjustHueRadians(radians:Float):Void
	{
		var cos:Float = Math.cos(radians);
		var sin:Float = Math.sin(radians);
		fastConcat(((LUMA_R + (cos * (1 - LUMA_R))) + (sin * -(LUMA_R))), ((LUMA_G + (cos * -(LUMA_G))) + (sin * -(LUMA_G))), ((LUMA_B + (cos * -(LUMA_B))) + (sin * (1 - LUMA_B))), 0, 0, 
				((LUMA_R + (cos * -(LUMA_R))) + (sin * 0.143)), ((LUMA_G + (cos * (1 - LUMA_G))) + (sin * 0.14)), ((LUMA_B + (cos * -(LUMA_B))) + (sin * -0.283)), 0, 0, 
				((LUMA_R + (cos * -(LUMA_R))) + (sin * -((1 - LUMA_R)))), ((LUMA_G + (cos * -(LUMA_G))) + (sin * LUMA_G)), ((LUMA_B + (cos * (1 - LUMA_B))) + (sin * LUMA_B)), 0, 0, 
				0, 0, 0, 1, 0);
	}  
	
	private var preHue:ColorMatrix;
	private var postHue:ColorMatrix;
	public inline function rotateHue(degrees:Float):Void
	{
		if (preHue == null)
		{
			var greenRotation:Float = 39.182655;
			preHue = new ColorMatrix();
			preHue.rotateRed(45);
			preHue.rotateGreen(-greenRotation );

			var lum:Array<Float> = [ LUMA_R2, LUMA_G2, LUMA_B2, 1.0 ];

			preHue.transformVector(lum);

			var red:Float = lum[0] / lum[2];
			var green:Float = lum[1] / lum[2];

			preHue.shearBlue(red, green);

			postHue = new ColorMatrix();
			postHue.shearBlue( -red, -green);
			postHue.rotateGreen(greenRotation);
			postHue.rotateRed(-45);
		}
		
		fastConcat(preHue.matrix[0], preHue.matrix[1], preHue.matrix[2], preHue.matrix[3], preHue.matrix[4], 
				preHue.matrix[5], preHue.matrix[6], preHue.matrix[7], preHue.matrix[8], preHue.matrix[9], 
				preHue.matrix[10], preHue.matrix[11], preHue.matrix[12], preHue.matrix[13], preHue.matrix[14], 
				preHue.matrix[15], preHue.matrix[16], preHue.matrix[17], preHue.matrix[18], preHue.matrix[19]);
		rotateBlue(degrees);
		fastConcat(postHue.matrix[0], postHue.matrix[1], postHue.matrix[2], postHue.matrix[3], postHue.matrix[4], 
				postHue.matrix[5], postHue.matrix[6], postHue.matrix[7], postHue.matrix[8], postHue.matrix[9], 
				postHue.matrix[10], postHue.matrix[11], postHue.matrix[12], postHue.matrix[13], postHue.matrix[14], 
				postHue.matrix[15], postHue.matrix[16], postHue.matrix[17], postHue.matrix[18], postHue.matrix[19]);
	}
	
	public inline function luminanceToAlpha():Void
	{
		fastConcat(0, 0, 0, 0, 255, 
				0, 0, 0, 0, 255, 
				0, 0, 0, 0, 255, 
				LUMA_R, LUMA_G, LUMA_B, 0, 0);
	}
	
	public inline function adjustAlphaContrast(amount:Float):Void
	{
		amount += 1;
		fastConcat(1, 0, 0, 0, 0, 
				0, 1, 0, 0, 0, 
				0, 0, 1, 0, 0, 
				0, 0, 0, amount, (128 * (1 - amount)));
	}
	
	public inline function colorize(r:Float, g:Float, b:Float, amount:Float = 1):Void
	{
		r = Math.max(0, Math.min(1, r));
		g = Math.max(0, Math.min(1, g));
		b = Math.max(0, Math.min(1, b));
		var invAmount:Float = (1 - amount);
		
		fastConcat((invAmount + ((amount * r) * LUMA_R)), ((amount * r) * LUMA_G), ((amount * r) * LUMA_B), 0, 0, 
				((amount * g) * LUMA_R), (invAmount + ((amount * g) * LUMA_G)), ((amount * g) * LUMA_B), 0, 0, 
				((amount * b) * LUMA_R), ((amount * b) * LUMA_G), (invAmount + ((amount * b) * LUMA_B)), 0, 0, 
				0, 0, 0, 1, 0);
	}
	
	
	public inline function setChannels( r:Int = 1, g:Int = 2, b:Int = 4, a:Int = 8):Void
	{
		var rf:Float = ((r & 1 == 1 ? 1 : 0) + (r & 2 == 2 ? 1 : 0) + (r & 4 == 4 ? 1 : 0) + (r & 8 == 8 ? 1 : 0));
		if (rf > 0)
			rf = (1 / rf);
			
		var gf:Float = ((g & 1 == 1 ? 1 : 0) + (g & 2 == 2 ? 1 : 0) + (g & 4 == 4 ? 1 : 0) + (g & 8 == 8 ? 1 : 0));
		if (gf > 0)
			gf = (1 / gf);
		
		var bf:Float = ((b & 1 == 1 ? 1 : 0) + (b & 2 == 2 ? 1 : 0) + (b & 4 == 4 ? 1 : 0) + (b & 8 == 8 ? 1 : 0));
		if (bf > 0)
			bf = (1 / bf);
		
		var af:Float = ((a & 1 == 1 ? 1 : 0) + (a & 2 == 2 ? 1 : 0) + (a & 4 == 4 ? 1 : 0) + (a & 8 == 8 ? 1 : 0));
		if (af > 0)
			af = (1 / af);
		
		fastConcat((((r & 1) == 1)) ? rf : 0, (((r & 2) == 2)) ? rf : 0, (((r & 4) == 4)) ? rf : 0, (((r & 8) == 8)) ? rf : 0, 0, 
				(((g & 1) == 1)) ? gf : 0, (((g & 2) == 2)) ? gf : 0, (((g & 4) == 4)) ? gf : 0, (((g & 8) == 8)) ? gf : 0, 0, 
				(((b & 1) == 1)) ? bf : 0, (((b & 2) == 2)) ? bf : 0, (((b & 4) == 4)) ? bf : 0, (((b & 8) == 8)) ? bf : 0, 0, 
				(((a & 1) == 1)) ? af : 0, (((a & 2) == 2)) ? af : 0, (((a & 4) == 4)) ? af : 0, (((a & 8) == 8)) ? af : 0, 0);
	}
	
	
	public inline function blend(mat:Array<Float>, amount:Float):Void
	{
		for (i in 0...matrix.length)
			matrix[i] = (((1 - amount) * matrix[i]) + (amount * mat[i]));
	}
	
	public inline function average(r:Float=ONE_THIRD, g:Float=ONE_THIRD, b:Float=ONE_THIRD):Void
	{
		fastConcat(r, g, b, 0, 0, 
				r, g, b, 0, 0, 
				r, g, b, 0, 0, 
				0, 0, 0, 1, 0);
	}
	
	public inline function threshold(threshold:Float, factor:Float=256):Void
	{
		fastConcat((LUMA_R * factor), (LUMA_G * factor), (LUMA_B * factor), 0, (-(factor) * threshold), 
				(LUMA_R * factor), (LUMA_G * factor), (LUMA_B * factor), 0, (-(factor) * threshold), 
				(LUMA_R * factor), (LUMA_G * factor), (LUMA_B * factor), 0, (-(factor) * threshold), 
				0, 0, 0, 1, 0);
	}
	
	public inline function randomize(amount:Float=1):Void
	{
		var invAmount:Float = (1 - amount);
		var ran:Void->Float = function():Float	{	return(( Math.random() * 2) - 1);	};
	   
		fastConcat(invAmount + (amount * ran()), amount * ran(), amount * ran(), 0, (amount * 0xFF) * ran(), 
				amount * ran(), invAmount + (amount * ran()), amount * ran(), 0, (amount * 0xFF) * ran(), 
				amount * ran(), amount * ran(), invAmount + (amount * ran()), 0, (amount * 0xFF) * ran(), 
				0, 0, 0, 1, 0);
	}
	
	public inline function setMultipliers(red:Float = 1, green:Float = 1, blue:Float = 1, alpha:Float = 1 ):Void
	{	
		fastConcat(red, 0, 0, 0, 0,
					0, green, 0, 0, 0,
					0, 0, blue, 0, 0,
					0, 0, 0, alpha, 0);
	}
	
	public inline function clearChannels( red:Bool = false, green:Bool = false, blue:Bool = false, alpha:Bool = false):Void
	{
		if ( red )
			matrix[0] = matrix[1] = matrix[2] = matrix[3] = matrix[4] = 0;
		if ( green )
			matrix[5] = matrix[6] = matrix[7] = matrix[8] = matrix[9] = 0;
		if ( blue )
			matrix[10] = matrix[11] = matrix[12] = matrix[13] = matrix[14] = 0;
		if ( alpha )
			matrix[15] = matrix[16] = matrix[17] = matrix[18] = matrix[19] = 0;
	}
	
	public inline function thresholdAlpha( threshold:Float, factor:Float = 256):Void
	{
		fastConcat(1, 0, 0, 0, 0, 
				0, 1, 0, 0, 0, 
				0, 0, 1, 0, 0, 
				0, 0, 0, factor, (-factor * threshold));
	}
	
	public inline function averageRGBToAlpha():Void
	{
		fastConcat(0, 0, 0, 0, 255, 
				0, 0, 0, 0, 255, 
				0, 0, 0, 0, 255, 
				ONE_THIRD, ONE_THIRD, ONE_THIRD, 0, 0);
	}
	
	public inline function invertAlpha():Void
	{
		fastConcat(1, 0, 0, 0, 0, 
				0, 1, 0, 0, 0, 
				0, 0, 1, 0, 0, 
				0, 0, 0, -1, 255);
	}
	
	public inline function rgbToAlpha(r:Float, g:Float, b:Float):Void
	{
		fastConcat(0, 0, 0, 0, 255, 
				0, 0, 0, 0, 255, 
				0, 0, 0, 0, 255, 
				r, g, b, 0, 0);
	}
	
	public inline function setAlpha(alpha:Float):Void
	{
		fastConcat(1, 0, 0, 0, 0, 
				0, 1, 0, 0, 0, 
				0, 0, 1, 0, 0, 
				0, 0, 0, alpha, 0);
	}
	
	public inline function concat(mat:Array<Float>):Void
	{
		var temp:Array<Float> = [];
		
		for (y in 0...4)
			for (x in 0...5)
				temp[y * 5 + x] = mat[y * 5]   	* matrix[x] + 
							  mat[y * 5 + 1]	* matrix[x +  5] + 
							  mat[y * 5 + 2]	* matrix[x + 10] + 
							  mat[y * 5 + 3]	* matrix[x + 15] + (x == 4 ? mat[y * 5 + 4] : 0);
		
		matrix = temp;
	}
	
	static private var temp:Array<Float> = [];
	static private var paramTemp:Array<Float> = [];
	public inline function fastConcat(a:Float, b:Float, c:Float, d:Float, e:Float, 
										f:Float, g:Float, h:Float, i:Float, j:Float, 
										k:Float, l:Float, m:Float, n:Float, o:Float, 
										p:Float, q:Float, r:Float, s:Float, t:Float):Void
	{
		// a concat function that doesn't create new arrays
		var mat:Array<Float> = paramTemp;
		mat[0] = a;		mat[1] = b;		mat[2] = c;			mat[3] = d;		mat[4] = e;
		mat[5] = f;		mat[6] = g;		mat[7] = h;			mat[8] = i;		mat[9] = j;
		mat[10] = k;	mat[11] = l;		mat[12] = m;	mat[13] = n;	mat[14] = o;
		mat[15] = p;	mat[16] = q;		mat[17] = r;	mat[18] = s;	mat[19] = t;
		
		for (y in 0...4)
			for (x in 0...5)
				temp[y * 5 + x] = mat[y * 5]   	* matrix[x] + 
							  mat[y * 5 + 1]	* matrix[x +  5] + 
							  mat[y * 5 + 2]	* matrix[x + 10] + 
							  mat[y * 5 + 3]	* matrix[x + 15] + (x == 4 ? mat[y * 5 + 4] : 0);
		
		var t:Array<Float> = matrix;
		matrix = temp;
		temp = t;
	}
	
	public inline function rotateRed(degrees:Float):Void
	{
		rotateColor(degrees, 2, 1 ); 
		degrees *= Math.PI / 180;
		var cos:Float = Math.cos(degrees);
		var sin:Float = Math.sin(degrees);
		fastConcat(1, 0, 0, 0, 0,
					0, cos, -sin, 0, 0,
					0, sin, cos, 0, 0,
					0, 0, 0, 1, 0);
	}
	
	public inline function rotateGreen(degrees:Float):Void
	{
		rotateColor(degrees, 0, 2 ); 
		degrees *= Math.PI / 180;
		var cos:Float = Math.cos(degrees);
		var sin:Float = Math.sin(degrees);
		fastConcat(cos, 0, sin, 0, 0,
					0, 1, 0, 0, 0,
					-sin, 0, cos, 0, 0,
					0, 0, 0, 1, 0);
	}
	
	public inline function rotateBlue(degrees:Float):Void
	{
		degrees *= Math.PI / 180;
		var cos:Float = Math.cos(degrees);
		var sin:Float = Math.sin(degrees);
		fastConcat(cos, -sin, 0, 0, 0,
					sin, cos, 0, 0, 0,
					0, 0, 1, 0, 0,
					0, 0, 0, 1, 0);
	}
	
	private inline function rotateColor(degrees:Float, x:Int, y:Int):Void
	{
		  degrees *= Math.PI / 180;
		  var mat:Array<Float> = IDENTITY;
		  mat[ x + x * 5 ] = mat[ y + y * 5 ] = Math.cos(degrees);
		  mat[ y + x * 5 ] = Math.sin(degrees);
		  mat[ x + y * 5 ] = -Math.sin(degrees);
		  concat(mat);
	}
	
	public inline function shearRed(green:Float, blue:Float):Void
	{
		shearColor( 0, 1, green, 2, blue );
	}
	
	public inline function shearGreen(red:Float, blue:Float):Void
	{
		shearColor( 1, 0, red, 2, blue );
	}
	
	public inline function shearBlue(red:Float, green:Float):Void
	{
		shearColor( 2, 0, red, 1, green );
	}
	
	private inline function shearColor(x:Int, y1:Int, d1:Float, y2:Int, d2:Float):Void
	{
		var mat:Array<Float> = IDENTITY;
		mat[ y1 + x * 5 ] = d1;
		mat[ y2 + x * 5 ] = d2;
		concat( mat );
	}
	
	public inline function transformColor(color:UInt):UInt
	{
		var a:Int = ( color >>> 24 ) & 0xFF;
		var r:Int = ( color >>> 16 ) & 0xFF;
		var g:Int = ( color >>> 8 ) & 0xFF;
		var b:Int =  color & 0xFF;
		
		var r2:Int = Math.round(0.5 + r * matrix[0] + g * matrix[1] + b * matrix[2] + a * matrix[3] + matrix[4]);
		var g2:Int = Math.round(0.5 + r * matrix[5] + g * matrix[6] + b * matrix[7] + a * matrix[8] + matrix[9]);
		var b2:Int = Math.round(0.5 + r * matrix[10] + g * matrix[11] + b * matrix[12] + a * matrix[13] + matrix[14]);
		var a2:Int = Math.round(0.5 + r * matrix[15] + g * matrix[16] + b * matrix[17] + a * matrix[18] + matrix[19]);
		
		if ( a2 < 0 ) a2 = 0;
		else if ( a2 > 0xFF ) a2 = 0xFF;
		if ( r2 < 0 ) r2 = 0;
		else if ( r2 > 0xFF ) r2 = 0xFF;
		if ( g2 < 0 ) g2 = 0;
		else if ( g2 > 0xFF ) g2 = 0xFF;
		if ( b2 < 0 ) b2 = 0;
		else if ( b2 > 0xFF ) b2 = 0xFF;
		
		return (a2<<24 | r2<<16 | g2<<8 | b2);
	}
	
	
	public inline function transformVector(values:Array<Float>):Void
	{
		if (values.length != 4) return;
		
		var r:Float = values[0] * matrix[0] + values[1] * matrix[1] + values[2] * matrix[2] + values[3] * matrix[3] + matrix[4];
		var g:Float = values[0] * matrix[5] + values[1] * matrix[6] + values[2] * matrix[7] + values[3] * matrix[8] + matrix[9];
		var b:Float = values[0] * matrix[10] + values[1] * matrix[11] + values[2] * matrix[12] + values[3] * matrix[13] + matrix[14];
		var a:Float = values[0] * matrix[15] + values[1] * matrix[16] + values[2] * matrix[17] + values[3] * matrix[18] + matrix[19];
		
		values[0] = r;
		values[1] = g;
		values[2] = b;
		values[3] = a;
	}	
	
	static public function generatePreset(definition:ColorMatrixDefinition):ColorMatrix
	{
		// color deficiency values copied from http://www.nofunc.com/Color_Matrix_Library/ 
		// additional values copied from https://docs.rainmeter.net/tips/colormatrix-guide/
		
		switch (definition)
		{
			case ColorMatrixDefinition.Protanopia:	// Red Blindness
				return new ColorMatrix([
						0.567, 0.433, 0, 0, 0,
						0.558, 0.442, 0, 0, 0,
						0, 0.242, 0.758, 0, 0,
						0, 0, 0, 1, 0]);
			case ColorMatrixDefinition.Protanomaly: // Red Weakness
				return new ColorMatrix([
						0.817, 0.183, 0, 0, 0,
						0.333, 0.667, 0, 0, 0,
						0, 0.125, 0.875, 0, 0,
						0, 0, 0, 1, 0]);
			case ColorMatrixDefinition.Deuteranopia: // Green Blindness
				return new ColorMatrix([
						0.625, 0.375, 0, 0, 0,
						0.7, 0.3, 0, 0, 0,
						0, 0.3, 0.7, 0, 0,
						0, 0, 0, 1, 0]);
			case ColorMatrixDefinition.Deuteranomaly: // Green Weakness
				return new ColorMatrix([
						0.8, 0.2, 0, 0, 0,
						0.258, 0.742, 0, 0, 0,
						0, 0.142, 0.858, 0, 0,
						0, 0, 0, 1, 0]);
			case ColorMatrixDefinition.Tritanopia:	// Yellow Blindness
				return new ColorMatrix([
						0.95, 0.05, 0, 0, 0,
						0, 0.433, 0.567, 0, 0,
						0, 0.475, 0.525, 0, 0,
						0, 0, 0, 1, 0]);
			case ColorMatrixDefinition.Tritanomaly:	// Yellow Weakness
				return new ColorMatrix([
						0.967, 0.033, 0, 0, 0,
						0, 0.733, 0.267, 0, 0,
						0, 0.183, 0.817, 0, 0,
						0, 0, 0, 1, 0]);
			case ColorMatrixDefinition.Achromatopsia:	// Color Blindness
				return new ColorMatrix([
						0.299, 0.587, 0.114, 0, 0,
						0.299, 0.587, 0.114, 0, 0,
						0.299, 0.587, 0.114, 0, 0,
						0, 0, 0, 1, 0]);
			case ColorMatrixDefinition.Achromatomaly:	// Color Weakness
				return new ColorMatrix([
						0.618, 0.320, 0.062, 0, 0,
						0.163, 0.775, 0.062, 0, 0,
						0.163, 0.320, 0.516, 0, 0,
						0, 0, 0, 1, 0]);
			case ColorMatrixDefinition.Sepia:
				return new ColorMatrix([
						0.393, 0.349, 0.272, 0, 0,
						0.769, 0.686, 0.534, 0, 0,
						0.189, 0.168, 0.131, 0, 0,
						0, 0, 0, 1, 0]);
			case ColorMatrixDefinition.GrayScale:
				return new ColorMatrix([
						0.33, 0.33, 0.33, 0, 0,
						0.59, 0.59, 0.59, 0, 0,
						0.11, 0.11, 0.11, 0, 0,
						0, 0, 0, 1, 0]);
			case ColorMatrixDefinition.BlackAndWhite:
				return new ColorMatrix([
						1.5, 1.5, 1.5, 0, -0xFF,
						1.5, 1.5, 1.5, 0, -0xFF,
						1.5, 1.5, 1.5, 0, -0xFF,
						0, 0, 0, 1, 0]);
			case ColorMatrixDefinition.Inversion:
				return new ColorMatrix([
						-1, 0, 0, 0, 0xFF,
						0, -1, 0, 0, 0xFF,
						0, 0, -1, 0, 0xFF,
						0, 0, 0, 1, 0]);
			case ColorMatrixDefinition.Polaroid:
				return new ColorMatrix([
						1.438, -0.062, -0.062, 0, -8,
						-0.122, 1.378, -0.122, 0, 13,
						-0.016, -0.016, 1.438, 0, 5,
						0, 0, 0, 1, 0]);			
			case ColorMatrixDefinition.WhiteToAlpha:
				return new ColorMatrix([
						1.0, 0, 0, -1, 0,
						0, 1.0, 0, -1, 0,
						0, 0, 1.0, -1, 0,
						0, 0, 0, 1, 0]);
		}

	}
}