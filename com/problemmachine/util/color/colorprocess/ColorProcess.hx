package com.problemmachine.util.color.colorprocess;
import com.problemmachine.util.color.Color;
import com.problemmachine.util.color.colorprocess.operations.ColorProcessOperation;
import com.problemmachine.util.color.colorvaluemap.ColorValueMap;
import flash.display.Shader;
import haxe.ds.GenericStack;

class ColorProcess
{
	private static var sComparand:Float = 1;
	private static var sSkip:Bool = false;
	
	// indentations visualize depth of stack
	public static var add(get, null):Array<ColorProcessOperation> = 
	[	
		val1, 
			val1, 
				imix, 
					mult,
				val2, 
					mix, 
						mult, 
					ColorProcessOperation.add,
				ColorProcessOperation.add,
			clamp
	];
	private static inline function get_add():Array<ColorProcessOperation>
	{	return add.copy();		}
	
	public static var darken(get, null):Array<ColorProcessOperation> = 
	[	
		val1, 
			val1, 
				imix, 
					mult, 
				val2, 
					mix, 
						mult, 
					ColorProcessOperation.add,
				min
	];
	private static inline function get_darken():Array<ColorProcessOperation>
	{	return darken.copy();	}
	
	public static var difference(get, null):Array<ColorProcessOperation> = 
	[	
		val1, 
			val1, 
				imix, 
					mult, 
				val2, 
					mix, 
						mult, 
					ColorProcessOperation.add,
				min,
			val1,
				val1, 
					imix, 
						mult, 
					val2, 
						mix, 
							mult, 
						ColorProcessOperation.add,
					max,
				sub
	];
	private static inline function get_difference():Array<ColorProcessOperation>
	{	return difference.copy();	}
	
	public static var hardLight(get, null):Array<ColorProcessOperation> = 
	[	
		val1,
			val2, 
				comp(0.5),
				if_lt,
			imix, 
				mult, 
			val1, 
				val2, 
					mult, 
				mix, 
					mult, 
				ColorProcessOperation.add,
			els,
			imix, 
				mult, 
			inv1, 
				inv2, 
					mult, 
				mix, 
					mult, 
				ColorProcessOperation.add,
			endif
	];
	private static inline function get_hardLight():Array<ColorProcessOperation>
	{	return hardLight.copy();	}
	
	public static var invert(get, null):Array<ColorProcessOperation> = 
	[	
		val1, 
			imix, 
				mult, 
			inv1, 
				mix, 
					mult, 
				ColorProcessOperation.add
	];
	private static inline function get_invert():Array<ColorProcessOperation>
	{	return invert.copy();	}
	
	public static var lighten(get, null):Array<ColorProcessOperation> = 
	[	
		val1,
			val1, 
				imix, 
					mult, 
				val2, 
					mix, 
						mult, 
					ColorProcessOperation.add,
				max
	];
	private static inline function get_lighten():Array<ColorProcessOperation>
	{	return lighten.copy();	}
	
	public static var multiply(get, null):Array<ColorProcessOperation> = 
	[	
		val1, 
			imix, 
				mult, 
			val1, 
				val2, 
					mult, 
				mix, 
					mult, 
				ColorProcessOperation.add
	];
	private static inline function get_multiply():Array<ColorProcessOperation>
	{	return multiply.copy();	}
	
	public static var normalBlend(get, null):Array<ColorProcessOperation> = 
	[	
		val1, 
			imix, 
				mult, 
			val2, 
				mix, 
					mult, 
				ColorProcessOperation.add
	];
	private static inline function get_normalBlend():Array<ColorProcessOperation>
	{	return normalBlend.copy();	}
	
	public static var overlay(get, null):Array<ColorProcessOperation> = 
	[	
		val1,
			val1, 
				comp(0.5),
				if_lt,
			imix, 
				mult, 
			val1, 
				val2, 
					mult, 
				mix, 
					mult, 
				ColorProcessOperation.add,
			els,
			imix, 
				mult, 
			inv1, 
				inv1, 
					mult, 
				mix, 
					mult, 
				ColorProcessOperation.add,
			endif
	];
	private static inline function get_overlay():Array<ColorProcessOperation>
	{	return overlay.copy();	}
	
	public static var screen(get, null):Array<ColorProcessOperation> = 
	[	
		val1, 
			imix, 
				mult, 
			inv1, 
				inv2, 
					mult, 
				mix, 
					mult, 
				ColorProcessOperation.add,
	];
	private static inline function get_screen():Array<ColorProcessOperation>
	{	return screen.copy();	}
	
	public static var subtract(get, null):Array<ColorProcessOperation> = 
	[	
		val1, 
			val2, 
				mix, 
					mult,
				sub,
			clamp
	];
	private static inline function get_subtract():Array<ColorProcessOperation>
	{	return subtract.copy();	}
	
	static public function parseStringToCommandList(string:String):Array<ColorProcessOperation>
	{
		string = "= (v1 - (v2 * imix)) - 0.5";
		/* desired result =
		 * 	[const(0.5), v2, imix, mult, v1, sub, sub]		*/
		return null;
	}
	
	static public function convertCommandListToString(list:Array<ColorProcessOperation>):String
	{
		return null;
	}
	
	static public function process(c1:Color, c2:Color, map1:ColorValueMap, map2:ColorValueMap, mixC:Color, mixMap:ColorValueMap, 
		operations:Array<ColorProcessOperation>):Float
	{
		sSkip = false;
		
		var stack:GenericStack<Float> = new GenericStack<Float>();
		
		var m:Float = mixMap.getScalarFrom(mixC);
		var v1:Float = map1.getScalarFrom(c1);
		var v2:Float = map2.getScalarFrom(c2);
		for (o in operations)
			if (sSkip && o != endif && o != els)
				continue;
			else switch(o)
			{
				case val1:
					stack.add(v1);
				case val2:
					stack.add(v2);
				case inv1:
					stack.add(1 - v1);
				case inv2:
					stack.add(1 - v2);
				case mix:
					stack.add(m);
				case imix:
					stack.add(1 - m);
					
				case add:
					stack.add(stack.pop() + stack.pop());
				case sub:
					stack.add(stack.pop() - stack.pop());
				case mult:
					stack.add(stack.pop() * stack.pop());
				case div:
					stack.add(stack.pop() / stack.pop());
				case const(value):
					stack.add(value);
					
				case max:
					stack.add(Math.max(stack.pop(), stack.pop()));
				case min:
					stack.add(Math.min(stack.pop(), stack.pop()));
				case clamp:
					stack.add(Math.min(1, Math.max(0, stack.pop())));
					
				case comp(value):
					sComparand = value;
					
				case if_lt:
					if (stack.pop() >= sComparand)
						sSkip = true;
				case if_lte:
					if (stack.pop() > sComparand)
						sSkip = true;
				case if_e:
					if (stack.pop() != sComparand)
						sSkip = true;
					
				case endif:
					sSkip = false;
				case els:
					sSkip = !sSkip;
			}
		
		return map1.setScalarTo(c1, stack.pop());
	}
	
	public function toFlashShader():Shader
	{
		//var s:Shader = new Shader();
		return null;
	}
	
}