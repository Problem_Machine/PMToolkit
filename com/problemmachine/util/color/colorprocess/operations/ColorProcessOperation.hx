package com.problemmachine.util.color.colorprocess.operations;

enum ColorProcessOperation 
{
	val1;
	val2;
	inv1;
	inv2;
	mix;
	imix;
	const(value:Float);
	
	add;
	sub;
	mult;
	div;
	
	max;
	min;
	clamp;
	
	comp(value:Float);
	
	if_lt;
	if_lte;
	if_e;
	endif;
	els;
}