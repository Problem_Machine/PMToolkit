package com.problemmachine.util.color.colorvaluemap;
import com.problemmachine.util.color.Color;
import flash.errors.Error;

class ColorValueMap 
{
	public static var AlphaMap(default, null):ColorValueMap = 
		new ColorValueMap(getAlphaByte, setAlphaByte, null, null, false);
	public static var RedMap(default, null):ColorValueMap = 
		new ColorValueMap(getRedByte, setRedByte, null, null, false);
	public static var GreenMap(default, null):ColorValueMap = 
		new ColorValueMap(getGreenByte, setGreenByte, null, null, false);
	public static var BlueMap(default, null):ColorValueMap = 
		new ColorValueMap(getBlueByte, setBlueByte, null, null, false);
		
	public static var CyanMap(default, null):ColorValueMap = 
		new ColorValueMap(null, null, getCyanScalar, setCyanScalar, true);
	public static var MagentaMap(default, null):ColorValueMap = 
		new ColorValueMap(null, null, getMagentaScalar, setMagentaScalar, true);
	public static var YellowMap(default, null):ColorValueMap = 
		new ColorValueMap(null, null, getYellowScalar, setYellowScalar, true);
	public static var BlackMap(default, null):ColorValueMap = 
		new ColorValueMap(null, null, getBlackScalar, setBlackScalar, true);
		
	public static var HueMap(default, null):ColorValueMap = 
		new ColorValueMap(null, null, getHueScalar, setHueScalar, true);
	public static var SaturationMap(default, null):ColorValueMap = 
		new ColorValueMap(null, null, getSaturationScalar, setSaturationScalar, true);
	public static var ValueMap(default, null):ColorValueMap = 
		new ColorValueMap(null, null, getValueScalar, setValueScalar, true);
		
	public static var MaximumMap(default, null):ColorValueMap = 
		new ColorValueMap(getMaxByte, setNullByte, getMaxScalar, setNullScalar, true);
	public static var NullMap(default, null):ColorValueMap = 
		new ColorValueMap(getNullByte, setNullByte, getNullScalar, setNullScalar, true);
	
	public var preferScalar(default, null):Bool;
	public var getByteFrom(default, null):Color->Int;
	public var setByteTo(default, null):Color->Int->Int;
	public var getScalarFrom(default, null):Color->Float;
	public var setScalarTo(default, null):Color->Float->Float;
	
	public function new(?getByte:Color->Int, ?setByte:Color->Int->Int, 
		?getScalar:Color->Float, ?setScalar:Color->Float->Float, preferScalar:Bool = false)
	{
		if (getByte == null && getScalar == null)
			throw new Error("Cannot use default functions for both getByteFrom() and getScalarFrom()");
		if (setByte == null && setScalar == null)
			throw new Error("Cannot use default functions for both setByteTo() and setScalarTo()");
		this.preferScalar = preferScalar;
		this.getByteFrom = (getByte != null ? getByte : getDefaultByte);
		this.setByteTo = (setByte != null ? setByte : setDefaultByte);
		this.getScalarFrom = (getScalar != null ? getScalar : getDefaultScalar);
		this.setScalarTo = (setScalar != null ? setScalar : setDefaultScalar);
	}
	
	
	private function getDefaultByte(color:Color):Int
	{	
		return Std.int(getScalarFrom(color) * 0xFF);		
	}
	private function setDefaultByte(color:Color, val:Int):Int
	{	
		setScalarTo(color, val / 0xFF);
		return val;
	}	
	private function getDefaultScalar(color:Color):Float
	{	
		return (getByteFrom(color) / 0xFF);		
	}
	private function setDefaultScalar(color:Color, val:Float):Float
	{	
		setByteTo(color, Std.int(val * 0xFF));
		return val;	
	}
	
	static private function getNullByte(color:Color):Int
	{	
		return 0;		
	}
	static private function setNullByte(color:Color, val:Int):Int
	{	
		return val;
	}	
	static private function getNullScalar(color:Color):Float
	{	
		return 0;		
	}
	static private function setNullScalar(color:Color, val:Float):Float
	{	
		return val;	
	}
	
	static private function getMaxByte(color:Color):Int
	{	
		return 0xFF;		
	}
	static private function getMaxScalar(color:Color):Float
	{	
		return 1;		
	}
	
	static public function getAlphaByte(color:Color):Int
	{	
		return (color.raw >> 24 & 0xFF);		
	}
	static public function setAlphaByte(color:Color, val:Int):Int
	{	
		color.raw = (color.raw & 0x00FFFFFF) | min(0xFF, max(0, val)) << 24;
		return val;
	}
	
	static public function getRedByte(color:Color):Int
	{	
		return (color.raw >> 16 & 0xFF);		
	}
	static public function setRedByte(color:Color, val:Int):Int
	{	
		color.raw = (color.raw & 0xFF00FFFF) | min(0xFF, max(0, val)) << 16;
		return val;
	}	
	
	static public function getGreenByte(color:Color):Int
	{	
		return (color.raw >> 8 & 0xFF);		
	}
	static public function setGreenByte(color:Color, val:Int):Int
	{	
		color.raw = (color.raw & 0xFFFF00FF) | min(0xFF, max(0, val)) << 8;
		return val;
	}
	
	static public function getBlueByte(color:Color):Int
	{	
		return (color.raw & 0xFF);		
	}
	static public function setBlueByte(color:Color, val:Int):Int
	{	
		color.raw = (color.raw & 0xFFFFFF00) | min(0xFF, max(0, val));
		return val;
	}
	
	static public function getCyanScalar(color:Color):Float
	{	
		var k:Float = getBlackScalar(color);
		if (k == 1)
			return 0;
		else
			return (1 - ((getRedByte(color) / 0xFF) - k) / (1 - k));
	}
	static public function setCyanScalar(color:Color, val:Float):Float
	{	
		var k:Float = getBlackScalar(color);
		setRedByte(color, Std.int(((k == 1) ? 0 : (1 - val) * (1 - k)) * 0xFF));
		return val; 
	}
	
	static public function getMagentaScalar(color:Color):Float
	{	
		var k:Float = getBlackScalar(color);
		if (k == 1)
			return 0;
		else
			return (1 - ((getGreenByte(color) / 0xFF) - k) / (1 - k));
	}
	static public function setMagentaScalar(color:Color, val:Float):Float
	{	
		var k:Float = getBlackScalar(color);
		setGreenByte(color, Std.int(((k == 1) ? 0 : (1 - val) * (1 - k)) * 0xFF));
		return val; 
	}
	
	static public function getYellowScalar(color:Color):Float
	{	
		var k:Float = getBlackScalar(color);
		if (k == 1)
			return 0;
		else
			return (1 - ((getBlueByte(color) / 0xFF) - k) / (1 - k));
	}
	static public function setYellowScalar(color:Color, val:Float):Float
	{	
		var k:Float = getBlackScalar(color);
		setBlueByte(color, Std.int(((k == 1) ? 0 : (1 - val) * (1 - k)) * 0xFF));
		return val; 
	}
	
	static public function getBlackScalar(color:Color):Float
	{	
		return (1 - (Math.max(getRedByte(color), Math.max(getGreenByte(color), getBlueByte(color))) / 0xFF));		
	}
	static public function setBlackScalar(color:Color, val:Float):Float
	{	
		color.setCMYK(getCyanScalar(color), 
			getMagentaScalar(color), 
			getYellowScalar(color), 
			val);	
		return val; 
	}
	
	static public function getHueScalar(color:Color):Float
	{	
		var r:Float = getRedByte(color) / 0xFF;
		var g:Float = getGreenByte(color) / 0xFF;
		var b:Float = getBlueByte(color) / 0xFF;
		var hi:Float = Math.max(r, Math.max(g, b));
		if (hi == 0)
			return 0;
		else
		{
			var lo:Float = Math.min(r, Math.min(g, b));
			var d:Float = hi - lo;
			var h:Float = 0;
			if(r == hi)
				h = ((g - b) / d / 3);
			else if(g == hi)
				h = 1 / 3 + ((b - r) / d / 3);
			else
				h = 2 / 3 + ((r - g) / d / 3);

			if( h < 0 )
				h += 1;
			return h;
		}
	}
	static public function setHueScalar(color:Color, val:Float):Float
	{	
		color.setHSV(val, getSaturationScalar(color), getValueScalar(color));
		return val;
	}
	
	static public function getSaturationScalar(color:Color):Float
	{	
		var r:Int = getRedByte(color);
		var hi:Int = r;
		var lo:Int = r;
		
		var g:Int = getGreenByte(color);
		if (g > r)	hi = g;
		else lo = g;
		
		var b:Int = getBlueByte(color);
		if (b > hi) hi = b;
		else if (b < lo) lo = b;
		
		if (hi == 0)
			return 0;
		else
			return (((hi - lo) / hi) / 0xFF);
	}
	static public function setSaturationScalar(color:Color, val:Float):Float
	{	
		color.setHSV(getHueScalar(color), val, getValueScalar(color));
		return val;
	}
	
	static public function getValueScalar(color:Color):Float
	{	
		return (Math.max(getRedByte(color), Math.max(getGreenByte(color), getBlueByte(color))) / 0xFF);
	}
	static public function setValueScalar(color:Color, val:Float):Float
	{	
		color.setHSV(getHueScalar(color), getSaturationScalar(color), val);
		return val;
	}
	
	static private inline function min(a:Int, b:Int):Int
	{	if (a < b) return a; else return b;	}
	static private inline function max(a:Int, b:Int):Int
	{	if (a > b) return a; else return b;	}	
}