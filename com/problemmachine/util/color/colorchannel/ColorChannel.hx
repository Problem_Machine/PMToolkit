package com.problemmachine.util.color.colorchannel;
import com.problemmachine.util.color.colorprocess.ColorProcess;
import com.problemmachine.util.color.colorvaluemap.ColorValueMap;

class ColorChannel
{	
	public var source:Color;
	public var map:ColorValueMap;
	
	public var byte(get, set):Int;
	public var scalar(get, set):Float;
	public var inverseByte(get, never):Int;
	public var inverseScalar(get, never):Float;

	public function new(src:Color, map:ColorValueMap) 
	{	
		source = src;
		this.map = map;
	}
	
	private inline function get_byte():Int
	{	return map.getByteFrom(source);	}
	private inline function set_byte(val:Int):Int
	{	return map.setByteTo(source, val);		}	
	
	private function get_scalar():Float
	{	return map.getScalarFrom(source);	}
	private function set_scalar(val:Float):Float
	{	return map.setScalarTo(source, val);		}
	
	private inline function get_inverseByte():Int
	{	return 0xFF - byte;	}
	private inline function get_inverseScalar():Float
	{	return 1 - scalar;		}
	
	public inline function add(c:ColorChannel, ?mix:ColorValueMap):Void
	{	
		if (mix == null)	mix = ColorValueMap.AlphaMap;
		ColorProcess.process(source, c.source, map, c.map, c.source, mix, ColorProcess.add);
		//byte = min(0xFF, Std.int(byte + (mix.getScalarFrom(c.source) * c.byte)));									
	}
	
	public inline function alpha(c:ColorChannel, ?mix:ColorValueMap):Void
	{
		if (mix == null)	mix = ColorValueMap.AlphaMap;
		ColorProcess.process(source, c.source, map, mix, c.source, ColorValueMap.MaximumMap, ColorProcess.multiply);
		//scalar = scalar * mix.getScalarFrom(c.source);																
	}
	
	public inline function darken(c:ColorChannel, ?mix:ColorValueMap):Void
	{	
		if (mix == null)	mix = ColorValueMap.AlphaMap;
		ColorProcess.process(source, c.source, map, c.map, c.source, mix, ColorProcess.darken);
		//byte = min(byte, mixIn(c.byte, mix.getScalarFrom(c.source)));
	}
	
	public inline function difference(c:ColorChannel, ?mix:ColorValueMap):Void
	{
		if (mix == null)	mix = ColorValueMap.AlphaMap;
		ColorProcess.process(source, c.source, map, c.map, c.source, mix, ColorProcess.difference);
		//var col:Int = mixIn(c.byte, mix.getScalarFrom(c.source));
		//byte = max(byte, col) - min(byte, col);
	}
	
	public inline function erase(c:ColorChannel, ?mix:ColorValueMap):Void
	{	
		if (mix == null)	mix = ColorValueMap.AlphaMap;
		ColorProcess.process(source, c.source, map, mix, c.source, ColorValueMap.MaximumMap, ColorProcess.subtract);
		//scalar = scalar * (1 - mix.getScalarFrom(c.source));
	}
	
	public inline function hardLight(c:ColorChannel, ?mix:ColorValueMap):Void
	{	
		if (mix == null)	mix = ColorValueMap.AlphaMap;
		ColorProcess.process(source, c.source, map, c.map, c.source, mix, ColorProcess.hardLight);
		//c.byte < 0x7F ? multiply(c, mix) : screen(c, mix);
	}
	
	public inline function invert(c:ColorChannel, ?mix:ColorValueMap):Void
	{	
		if (mix == null)	mix = ColorValueMap.AlphaMap;
		ColorProcess.process(source, c.source, map, c.map, c.source, mix, ColorProcess.invert);
		//byte = mixIn(inverseByte, mix.getScalarFrom(c.source));		
	}
	
	public inline function lighten(c:ColorChannel, ?mix:ColorValueMap):Void
	{	
		if (mix == null)	mix = ColorValueMap.AlphaMap;
		ColorProcess.process(source, c.source, map, c.map, c.source, mix, ColorProcess.lighten);
		//byte = max(byte, mixIn(c.byte, mix.getScalarFrom(c.source)));
	}
	
	public inline function multiply(c:ColorChannel, ?mix:ColorValueMap):Void
	{	
		if (mix == null)	mix = ColorValueMap.AlphaMap;
		ColorProcess.process(source, c.source, map, c.map, c.source, mix, ColorProcess.multiply);
		//byte = mixIn(Std.int(byte * c.scalar), mix.getScalarFrom(c.source));
	}
	
	public inline function normalBlend(c:ColorChannel, ?mix:ColorValueMap):Void
	{	
		if (mix == null)	mix = ColorValueMap.AlphaMap;
		ColorProcess.process(source, c.source, map, c.map, c.source, mix, ColorProcess.normalBlend);
		//byte = mixIn(c.byte, mix.getScalarFrom(c.source));
	}
	
	public inline function overlay(c:ColorChannel, ?mix:ColorValueMap):Void
	{	
		if (mix == null)	mix = ColorValueMap.AlphaMap;
		ColorProcess.process(source, c.source, map, c.map, c.source, mix, ColorProcess.overlay);
		//byte < 0x7F ? multiply(c, mix) : screen(c, mix);
	}
	
	public inline function screen(c:ColorChannel, ?mix:ColorValueMap):Void
	{	
		if (mix == null)	mix = ColorValueMap.AlphaMap;
		ColorProcess.process(source, c.source, map, c.map, c.source, mix, ColorProcess.screen);
		//byte = mixIn(0xFF - Std.int((0xFF - byte) * (1 - c.scalar)), mix.getScalarFrom(c.source));
	}
	
	public inline function subtract(c:ColorChannel, ?mix:ColorValueMap):Void
	{	
		if (mix == null)	mix = ColorValueMap.MaximumMap;
		ColorProcess.process(source, c.source, map, c.map, c.source, mix, ColorProcess.subtract);
		//byte = byte - Std.int(mix.getScalarFrom(c.source) * c.byte);			
	}
	
	private inline function mixIn(c:Int, a:Float):Int
	{	return Std.int(c * a + byte * (1 - a));												}
	

	private inline function min(a:Int, b:Int):Int
	{	if (a < b) return a; else return b;	}
	private inline function max(a:Int, b:Int):Int
	{	if (a > b) return a; else return b;	}	
}

