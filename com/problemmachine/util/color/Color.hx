package com.problemmachine.util.color;
import com.problemmachine.util.color.colorchannel.ColorChannel;
import com.problemmachine.util.color.colorvaluemap.ColorValueMap;
import flash.display.BlendMode;

class Color
{
	static private var sMix:ColorValueMap = ColorValueMap.AlphaMap;
	
	public var raw:UInt;
	
	public var alpha(get, never):ColorChannel;
	public var red(get,  never):ColorChannel;
	public var green(get,  never):ColorChannel;
	public var blue(get,  never):ColorChannel;
	
	public var hue(get, never):ColorChannel;
	public var saturation(get, never):ColorChannel;
	public var value(get, never):ColorChannel;
	
	public var cyan(get, never):ColorChannel;
	public var yellow(get, never):ColorChannel;
	public var magenta(get, never):ColorChannel;
	public var black(get, never):ColorChannel;
	
	public var inverse(get, never):Color;
	
	private inline function new()
	{}
	
	static public inline function createFromRaw(raw:UInt = 0xFFFFFF):Color
	{
		var c:Color = new Color();
		c.raw = raw;
		return c;
	}
	
	static public inline function createFromRGB(r:Int, g:Int, b:Int, ?a:Int):Color
	{
		var c:Color = createFromRaw(0xFFFFFFFF);
		c.setRGB(r, g, b);
		if (a != null)
			ColorValueMap.AlphaMap.setByteTo(c, a);
		return c;
	}
	static public inline function createFromFloatRGB(r:Float, g:Float, b:Float, ?a:Float):Color
	{
		var c:Color = createFromRaw(0xFFFFFFFF);
		c.setFloatRGB(r, g, b);
		if (a != null)
			ColorValueMap.AlphaMap.setScalarTo(c, a);
		return c;
	}
	static public inline function createFromCMYK(c:Float, m:Float, y:Float, k:Float, ?a:Float):Color
	{
		var col:Color = createFromRaw(0xFFFFFFFF);
		col.setCMYK(c, m, y, k);
		if (a != null)
			ColorValueMap.AlphaMap.setScalarTo(col, a);
		return col;
	}
	static public inline function createFromHSV(h:Float, s:Float, v:Float, ?a:Float, useDegrees:Bool = false):Color
	{
		var c:Color = createFromRaw(0xFFFFFFFF);
		c.setHSV(h, s, v, useDegrees);
		if (a != null)
			ColorValueMap.AlphaMap.setScalarTo(c, a);
		return c;
	}
	
	private inline function get_alpha():ColorChannel
	{	return new ColorChannel(this, ColorValueMap.AlphaMap);			}
	private inline function get_red():ColorChannel
	{	return new ColorChannel(this, ColorValueMap.RedMap);			}
	private inline function get_green():ColorChannel
	{	return new ColorChannel(this, ColorValueMap.GreenMap);			}
	private inline function get_blue():ColorChannel
	{	return new ColorChannel(this, ColorValueMap.BlueMap);			}
	
	private inline function get_cyan():ColorChannel
	{	return new ColorChannel(this, ColorValueMap.CyanMap);			}
	private inline function get_magenta():ColorChannel
	{	return new ColorChannel(this, ColorValueMap.MagentaMap);		}
	private inline function get_yellow():ColorChannel
	{	return new ColorChannel(this, ColorValueMap.YellowMap);			}
	private inline function get_black():ColorChannel
	{	return new ColorChannel(this, ColorValueMap.BlackMap);			}
	
	private inline function get_hue():ColorChannel
	{	return new ColorChannel(this, ColorValueMap.HueMap);			}
	private inline function get_saturation():ColorChannel
	{	return new ColorChannel(this, ColorValueMap.SaturationMap);		}
	private inline function get_value():ColorChannel
	{	return new ColorChannel(this, ColorValueMap.ValueMap);			}
	
	private inline function get_inverse():Color
	{	return createFromRGB(red.inverseByte, green.inverseByte, blue.inverseByte, alpha.byte);		}
	
	@:allow(com.problemmachine.util.color.ColorChannel)
	private inline function cMax():Float
	{	return Math.max(red.scalar, Math.max(green.scalar, blue.scalar));		}
	@:allow(com.problemmachine.util.color.ColorChannel)
	private inline function cMin():Float
	{	return Math.min(red.scalar, Math.max(green.scalar, blue.scalar));		}
	
	public inline function multiplyByScalar(rhs:Float):Color
	{
		var col:ColorChannel = red;
		col.scalar *= rhs;
		col.map = ColorValueMap.GreenMap;
		col.scalar *= rhs;
		col.map = ColorValueMap.BlueMap;
		col.scalar *= rhs;
		return this;
	}
	
	public inline function setHSV(h:Float, s:Float, v:Float, useDegrees:Bool = false):Void
	{
		if (useDegrees)
			h /= 360;
		if (s == 0) 
		{
			// achromatic (grey)
			setRGB(Std.int(v * 0xFF), Std.int(v * 0xFF), Std.int(v * 0xFF));
			return;
		}
		while (h < 0)
			++h;
		h *= 6;
		var i:Int = Math.floor(h);
		var f:Float = h - i;			// factorial part of h
		var p:Float = v * (1 - s);
		var q:Float = v * (1 - (s * f));
		var t:Float = v * (1 - (s * (1 - f)));
		
		switch(i) 
		{
			case 0:
				setRGB(Std.int(v * 0xFF), Std.int(t * 0xFF), Std.int(p * 0xFF));
			case 1:
				setRGB(Std.int(q * 0xFF), Std.int(v * 0xFF), Std.int(p * 0xFF));
			case 2:
				setRGB(Std.int(p * 0xFF), Std.int(v * 0xFF), Std.int(t * 0xFF));
			case 3:
				setRGB(Std.int(p * 0xFF), Std.int(q * 0xFF), Std.int(v * 0xFF));
			case 4:
				setRGB(Std.int(t * 0xFF), Std.int(p * 0xFF), Std.int(v * 0xFF));
			default:		// case 5:
				setRGB(Std.int(v * 0xFF), Std.int(p * 0xFF), Std.int(q * 0xFF));
		}
	}
	
	public inline function setRGB(r:Int, g:Int, b:Int):Void
	{
		setBytes([ColorValueMap.RedMap, ColorValueMap.GreenMap, ColorValueMap.BlueMap], [r, g, b]);
	}
	
	public inline function setFloatRGB(r:Float, g:Float, b:Float):Void
	{
		setScalars([ColorValueMap.RedMap, ColorValueMap.GreenMap, ColorValueMap.BlueMap], [r, g, b]);
	}
	
	public inline function setCMYK(c:Float, m:Float, y:Float, k:Float):Void
	{
		setScalars([ColorValueMap.RedMap, ColorValueMap.GreenMap, ColorValueMap.BlueMap], 
			[		(1 - c) * (1 - k), 	(1 - m) * (1 - k), 	(1 - y) * (1 - k)]);
	}
	
	public inline function setBytes(maps:Array<ColorValueMap>, bytes:Array<Int>):Void
	{
		for (i in 0...maps.length)
			maps[i].setByteTo(this, bytes[i]);
	}
	public inline function setScalars(maps:Array<ColorValueMap>, scalars:Array<Float>):Void
	{
		for (i in 0...maps.length)
			maps[i].setScalarTo(this, scalars[i]);
	}
	
	public inline function blend(color:Color, blendMode:BlendMode, as32:Bool = true, with32:Bool = true):Color
	{
		switch(blendMode)
		{
			case BlendMode.ADD:
				return addBlend(color, as32, with32);
			case BlendMode.ALPHA:
				return alphaBlend(color, as32, with32);
			case BlendMode.DARKEN:
				return darkenBlend(color, as32, with32);
			case BlendMode.DIFFERENCE:
				return differenceBlend(color, as32, with32);	
			case BlendMode.ERASE:
				return eraseBlend(color, as32, with32);
			case BlendMode.HARDLIGHT:
				return hardLightBlend(color, as32, with32);
			case BlendMode.INVERT:
				return invertBlend(color, as32, with32);			
			case BlendMode.LIGHTEN:
				return lightenBlend(color, as32, with32);				
			case BlendMode.MULTIPLY:
				return multiplyBlend(color, as32, with32);				
			case BlendMode.NORMAL:
				return normalBlend(color, as32, with32);				
			case BlendMode.OVERLAY:
				return overlayBlend(color, as32, with32);					
			case BlendMode.SCREEN:
				return screenBlend(color, as32, with32);				
			case BlendMode.SUBTRACT:
				return subtractBlend(color, as32, with32);				
			case BlendMode.SHADER:
				return this;
			case BlendMode.LAYER:
				return this;
			default:
				return this;
		}
	}
	
	public inline function addBlend(color:Color, as32:Bool = true, with32:Bool = true):Color
	{
		sMix = with32 ? ColorValueMap.AlphaMap : ColorValueMap.MaximumMap;
		red.add(color.red, sMix);
		green.add(color.green, sMix);
		blue.add(color.blue, sMix);
		return this;
	}
	
	public inline function alphaBlend(color:Color, as32:Bool = true, with32:Bool = true):Color
	{
		if (as32 && with32)
			alpha.byte = color.alpha.byte;
		else if (with32)
		{
			red.alpha(color.red);
			green.alpha(color.green);
			blue.alpha(color.blue);
		}
		return this;
	}
	
	public inline function darkenBlend(color:Color, as32:Bool = true, with32:Bool = true):Color
	{
		sMix = with32 ? ColorValueMap.AlphaMap : ColorValueMap.MaximumMap;
		red.darken(color.red, sMix);
		green.darken(color.green, sMix);
		blue.darken(color.blue, sMix);
		return this;
	}
	
	public inline function differenceBlend(color:Color, as32:Bool = true, with32:Bool = true):Color
	{
		sMix = with32 ? ColorValueMap.AlphaMap : ColorValueMap.MaximumMap;
		red.difference(color.red, sMix);
		green.difference(color.green, sMix);
		blue.difference(color.blue, sMix);
		return this;
	}
	
	public inline function eraseBlend(color:Color, as32:Bool = true, with32:Bool = true):Color
	{
		if (as32 && with32)
			alpha.byte = color.alpha.inverseByte;
		else if (with32)
		{
			sMix = with32 ? ColorValueMap.AlphaMap : ColorValueMap.MaximumMap;
			red.erase(color.red, sMix);
			green.erase(color.green, sMix);
			blue.erase(color.blue, sMix);
		}
		return this;
	}
	
	public inline function hardLightBlend(color:Color, as32:Bool = true, with32:Bool = true):Color
	{
		sMix = with32 ? ColorValueMap.AlphaMap : ColorValueMap.MaximumMap;
		red.hardLight(color.red, sMix);
		green.hardLight(color.green, sMix);
		blue.hardLight(color.blue, sMix);
		return this;
	}
	
	public inline function invertBlend(color:Color, as32:Bool = true, with32:Bool = true):Color
	{
		sMix = with32 ? ColorValueMap.AlphaMap : ColorValueMap.MaximumMap;
		red.invert(color.red, sMix);
		green.invert(color.green, sMix);
		blue.invert(color.blue, sMix);
		return this;
	}
	
	public inline function lightenBlend(color:Color, as32:Bool = true, with32:Bool = true):Color
	{
		sMix = with32 ? ColorValueMap.AlphaMap : ColorValueMap.MaximumMap;
		red.lighten(color.red, sMix);
		green.lighten(color.green, sMix);
		blue.lighten(color.blue, sMix);
		return this;
	}
	
	public inline function multiplyBlend(color:Color, as32:Bool = true, with32:Bool = true):Color
	{
		sMix = with32 ? ColorValueMap.AlphaMap : ColorValueMap.MaximumMap;
		red.multiply(color.red, sMix);
		green.multiply(color.green, sMix);
		blue.multiply(color.blue, sMix);
		return this;
	}
	
	public inline function normalBlend(color:Color, as32:Bool = true, with32:Bool = true):Color
	{
		if (!with32)
		{
			raw &= 0xFF000000;
			raw |= color.raw & 0x00FFFFFF;
		}
		else
		{
			sMix = with32 ? ColorValueMap.AlphaMap : ColorValueMap.MaximumMap;
			red.normalBlend(color.red, sMix);
			green.normalBlend(color.green, sMix);
			blue.normalBlend(color.blue, sMix);
		}
		return this;
	}
	
	public inline function overlayBlend(color:Color, as32:Bool = true, with32:Bool = true):Color
	{
		sMix = with32 ? ColorValueMap.AlphaMap : ColorValueMap.MaximumMap;
		red.overlay(color.red, sMix);
		green.overlay(color.green, sMix);
		blue.overlay(color.blue, sMix);
		return this;
	}
	
	public inline function screenBlend(color:Color, as32:Bool = true, with32:Bool = true):Color
	{
		sMix = with32 ? ColorValueMap.AlphaMap : ColorValueMap.MaximumMap;
		red.screen(color.red, sMix);
		green.screen(color.green, sMix);
		blue.screen(color.blue, sMix);
		return this;
	}
	
	public inline function subtractBlend(color:Color, as32:Bool = true, with32:Bool = true):Color
	{
		sMix = with32 ? ColorValueMap.AlphaMap : ColorValueMap.MaximumMap;
		red.subtract(color.red, sMix);
		green.subtract(color.green, sMix);
		blue.subtract(color.blue, sMix);
		return this;
	}	
	
		
	private inline function min(a:Int, b:Int):Int
	{	if (a < b) return a; else return b;	}
	private inline function max(a:Int, b:Int):Int
	{	if (a > b) return a; else return b;	}	
}