package com.problemmachine.util.waveform;

class SawWaveform implements IWaveform
{
	public function new(){}
	
	public function f(radians:Float):Float 
	{
		return (((radians / Math.PI) + 0.5) % 2) - 1;
	}	
}