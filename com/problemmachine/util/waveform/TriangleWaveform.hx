package com.problemmachine.util.waveform;

class TriangleWaveform implements IWaveform
{
	public function new(){}
	
	public function f(radians:Float):Float 
	{
		var val:Float = ((radians / Math.PI) + 0.5) % 2;
		return (val < 1 ? 
			val * 2 - 1 : 
			1 - ((val - 1) * 2));
	}	
}