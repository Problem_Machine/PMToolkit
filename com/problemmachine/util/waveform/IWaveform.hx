package com.problemmachine.util.waveform;

interface IWaveform 
{
	// takes any float value and returns a value between -1 and 1 with a period of 2pi
	function f(radians:Float):Float;
}