package com.problemmachine.util.waveform;

class SquareWaveform implements IWaveform
{
	public function new(){}
	
	public function f(radians:Float):Float 
	{
		return ((radians / Math.PI) % 2) < 1 ? 1 : -1;
	}	
}