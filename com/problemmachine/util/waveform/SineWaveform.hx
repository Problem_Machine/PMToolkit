package com.problemmachine.util.waveform;

class SineWaveform implements IWaveform
{
	public function new(){}
	
	public function f(radians:Float):Float 
	{
		return Math.sin(radians);
	}	
}