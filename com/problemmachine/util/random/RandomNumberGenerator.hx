package com.problemmachine.util.random;
import com.problemmachine.tools.hash.HashTools;
import com.problemmachine.tools.hash.XXHash;

class RandomNumberGenerator
{
	private var mLastVal:UInt;
	
	private function new(seed:UInt)
	{
		// salt function from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
		mLastVal = new XXHash(seed).getHash((seed * 9301 + 49297) % 233280);
	}
	
	public inline function random():Float
	{
		// Xorshift algorithm from George Marsaglia's paper.
		// Implementation sourced from https://bitbucket.org/runevision/random-numbers-testing/src/16491c9dfa60417a5b25bd496e06a8f75b8f4f50/Assets/Implementations/RandomNumberGenerators/XorShift.cs
		mLastVal ^= (mLastVal << 13);
		mLastVal ^= (mLastVal >>> 17);
		mLastVal ^= (mLastVal << 5);
		return (mLastVal / 0x7FFFFFFF) / 2;
	}

	static public function fromIntSeed(val:Int):RandomNumberGenerator
	{
		return new RandomNumberGenerator(val);
	}
	
	static public function fromFloatSeed(val:Float):RandomNumberGenerator
	{
		while (val > 1)
			val *= 0.1;
		return new RandomNumberGenerator(cast (val * 0xFFFFFF));
	}
	
	static public function fromStringSeed(val:String):RandomNumberGenerator
	{
		return fromFloatSeed(HashTools.hashStringToFloat(val));
	}
	
	static public function fromRandomSeed():RandomNumberGenerator
	{
		return new RandomNumberGenerator(Std.random(0xFFFFFF));
	}
}