package com.problemmachine.util.geometry;
import flash.display.Graphics;
import flash.geom.Point;

class Ellipse
{
	public var centerX:Float;
	public var centerY:Float;
	public var width:Float;
	public var height:Float;
	public var center(get, set):Point;
	public var radius(get, set):Float;
	public var a(get, set):Float;
	public var b(get, set):Float;
	public var c(get, set):Float
	public var focus1x(get, never):Float;
	public var focus1y(get, never):Float;
	public var focus2x(get, never):Float;
	public var focus2y(get, never):Float;
	public var focus1(get, never):Point;
	public var focus2(get, never):Point;
	public var eccentricity(get, set):Float;

	public function new(centerX:Float, centerY:Float, width:Float, height:Float) 
	{
		this.centerX = centerX;
		this.centerY = centerY;
		this.width = width;
		this.height = height;
	}
	
	public function draw(graphics:Graphics):Void
	{
		graphics.drawEllipse(x - (width * 0.5), y - (height * 0.5), width, height);
	}
	
	public function drawArcRadians(graphics:Graphics, from:Float, to:Float, precision:Float = 100, wedge:Bool = false):Void
	{
		var diff:Float = to - from;
		var steps:Int = Math.round(diff * precision);
		var stepLength:Float = diff / steps;
		if (wedge)
			graphics.moveTo(x, y);
		else
			graphics.moveTo(x + width * 0.5 * Math.cos(from), y + height * 0.5 * Math.sin(from));
		if (wedge)
		{
			for (i in 0...steps) 
				graphics.lineTo(x + (width * 0.5 * Math.cos(stepLength * i)), y + (height * 0.5 * Math.sin(stepLength * i)));
			graphics.lineTo(x, y);
		}
		else
			for (i in 1...steps) 
				graphics.lineTo(x + (width * 0.5 * Math.cos(stepLength * i)), y + (height * 0.5 * Math.sin(stepLength * i)));
	}
	
	public function drawArcDegrees(graphics:Graphics, from:Float, to:Float, precision:Float = 100, wedge:Bool = false):Void
	{
		drawArc(graphics, (from / 180) * Math.PI, (to / 180) * Math.PI, precision, wedge);
	}
	
	public inline function get_center():Point
	{
		return new Point(centerX, centerY);
	}
	public inline function set_center(val:Point):Point
	{
		centerX = val.x;
		centerY = val.y;
		return val;
	}

	public inline function get_a():Float
	{
		return Math.max(width, height) * 0.5;
	}
	public inline function set_a(val:Float):Float
	{
		if (width > height)
			width = 2 * val;
		else
			height = 2 * val;
		return val;
	}
	
	public inline function get_b():Float
	{
		return Math.min(width, height) * 0.5;
	}
	public inline function set_b(val:Float):Float
	{
		if (width < height)
			width = 2 * val;
		else
			height = 2 * val;
		return val;
	}
	
	public inline function get_c():Float
	{
		return Math.sqrt(a * a - b * b);
	}
	public inline function set_c(val:Float):Float
	{
		var scale:Float = val / c;
		scale = Math.sqrt(scale);
		a *= scale;
		b *= scale;
	}
	
	public inline function get_focus1x():Float
	{
		if (width > height)
			return centerX - c;
		else
			return centerX;
	}
	
	public inline function get_focus1y():Float
	{
		if (width > height)
			return centerX;
		else
			return centerX - c;
	}
	
	public inline function get_focus2x():Float
	{
		if (width > height)
			return centerX + c;
		else
			return centerX;
	}
	
	public inline function get_focus2y():Float
	{
		if (width > height)
			return centerX;
		else
			return centerX + c;
	}
	public inline function get_focus1():Point
	{
		return new Point(focus1x, focus1y);
	}
	public inline function get_focus2():Point
	{
		return new Point(focus2x, focus2y);
	}
	public inline function get_eccentricity():Float
	{
		return c / a;
	}
	public inline function set_eccentricity(val:Float):Float
	{
		c = a * val;
	}
}