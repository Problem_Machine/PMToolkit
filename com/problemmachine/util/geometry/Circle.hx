package com.problemmachine.util.geometry;
import flash.display.Graphics;

class Circle
{
	public var x:Float;
	public var y:Float;
	public var radius:Float;
	public var diameter(get, set):Float;
	public var circumference(get, set):Float;
	public var area(get, set):Float;
	
	public function new(x:Float = 0, y:Float = 0, radius:Float = 0) 
	{
		this.x = x;
		this.y = y;
		this.radius = radius;
	}
	
	public function draw(graphics:Graphics):Void
	{
		graphics.drawCircle(x, y, radius);
	}
	
	public function drawArcRadians(graphics:Graphics, from:Float, to:Float, precision:Float = 100, wedge:Bool = false):Void
	{
		var diff:Float = to - from;
		var steps:Int = Math.round(diff * precision);
		var stepLength:Float = diff / steps;
		if (wedge)
			graphics.moveTo(x, y);
		else
			graphics.moveTo(x + radius * Math.cos(from), y + radius * Math.sin(from));
		if (wedge)
		{
			for (i in 0...steps) 
				graphics.lineTo(x + (radius * Math.cos(stepLength * i)), y + (radius * Math.sin(stepLength * i)));
			graphics.lineTo(x, y);
		}
		else
			for (i in 1...steps) 
				graphics.lineTo(x + (radius * Math.cos(stepLength * i)), y + (radius * Math.sin(stepLength * i)));
	}
	
	public function drawArcDegrees(graphics:Graphics, from:Float, to:Float, precision:Float = 100, wedge:Bool = false):Void
	{
		drawArc(graphics, (from / 180) * Math.PI, (to / 180) * Math.PI, precision, wedge);
	}
	
	private inline function get_diameter():Float
	{
		return radius * 2;
	}
	private inline function set_diameter(val:Float):Float
	{
		radius = val / 2;
		return val;
	}
	
	private inline function get_circumference():Float
	{
		return radius * 2 * Math.PI;
	}
	private inline function set_circumference(val:Float):Float
	{
		radius = val / 2 / Math.PI;
		return val;
	}
	
	private inline function get_area():Float
	{
		return Math.PI * radius * radius;
	}
	private inline function set_area(val:Float):Float
	{
		radius = Math.sqrt(val / Math.PI);
		return val;
	}
}