package com.problemmachine.util.multithreading.event;

import flash.events.Event;

class WorkerMessengerEvent extends Event
{
	static public inline var RECEIVED_NEW_MESSAGE:String = "ReceivedNewMessage (ThreadMessengerEvent)";
	static public inline var SENT_MESSAGE_RECEIVED:String = "SentMessageReceived (ThreadMessengerEvent)";

	public function new(type:String, bubbles:Bool=false, cancelable:Bool=false) 
	{
		super(type, bubbles, cancelable);
	}
	
}