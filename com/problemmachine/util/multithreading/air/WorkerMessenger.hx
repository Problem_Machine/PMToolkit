package com.problemmachine.util.multithreading.air;
import com.problemmachine.util.multithreading.event.WorkerMessengerEvent;
import flash.errors.Error;
import flash.events.EventDispatcher;
import flash.events.TimerEvent;
import flash.utils.Timer;
import flash.system.Worker;
import flash.concurrent.Mutex;

class WorkerMessenger extends EventDispatcher
{
	private var mMessageReceiveLocation:String;
	private var mMessageSendLocation:String;
	
	private var mWorker:Worker;
	private var mTimer:Timer;
	private var mLastNumReceived:Int;
	private var mLastNumSent:Int;
	
	public var numMessages(get, never):Int;
	public var numSentMessages(get, never):Int;
	public var oneWay(get, never):Bool;
	public var enableNotifications(default, set):Bool;
	public var sendMutex(default, null):Mutex;
	public var receiveMutex(default, null):Mutex;
	
	public function new(?worker:Worker, receiveLocation:String, ?sendLocation:String) 
	{
		super();
		if (worker == null)
			worker = Worker.current;
		mWorker = worker;
		mMessageReceiveLocation = receiveLocation;
		mMessageSendLocation = sendLocation;
		if (worker.getSharedProperty(receiveLocation) == null)
			worker.setSharedProperty(receiveLocation, new Array<Int>());
		if (worker.getSharedProperty(receiveLocation + "_MUTEX") == null)
			worker.setSharedProperty(receiveLocation + "_MUTEX", receiveMutex = new Mutex());
		else 
			receiveMutex = worker.getSharedProperty(receiveLocation + "_MUTEX");
		if (sendLocation != null)
		{
			if (worker.getSharedProperty(sendLocation) == null)
				worker.setSharedProperty(sendLocation, new Array<Int>());
			if (worker.getSharedProperty(sendLocation + "_MUTEX") == null)
				worker.setSharedProperty(sendLocation + "_MUTEX", sendMutex = new Mutex());
			else 
				sendMutex = worker.getSharedProperty(sendLocation + "_MUTEX");
		}
		mLastNumReceived = 0;
		mLastNumSent = 0;
	}
	
	private inline function get_oneWay():Bool
	{	return mMessageSendLocation == null;	}
	
	private function get_numMessages():Int
	{	
		var indices:Array<Int> = cast mWorker.getSharedProperty(mMessageReceiveLocation);
		return indices.length;
	}
	
	private function get_numSentMessages():Int
	{	
		var indices:Array<Int> = cast mWorker.getSharedProperty(mMessageSendLocation);
		return indices.length;
	}
	
	private function set_enableNotifications(val:Bool):Bool
	{
		if (val != enableNotifications)
		{
			enableNotifications = val;
			if (enableNotifications)
			{
				mTimer = new Timer(1);
				mTimer.addEventListener(TimerEvent.TIMER, notificationListener);
				mTimer.start();
				mLastNumReceived = numMessages;
				mLastNumSent = cast (mWorker.getSharedProperty(mMessageSendLocation), Array<Dynamic>).length;
			}
			else if (mTimer != null)
			{
				mTimer.removeEventListener(TimerEvent.TIMER, notificationListener);
				mTimer.stop();
				mTimer = null;
			}
		}
		return val;
	}
	
	private function notificationListener(e:TimerEvent):Void
	{
		if (numMessages > mLastNumReceived)
			dispatchEvent(new WorkerMessengerEvent(WorkerMessengerEvent.RECEIVED_NEW_MESSAGE));
		var sent:Int = cast (mWorker.getSharedProperty(mMessageSendLocation), Array<Dynamic>).length;
		if (sent < mLastNumSent)
			dispatchEvent(new WorkerMessengerEvent(WorkerMessengerEvent.SENT_MESSAGE_RECEIVED));
		mLastNumSent = sent;
		mLastNumReceived = numMessages;
	}
	
	public function send(message:Dynamic):Void
	{
		if (mMessageSendLocation == null)
			throw new Error("ERROR in WorkerMessenger.send(): this WorkerMessenger is one-way");
		sendMutex.lock();
		var indices:Array<Int> = cast mWorker.getSharedProperty(mMessageSendLocation);
		var i:Int = 0;
		while (indices.indexOf(i) >= 0)
			++i;
		indices.push(i);
		mWorker.setSharedProperty(mMessageSendLocation, indices);
		mWorker.setSharedProperty(mMessageSendLocation + "_#" + i, message);
		sendMutex.unlock();
	}
	
	public function receive():Dynamic
	{
		receiveMutex.lock();
		var indices:Array<Int> = cast mWorker.getSharedProperty(mMessageReceiveLocation);
		if (indices.length == 0)
			return null;
		var i:Int = indices.shift();
		var d:Dynamic = mWorker.getSharedProperty(mMessageReceiveLocation + "_#" + i);
		mWorker.setSharedProperty(mMessageReceiveLocation + "_#" + i, null);
		mWorker.setSharedProperty(mMessageReceiveLocation, indices);
		receiveMutex.unlock();
		return d;
	}
	
	public function dispose():Void
	{
		enableNotifications = false;
	}
}