package com.problemmachine.util.variablewrapper;
import haxe.ds.Vector.Vector;

class VariableWrapper<T>
{
	public var value(get, set):T;
	private var get_value:Void->T;
	private var set_value:T->T;
	
	private function new(get:Void->T, set:T->T)
	{	
		get_value = get;
		set_value = set;
	}
	
	static inline public function wrapObjectProperty<T>(obj:Dynamic, prop:String):VariableWrapper<T>
	{
		return new VariableWrapper<T>(getProperty(obj, prop), setProperty(obj, prop));
	}
	
	static inline public function wrapObjectField<T>(obj:Dynamic, field:String):VariableWrapper<T>
	{
		return new VariableWrapper<T>(getField(obj, field), setField(obj, field));
	}
	
	static inline public function wrapArrayValue<T>(arr:Array<T>, index:Int):VariableWrapper<T>
	{
		return new VariableWrapper<T>(getArrayVal(arr, index), setArrayVal(arr, index));
	}
	
	static inline public function wrapVectorValue<T>(vec:Vector<T>, index:Int):VariableWrapper<T>
	{
		return new VariableWrapper<T>(getVectorVal(vec, index), setVectorVal(vec, index));
	}
	
	static inline public function wrapMappedValue<T, K>(map:Map<K, T>, key:K):VariableWrapper<T>
	{
		return new VariableWrapper<T>(getMapVal(map, key), setMapVal(map, key));
	}
	
	static inline public function wrapFunctionCalls<T>(get:Void->T, set:T->T):VariableWrapper<T>
	{
		return new VariableWrapper<T>(get, set);
	}
	
	static inline private function getProperty(obj:Dynamic, prop:String):Void->Dynamic
	{	
		return (
			function():Dynamic
			{
				return Reflect.getProperty(obj, prop);
			});
	}
	static inline private function setProperty(obj:Dynamic, prop:String):Dynamic->Dynamic
	{	
		return (
			function(val:Dynamic):Dynamic
			{
				Reflect.setProperty(obj, prop, val);	return val;
			});
	}
	static inline private function getField(obj:Dynamic, field:String):Void->Dynamic
	{	
		return (
			function():Dynamic
			{
				return Reflect.field(obj, field);
			});
	}
	static inline private function setField(obj:Dynamic, field:String):Dynamic->Dynamic
	{	
		return (
			function(val:Dynamic):Dynamic
			{
				Reflect.setField(obj, field, val);	return val;
			});
	}
	static inline private function getArrayVal<T>(arr:Array<T>, index:Int):Void->T
	{	
		return (
			function():T
			{ 
				return arr[index]; 
			} );						
	}
	static inline private function setArrayVal<T>(arr:Array<T>, index:Int):T->T
	{	
		return (
			function(val:T):T
			{ 
				return (arr[index] = val); 
			} );		
	}
	static inline private function getVectorVal<T>(arr:Vector<T>, index:Int):Void->T
	{	
		return (
			function():T 
			{ 
				return arr[index]; 
			} );						
	}
	static inline private function setVectorVal<T>(arr:Vector<T>, index:Int):T->T
	{	
		return (
			function(val:T):T 
			{ 
				return (arr[index] = val); 
			} );		
	}
	static inline private function getMapVal<T, K>(map:Map<K, T>, key:K):Void->T
	{	
		return (
			function():T 
			{ 
				return map.get(key); 
			} );
	}
	static inline private function setMapVal<T, K>(map:Map<K, T>, key:K):T->T
	{	
		return (
			function(val:T):T 
			{ 
				map.set(key, val); return val; 
			} );	
	}
}