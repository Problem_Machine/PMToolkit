package com.problemmachine.util.bitset;
import flash.utils.ByteArray;

class BitSet
{
	public var length(default, set):UInt;
	public var contents:ByteArray;

	public function new(length:Int)
	{
		contents = new ByteArray();
		this.length = length;
	}
	
	private inline function set_length(val:UInt):UInt
	{
		contents.length = val >>> 3;
		return length = val;
	}
	
	public inline function get(index:Int):Bool
	{
		return (((contents[index >> 3] >>> (7 - (index & 0x7))) & 0x01) == 1);
	}
	
	public inline function set(index:Int, val:Bool):BitSet
	{
		contents[index >> 3] ^= (val ? 1 : 0) << (7 - (index & 0x7));
		return this;
	}
	
	public inline function flip(index:Int):BitSet
	{
		contents[index >> 3] ^= 1 << (7 - (index & 0x7));
		return this;
	}
	
	public function toString():String
	{
		var s:String = "";
		contents.position = 0;
		while (contents.position < contents.length)
		{
			var byte:Int = contents.readByte();
			for (i in 0...8)
			{
				if (contents.position * 8 + i >= length)
					break;
				if ((byte >> (7 - i)) & 0x01 == 1)
					s += "1";
				else
					s += "0";
			}
			s += " ";
			if (contents.position % 8 == 0)
				s += "\n";
		}
		return s;
	}
	
}