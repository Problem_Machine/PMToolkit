package com.problemmachine.util.multiscalar;

class MultiScalar
{
	private var mScalarValues:Array<MultiScalarValue>;

	public var baseValue:Float;
	public var value(get, never):Float;
	public var minValue:Null<Float>;
	public var maxValue:Null<Float>;
	
	public function new(baseValue:Float = 1, ?minValue:Null<Float>, ?maxValue:Null<Float>) 
	{	
		mScalarValues = [];
		this.baseValue = baseValue;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}
	
	public inline function modify(source:Dynamic, multiplier:Float = 1, offset:Float = 0):Void
	{
		var index:Int = -1;
		for (i in 0...mScalarValues.length)
			if (mScalarValues[i].source == source)
			{
				index = i;
				break;
			}
		if (multiplier == 1 && offset == 0)
		{
			if (index >= 0)
				mScalarValues.splice(index, 1);
		}
		else
		{
			if (index >= 0)
			{
				mScalarValues[index].mult = multiplier;
				mScalarValues[index].offset = offset;
			}
			else
				mScalarValues.push(new MultiScalarValue(source, multiplier, offset));
		}
	}	
	
	private inline function get_value():Float
	{
		var val:Float = baseValue;
		for (v in mScalarValues)
			val += v.offset;
		for (v in mScalarValues)
			val *= v.mult;
		if (minValue != null)
			val = Math.max(val, minValue);
		if (maxValue != null)
			val = Math.min(val, maxValue);
		return val;
	}
}

private class MultiScalarValue
{
	public var source:Dynamic;
	public var mult:Float;
	public var offset:Float;
	
	public function new(source:Dynamic, mult:Float = 1, offset:Float = 0)
	{
		this.source = source;
		this.mult = mult;
		this.offset = offset;
	}
}