package com.problemmachine.ui.selector.event;

import flash.events.Event;

class SelectorEvent extends Event
{
	public static inline var SELECTED:String = "SelectorEvent.Selected";
	public static inline var CLICK_SELECTED:String = "SelectorEvent.ClickSelected";
	public static inline var SWAPPED:String = "SelectorEvent.Swapped";
	public static inline var DRAG_OFF:String = "SelectorEvent.DragOff";
	public var item(default, null):Dynamic;
	
	public function new(type:String, item:Dynamic, bubbles:Bool = false, cancelable:Bool = false)
	{
		super(type, bubbles, cancelable);
		this.item = item;
	}	
}