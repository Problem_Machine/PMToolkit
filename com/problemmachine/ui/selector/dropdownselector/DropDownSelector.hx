package com.problemmachine.ui.selector.dropdownselector;
import com.problemmachine.ui.selector.event.SelectorEvent;
import com.problemmachine.ui.selector.textlistselector.TextListSelector;
import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.display.Stage;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.text.TextFormat;

class DropDownSelector extends Sprite
{
	private var mSelector:TextListSelector;
	private var mTextBox:Sprite;
	private var mCurrentText:TextField;
	private var mWidth:Float;
	private var mInitialHeight:Float;
	
	public var enabled(default, set):Bool;
	public var openUpwards(default, set):Bool;
	
	public function new(width:Float, initialHeight:Float, expandedHeight:Float, color1:UInt, color2:UInt, 
		?items:Array<String>, barThickness:Float = 30, textSize:Int = 16) 
	{
		super();
		mWidth = width;
		mInitialHeight = initialHeight;
		mSelector = new TextListSelector(mWidth, expandedHeight, color1, color2, barThickness, textSize);
		mTextBox = new Sprite();
		mTextBox.buttonMode = true;
		mTextBox.addEventListener(MouseEvent.CLICK, showListListener);
		mSelector.enableHorizontalResize = false;
		mSelector.enableHorizontalScroll = false;
		mSelector.enableMovement = false;
		mSelector.enableVerticalResize = false;
		mCurrentText = new TextField();
		mCurrentText.defaultTextFormat = new TextFormat(null, textSize, color1);
		mCurrentText.width = width - mSelector.barThickness;
		mCurrentText.height = initialHeight;
		mCurrentText.mouseEnabled = false;
		mTextBox.addChild(mCurrentText);
		displayList(items);
		addChild(mTextBox);
		setColor(color1, color2);
		enabled = true;
		openUpwards = false;
	}
	
	private function set_enabled(val:Bool):Bool
	{
		if (val != enabled)
		{
			mouseEnabled = val;
			if (mSelector.parent != null)
				hideListListener(null);
		}
		return enabled = val;
	}
	
	private function set_openUpwards(val:Bool):Bool
	{
		if (val != openUpwards)
		{
			if (val)
				mSelector.y = -mSelector.height + height;
			else
				mSelector.y = 0;
		}
		return openUpwards = val;
	}
	
	public function setColor(color1:UInt, color2:UInt):Void
	{
		var t:Float = mSelector.barThickness;
		mCurrentText.textColor = color1;
		mTextBox.graphics.clear();
		mTextBox.graphics.lineStyle(t * 0.1, color1);
		mTextBox.graphics.beginFill(color2);
		mTextBox.graphics.drawRect(0, 0, mWidth, mInitialHeight);
		mTextBox.graphics.endFill();
		mTextBox.graphics.lineStyle(t * 0.1, color2);
		mTextBox.graphics.beginFill(color1);
		mTextBox.graphics.drawRect(mWidth - t * 0.95, 0, t * 0.90, mInitialHeight);
		mTextBox.graphics.lineStyle();
		mTextBox.graphics.beginFill(color2);
		mTextBox.graphics.moveTo(mWidth - t * 0.8, t * 0.4); 	mTextBox.graphics.lineTo(mWidth - t * 0.2, t * 0.4);
		mTextBox.graphics.lineTo(mWidth - t * 0.5, t * 0.6);	mTextBox.graphics.lineTo(mWidth - t * 0.8, t * 0.4);
		mSelector.color1 = color1;
		mSelector.color2 = color2;
	}
	
	public function setSelectedEntry(selected:String = ""):Void
	{
		mSelector.selected = selected;
		if (mSelector.selected != null)
		{
			mCurrentText.text = selected;
			dispatchEvent(new SelectorEvent(SelectorEvent.SELECTED, mSelector.selected));
		}
		else
			mCurrentText.text = "";
	}
	
	private function showListListener(e:MouseEvent):Void
	{
		mSelector.selected = mCurrentText.text;
		mSelector.addEventListener(SelectorEvent.SELECTED, selectItemListener);
		stage.addEventListener(MouseEvent.CLICK, hideListListener);
	}
	
	private function selectItemListener(e:SelectorEvent):Void
	{
		mCurrentText.text = (e.item != null) ? cast e.item : "";
		dispatchEvent(new SelectorEvent(SelectorEvent.SELECTED, e.item));
	}
	
	private function hideListListener(e:MouseEvent):Void
	{	
		if (mSelector.parent == null)
		{
			addChild(mSelector.panel);
			return;
		}
		if (e != null && cast (e.target, DisplayObject).parent == mSelector.panel)
			return;
		removeChild(mSelector.panel);
		mSelector.removeEventListener(SelectorEvent.SELECTED, selectItemListener);
		if (e != null)
			cast (e.currentTarget, Stage).removeEventListener(MouseEvent.CLICK, hideListListener);
	}
	
	public inline function getDisplayItems():Array<String>
	{
		return cast mSelector.getDisplayItems();
	}
	
	public inline function displayList(items:Array<String>):Void
	{
		mSelector.displayList(items);
	}
}