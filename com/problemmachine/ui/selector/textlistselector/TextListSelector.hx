package com.problemmachine.ui.selector.textlistselector;
import com.problemmachine.tools.classes.ClassTools;
import com.problemmachine.ui.scrollwindow.ScrollWindow;
import com.problemmachine.ui.selector.event.SelectorEvent;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.geom.Rectangle;
import flash.Lib;
import flash.text.TextField;
import flash.text.TextFormat;

class TextListSelector extends ScrollWindow
{
	private var mTextFields:Array<TextField>;
	private var mTextLinks:Array<Dynamic>;
	private var mSelectedBox:Sprite;
	private var mDraggingItem:Bool;
	private var mFont:String;
	
	public var selected(default, set):Dynamic;
	public var enableReorderList:Bool;
	public var enableMultipleSelection(default, set):Bool;
	public var textSize(default, set):Int;
	
	public var numberOfItems(get, never):Int;
		
	public function new(width:Float, height:Float, color1:UInt, ?color2:UInt, barThickness:Float = 30, textSize:Int = 12, ?font:String)
	{
		super(width, height, color1, color2, barThickness);

		mDraggingItem = false;
		enableReorderList = false;
		enableMultipleSelection = false;
		mFont = font;
		
		mSelectedBox = new Sprite();
		mSelectedBox.x = textSize * 0.25;
		addChild(mSelectedBox);
		
		mTextFields = new Array<TextField>();
		mTextLinks = new Array<Dynamic>();
		
		this.textSize = textSize;
		
		Lib.current.stage.addEventListener(MouseEvent.MOUSE_UP, panelMouseListener);
	}
	
	private function set_selected(val:Dynamic):Dynamic
	{
		if (val != selected)
		{
			selected = null;
			if (Std.is(val, Array))
			{				
				selected = val;
				var remove:Array<Dynamic> = [];
				for (d in cast(selected, Array<Dynamic>))
					if (mTextLinks.indexOf(d) < 0)
						remove.push(d);
				for (d in remove)
					selected.remove(d);
				if (selected.length == 0)
					selected = null;
			}
			else if (val != null)
			{
				if (mTextLinks.indexOf(val) >= 0)
					selected = val;
			}
			dispatchEvent(new SelectorEvent(SelectorEvent.SELECTED, selected));
			drawSelectionBox();
		}
		return selected;
	}
	
	private function set_textSize(val:Int):Int
	{
		if (val != textSize)
		{
			textSize = val;
			displayList(mTextLinks);
		}
		return val;
	}
	
	private function set_enableMultipleSelection(val:Bool):Bool
	{
		if (val != enableMultipleSelection)
		{
			enableMultipleSelection = val;
			if (!val && Std.is(selected, Array))
				selected = null;				
		}
		return val;
	}
	
	private inline function get_numberOfItems():Int
	{	return mTextFields.length;	}
	
	private function drawSelectionBox():Void
	{
		mSelectedBox.graphics.clear();
		if (selected == null)
			return;
		mSelectedBox.graphics.lineStyle(Math.min(width, height) * 0.015, color1);
		if (Std.is(selected, Array))
		{
			mSelectedBox.graphics.beginFill(color2);
			for (d in cast(selected, Array<Dynamic>))
				mSelectedBox.graphics.drawRoundRect(0, mTextFields[mTextLinks.indexOf(d)].y + textSize * 0.15, 
					width - textSize * 0.5, textSize * 1.7, width * 0.05, textSize * 0.05);
			mSelectedBox.graphics.endFill();
		}
		else
		{
			mSelectedBox.graphics.beginFill(color2);
			mSelectedBox.graphics.drawRoundRect(0, mTextFields[mTextLinks.indexOf(selected)].y, 
				width - textSize * 0.5, textSize * 2, width * 0.05, textSize * 0.05);
		}
	}
	
	public function displayList(?list:Array<Dynamic>):Void
	{
		if (list == null)
			list = [];
		if (Std.is(selected, Array))
		{
			var remove:Array<Dynamic> = [];
			for (d in cast(selected, Array<Dynamic>))
				if (list.indexOf(d) < 0)
					remove.push(d);
			for (d in remove)
				selected.remove(d);
			if (selected.length == 0)
				selected = null;
		}
		else if (list.indexOf(selected) < 0)
			selected = null;
		if (selected == null && mDraggingItem)
			cancelDrag();
		
		while (list.length > mTextFields.length)
		{
			var tf:TextField = new TextField();
			// dispatch event on mouse down (instead of click) to allow dragging from selector
			tf.addEventListener(MouseEvent.MOUSE_DOWN, textMouseListener);
			tf.defaultTextFormat = new TextFormat(mFont, textSize, color1);
			tf.width = width * 0.9;
			tf.x = width * 0.05;
			tf.selectable = false;
			addChild(tf);
			mTextFields.push(tf);
			mTextLinks.push(null);
		}
		while (mTextFields.length > list.length)
		{
			var tf:TextField = mTextFields.pop();
			tf.removeEventListener(MouseEvent.MOUSE_DOWN, textMouseListener);
			removeChild(tf);
			mTextLinks.pop();
		}
		
		for (i in 0...mTextFields.length)
		{			
			mTextFields[i].height = textSize * 2;
			mTextFields[i].y = i * textSize * 2;
			if (list[i] != null)
			{
				var s:String =
					if (Std.is(list[i], String))
						list[i];
					else if (Std.is(list[i], Class))
						ClassTools.getClassName(cast list[i]);
					else if (Reflect.hasField(list[i], "toString"))
						cast(Reflect.callMethod(list[i], Reflect.field(list[i], "toString"), []), String);
					else
						Std.string(list[i]);
				mTextFields[i].text = s;
			}
			else
				mTextFields[i].text = "";
			mTextLinks[i] = list[i];
		}
		checkSize();
		drawSelectionBox();
	}
	
	public function getDisplayItems():Array<Dynamic>
	{
		return mTextLinks.copy();
	}
	
	private function textMouseListener(e:MouseEvent):Void
	{
		var index:Int = -1;
		for (i in 0...mTextFields.length)
			if (mTextFields[i] == e.target)
			{
				index = i;
				break;
			}
		if (selected == mTextLinks[index])
		{
			if (enableMultipleSelection && e.ctrlKey)
				selected = null;
			else
				dispatchEvent(new SelectorEvent(SelectorEvent.CLICK_SELECTED, selected));
		}
		else if (Std.is(selected, Array) && selected.indexOf(mTextLinks[index]) >= 0 && !(e.ctrlKey || e.shiftKey))
		{
			if (enableReorderList)
				selected = mTextLinks[index];
			else
				dispatchEvent(new SelectorEvent(SelectorEvent.CLICK_SELECTED, mTextLinks[index]));
		}
		else if (enableMultipleSelection && e.ctrlKey)
		{
			if (!Std.is(selected, Array))
				selected = [selected];
				
			if (selected.indexOf(mTextLinks[index]) >= 0)
				selected.remove(mTextLinks[index]);
			else
				selected.push(mTextLinks[index]);
			if (selected.length == 0)
				selected = null;
		}
		else if (enableMultipleSelection && e.shiftKey)
		{
			var start:Int = 0;
			
			if (Std.is(selected, Array))
			{
				start = 0xFFFFFF;
				for (d in cast(selected, Array<Dynamic>))
				{
					var i:Int = mTextLinks.indexOf(d);
					if (i < start)
						start = i;
				}
			}
			else
			{
				start = mTextLinks.indexOf(selected);
				selected = [selected];
			}
			if (index < start)
				selected = mTextLinks.slice(index, start + 1);
			else
				selected = mTextLinks.slice(start, index + 1);
		}
		else
			selected = mTextLinks[index];
			
		if (selected != null)
		{
			panel.stage.addEventListener(MouseEvent.MOUSE_MOVE, panelMouseListener);
			mDraggingItem = true;
		}
		
		drawSelectionBox();
		
		if (enableReorderList && mTextFields.length > 1 && !(Std.is(selected, Array)))
		{
			var h:Float = 0;
			var tf:TextField = null;
			for (i in 0...mTextFields.length)
				if (mTextLinks[i] == mDraggingItem)
					tf = mTextFields[i];
				else
					h += mTextFields[i].height;
			orderTextFieldsDuringDrag(tf);
		}
	}
	
	private function orderTextFieldsDuringDrag(?draggingText:TextField):Void
	{
		var h:Float = 0;
		for (i in 0...mTextFields.length)
		{
			if (mTextFields[i].parent != null)
				mTextFields[i].parent.removeChild(mTextFields[i]);
			if (draggingText == null || mTextLinks[i] != selected)
				mTextFields[i].y = h;
			h += mTextFields[i].height;
			addChild(mTextFields[i]);
		}
		if (draggingText != null)
		{
			draggingText.parent.removeChild(draggingText);
			addChild(draggingText);
		}
		drawSelectionBox();
	}
	
	private function cancelDrag():Void
	{
		panel.stage.removeEventListener(MouseEvent.MOUSE_MOVE, panelMouseListener);
		orderTextFieldsDuringDrag();
		mDraggingItem = false;
	}
	
	private function panelMouseListener(e:MouseEvent):Void
	{
		if (!mDraggingItem)
			return;
			
		var tf:TextField = null;
		if (enableReorderList && mTextFields.length > 1)
		{
			var src:Int = 0;
			var tgt:Int = 0;
			for (i in 0...mTextFields.length)
			{
				if (mTextLinks[i] == selected)
				{
					src = i;
					tf = mTextFields[i];
					tf.y = Math.max(0, Math.min((mTextFields.length - 1) * mTextFields[0].height, panel.mouseY));
					if (i > 0 && mTextFields[i - 1].mouseY > mTextFields[i - 1].height)
						tgt = i;
				}
				else if (mTextFields[i].mouseY > 0)
					tgt = i;
			}
			if (src != tgt && tf != null)
			{
				mTextFields.splice(src, 1);
				mTextFields.insert(tgt, tf);
				if (Std.is(selected, Array))
				{
					var arr:Array<Dynamic> = cast selected;
					for (s in arr)
						mTextLinks.remove(selected);
					for (i in 0...arr.length)
						mTextLinks.insert(tgt, arr[arr.length - 1 - i]);
				}
				else
				{
					mTextLinks.splice(src, 1);
					mTextLinks.insert(tgt, selected);
				}
				// target and source positions are now reversed, so [tgt] refers to the dragged text field
				dispatchEvent(new SelectorEvent(SelectorEvent.SWAPPED, [mTextLinks[tgt], mTextLinks[src]]));
				orderTextFieldsDuringDrag();
			}
		}
		var out:Bool = !mMask.hitTestPoint(mMask.stage.mouseX, mMask.stage.mouseY);
		if (e.type == MouseEvent.MOUSE_UP || out)
		{
			panel.stage.removeEventListener(MouseEvent.MOUSE_MOVE, panelMouseListener);
			orderTextFieldsDuringDrag();
			mDraggingItem = false;
			if (out)
				dispatchEvent(new SelectorEvent(SelectorEvent.DRAG_OFF, selected));
		}
	}
}