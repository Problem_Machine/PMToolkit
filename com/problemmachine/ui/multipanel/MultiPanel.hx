package com.problemmachine.util.multipanel;

import com.problemmachine.ui.scrollwindow.ScrollWindow;
import flash.display.DisplayObject;
import flash.display.GradientType;
import flash.display.Sprite;
import flash.errors.ArgumentError;
import flash.events.Event;
import flash.events.MouseEvent;

class MultiPanel extends ScrollWindow
{
	private var mDisplays:Array<DisplayObject>;
	
	private var mInterfaceOverlay:Sprite;
	private var mBack:Sprite;
	
	public function new(width:Float, height:Float, color1:UInt = 0xFFFFFF, color2:UInt = 0x444444, barThickness:Float = 30) 
	{
		mInterfaceOverlay = new Sprite();
		mDisplays = new Array<DisplayObject>();
		
		super(width, height, color1, color2, barThickness);
		
		enableMovement = false;
		enableHorizontalResize = false;
		enableVerticalResize = false;
		enableVerticalScroll = false;
		enableHorizontalScroll = true;
		
		addChild(mInterfaceOverlay);
		mInterfaceOverlay.mouseEnabled = false;
	}
	
	public function clear():Void
	{
		var d:DisplayObject;
		while ((d = mDisplays.pop()) != null)
			mContainer.removeChild(d);
	}
	override public function addChild(child:DisplayObject):DisplayObject 
	{
		for (s in mDisplays)
			if (s == child)
				return super.addChild(child);		
		mDisplays.push(child);		
		return super.addChild(child);
	}
	
	override public function addChildAt(child:DisplayObject, index:Int):DisplayObject 
	{
		for (s in mDisplays)
			if (s == child)
				super.addChildAt(child, index);
		mDisplays.insert(index, child);		
		return super.addChildAt(child, index);
	}
	
	override public function removeChild(child:DisplayObject):DisplayObject 
	{
		for (i in 0...mDisplays.length)
			if (mDisplays[i] == child)
			{
				mDisplays.splice(i, 1);
				break;
			}
		super.removeChild(child);
		return child;
	}
	
	override public function removeChildAt(index:Int):DisplayObject 
	{
		var c:DisplayObject = super.removeChild(mDisplays[index]);
		mDisplays.splice(index, 1);
		return c;
	}
	
	override public function getChildAt(index:Int):DisplayObject 
	{
		if (index >= 0 && index < mDisplays.length)
			return mDisplays[index];
		else
			return null;
	}
	
	override public function getChildIndex(child:DisplayObject):Int 
	{
		for (i in 0...mDisplays.length)
			if (mDisplays[i] == child)
				return i;
		throw new ArgumentError("ERROR -MultiPanel.getChildIndex(): DisplayObject instance parameter is not a child of this window");
	}
		
	override private function checkSize(?e:Event):Void
	{
		arrange();
		super.checkSize(e);
	}
	
	private function arrange():Void
	{
		var w:Float = 0;
		mInterfaceOverlay.graphics.clear();
		mInterfaceOverlay.graphics.lineStyle(barThickness * 0.1, color1);
		mInterfaceOverlay.graphics.beginGradientFill(GradientType.LINEAR, [color1, color2], [255, 255], [0, 255]);
		for (i in 0...mDisplays.length)
		{
			mInterfaceOverlay.graphics.drawRect(w + barThickness * 0.05, barThickness * 0.05, barThickness * 0.9, height - barThickness * 0.1);
			w += barThickness * 0.9;
			mDisplays[i].x = w;
			mDisplays[i].y = barThickness * 0.1;
			if (mDisplays[i].mask != null)
				w += mDisplays[i].mask.width;
			else
				w += mDisplays[i].width;
		}
	}
}