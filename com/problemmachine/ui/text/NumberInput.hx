package com.problemmachine.ui.text;
import com.problemmachine.tools.text.TextTools;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.FocusEvent;
import flash.Lib;
import flash.text.TextField;
import flash.text.TextFieldType;
import flash.text.TextFormat;

class NumberInput extends Sprite
{
	static public inline var VALUE_CHANGE:String = "Value Change Event";
	
	public var textField(default, null):TextField;
	public var precision(default, set):Int;
	public var value(default, set):Float;
	public var autoSize(default, set):Bool;
	public var editing(get, never):Bool;
	
	public function new(value:Float, width:Float, height:Float, ?textFormat:TextFormat, precision:Int = -1, border:Bool = true, background:Bool = true, 
		borderColor:UInt = 0x000000, backgroundColor:UInt = 0xFFFFFF, autoSize:Bool = true) 
	{
		super(); 
		textField = new TextField();
		if (textFormat != null)
			textField.defaultTextFormat = textFormat;
		textField.type = TextFieldType.INPUT;
		textField.multiline = true;
		textField.addEventListener(Event.CHANGE, textListener);
		textField.addEventListener(FocusEvent.FOCUS_OUT, textCompleteListener);
		textField.width = width;
		textField.height = height;
		
		textField.border = border;
		textField.borderColor = borderColor;
		textField.background = background;
		textField.backgroundColor = backgroundColor;
		
		addChild(textField);
		
		this.precision = precision;
		
		this.value = value;
		this.autoSize = autoSize;
	}
	
	private function set_autoSize(val:Bool):Bool
	{
		if (val != autoSize)
		{
			autoSize = val;
			if (autoSize)
				TextTools.scaleTextToFit(textField);
		}
		return val;
	}
	
	private function set_precision(val:Int):Int
	{
		if (val != precision)
		{
			precision = val;
			value = Math.round(value * Math.pow(10, precision)) / Math.pow(10, precision);
			fillPrecision();
			if (autoSize)
				TextTools.scaleTextToFit(textField);
		}
		return val;
	}
	
	private function get_editing():Bool
	{	return (Lib.current.stage.focus == textField);	}
	
	private function set_value(val:Float):Float
	{
		if (val != value)
		{
			value = Math.round(val * Math.pow(10, precision)) / Math.pow(10, precision);
			textField.text = Std.string(value);
			fillPrecision();
			if (autoSize)
				TextTools.scaleTextToFit(textField);
		}
		return val;
	}
	
	public function resize(newWidth:Float, newHeight:Float):Void
	{
		textField.width = newWidth;
		textField.height = newHeight;
		if (autoSize)
			TextTools.scaleTextToFit(textField);
	}
	
	private function textListener(e:Event):Void
	{
		var s:String = "";
		var done:Bool = false;
		var dec:Bool = false;
		var remainingPrecision:UInt = ((precision >= 0) ? precision : 0xFFFFFF);
		for (i in 0...textField.text.length)
		{
			var char:String = textField.text.charAt(i);
			if (char == "\n" || char == "\r")
				done = true;
			else if ((i == 0 && char == "-") ||
				(precision != 0 && !dec && char == ".") ||
				("0123456789".indexOf(char) >= 0 && !(dec && remainingPrecision <= 0)))
				s += char;
			if (char == ".")
				dec == true;
			else if (dec)
				--remainingPrecision;
		}
		textField.text = s;
		if (autoSize)
			TextTools.scaleTextToFit(textField);
		if (done)
			stage.focus = stage;
	}
	
	private function textCompleteListener(e:FocusEvent):Void
	{
		var oldValue:Float = value;
		value = Std.parseFloat(textField.text);
		fillPrecision();
		if (value != oldValue)
			dispatchEvent(new Event(VALUE_CHANGE, true));
	}
	
	private function fillPrecision():Void
	{
		var dot:Int = textField.text.lastIndexOf(".");
		var remainingPrecision:Int = 0;
		if (dot < 0 && precision != 0)
		{
			textField.text += ".";
			remainingPrecision = precision;
		}
		else
			remainingPrecision = precision - (textField.length - dot - 1);
		
		while (remainingPrecision-- > 0)
			textField.text += "0";
	}
	
}