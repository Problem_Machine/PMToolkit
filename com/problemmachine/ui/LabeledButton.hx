package com.problemmachine.ui;

import com.problemmachine.tools.text.TextTools;
import flash.display.Sprite;
import flash.text.TextField;
import flash.text.TextFormatAlign;

class LabeledButton extends Sprite
{
	public var label(default, null):TextField;
	public var color1(default, set):UInt;
	public var color2(default, set):UInt;
	public var roundedCorners(default, set):Bool;
	
	public function new(label:String, width:Float, height:Float, color1:UInt, color2:UInt, roundedCorners:Bool = false) 
	{
		super();
		buttonMode = useHandCursor = true;
		Reflect.setField(this, "color1", color1);
		Reflect.setField(this, "color2", color2);
		Reflect.setField(this, "roundedCorners", roundedCorners);
		this.label = new TextField();
		this.label.defaultTextFormat.align = TextFormatAlign.CENTER;
		this.label.mouseEnabled = false;
		this.label.text = label;
		this.label.textColor = color1;
		addChild(this.label);
		resize(width, height);
	}
	
	private function set_color1(val:UInt):UInt
	{
		if (val != color1)
		{
			color1 = val;
			label.textColor = color1;
			resize(width, height);
		}
		return val;
	}
	
	private function set_color2(val:UInt):UInt
	{
		if (val != color2)
		{
			color2 = val;
			resize(width, height);
		}
		return val;
	}
	
	private function set_roundedCorners(val:Bool):Bool
	{
		if (val != roundedCorners)
		{
			roundedCorners = val;
			resize(width, height);
		}
		return val;
	}
	
	public function resize(width:Float, height:Float):Void
	{
		graphics.clear();
		graphics.lineStyle(width / 50, color1, 1);
		graphics.beginFill(color2);
		if (roundedCorners)
			graphics.drawRoundRect(width / 100, width / 100, width - width / 50, height - width / 50, width / 50);
		else
			graphics.drawRect(width / 100, width / 100, width - width / 50, height - width / 50);
		label.width = width;
		label.height = height;
		TextTools.scaleTextToFit(label);
	}
	
}