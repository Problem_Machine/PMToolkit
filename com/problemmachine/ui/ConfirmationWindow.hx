package com.problemmachine.ui;

import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.text.TextFormat;

class ConfirmationWindow extends Sprite 
{
	private var mButtons:Array<Sprite>;
	private var mFunctions:Array<Dynamic>;
	private var mWidth:Float;
	private var mHeight:Float;
	private var mTextField:TextField;
	private var mDefaultTextFormat:TextFormat;
	
	public function new(w:Float, h:Float, msg:String, buttonLabels:Array<String>, 
		functions:Array<Dynamic>, ?buttonColors:Array<UInt>, ?defaultTextFormat:TextFormat) 
	{
		super();
		mWidth = w;
		mHeight = h;
		
		mFunctions = functions;
		mDefaultTextFormat = defaultTextFormat;
		if (defaultTextFormat == null)
			mDefaultTextFormat = new TextFormat(null, Math.ceil(mHeight * 0.06), 0x000000);
		
		makeBackground();
		
		mButtons = new Array<Sprite>();
		for (i in 0...buttonLabels.length)
		{
			var s:Sprite = makeButton(buttonLabels[i], 
				buttonColors != null ? buttonColors[i] : 0x999999, 
				w / (buttonLabels.length * 1.2), 
				h * 0.1);
			s.x = (s.width * i * 1.05) + (s.width * 0.125);
			s.y = mHeight * 0.7;
			mButtons.push(s);
			s.addEventListener(MouseEvent.CLICK, doFunction);
			addChild(s);
		}
		addChild(mTextField = makeMessage(msg));
	}
			
	private function doFunction(e:MouseEvent):Void
	{
		var length:Int = Std.int(Math.min(mButtons.length, mFunctions.length));
		for (i in 0...length)
			if (mButtons[i] == e.currentTarget)
			{
				if (mFunctions[i] != null)
					mFunctions[i]();
				return;
			}
	}
	
	private function makeBackground():Void
	{
		graphics.clear();
		graphics.lineStyle(5, 0xFFFFFF, 0.9);
		graphics.beginFill(0xFFFFFF, 0.75);
		graphics.drawRect(0, 0, mWidth, mHeight);
	}
	
	private function makeMessage(msg:String):TextField
	{
		var t:TextField = new TextField();
		
		t.defaultTextFormat = mDefaultTextFormat;
		t.text = msg;
		t.x = mWidth * 0.1;
		t.width = mWidth * 0.8;
		t.height = mHeight * 0.6;
		t.y = mHeight * 0.1;
		t.mouseEnabled = false;
		return t;
	}
	
	private function makeButton(label:String, color:UInt, w:Float, h:Float):Sprite
	{
		var s:Sprite = new Sprite();
		s.graphics.lineStyle(w * 0.1, color, 1);
		s.graphics.beginFill(color, 0.7);
		s.graphics.drawRect(0, 0, w, h);
		s.buttonMode = true;
		
		var t:TextField = new TextField();
		t.defaultTextFormat = mDefaultTextFormat;
		t.text = label;
		t.x = s.width * 0.1;
		t.width = s.width * 0.8;
		t.y = s.height * 0.1;
		t.height = s.height * 0.8;
		t.mouseEnabled = false;
		
		s.addChild(t);
		return s;
	}
}