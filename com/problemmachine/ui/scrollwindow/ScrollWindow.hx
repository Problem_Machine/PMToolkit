package com.problemmachine.ui.scrollwindow;
import com.problemmachine.tools.display.DisplayTools;
import com.problemmachine.ui.slider.Slider;
import com.problemmachine.ui.slider.SliderEvent;
import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.geom.Rectangle;
import flash.Lib;
import flash.utils.Timer;

class ScrollWindow extends EventDispatcher
{		
	private static inline var MIN_WIDTH:Float = 80;
	private static inline var MIN_HEIGHT:Float = 60;
	
	public static inline var EVENT_RESIZED:String = "ResizedEvent";
	public static inline var EVENT_SCROLLED:String = "ScrolledEvent";
	
	private var mLeftArrow:Sprite;
	private var mHorizontalScrollTrack:Sprite;
	private var mHorizontalScrollHead:Sprite;
	private var mHorizontalSlider:Slider;
	private var mRightArrow:Sprite;	
	
	private var mUpArrow:Sprite;	
	private var mVerticalScrollTrack:Sprite;
	private var mVerticalScrollHead:Sprite;
	private var mVerticalSlider:Slider;
	private var mDownArrow:Sprite;
	
	private var mContainer:Sprite;
	
	public var panel(default, null):Sprite;
	public var width(default, set):Float;
	public var height(default, set):Float;
	public var color1(default, set):UInt;
	public var color2(default, set):UInt;
	public var enableColor2(default, set):Bool;
	public var enableHorizontalScroll(default, set):Bool;
	public var enableHorizontalResize(default, set):Bool;
	public var enableVerticalScroll(default, set):Bool;
	public var enableVerticalResize(default, set):Bool;
	public var enableMovement(default, set):Bool;
	public var barThickness(default, set):Float;
	
	public var x(get, set):Float;
	public var y(get, set):Float;
	public var visible(get, set):Bool;
	public var parent(get, never):DisplayObjectContainer;
	public var mouseX(get, never):Float;
	public var mouseY(get, never):Float;
	
	private var mDragEdges:Sprite;
	private var mDraggingSide:DragSide;
	private var mDragTimer:Timer;
	
	private var mBottomRightCorner:Sprite;
	private var mUpperLeftCorner:Sprite;
	private var mMask:Sprite;
	
	public function new(width:Float, height:Float, color1:UInt = 0xFFFFFF, ?color2:UInt, barThickness:Float = 30) 
	{	
		super(this);
		panel = new Sprite();
		panel.addEventListener(MouseEvent.MOUSE_WHEEL, mouseWheelListener);
		
		mMask = new Sprite();
		mMask.mouseEnabled = false;
		mMask.mouseChildren = false;
		panel.addChild(mMask);
		panel.mask = mMask;
		panel.mouseEnabled = false;
		panel.mouseChildren = true;
		
		mContainer = new Sprite();
		panel.addChild(mContainer);
		mContainer.addEventListener(Event.ENTER_FRAME, checkSize);
		mContainer.mouseEnabled = false;
		mContainer.mouseChildren = true;
		
		mUpArrow = new Sprite();
		mUpArrow.addEventListener(MouseEvent.CLICK, arrowListener);
		mVerticalScrollTrack = new Sprite();
		mVerticalScrollTrack.addEventListener(MouseEvent.CLICK, clickTrackListener);
		mVerticalScrollHead = new Sprite();
		mVerticalSlider = Slider.makeCustomSlider(mVerticalScrollTrack, mVerticalScrollHead, 0, 1, 0.5, true, 1, 3, true);
		mVerticalSlider.addEventListener(SliderEvent.ON_CHANGE, sliderListener);
		mDownArrow = new Sprite();
		mDownArrow.addEventListener(MouseEvent.CLICK, arrowListener);
		mUpArrow.useHandCursor = mUpArrow.buttonMode = 
			mVerticalScrollHead.useHandCursor = mVerticalScrollHead.buttonMode = 
				mDownArrow.useHandCursor = mDownArrow.buttonMode = true;
		//panel.addChild(mVerticalScrollTrack);
		//panel.addChild(mVerticalScrollHead);
		panel.addChild(mVerticalSlider);
		panel.addChild(mUpArrow);
		panel.addChild(mDownArrow);
		
		mLeftArrow = new Sprite();		
		mLeftArrow.addEventListener(MouseEvent.CLICK, arrowListener);
		mHorizontalScrollTrack = new Sprite();
		mHorizontalScrollTrack.addEventListener(MouseEvent.CLICK, clickTrackListener);
		mHorizontalScrollHead = new Sprite();
		mHorizontalSlider = Slider.makeCustomSlider(mHorizontalScrollTrack, mHorizontalScrollHead, 0, 1, 0.5, false, 0, 3, true);
		mHorizontalSlider.addEventListener(SliderEvent.ON_CHANGE, sliderListener);
		mRightArrow = new Sprite();
		mRightArrow.addEventListener(MouseEvent.CLICK, arrowListener);
		mLeftArrow.useHandCursor = mLeftArrow.buttonMode = 
			mHorizontalScrollHead.useHandCursor = mHorizontalScrollHead.buttonMode = 
				mRightArrow.useHandCursor = mRightArrow.buttonMode = true;
		//panel.addChild(mHorizontalScrollTrack);
		//panel.addChild(mHorizontalScrollHead);
		panel.addChild(mHorizontalSlider);
		panel.addChild(mLeftArrow);
		panel.addChild(mRightArrow);
		
		mUpperLeftCorner = new Sprite();
		mUpperLeftCorner.addEventListener(MouseEvent.MOUSE_DOWN, dragEdgesListener);
		mUpperLeftCorner.useHandCursor = mUpperLeftCorner.buttonMode = true;
		panel.addChild(mUpperLeftCorner);
		
		mBottomRightCorner = new Sprite();
		mBottomRightCorner.addEventListener(MouseEvent.MOUSE_DOWN, dragEdgesListener);
		mBottomRightCorner.useHandCursor = mBottomRightCorner.buttonMode = true;
		panel.addChild(mBottomRightCorner);
		
		mDragEdges = new Sprite();
		mDragEdges.buttonMode = mDragEdges.useHandCursor = true;
		mDraggingSide = DragSide.RIGHT;
		mDragTimer = new Timer(30);
		mDragTimer.addEventListener(TimerEvent.TIMER, dragTimerListener);
		mDragEdges.addEventListener(MouseEvent.MOUSE_DOWN, dragEdgesListener);
		panel.addChild(mDragEdges);
		
		this.barThickness = barThickness;
		this.width = width;
		this.height = height;
		this.color1 = color1;//drawComponents(); //checkSize();
		if (color2 != null)
		{
			this.color2 = color2;
			this.enableColor2 = true;
		}
		else
		{
			this.color2 = 0x000000;
			this.enableColor2 = false;
		}
		enableHorizontalResize = true;
		enableHorizontalScroll = true;
		enableMovement = true;
		enableVerticalResize = true;
		enableVerticalScroll = true;
	}
	
	private function set_width(val:Float):Float
	{
		if (width != val)
		{
			width = val;
			if (Math.isNaN(width + height + barThickness))
				return val;
			drawMask();
			drawComponents();
			dispatchEvent(new Event(EVENT_RESIZED));
		}
		return val;
	}
	
	private function set_height(val:Float):Float
	{
		if (height != val)
		{
			height = val;
			if (Math.isNaN(width + height + barThickness))
				return val;
			drawMask();
			drawComponents();
			dispatchEvent(new Event(EVENT_RESIZED));
		}
		return val;
	}
	
	private function set_color1(val:UInt):UInt
	{
		if (color1 != val)
		{
			color1 = val;
			drawComponents();
		}
		return val;
	}
	
	private function set_color2(val:UInt):UInt
	{
		if (color2 != val)
		{
			color2 = val;
			drawComponents();
		}
		return val;
	}
	
	private function set_enableColor2(val:Bool):Bool
	{
		if (enableColor2 != val)
		{
			enableColor2 = val;
			drawComponents();
		}
		return val;
	}
	
	private function set_enableVerticalScroll(val:Bool):Bool
	{
		if (val != enableVerticalScroll)
		{
			enableVerticalScroll = val;
			checkSize();
		}
		return val;
	}
	
	private function set_enableVerticalResize(val:Bool):Bool
	{
		if (val != enableVerticalResize)
		{
			enableVerticalResize = val;
			mBottomRightCorner.buttonMode = mDragEdges.buttonMode = val;
		}
		return val;
	}
	
	private function set_enableHorizontalScroll(val:Bool):Bool
	{
		if (val != enableHorizontalScroll)
		{
			enableHorizontalScroll = val;
			checkSize();
		}
		return val;
	}
	
	private function set_enableHorizontalResize(val:Bool):Bool
	{
		if (val != enableHorizontalResize)
		{
			enableHorizontalResize = val;
			mBottomRightCorner.buttonMode = mDragEdges.buttonMode = val;
		}
		return val;
	}
	
	private function set_enableMovement(val:Bool):Bool
	{
		if (val != enableMovement)
		{
			enableMovement = val;
			mUpperLeftCorner.visible = val;
		}
		return val;
	}
	
	private function set_barThickness(val:Float):Float
	{
		if (barThickness != val)
		{
			barThickness = Math.abs(val);
			if (Math.isNaN(width + height + barThickness))
				return val;
			drawComponents();
		}
		return val;
	}
	
	private inline function set_x(val:Float):Float
	{	return panel.x = val;	}
	private inline function get_x():Float
	{	return panel.x;	}
	private inline function set_visible(val:Bool):Bool
	{	return panel.visible = val;	}
	private inline function get_visible():Bool
	{	return panel.visible;	}
	private inline function set_y(val:Float):Float
	{	return panel.y = val;	}
	private inline function get_y():Float
	{	return panel.y;	}
	private inline function get_parent():DisplayObjectContainer
	{	return panel.parent;	}
	private inline function get_mouseX():Float
	{	return panel.mouseX;	}
	private inline function get_mouseY():Float
	{	return panel.mouseY;	}
	
	public function addChild(child:DisplayObject):DisplayObject 
	{	
		mContainer.addChild(child);
		checkSize();
		return child;
	}
	
	public function addChildAt(child:DisplayObject, index:Int):DisplayObject 
	{	
		mContainer.addChildAt(child, index);
		checkSize();
		return child;
	}
	
	public function removeChild(child:DisplayObject):DisplayObject 
	{	return mContainer.removeChild(child);	}
	
	public function removeChildAt(index:Int):DisplayObject 
	{	return mContainer.removeChildAt(index);	}
	
	public function removeChildren(beginIndex:Int = 0, endIndex:Int = 2147483647):Void 
	{	mContainer.removeChildren(beginIndex, endIndex);	}
	
	public function getChildAt(index:Int):DisplayObject 
	{	return mContainer.getChildAt(index);		}
	
	public function getChildByName(name:String):DisplayObject 
	{	return mContainer.getChildByName(name);	}
	
	public function getChildIndex(child:DisplayObject):Int 
	{	return mContainer.getChildIndex(child);	}
	
	public function objectCurrentlyVisible(child:DisplayObject):Bool
	{
		if (!child.visible)
			return false;
		getChildIndex(child); // test to make sure object is actually child
		var childRect:Rectangle = child.getBounds(mMask);
		return (childRect.right > 0 && childRect.left < mMask.width && childRect.bottom > 0 && childRect.top < mMask.height);
	}
	
	private function checkSize(?e:Event)
	{
		mVerticalScrollTrack.visible =	mVerticalScrollHead.visible = 
			mDownArrow.visible = mUpArrow.visible = 
				(enableVerticalScroll && mContainer.height > height - (mLeftArrow.visible ? mLeftArrow.height : 0));
		
		if (mUpArrow.visible)
			updateVerticalScrollBar();
		else
			mContainer.y = 0;
			
		mHorizontalScrollTrack.visible = mHorizontalScrollHead.visible = 
			mLeftArrow.visible = mRightArrow.visible = 
				(enableHorizontalScroll && mContainer.width > width - (mUpArrow.visible ? mUpArrow.width : 0));
				
		if (mLeftArrow.visible)
			updateHorizontalScrollBar();
		else
			mContainer.x = 0;
	}
	
	private function updateVerticalScrollBar():Void
	{
		var t:Float = Math.min(barThickness, height * 0.5);
		mUpArrow.graphics.clear();
		if (enableColor2)
		{
			mUpArrow.graphics.lineStyle(t * 0.1, color2);
			mUpArrow.graphics.beginFill(color1);
		}
		else
			mUpArrow.graphics.lineStyle(t * 0.1, color1);
		mUpArrow.graphics.drawRect(t * 0.05, t * 0.05, t * 0.9, t * 0.9);
		if (enableColor2)
			mUpArrow.graphics.beginFill(color2);
		else
			mUpArrow.graphics.beginFill(color1);
		mUpArrow.graphics.moveTo(t * 0.25, t * 0.65);
		mUpArrow.graphics.lineTo(t * 0.5, t * 0.35);
		mUpArrow.graphics.lineTo(t * 0.75, t * 0.65);
		mUpArrow.graphics.lineTo(t * 0.25, t * 0.65);
		mUpArrow.graphics.endFill();
		mUpArrow.x = width - t;
		mUpArrow.y = 0;
		
		var h:Float = height - t * 2;
		if (mLeftArrow.visible)
		{
			h -= t; // Leave corner space open if both tracks are active
			mBottomRightCorner.visible = true;
			mBottomRightCorner.x = width - t;
			mBottomRightCorner.y = height - t;
			mBottomRightCorner.graphics.clear();
			if (enableColor2)
			{
				mBottomRightCorner.graphics.lineStyle(t * 0.1, color2);
				mBottomRightCorner.graphics.beginFill(color1);
			}
			else
				mBottomRightCorner.graphics.lineStyle(t * 0.1, color1);
			mBottomRightCorner.graphics.drawRect(t * 0.05, t * 0.05, t * 0.9, t * 0.9);
			mBottomRightCorner.graphics.endFill();
		}
		else
			mBottomRightCorner.visible = false;
		if (h > 0)
		{
			mVerticalScrollTrack.graphics.clear();
			mVerticalScrollTrack.graphics.lineStyle(t * 0.05, color1);
			if (enableColor2)
				mVerticalScrollTrack.graphics.beginFill(color2);
			mVerticalScrollTrack.graphics.drawRect(t * 0.025, t * 0.025, t * 0.95, h);
			mVerticalScrollTrack.graphics.endFill();
			mVerticalScrollTrack.x = mUpArrow.x;
			mVerticalScrollTrack.y = mUpArrow.y + mUpArrow.height;
			
			if (h > 10) // No point in bothering if it's smaller than this
			{
				h = Math.max(8, Math.min(height / mContainer.height, 1) * h);
				mVerticalScrollHead.graphics.clear();
				if (enableColor2)
					mVerticalScrollHead.graphics.lineStyle(t * 0.1, color2);
				else
					mVerticalScrollHead.graphics.lineStyle(t * 0.1, color1);
				mVerticalScrollHead.graphics.beginFill(color1);
				mVerticalScrollHead.graphics.drawRect(t * 0.05, t * 0.05, t * 0.9, h);
				mVerticalScrollHead.x = mUpArrow.x;
			}
			else
				mVerticalScrollHead.visible = false;
		}
		
		mDownArrow.graphics.clear();
		if (enableColor2)
		{
			mDownArrow.graphics.lineStyle(t * 0.1, color2);
			mDownArrow.graphics.beginFill(color1);
		}
		else
			mDownArrow.graphics.lineStyle(t * 0.1, color1);
		mDownArrow.graphics.drawRect(t * 0.05, t * 0.05, t * 0.9, t * 0.9);
		if (enableColor2)
			mDownArrow.graphics.beginFill(color2);
		else
			mDownArrow.graphics.beginFill(color1);
		mDownArrow.graphics.moveTo(t * 0.25, t * 0.35);
		mDownArrow.graphics.lineTo(t * 0.5, t * 0.65);
		mDownArrow.graphics.lineTo(t * 0.75, t * 0.35);
		mDownArrow.graphics.lineTo(t * 0.25, t * 0.35);
		mDownArrow.graphics.endFill();
		mDownArrow.x = width - t;
		mDownArrow.y = mVerticalScrollTrack.y + mVerticalScrollTrack.height;
		
		mVerticalSlider.refresh();
	}
	
	private function updateHorizontalScrollBar():Void
	{
		var t:Float = Math.min(barThickness, height * 0.5);
		
		mLeftArrow.graphics.clear();
		if (enableColor2)
		{
			mLeftArrow.graphics.lineStyle(t * 0.1, color2);
			mLeftArrow.graphics.beginFill(color1);
		}
		else
			mLeftArrow.graphics.lineStyle(t * 0.1, color1);
		mLeftArrow.graphics.drawRect(t * 0.05, t * 0.05, t * 0.9, t * 0.9);
		if (enableColor2)
			mLeftArrow.graphics.beginFill(color2);
		else
			mLeftArrow.graphics.beginFill(color1);
		mLeftArrow.graphics.moveTo(t * 0.65, t * 0.25);
		mLeftArrow.graphics.lineTo(t * 0.35, t * 0.5);
		mLeftArrow.graphics.lineTo(t * 0.65, t * 0.75);
		mLeftArrow.graphics.lineTo(t * 0.65, t * 0.25);
		mLeftArrow.graphics.endFill();
		mLeftArrow.x = 0;
		mLeftArrow.y = height - t;
		
		var w:Float = width - t * 2;
		if (mUpArrow.visible)
		{
			w -= t; // Leave corner space open if both tracks are active
			mBottomRightCorner.visible = true;
			mBottomRightCorner.x = width - t;
			mBottomRightCorner.y = height - t;
			mBottomRightCorner.graphics.clear();
			if (enableColor2)
			{
				mBottomRightCorner.graphics.lineStyle(t * 0.1, color2);
				mBottomRightCorner.graphics.beginFill(color1);
			}
			else
				mBottomRightCorner.graphics.lineStyle(t * 0.1, color1);
			mBottomRightCorner.graphics.drawRect(t * 0.05, t * 0.05, t * 0.9, t * 0.9);
			mBottomRightCorner.graphics.endFill();
		}
		else
			mBottomRightCorner.visible = false;
		if (w > 0)
		{
			mHorizontalScrollTrack.graphics.clear();
			mHorizontalScrollTrack.graphics.lineStyle(t * 0.05, color1);
			if (enableColor2)
				mHorizontalScrollTrack.graphics.beginFill(color2);
			else
				mHorizontalScrollTrack.graphics.beginFill(color1);
			mHorizontalScrollTrack.graphics.drawRect(t * 0.025, t * 0.025, w, t * 0.95);
			mHorizontalScrollTrack.graphics.endFill();
			mHorizontalScrollTrack.x = mLeftArrow.x + mLeftArrow.width;
			mHorizontalScrollTrack.y = mLeftArrow.y;
			
			if (w > 10) // No point in bothering if it's smaller than this
			{
				w = Math.max(8, Math.min(width / mContainer.width, 1) * w);
				mHorizontalScrollHead.graphics.clear();
				if (enableColor2)
				{
					mHorizontalScrollHead.graphics.lineStyle(t * 0.1, color2);
					mHorizontalScrollHead.graphics.beginFill(color1);
				}
				else
					mHorizontalScrollHead.graphics.lineStyle(t * 0.1, color1);
				mHorizontalScrollHead.graphics.drawRect(t * 0.05, t * 0.05, w, t * 0.9);
				mHorizontalScrollHead.y = mLeftArrow.y;
			}
			else
				mHorizontalScrollHead.visible = false; 
		}
		
		mRightArrow.graphics.clear();
		if (enableColor2)
		{
			mRightArrow.graphics.lineStyle(t * 0.1, color2);
			mRightArrow.graphics.beginFill(color1);
		}
		else
			mRightArrow.graphics.lineStyle(t * 0.1, color1);
		mRightArrow.graphics.drawRect(t * 0.05, t * 0.05, t * 0.9, t * 0.9);
		if (enableColor2)
			mRightArrow.graphics.beginFill(color2);
		else
			mRightArrow.graphics.beginFill(color1);
		mRightArrow.graphics.moveTo(t * 0.35, t * 0.25);
		mRightArrow.graphics.lineTo(t * 0.65, t * 0.5);
		mRightArrow.graphics.lineTo(t * 0.35, t * 0.75);
		mRightArrow.graphics.lineTo(t * 0.35, t * 0.25);
		mRightArrow.graphics.endFill();
		mRightArrow.x = mHorizontalScrollTrack.x + mHorizontalScrollTrack.width;
		mRightArrow.y = height - t;
		
		mHorizontalSlider.refresh();
	}
	
	private function drawMask(?e:Event):Void
	{
		mMask.graphics.clear();
		mMask.graphics.beginFill(0xFFFFFF);
		mMask.graphics.drawRoundRect(0, 0, width, height, 2, 2);
		mMask.graphics.endFill();
	}
	
	private function drawComponents(?e:Event):Void
	{
		panel.graphics.clear();
		if (enableColor2)
		{
			panel.graphics.beginFill(color2);
			panel.graphics.drawRoundRect(0, 0, width, height, 2);
		}
		mDragEdges.graphics.clear();
		mDragEdges.graphics.lineStyle(5, color1);
		mDragEdges.graphics.drawRoundRect(0, 0, width, height, 2);
		mUpperLeftCorner.graphics.clear();
		mUpperLeftCorner.graphics.lineStyle(5, color1, 0.3);
		mUpperLeftCorner.graphics.beginFill(color1, 0.1);
		mUpperLeftCorner.graphics.drawRect(0, 0, Math.min(barThickness, width * 0.5), Math.min(barThickness, height * 0.5));
		checkSize();
	}
	
	public inline function fitWidth():Void
	{
		width = mContainer.width + (mUpArrow.visible ? barThickness : 0);
	}
	
	public inline function fitHeight():Void
	{
		height = mContainer.height + (mLeftArrow.visible ? barThickness : 0);
	}
	
	public inline function getHorizontalScrollPixels():Float
	{	return -mContainer.x;	}	
	public inline function getVerticalScrollPixels():Float
	{	return -mContainer.y;	}	
	public inline function setHorizontalScrollPixels(position:Float):Void
	{	setHorizontalScrollPercent(position / (mContainer.width - width + (mUpArrow.visible ? barThickness : 0)));	}	
	public inline function setVerticalScrollPixels(position:Float):Void
	{	setVerticalScrollPercent(1 - (position / (mContainer.height - height + (mLeftArrow.visible ? barThickness : 0))));	}
	
	
	public inline function getHorizontalScrollPercent():Float
	{	return mHorizontalSlider.position;	}	
	public inline function getVerticalScrollPercent():Float
	{	return (1 - mVerticalSlider.position);	}	
	public inline function setHorizontalScrollPercent(position:Float):Void
	{
		mHorizontalSlider.position = position;	
		mHorizontalSlider.dispatchEvent(new SliderEvent(SliderEvent.ON_CHANGE, mHorizontalSlider));
		dispatchEvent(new Event(EVENT_SCROLLED));
	}	
	public function setVerticalScrollPercent(position:Float):Void
	{
		mVerticalSlider.position = position;	
		mVerticalSlider.dispatchEvent(new SliderEvent(SliderEvent.ON_CHANGE, mVerticalSlider));
		dispatchEvent(new Event(EVENT_SCROLLED));
	}
	
	public inline function scrollRightPixels(amount:Float):Void
	{	setHorizontalScrollPixels(getHorizontalScrollPixels() + amount);	}
	public inline function scrollLeftPixels(amount:Float):Void
	{	scrollRightPixels(-amount);	}
	public inline function scrollUpPixels(amount:Float):Void
	{	setVerticalScrollPixels(getVerticalScrollPixels() - amount);		}
	public inline function scrollDownPixels (amount:Float):Void
	{	scrollUpPixels( -amount);	}
	
	public inline function scrollRightPercent(amount:Float):Void
	{	setHorizontalScrollPercent(mHorizontalSlider.position + amount);	}
	public inline function scrollLeftPercent(amount:Float):Void
	{	scrollRightPercent(-amount);	}
	public inline function scrollUpPercent(amount:Float):Void
	{	setVerticalScrollPercent(mVerticalSlider.position + amount);		}
	public inline function scrollDownPercent(amount:Float):Void
	{	scrollUpPercent( -amount);	}
	
	private function clickTrackListener(e:MouseEvent):Void
	{
		if (e.target == mVerticalScrollTrack)
		{
			if (mVerticalScrollHead.mouseY > 0)
				scrollDownPixels(height * 0.5);
			else
				scrollUpPixels(height * 0.5);
		}
		else if (e.target == mHorizontalScrollTrack)
		{
			if (mHorizontalScrollHead.mouseX > 0)
				scrollRightPixels(width * 0.5);
			else
				scrollLeftPixels(width * 0.5);
		}
	}
	
	private function arrowListener(e:MouseEvent):Void
	{
		if (e.target == mLeftArrow)
			scrollLeftPixels(width * 0.05);
		else if (e.target == mRightArrow)
			scrollRightPixels(width * 0.05);
		else if (e.target == mUpArrow)
			scrollUpPixels(height * 0.05);
		else if (e.target == mDownArrow)
			scrollDownPixels(height * 0.05);
	}
	
	private function mouseWheelListener(e:MouseEvent):Void
	{
		if (e.ctrlKey)
		{
			if (e.delta < 0 && mRightArrow.visible)
				scrollRightPixels(Math.abs(e.delta) * width * 0.025);
			else if (e.delta > 0 && mLeftArrow.visible)
				scrollLeftPixels(e.delta * width * 0.025);
		}
		else
		{
			if (e.delta < 0 && mDownArrow.visible)
				scrollDownPixels(Math.abs(e.delta) * height * 0.025);
			else if (e.delta > 0 && mUpArrow.visible)
				scrollUpPixels(e.delta * height * 0.025);
		}
		
	}
	
	private function sliderListener(e:SliderEvent):Void
	{
		var r:Rectangle = DisplayTools.getRealDimensions(mContainer, mContainer);//
		if (e.source == mHorizontalSlider)
		{
			if (mContainer.width < width)
			{
				mContainer.x = 0;
				return;
			}
			var min:Float = -r.right + (width - (mUpArrow.visible ? mUpArrow.width : 0));
			var max:Float = -r.left;
			mContainer.x = min + ((1 - e.source.position) * (max - min));
		}
		else if (e.source == mVerticalSlider)
		{
			if (mContainer.height < height)
			{
				mContainer.y = 0;
				return;
			}
			var min:Float = -r.bottom + (height - (mLeftArrow.visible ? mLeftArrow.height : 0));
			var max:Float = -r.top;
			mContainer.y = min + (e.source.position * (max - min));
		}
		dispatchEvent(new Event(EVENT_SCROLLED));
	}
	
	private function dragEdgesListener(e:MouseEvent):Void
	{
		Lib.current.stage.addEventListener(MouseEvent.MOUSE_UP, stopDragEdgesListener);
		if (e.target == mUpperLeftCorner)
			mDraggingSide = UPLEFT;
		else if (e.target == mBottomRightCorner)
			mDraggingSide = DOWNRIGHT;
		else
		{
			var t:Float = Math.min(barThickness, width * 0.5);
			if (panel.mouseX <= t)
				mDraggingSide = LEFT;
			else if (panel.mouseX >= mDragEdges.width - t)
				mDraggingSide = RIGHT;
			else
				mDraggingSide = DOWN;
			t = Math.min(barThickness, height * 0.5);
			if (panel.mouseY < t)
			{
				if (mDraggingSide == LEFT)
					mDraggingSide = UPLEFT;
				else if (mDraggingSide == RIGHT)
					mDraggingSide = UPRIGHT;
				else
					mDraggingSide = UP;
			}
			else if (panel.mouseY > mDragEdges.width - t)
			{
				if (mDraggingSide == LEFT)
					mDraggingSide = DOWNLEFT;
				else if (mDraggingSide == RIGHT)
					mDraggingSide = DOWNRIGHT;
				else
					mDraggingSide = DOWN;
			}
			else if (mDraggingSide == DOWN)
			{
				trace ("WARNING: Unhandled drag click location");
				return;
			}
		}
		if (!enableHorizontalResize)
		{
			if (mDraggingSide == RIGHT ||
					mDraggingSide == LEFT)
				return;
			if ((mDraggingSide == UPLEFT && !enableMovement) ||
					mDraggingSide == UPRIGHT)
				mDraggingSide = UP;
			else if (mDraggingSide == DOWNLEFT ||
					mDraggingSide == DOWNRIGHT)
				mDraggingSide = DOWN;				
		}
		if (!enableVerticalResize)
		{
			if (mDraggingSide == DOWN ||
					mDraggingSide == UP)
				return;
			if ((mDraggingSide == UPLEFT && !enableMovement) ||
					mDraggingSide == DOWNLEFT)
				mDraggingSide = LEFT;
			else if (mDraggingSide == UPRIGHT ||
					mDraggingSide == DOWNRIGHT)
				mDraggingSide = RIGHT;				
		}
		mDragTimer.start();
	}
	
	private function dragTimerListener(e:TimerEvent):Void
	{
		var x:Float = panel.mouseX;
		var y:Float = panel.mouseY;
		switch(mDraggingSide)
		{
			case RIGHT:
				width = Math.max(MIN_WIDTH, x);
			case DOWNRIGHT:
				width = Math.max(MIN_WIDTH, x);
				height = Math.max(MIN_HEIGHT, y);
			case DOWN:
				height = Math.max(MIN_HEIGHT, y);
			case DOWNLEFT:
				if (MIN_WIDTH < width - x)
					panel.x += x;
				else
					panel.x += MIN_WIDTH - (width - x);
				width = Math.max(MIN_WIDTH, width - x);
				height = Math.max(MIN_HEIGHT, y);
			case LEFT:
				if (MIN_WIDTH < width - x)
					panel.x += x;
				else
					panel.x += MIN_WIDTH - (width - x);
				width = Math.max(MIN_WIDTH, width - x);
			case UPLEFT:				
				if (enableMovement)
				{
					panel.x += x;
					panel.y += y;
				}
				else
				{					
					width = Math.max(MIN_WIDTH, width - x);
					panel.x += x;
					height = Math.max(MIN_HEIGHT, height - y);
					panel.y += y;
				}
			case UP:
				height = Math.max(MIN_HEIGHT, height - y);
				panel.y += y;
			case UPRIGHT:
				width = Math.max(MIN_WIDTH, x);
				height = Math.max(MIN_HEIGHT, height - y);
				panel.y += y;
		}
	}
	
	private function stopDragEdgesListener(e:MouseEvent):Void
	{
		mDragTimer.reset();
		Lib.current.stage.removeEventListener(MouseEvent.MOUSE_UP, stopDragEdgesListener);
	}
	
}

private enum DragSide
{
	RIGHT;
	DOWNRIGHT;
	DOWN;
	DOWNLEFT;
	LEFT;
	UPLEFT;
	UP;
	UPRIGHT;
}