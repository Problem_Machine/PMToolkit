package com.problemmachine.ui.controlpanel;

import com.problemmachine.tools.display.DisplayTools;
import com.problemmachine.tools.numstring.NumString;
import com.problemmachine.ui.scrollwindow.ScrollWindow;
import com.problemmachine.ui.slider.Slider;
import com.problemmachine.ui.slider.SliderEvent;
import com.problemmachine.util.variablewrapper.VariableWrapper;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.FocusEvent;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.geom.Rectangle;
import flash.Lib;
import flash.text.TextField;
import flash.text.TextFieldType;
import flash.text.TextFormat;
import flash.ui.Keyboard;

class ControlPanel extends ScrollWindow
{
	private var mParentPanel:ControlPanel;
	private var mControls:Array<Dynamic>;
	
	public function new(width:Float, height:Float, color1:UInt, color2:UInt, barThickness:Float = 30) 
	{
		mParentPanel = null;
		mControls = new Array<Sprite>();
		
		super(width, height, color1, color2, barThickness);
		
		mContainer.mouseEnabled = false;
		
		enableVerticalScroll = false;
		enableHorizontalResize = false;
	}
	
	override private function set_color1(val:UInt):UInt 
	{
		val &= 0xFFFFFF;
		if (color1 != val)
		{
			super.set_color1(val);
			for (c in mControls)
				if (Std.is(c, Control))
					cast(c, Control).color = val;			
		}
		return val;
	}
	
	override private function set_height(val:Float):Float 
	{
		if (val != height)
		{
			super.set_height(val);
			arrange();
			if (mParentPanel != null)
				mParentPanel.arrange();
		}
		return val;
	}
	override private function set_width(val:Float):Float 
	{
		if (val != width)
		{
			super.set_width(val);
			arrange();
			if (mParentPanel != null)
				mParentPanel.arrange();
		}
		return val;
	}
	
	public function update():Void
	{
		for (c in mControls)
			if (Std.is(c, Control))
				cast(c, Control).update();
			else if (Std.is(c, ControlPanel))
				cast(c, ControlPanel).update();
			else if (Reflect.hasField(c, "update"))
				Reflect.callMethod(c, Reflect.field(c, "update"), []);
	}
	
	public function addSubControlPanel(control:ControlPanel):Void
	{
		mControls.push(control);
		control.mParentPanel = this;
		control.color1 = color1;
		control.color2 = color2;
		arrange();
	}
	
	public function addCustomControl(control:Sprite):Void
	{
		if (Std.is(control, ControlPanel))
			return addSubControlPanel(cast control);
		mControls.push(control);
		arrange();
	}
	
	public function removeCustomControl(control:Sprite):Void
	{
		for (i in 0...mControls.length)
			if (Std.is(mControls[i], Sprite) && mControls[i] == control)
			{
				mControls.splice(i, 1);
				break;
			}
		if (control.parent == mContainer)
			mContainer.removeChild(control);
	}
	
	public function addDescription(text:String):Void
	{
		var control:Control = new Control(null, text, color1);
		mControls.push(control);
		arrange();
	}
	
	public function addFloatControl(wrapper:VariableWrapper<Float>, description:String, minVal:Float, maxVal:Float, percentage:Bool = false, 
		precision:UInt = 0, restrict:Bool = true):Void
	{
		var control:Control = new FloatControl(wrapper, description, minVal, maxVal, 150, color1, percentage, precision, restrict);
		control.addEventListener(Event.RESIZE, arrange);
		mControls.push(control);
		arrange();
	}
	
	public function addIntControl(wrapper:VariableWrapper<Int>, description:String, minVal:Int, maxVal:Int, restrict:Bool = true):Void
	{
		var control:Control = new IntControl(wrapper, description, minVal, maxVal, 150, color1, restrict);
		control.addEventListener(Event.RESIZE, arrange);
		mControls.push(control);
		arrange();
	}
	
	public function addStringControl(wrapper:VariableWrapper<String>, description:String, fieldWidth:Float = 200, ?charWhiteList:String, ?charBlackList:String):Void
	{
		var control:Control = new StringControl(wrapper, description, color1, fieldWidth, charWhiteList, charBlackList);
		control.addEventListener(Event.RESIZE, arrange);
		mControls.push(control);
		arrange();
	}
	
	public function addStringArrayControl(wrapper:VariableWrapper<Array<String>>, width:Float, description:String, fieldWidth:Float = 200, ?charWhiteList:String, ?charBlackList:String):Void
	{
		var control:Control = new StringArrayControl(wrapper, description, color1, (height - barThickness) * 0.5, fieldWidth, charWhiteList, charBlackList);
		control.addEventListener(Event.RESIZE, arrange);
		mControls.push(control);
		arrange();
	}
	
	public function addBoolControl(wrapper:VariableWrapper<Bool>, description:String):Void
	{
		var control:Control = new BoolControl(wrapper, description, color1);
		mControls.push(control);
		arrange();
	}
	
	public function removeControl(description:String):Void
	{
		for (i in 0...mControls.length)
			if (Std.is(mControls[i], Control))
			{
				var c:Control = cast mControls[i];
				if (c.description == description)
				{
					mControls.splice(i, 1);
					if (c.parent != null)
						c.parent.removeChild(c);
					c.dispose();
					break;
				}
			}
	}
	
	private function arrange(?e:Event):Void
	{
		var tempX:Float = 10;
		var tempY:Float = 10;
		var widest:Float = 0;
		for (c in mControls)
		{
			var r:Rectangle = Std.is(c, ScrollWindow) ? 
				DisplayTools.getRealDimensions(c.panel, c.panel) : DisplayTools.getRealDimensions(c, c);
			if (tempY + r.height > height)
			{
				tempY = 10;
				tempX += widest;
				widest = 0;
			}
			if (r.width > widest)
				widest = r.width;
			
			if (Std.is(c, ScrollWindow))
			{
				c.panel.x = tempX;
				c.panel.y = tempY;
				addChild(c.panel);
			}
			else
			{
				var control:Control = cast c;
				control.x = tempX;
				control.y = tempY;
				addChild(control);
			}
			tempY += r.height;
		}
	}
	
	public function dispose():Void
	{
		var control:Sprite;
		while ((control = mControls.pop()) != null)
		{
			if (Std.is(control, Control))
				cast(control, Control).dispose();
		}
	}
}

private class Control extends Sprite
{
	public var value(get, set):Dynamic;
	public var description(default, set):String;
	public var color(default, set):UInt;
	
	private var mDescription:TextField;
	private var mWrapper:VariableWrapper<Dynamic>;
	
	public function new(wrapper:VariableWrapper<Dynamic>, description:String, color:UInt)
	{
		// base control class can't directly control parameters, but can be used as a text-only description
		super();
		
		mouseEnabled = false;
		mouseChildren = true;
		mWrapper = wrapper;
		mDescription = new TextField();
		mDescription.height = 25;
		mDescription.defaultTextFormat = new TextFormat(null, 16, color);
		mDescription.mouseEnabled = false;
		addChild(mDescription);
		
		this.color = color;
		this.description = description;
	}
	
	private inline function get_value():Dynamic
	{	return mWrapper != null ? mWrapper.value : null;			}
	private inline function set_value(val:Dynamic):Dynamic
	{	return mWrapper != null ? mWrapper.value = val : val;		}
	private inline function set_description(val:String):String
	{	
		mDescription.text = val;
		mDescription.width = mDescription.textWidth + 15;
		return description = val;		
	}
	private function set_color(val:UInt):UInt
	{
		mDescription.textColor = val;
		return color = val;
	}
	
	public function dispose():Void
	{
		mWrapper = null;
		mDescription = null;
		removeChildren();
	}
	
	public function update():Void
	{
		//abstract
	}
}

private class BoolControl extends Control
{	
	private var mButton:Sprite;
	
	public function new(wrapper:VariableWrapper<Bool>, description:String, color:UInt)
	{
		mButton = new Sprite();
		mButton.buttonMode = true;
		super(wrapper, description, color);
		
		mButton.x = mDescription.width + 25;
		addChild(mButton);
		mButton.addEventListener(MouseEvent.CLICK, mouseListener);
	}
	
	override private function set_color(val:UInt):UInt
	{
		super.set_color(val);
		update();
		return val;
	}
	
	private function mouseListener(e:MouseEvent):Void
	{
		value = !cast(value, Bool);
		update();
	}
	
	override public function dispose():Void 
	{
		super.dispose();
		mButton = null;
	}
	
	override public function update():Void
	{
		mButton.graphics.clear();
		mButton.graphics.lineStyle(5, color, 1);
		mButton.graphics.beginFill(color, 0.3);
		mButton.graphics.drawRect(0, 0, 30, 30);
		mButton.graphics.endFill();
		if (value)
		{
			mButton.graphics.moveTo(5, 5);
			mButton.graphics.lineTo(25, 25);
			mButton.graphics.moveTo(5, 25);
			mButton.graphics.lineTo(25, 5);
		}
	}
}

private class FloatControl extends Control
{
	private static inline var VALID_CHARS:String = "1234567890.-";
	public var min(default, null):Float;
	public var max(default, null):Float;
	public var restrict(default, null):Bool;
	
	public var slider(default, null):Slider;
	private var mHead:Sprite;
	private var mTrack:Sprite;
	
	private var mPrecision:UInt;
	private var mDisplayAsPercent:Bool;
	private var mTextField:TextField;
	private var mStoreAsInt:Bool;
	
	public function new(wrapper:VariableWrapper<Float>, description:String, min:Float, max:Float, width:Float, color:UInt,
		displayPercent:Bool = false, precision:UInt = 0, restrict:Bool = true)
	{
		mTextField = new TextField();
		mTextField.type = TextFieldType.INPUT;
		mTextField.defaultTextFormat = new TextFormat(null, 16, color);
		mTextField.border = true;
		mTextField.addEventListener(MouseEvent.CLICK, selectTextBlock);
		mTextField.addEventListener(Event.CHANGE, textListener);
		mTextField.addEventListener(FocusEvent.FOCUS_OUT, fillTextListener);
		mTextField.height = 20;
		mTextField.text = "1234";
		
		var track:Sprite = new Sprite();	
		track.y = 40;
		var head:Sprite = new Sprite();
		head.y = 40;
		drawSliderComponents(track, head);
		
		slider = Slider.makeCustomSlider(track, head, min, max, (min + max) / 2, false, min, precision + (mDisplayAsPercent ? 2 : 0));
		slider.addEventListener(SliderEvent.ON_CHANGE, sliderListener);
		addChild(slider);
		
		super(wrapper, description, color);
		
		this.min = min;
		this.max = max;
		this.restrict = restrict;
		mDisplayAsPercent = displayPercent;
		mPrecision = precision;
		mTextField.x = mDescription.width + 25;
		mTextField.width = 50;
		addChild(mTextField);
		
		if (wrapper != null && value == null)
			value = 0.0;
			
		slider.value = value;
		slider.dispatchEvent(new SliderEvent(SliderEvent.ON_CHANGE, slider));
	}
	
	override private function set_color(val:UInt):UInt
	{
		if (val != color)
		{
			super.set_color(val);
			mTextField.textColor = color;
			mTextField.borderColor = color;
			mTextField.defaultTextFormat.color = color;
			drawSliderComponents(slider.track, slider.head);
			return val;
		}
		else		
			return super.set_color(val);
	}
	
	override public function update():Void 
	{
		slider.value = value;
		if (mDisplayAsPercent)
			mTextField.text = NumString.toPercent(slider.value, mPrecision);
		else
			mTextField.text = NumString.toFixed(slider.value, mPrecision);
	}
	
	private function drawSliderComponents(track:Sprite, head:Sprite):Void
	{
		track.graphics.clear();
		track.graphics.lineStyle(5, color, 1);
		track.graphics.moveTo(0, 0);
		track.graphics.lineTo(width, 0);
		track.graphics.lineStyle(3, color, 1);
		track.graphics.moveTo(0, -10);
		track.graphics.lineTo(0, 10);
		track.graphics.moveTo(width, -10);
		track.graphics.lineTo(width, 10);
		
		head.graphics.clear();
		head.graphics.lineStyle(3, color);
		head.graphics.beginFill(color, 0.7);
		head.graphics.drawRect( -10, -10, 10, 10);
	}
	
	private function sliderListener(e:SliderEvent):Void
	{
		value = mStoreAsInt ? Std.int(e.source.value) : e.source.value;
		if (mDisplayAsPercent)
			mTextField.text = NumString.toPercent(e.source.value, mPrecision);
		else
			mTextField.text = NumString.toFixed(e.source.value, mPrecision);
	}
	
	private function textListener(e:Event):Void
	{
		var tf:TextField = cast(e.target, TextField);
		var text:String = "";
		var afterPoint:Int = -1;
		for (i in 0...tf.text.length)
		{
			var char:String = tf.text.charAt(i);
			if (char == "-" && i != 0)
				continue; 		//disregard extra minus symbols
			if (char == "." || afterPoint >= 0)
			{
				if (char == "." && afterPoint >= 0)
					continue; 	//disregard multiple points
				if (++afterPoint >= Std.int(mPrecision))
					break;		//disregard characters entered beyond possible precision
			}
			if (VALID_CHARS.indexOf(char) >= 0)
				text += char;
		}
		var val:Float = Std.parseFloat(mTextField.text) * (mDisplayAsPercent ? 0.01 : 1);
		if (restrict)
			val = Math.min(max, Math.max(min, val));
		slider.value = val;	
		value = mStoreAsInt ? Std.int(val) : val;
		
		tf.text = text;
		if (mDisplayAsPercent)
			tf.text += "%";
	}
	
	private function selectTextBlock(e:MouseEvent):Void
	{
		var t:TextField = cast(e.target, TextField);
		t.setSelection(0, t.text.length);
	}	
	
	private function fillTextListener(e:FocusEvent):Void
	{
		if (mDisplayAsPercent)
			mTextField.text = NumString.toPercent(slider.value, mPrecision);
		else
			mTextField.text = NumString.toFixed(slider.value, mPrecision);
	}
	
	override public function dispose():Void 
	{
		super.dispose();
		slider = null;
		mTextField = null;
	}
}

private class IntControl extends FloatControl
{
	public function new(wrapper:VariableWrapper<Int>, description:String, min:Float, max:Float, width:Float, color:UInt, restrict:Bool = true)
	{
		super(cast wrapper, description, min, max, width, color, false, 0, restrict);
		mStoreAsInt = true;
		if (wrapper != null && value == null)
			value = Std.int(0);
	}
}

private class StringControl extends Control
{
	public var charWhiteList(default, null):String;
	public var charBlackList(default, null):String;
	
	private var mTextField:TextField;
	
	public function new(wrapper:VariableWrapper<String>, description:String, color:UInt, fieldWidth:Float = 0, ?charWhiteList:String, ?charBlackList:String)
	{
		mTextField = new TextField();
		mTextField.defaultTextFormat = new TextFormat(null, 16);
		mTextField.height = 20;
		mTextField.border = true;
		mTextField.type = TextFieldType.INPUT;
		mTextField.addEventListener(Event.CHANGE, textListener);
		
		super(wrapper, description, color);
		if (value == null)
			value = "";
		mTextField.text = value;
		mTextField.x = mDescription.width + 25;
		mTextField.width = fieldWidth;
		addChild(mTextField);
		
		if (charWhiteList != null)
			this.charWhiteList = charWhiteList;
		else
			this.charWhiteList = null;
		if (charBlackList != null)
			this.charBlackList = charBlackList;
		else
			this.charBlackList = null;
	}
	
	override private function set_color(val:UInt):UInt
	{
		super.set_color(val);
		mTextField.textColor = color;
		mTextField.defaultTextFormat.color = color;
		mTextField.borderColor = color;
		return val;
	}
	
	override public function update():Void 
	{
		mTextField.text = value;
	}
	
	private function textListener(e:Event):Void
	{
		var tf:TextField = cast(e.target, TextField);
		var text:String = "";
		for (i in 0...tf.text.length)
		{
			var char:String = tf.text.charAt(i);
			if ((charWhiteList == null || charWhiteList.indexOf(char) >= 0) &&
				(charBlackList == null || charBlackList.indexOf(char) < 0))
				text += char;
		}
		
		value = text;
		
		tf.text = text;
	}
	
	override public function dispose():Void 
	{
		super.dispose();
		charBlackList = null;
		charWhiteList = null;
		mTextField = null;
	}
}

private class StringArrayControl extends Control
{
	public var charWhiteList(default, null):String;
	public var charBlackList(default, null):String;
	
	private var mWindow:ScrollWindow;
	private var mFieldWidth:Float;
	private var mTextFields:Array<TextField>;
	
	public function new(wrapper:VariableWrapper<Array<String>>, description:String, color:UInt, height:Float, fieldWidth:Float, ?charWhiteList:String, ?charBlackList:String)
	{
		mWindow = new ScrollWindow(100, height - 30, color, null, 15);
		addChild(mWindow.panel);
		mWindow.panel.y = 20;
		mWindow.enableHorizontalResize = false;
		mWindow.enableHorizontalScroll = false;
		mWindow.enableMovement = false;
		mWindow.enableVerticalScroll = true;
		mWindow.enableVerticalResize = false;
		mTextFields = new Array<TextField>();
		super(wrapper, description, color);
		if (value == null)
			value = new Array<String>();
		
		if (charWhiteList != null)
			this.charWhiteList = charWhiteList;
		else
			this.charWhiteList = null;
		if (charBlackList != null)
			this.charBlackList = charBlackList;
		else
			this.charBlackList = null;
		
		createFields();
		update();
	}
	
	override private function set_color(val:UInt):UInt
	{
		super.set_color(val);
		for (t in mTextFields)
		{
			t.textColor = color;
			t.defaultTextFormat.color = color;
		}
		update();
		return val;
	}
	
	override public function update():Void
	{
		if (value == null)
			return;
			
		populateFields();
	}
	
	private function createFields():Void
	{
		while (mTextFields.length < value.length + 1)
		{
			var t:TextField = new TextField();
			t.height = 20;
			t.defaultTextFormat = new TextFormat(null, 16, color);
			t.multiline = false;
			t.border = true;
			t.borderColor = color;
			t.y = mTextFields.length * 20;
			t.type = TextFieldType.INPUT;
			t.addEventListener(Event.CHANGE, textListener);
			t.addEventListener(KeyboardEvent.KEY_DOWN, keyListener);
			t.width = mFieldWidth;
			mWindow.addChild(t);
			mTextFields.push(t);
		}
	}
	
	private function populateFields():Void
	{		
		var remove:Array<Int> = new Array<Int>();
		//var maxWidth:Float = 50;
		for (i in 0...mTextFields.length)
		{
			var t:TextField = mTextFields[i];
			if (value[i] == null)
			{
				remove.push(i);
				break;
			}

			t.text = value[i];
			t.width = mFieldWidth;
			//maxWidth = Math.max(maxWidth, t.width);
		}
		//mWindow.width = maxWidth;
		dispatchEvent(new Event(Event.RESIZE));
		
		remove.sort(function(a:Int, b:Int) {	return b - a;	} );
		for (i in remove)
		{
			value.splice(i, 1);
			mTextFields[i].removeEventListener(Event.CHANGE, textListener);
			mTextFields[i].removeEventListener(KeyboardEvent.KEY_DOWN, keyListener);
			mWindow.removeChild(mTextFields[i]);
			mTextFields.splice(i, 1);
		}
		createFields();
	}
	
	private function parseFields():Void
	{
		var remove:Array<Int> = new Array<Int>();
		for (i in 0...mTextFields.length)			
		{
			if (mTextFields[i].text != "")
				value[i] = mTextFields[i].text;
			else if (i != mTextFields.length - 1)
				remove.push(i);
		}
		remove.sort(function(a:Int, b:Int) {	return b - a;	} );
		for (i in remove)
		{
			value.splice(i, 1);
			mTextFields[i].removeEventListener(Event.CHANGE, textListener);
			mTextFields[i].removeEventListener(KeyboardEvent.KEY_DOWN, keyListener);
			mWindow.removeChild(mTextFields[i]);
			mTextFields.splice(i, 1);
		}
		while (value.length > mTextFields.length)
			value.pop();
		createFields();
	}
	
	private function textListener(e:Event):Void
	{
		var tf:TextField = cast(e.target, TextField);
		var text:String = "";
		for (i in 0...tf.text.length)
		{
			var char:String = tf.text.charAt(i);
			if ((charWhiteList == null || charWhiteList.indexOf(char) >= 0) &&
				(charBlackList == null || charBlackList.indexOf(char) < 0))
				text += char;
		}
		
		tf.text = text;
		var maxWidth:Float = 50;
		for (i in 0...mTextFields.length)
		{
			var t:TextField = mTextFields[i];
			t.width = t.textWidth + 40;
			maxWidth = Math.max(maxWidth, t.width);
		}
		mWindow.width = maxWidth;

		parseFields();			
	}
	
	private function keyListener(e:KeyboardEvent):Void
	{
		var tf:TextField = cast(e.target, TextField);
		if (e.keyCode == Keyboard.ENTER || e.keyCode == Keyboard.DOWN)
		{
			for (i in 0...mTextFields.length)
				if (mTextFields[i] == tf && i < mTextFields.length)
				{
					Lib.current.stage.focus = mTextFields[i + 1];
					mTextFields[i + 1].setSelection(0, 0);
					return;
				}
		}
		else if (e.keyCode == Keyboard.UP)
		{
			for (i in 0...mTextFields.length)
				if (mTextFields[i] == tf && i > 0)
				{
					Lib.current.stage.focus = mTextFields[i - 1];
					mTextFields[i - 1].setSelection(0, 0);
					return;
				}
		}
			
	}
	
	override public function dispose():Void 
	{
		super.dispose();
		mTextFields = null;
		charBlackList = null;
		charWhiteList = null;
	}
}