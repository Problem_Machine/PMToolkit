package com.problemmachine.ui;

import flash.display.DisplayObjectContainer;
import flash.events.TimerEvent;
import flash.text.TextField;
import flash.text.TextFormat;
import flash.utils.Timer;

class AutoFadeText 
{
	private var mText:TextField;
	private var mTimer:Timer;
	
	public function new(text:String, x:Float, y:Float, duration:Float, container:DisplayObjectContainer, ?format:TextFormat) 
	{
		mText = new TextField();
		if (format != null)
			mText.defaultTextFormat = format;
		mText.text = text;
		mText.x = x;
		mText.y = y;
		mText.mouseEnabled = false;
		mText.width = format.size * text.length;
		mText.height = format.size * 10;
		
		container.addChild(mText);
		
		mTimer = new Timer(25, Math.ceil(duration * 40));
		mTimer.addEventListener(TimerEvent.TIMER, updateText);
		mTimer.addEventListener(TimerEvent.TIMER_COMPLETE, die);
		mTimer.start();
	}
	
	private function updateText(e:TimerEvent):Void
	{
		mText.alpha = 1 - (mTimer.currentCount / mTimer.repeatCount);
	}
	
	private function die(e:TimerEvent):Void
	{
		mTimer.removeEventListener(TimerEvent.TIMER, updateText);
		mTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, die);
		mTimer = null;
		if (mText.parent != null)
			mText.parent.removeChild(mText);
		mText = null;
	}
	
}