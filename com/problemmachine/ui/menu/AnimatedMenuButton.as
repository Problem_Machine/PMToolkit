package com.problemmachine.ui.menu 
{
	import com.problemmachine.graphics.bitmapanimation.BitmapAnimation;
	import com.problemmachine.graphics.bitmapanimation.TransitionalAnimation;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author 
	 */
	public class AnimatedMenuButton implements IMenuButton
	{
		private var mAction:Function;
		private var mActionParams:Array;
		private var mClickRect:Rectangle;
		private var mDrawLocation:Point;
		
		private var mAnimationIdle:TransitionalAnimation = null;
		private var mAnimationActive:TransitionalAnimation = null;
		private var mAnimationSelect:TransitionalAnimation = null;
		private var mAnimationConfirm:TransitionalAnimation = null;
		private var mCurrentAnimation:TransitionalAnimation = null;
		
		public function MenuButton(func:Function, funcParams:Array = null) 
		{
			mClickRect = new Rectangle();
			mDrawLocation = new Point();
			mAction = func;
			mActionParams = funcParams.concat();
		}
		
		public static function xmlToButton(xml:XML, func:Function, funcParams:Array = null):MenuButton
		{
			var b:MenuButton = new MenuButton(func, funcParams);
			b.mDrawLocation.x = xml.parameters.x[0];
			b.mDrawLocation.y = xml.parameters.y[0];
			b.mClickRect.x = xml.parameters.rectOffsetX[0];
			b.mClickRect.y = xml.parameters.rectOffsetY[0];
			b.mClickRect.width = xml.parameters.rectWidth[0];
			b.mClickRect.height = xml.parameters.rectHeight[0];
			if (xml.idle.length() > 0)
				b.xmlToIdleAnimation(xml.idle[0]);
			if (xml.active.length() > 0)
				b.xmlToActiveAnimation(xml.active[0]);
			if (xml.select.length() > 0)
				b.xmlToSelectAnimation(xml.select[0]);
			if (xml.confirm.length() > 0)
				b.xmlToConfirmAnimation(xml.confirm[0]);
			b.mCurrentAnimation = b.mAnimationIdle;
			b.mCurrentAnimation.current = b.mCurrentAnimation.length;
			// TODO: Implement MenuButton.xmlToButton()
			return b;
		}
		
		public function progress(time:Number):void
		{
			if (mCurrentAnimation)
				mCurrentAnimation.progress(time);
		}
		
		public function draw(surface:BitmapData, offset:Point):void
		{
			if (mCurrentAnimation)
				mCurrentAnimation.getCurrentAnimation().draw(surface, mDrawLocation.add(offset));
		}
		
		public function dispose():void
		{
			if (mAnimationIdle)
				mAnimationIdle.dispose();
			mAnimationIdle = null;
			if (mAnimationActive)
				mAnimationActive.dispose()
			mAnimationActive = null;
			if (mAnimationSelect)
				mAnimationSelect.dispose();
			mAnimationSelect = null;
			if (mAnimationConfirm)
				mAnimationConfirm.dispose()
			mAnimationConfirm = null;
			mCurrentAnimation = null;
			mAction = null;
			mActionParams = null;
		}
		
		public function callFunction():void
		{
			mAction.apply(NaN, mActionParams ? mActionParams : NaN);
		}
		
		private function xmlToIdleAnimation(xml:XML):void
		{	mAnimationIdle = TransitionalAnimation.create(xml);			}
		private function xmlToActiveAnimation(xml:XML):void
		{	mAnimationActive = TransitionalAnimation.create(xml);		}
		private function xmlToSelectAnimation(xml:XML):void
		{	mAnimationSelect = TransitionalAnimation.create(xml);		}
		private function xmlToConfirmAnimation(xml:XML):void
		{	mAnimationConfirm = TransitionalAnimation.create(xml);		}
		
		public function idleAnimation():void
		{
			if (mAnimationIdle)
			{
				mCurrentAnimation = mAnimationIdle;
				mCurrentAnimation.current = 0;
			}
		}
		
		public function activeAnimation():void
		{
			if (mAnimationActive)
			{
				mCurrentAnimation = mAnimationActive;
				mCurrentAnimation.current = 0;
			}
		}
		
		public function selectAnimation():void
		{
			if (mAnimationSelect)
			{
				mCurrentAnimation = mAnimationSelect;
				mCurrentAnimation.current = 0;
			}
		}
		
		public function confirmAnimation():void
		{
			if (mAnimationConfirm)
			{
				mCurrentAnimation = mAnimationConfirm;
				mCurrentAnimation.current = 0;
			}
		}
		
		public function hitTest(x:Number, y:Number):Boolean
		{
			var r:Rectangle = mClickRect;
			r.x += mDrawLocation.x;
			r.y += mDrawLocation.y;
			return (r.contains(x, y));
		}
		
		public function get x():Number
		{	return (mDrawLocation.x)		}
		public function get y():Number
		{	return (mDrawLocation.y)		}
	}

}