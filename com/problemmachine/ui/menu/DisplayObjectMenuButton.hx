package com.problemmachine.ui.menu;

import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.display.BitmapData;

class DisplayObjectMenuButton implements IMenuButton 
{
	private var mLocation:Point;
	private var mFunction:Dynamic;
	private var mFunctionParameters:Array<Dynamic>;
	private var mUseShapeTest:Bool;
	
	private var mCurrentAnimation:DisplayObject;
	public var idleAnimation(default, set):DisplayObject;
	public var activeAnimation(default, default):DisplayObject;
	public var selectAnimation(default, default):DisplayObject;
	public var confirmAnimation(default, default):DisplayObject;
	public var x(get, set):Float;
	public var y(get, set):Float;
	
	public function new(func:Dynamic, ?funcParams:Array<Dynamic>) 
	{
		mUseShapeTest = false;
		mCurrentAnimation = null;
		idleAnimation = null;
		activeAnimation = null;
		selectAnimation = null;
		confirmAnimation = null;
		
		mFunction = func;
		mFunctionParameters = funcParams;
		mLocation = new Point();
	}
	
	public function hitTest(x:Float, y:Float):Bool
	{
		return 
			if (mCurrentAnimation != null)
				mCurrentAnimation.hitTestPoint(x - mLocation.x, y - mLocation.y, mUseShapeTest);
			else
				false;
	}
	
	public function progress(time:Float):Void 
	{
		
	}
	
	public function draw(surface:BitmapData, offset:Point):Void 
	{
		var m:Matrix = new Matrix();
		m.translate(mLocation.x + offset.x, mLocation.y + offset.y);
		if (mCurrentAnimation != null)
			surface.draw(mCurrentAnimation, m);
	}
	
	public function callFunction():Void 
	{	
		Reflect.callMethod(this, mFunction, mFunctionParameters);	
	}
	
	public function dispose():Void 
	{
		mCurrentAnimation = null;
		idleAnimation = null;
		activeAnimation = null;
		selectAnimation = null;
		confirmAnimation = null;
	}
	
	public function gotoIdleAnimation():Void 
	{
		if (idleAnimation != null)
			mCurrentAnimation = idleAnimation;
	}
	
	public function gotoActiveAnimation():Void 
	{
		if (activeAnimation != null)
			mCurrentAnimation = activeAnimation;
	}
	
	public function gotoSelectAnimation():Void 
	{
		if (selectAnimation != null)
			mCurrentAnimation = selectAnimation;
	}
	
	public function gotoConfirmAnimation():Void 
	{
		if (confirmAnimation != null)
			mCurrentAnimation = confirmAnimation;
	}
	
	public function set_idleAnimation(d:DisplayObject):DisplayObject
	{	
		idleAnimation = d;	
		if (mCurrentAnimation == null)
			mCurrentAnimation = idleAnimation;
		return d;
	}
	
	public function get_x():Float
	{	return mLocation.x;	}
	public function set_x(val:Float):Float
	{	return mLocation.x = val;		}
	
	public function get_y():Float
	{	return mLocation.y;	}
	public function set_y(val:Float):Float
	{	return mLocation.y = val;		}
}
