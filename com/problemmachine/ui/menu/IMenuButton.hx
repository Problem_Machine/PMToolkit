package com.problemmachine.ui.menu;

import flash.display.BitmapData;
import flash.geom.Point;

interface IMenuButton 
{
	var x(get, set):Float;
	var y(get, set):Float;
	function hitTest(x:Float, y:Float):Bool;
	function progress(time:Float):Void;
	function draw(surface:BitmapData, offset:Point):Void;
	function callFunction():Void;
	function dispose():Void;
	
	function gotoIdleAnimation():Void;
	function gotoActiveAnimation():Void;
	function gotoSelectAnimation():Void;
	function gotoConfirmAnimation():Void;
}