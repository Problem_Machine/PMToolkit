package com.problemmachine.ui.menu;

import com.problemmachine.animation.Animation;
import flash.display.BitmapData;
import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.geom.Matrix;
import flash.geom.Point;

class MenuWindow
{
	public var menuBackground(default, default):DisplayObject;
	public var x(get, set):Float;
	public var y(get, set):Float;
	
	private var mLocation:Point;
	private var mButtons:Array<IMenuButton>;
	private var mSelectedButton:Int;
	private var mSelectionActive:Bool;
	private var mAllowWraparound:Bool;
	
	public function new() 
	{
		mSelectedButton = -1;
		mSelectionActive = false;
		mAllowWraparound = true;
		mLocation = new Point();
		mButtons = new Array<IMenuButton>();
	}
	
	public function dispose():Void
	{
		menuBackground = null;
		for (b in mButtons)
			b.dispose();
	}
	
	public function mouseInput(type:String, x:Float, y:Float):Void
	{
		if (type == MouseEvent.MOUSE_DOWN)
		{
			for (i in 0...mButtons.length)
				if (mButtons[i].hitTest(x - mLocation.x, y - mLocation.y))
				{
					mSelectedButton = i;
					mSelectionActive = true;
					mButtons[i].gotoSelectAnimation();
					break;
				}
		}
		else if (type == MouseEvent.MOUSE_UP)
		{
			if (mSelectionActive && mButtons[mSelectedButton].hitTest(x - mLocation.x, y - mLocation.y))
			{
				mButtons[mSelectedButton].gotoConfirmAnimation();
				mButtons[mSelectedButton].callFunction();
			}
		}
		else if (type == MouseEvent.MOUSE_MOVE)
		{
			if (!mSelectionActive)
			{
				for (i in 0...mButtons.length)
					if (mButtons[i].hitTest(x - mLocation.x, y - mLocation.y))
					{
						mSelectedButton = i;
						mButtons[i].gotoActiveAnimation();
					}
					else
						mButtons[i].gotoIdleAnimation();
			}
		}
	}
	
	public function directionalInput(type:MenuDirectInput):Void
	{
		switch (type)
		{
			case MenuDirectInput.Left,
				 MenuDirectInput.Right,
				 MenuDirectInput.Down,
				 MenuDirectInput.Up:
				direction(type);
			case MenuDirectInput.Select:
				directionSelect();
			case MenuDirectInput.Confirm:
				directionSelectConfirm();
		}
	}
	
	private function direction(dir:MenuDirectInput):Void
	{
		if (mSelectionActive)
			mButtons[mSelectedButton].gotoIdleAnimation();
		mSelectionActive = false;
		var buttonsSorted:Array<IMenuButton> = mButtons.copy();
		if (dir == MenuDirectInput.Left || dir == MenuDirectInput.Right)
			buttonsSorted.sort(function(a:IMenuButton, b:IMenuButton):Int 
				{	return (a.x - b.x) < 0 ? -1 : Math.ceil(a.x - b.x);		} );
		else
			buttonsSorted.sort(function(a:IMenuButton, b:IMenuButton):Int 
				{	return (a.y - b.y) < 0 ? -1 : Math.ceil(a.y - b.y);		} );
		
		var startButton:IMenuButton = null;
		if (mSelectedButton > 0 && mSelectedButton < mButtons.length)
			startButton = mButtons[mSelectedButton];
		var index:Int = 0;
		if (startButton != null)
		{
			for (i in 0...buttonsSorted.length)
				if (buttonsSorted[i] == startButton)
				{
					index = i;
					break;
				}
			if (dir == MenuDirectInput.Up || dir == MenuDirectInput.Left)
				--index;
			else
				++index;
		}
		else
		{
			if (dir == MenuDirectInput.Up || dir == MenuDirectInput.Left)
				index = buttonsSorted.length - 1;
			else
				index = 0;
		}
		if (index < 0)
			index = mAllowWraparound ? buttonsSorted.length - 1 : 0;
		else if (index >= buttonsSorted.length)
			index = mAllowWraparound ? 0 : buttonsSorted.length - 1;
				
		var targetButton:IMenuButton = buttonsSorted[index];
		for (i in 0...mButtons.length)
			if (mButtons[i] == targetButton)
			{
				mSelectedButton = i;
				mButtons[i].gotoActiveAnimation();
			}
			else
				mButtons[i].gotoIdleAnimation();
	}
	
	private function directionSelect():Void
	{
		mSelectionActive = false;
		if (mSelectedButton >= 0 && mSelectedButton < mButtons.length)
		{
			mSelectionActive = true;
			mButtons[mSelectedButton].gotoSelectAnimation();
		}
	}
	
	private function directionSelectConfirm():Void
	{
		if (mSelectionActive)
		{
			mButtons[mSelectedButton].gotoConfirmAnimation();
			mButtons[mSelectedButton].callFunction();
		}
	}
	
	public function draw(surface:BitmapData):Void
	{
		var m:Matrix = new Matrix();
		m.translate(mLocation.x, mLocation.y);
		if (menuBackground != null)
			surface.draw(menuBackground, m);
		for (b in mButtons)
			b.draw(surface, mLocation);
	}
	
	public function addButton(button:IMenuButton):Void
	{
		mButtons.push(button);
	}
	
	public function resetButtons():Void
	{
		var b:IMenuButton;
		while ((b = mButtons.pop()) != null)
			b.dispose();
	}
	
	private inline function set_x(val:Float):Float
	{	return mLocation.x = val;	}
	private inline function set_y(val:Float):Float
	{	return mLocation.y = val;	}
	private inline function get_x():Float
	{	return mLocation.x;	}
	private inline function get_y():Float
	{	return mLocation.y;	}
	
}