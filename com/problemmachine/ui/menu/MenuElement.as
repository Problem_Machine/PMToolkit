package com.problemmachine.ui.menu 
{
	import com.problemmachine.animation.Animation;
	import flash.geom.Point;
	/**
	 * ...
	 * @author 
	 */
	internal class MenuElement
	{
		internal var anim:Animation;
		internal var location:Point
		
		public function MenuElement(a:Animation, loc:Point)
		{
			anim = a.clone(false);
			location = loc.clone();
		}
	}

}