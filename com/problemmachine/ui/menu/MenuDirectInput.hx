package com.problemmachine.ui.menu;

enum MenuDirectInput
{

	Left;
	Right;
	Down;
	Up;
	Select;
	Confirm;
}