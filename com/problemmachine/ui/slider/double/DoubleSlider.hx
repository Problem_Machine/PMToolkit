package com.problemmachine.ui.slider.double;

import com.problemmachine.ui.slider.Slider;
import com.problemmachine.ui.slider.SliderEvent;
import com.problemmachine.ui.slider.style.double.DefaultDoubleSliderStyle;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.geom.Rectangle;

class DoubleSlider extends Sprite
{
	public var lowValue(get, set):Float;
	public var highValue(get, set):Float;
	public var lowPosition(get, set):Float;
	public var highPosition(get, set):Float;
	
	public var precision(get, set):Int;
	public var minValue(get, set):Float;
	public var maxValue(get, set):Float;
	public var centerValue(get, set):Float;
	public var enabled(get, set):Bool;
	public var dragging(get, null):Bool;
	public var vertical(get, never):Bool;
	
	public var lowTrack(get, null):Sprite;
	public var highTrack(get, null):Sprite;
	public var lowHead(get, null):Sprite;
	public var highHead(get, null):Sprite;
	public var lowSlider(get, null):Slider;
	public var highSlider(get, null):Slider;

	public function new(lowTrack:Sprite, highTrack:Sprite, lowHead:Sprite, highHead:Sprite, headSize:Float,
		minVal:Float, maxVal:Float, centerVal:Float, vertical:Bool, lowStartVal:Float, highStartVal:Float, precision:Int, startEnabled:Bool = true) 
	{
		super();
		
		lowSlider = Slider.makeCustomSlider(lowTrack, lowHead, minVal, maxVal, centerVal, vertical, lowStartVal, precision, startEnabled);
		highSlider = Slider.makeCustomSlider(highTrack, highHead, minVal, maxVal, centerVal, vertical, highStartVal, precision, startEnabled);
		if (vertical)
			lowSlider.y = highSlider.head.height;
		else
			highSlider.x = lowSlider.head.width;
			
		if (vertical)
		{
			addChild(highSlider);
			addChild(lowSlider);
			lowSlider.removeChild(highHead);
			addChild(highHead);
		}
		else
		{
			addChild(lowSlider);
			addChild(highSlider);
			lowSlider.removeChild(lowHead);
			addChild(lowHead);
		}
		
		lowSlider.addEventListener(SliderEvent.ON_CHANGE, function(e:SliderEvent):Void
			{	
				if (lowSlider.value > highSlider.value)
				{
					highSlider.value = lowSlider.value;
					highSlider.dispatchEvent(new SliderEvent(SliderEvent.ON_CHANGE, highSlider));
				}
			} );
		highSlider.addEventListener(SliderEvent.ON_CHANGE, function(e:SliderEvent):Void
			{	
				if (highSlider.value < lowSlider.value)
				{
					lowSlider.value = highSlider.value; 	
					lowSlider.dispatchEvent(new SliderEvent(SliderEvent.ON_CHANGE, lowSlider));
				}
			} );
	}
	
	private function get_lowValue():Float
	{	return lowSlider.value;	}
	private function set_lowValue(val:Float):Float
	{	return lowSlider.value = val;	}
	private function get_highValue():Float
	{	return highSlider.value;	}
	private function set_highValue(val:Float):Float
	{	return highSlider.value = val;	}
	private function get_lowPosition():Float
	{	return lowSlider.position;	}
	private function set_lowPosition(val:Float):Float
	{	return lowSlider.position = val; }
	private function get_highPosition():Float
	{	return highSlider.position;	}
	private function set_highPosition(val:Float):Float
	{	return highSlider.position = val;	}
	
	private function get_precision():Int
	{	return lowSlider.precision;	}
	private function set_precision(val:Int):Int
	{	return highSlider.precision = lowSlider.precision = val;	}
	private function get_minValue():Float
	{	return lowSlider.minValue;	}
	private function set_minValue(val:Float):Float
	{	return highSlider.minValue = lowSlider.minValue = val;	}
	private function get_maxValue():Float
	{	return lowSlider.maxValue;	}
	private function set_maxValue(val:Float):Float
	{	return highSlider.maxValue = lowSlider.maxValue = val;	}
	private function get_centerValue():Float
	{	return lowSlider.centerValue;	}
	private function set_centerValue(val:Float):Float
	{	return highSlider.centerValue = lowSlider.centerValue = val;	}
	private function get_enabled():Bool
	{	return lowSlider.enabled;	}
	private function set_enabled(val:Bool):Bool
	{	return highSlider.enabled = lowSlider.enabled = val;	}
	private function get_dragging():Bool
	{	return lowSlider.dragging || highSlider.dragging;	}
	private function get_vertical():Bool
	{	return lowSlider.vertical;	}
	
	private function get_lowTrack():Sprite
	{	return lowSlider.track;		}
	private function get_highTrack():Sprite
	{	return highSlider.track;		}
	private function get_lowHead():Sprite
	{	return lowSlider.head;		}
	private function get_highHead():Sprite
	{	return highSlider.track;		}
	private function get_lowSlider():Slider
	{	return lowSlider;		}
	private function get_highSlider():Slider
	{	return highSlider;		}
	
	public static function makeDefaultDoubleSlider(length:Float, thickness:Float, headSize:Float, 
		lowHeadColor:UInt, highHeadColor:UInt, lowTrackColor:UInt, highTrackColor:UInt, minVal:Float, maxVal:Float, centerVal:Float, 
		vertical:Bool, lowStartVal:Float, highStartVal:Float, precision:Int, strikeMidPoint:Bool, startEnabled:Bool = true):DoubleSlider
	{		
		var lowTrack:Sprite = new Sprite();
		var highTrack:Sprite = new Sprite();
		var lowHead:Sprite = new Sprite();
		var highHead:Sprite = new Sprite();
		
		new DefaultDoubleSliderStyle().draw(lowTrack, highTrack, lowHead, highHead, length, thickness, headSize, 
			lowHeadColor, highHeadColor, lowTrackColor, highTrackColor, vertical, strikeMidPoint);
		return new DoubleSlider(lowTrack, highTrack, lowHead, highHead, headSize, minVal, maxVal, centerVal, vertical, lowStartVal, highStartVal, precision, startEnabled);
	}
	
	public function refresh():Void
	{
		lowSlider.refresh();
		highSlider.refresh();
	}
}