package com.problemmachine.ui.slider.complex;

import com.problemmachine.tools.text.TextTools;
import com.problemmachine.ui.slider.double.DoubleSlider;
import com.problemmachine.ui.slider.Slider;
import com.problemmachine.ui.slider.SliderEvent;
import com.problemmachine.ui.slider.style.DefaultSliderStyle;
import com.problemmachine.ui.slider.style.ISliderStyle;
import com.problemmachine.ui.text.NumberInput;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.FocusEvent;
import flash.text.TextField;
import flash.text.TextFieldType;
import flash.text.TextFormat;

class LabeledDoubleSlider extends Sprite
{
	public var label:TextField;
	public var lowInput:NumberInput;
	public var highInput:NumberInput;
	public var doubleSlider:DoubleSlider;
	
	public var precision(get, set):Int;
	public var minValue(get, set):Float;
	public var maxValue(get, set):Float;
	public var centerValue(get, set):Float;
	public var enabled(get, set):Bool;
	public var dragging(get, never):Bool;
	public var vertical(get, never):Bool;
	
	public var lowPosition(get, set):Float;
	public var lowValue(get, set):Float;
	public var highPosition(get, set):Float;
	public var highValue(get, set):Float;
	
	public var lowTrack(get, never):Sprite;
	public var lowHead(get, never):Sprite;
	public var highTrack(get, never):Sprite;
	public var highHead(get, never):Sprite;

	private function new(doubleSlider:DoubleSlider, label:String, labelWidth:Float, inputWidth:Float, textHeight:Float, 
		inputBack:UInt, vertical:Bool, ?textFormat:TextFormat) 
	{
		super();
		
		if (textFormat == null)
			textFormat = new TextFormat();
		
		this.doubleSlider = doubleSlider;
		
		this.label = new TextField();
		this.label.defaultTextFormat = textFormat;
		this.label.height = textHeight;
		this.label.width = labelWidth;
		this.label.text = label;
		this.label.mouseEnabled = false;
		TextTools.scaleTextToFit(this.label);
		
		this.lowInput = new NumberInput(doubleSlider.lowValue, inputWidth, textHeight, textFormat, 
			doubleSlider.precision, true, true, textFormat.color, inputBack);
		this.highInput = new NumberInput(doubleSlider.highValue, inputWidth, textHeight, textFormat, 
			doubleSlider.precision, true, true, textFormat.color, inputBack);
		
		this.lowInput.addEventListener(NumberInput.VALUE_CHANGE, numberListener);
		this.highInput.addEventListener(NumberInput.VALUE_CHANGE, numberListener);
		this.doubleSlider.addEventListener(SliderEvent.ON_CHANGE, sliderChangeListener);
		
		addChild(this.label);
		addChild(this.doubleSlider);
		addChild(this.lowInput);
		addChild(this.highInput);
		refresh();
	}
	
	static public function makeDefaultLabeledDoubleSlider(lowHeadColor:UInt, highHeadColor:UInt, lowTrackColor:UInt, highTrackColor:UInt, 
		inputBackColor:UInt, length:Float, thickness:Float, headSize:Float, label:String, labelWidth:Float, inputWidth:Float,	
		minValue:Float = 0, maxValue:Float = 1, centerValue:Float = 0.5, vertical:Bool = false, lowStartVal:Float, highStartVal:Float, 
		precision:Int = 0, strikeMidPoint:Bool = true, startEnabled:Bool = true, ?textFormat:TextFormat):LabeledDoubleSlider
	{
		var doubleSlider:DoubleSlider = DoubleSlider.makeDefaultDoubleSlider(length, thickness, headSize, lowHeadColor, highHeadColor, 
			lowTrackColor, highTrackColor, minValue, maxValue, centerValue, vertical, lowStartVal, highStartVal, precision, strikeMidPoint, startEnabled);
		var textHeight:Float = vertical ? 24 : Math.max(thickness, headSize);
		
		return new LabeledDoubleSlider(doubleSlider, label, labelWidth, inputWidth, textHeight, 
			inputBackColor, vertical, textFormat);
	}
	
	public function refresh():Void
	{
		doubleSlider.refresh();
		if (vertical)
		{
			doubleSlider.x = Math.max(label.width, Math.max(lowInput.width, doubleSlider.width)) * 0.5;
			doubleSlider.y = label.height;
			highInput.y = doubleSlider.y + doubleSlider.height;
			lowInput.y = highInput.y + highInput.height;
		}
		else
		{	
			doubleSlider.x = label.width;
			doubleSlider.y = label.height * 0.5;
			lowInput.x = doubleSlider.x + doubleSlider.width;	
			highInput.x = lowInput.x + lowInput.width;	
		}
	}
	
	private function sliderChangeListener(e:SliderEvent):Void
	{
		if (e.source == doubleSlider.lowSlider)
			lowInput.value = e.source.value;
		else
			highInput.value = e.source.value;
	}
	
	private function numberListener(e:Event):Void
	{
		var oldLow:Float = doubleSlider.lowValue;
		var oldHigh:Float = doubleSlider.highValue;
		doubleSlider.lowValue = lowInput.value;
		doubleSlider.highValue = highInput.value;
		lowInput.value = doubleSlider.lowValue;
		highInput.value = doubleSlider.highValue;
		if (doubleSlider.lowValue != oldLow)
			doubleSlider.lowSlider.dispatchEvent(new SliderEvent(SliderEvent.ON_CHANGE, doubleSlider.lowSlider));
		if (doubleSlider.highValue != oldHigh)
			doubleSlider.highSlider.dispatchEvent(new SliderEvent(SliderEvent.ON_CHANGE, doubleSlider.highSlider));
	}

	public function get_precision():Int
	{	return doubleSlider.precision;	}
	public function set_precision(val:Int):Int
	{	
		doubleSlider.precision = val;	
		highInput.precision = lowInput.precision = val;
		return doubleSlider.precision;
	}
	public function get_minValue():Float
	{	return doubleSlider.minValue;	}
	public function set_minValue(val:Float):Float
	{	
		doubleSlider.minValue = val;	
		lowInput.value = doubleSlider.lowValue;
		highInput.value = doubleSlider.highValue;
		return doubleSlider.minValue;
	}
	public function get_maxValue():Float
	{	return doubleSlider.maxValue;	}
	public function set_maxValue(val:Float):Float
	{	
		doubleSlider.maxValue = val;	
		lowInput.value = doubleSlider.lowValue;
		highInput.value = doubleSlider.highValue;
		return doubleSlider.maxValue;
	}
	public function get_centerValue():Float
	{	return doubleSlider.centerValue;	}
	public function set_centerValue(val:Float):Float
	{	return doubleSlider.centerValue = val;	}
	public function get_enabled():Bool
	{	return doubleSlider.enabled;	}
	public function set_enabled(val:Bool):Bool
	{	
		return lowInput.mouseEnabled = highInput.mouseEnabled = doubleSlider.enabled = val;	
	}
	public function get_dragging():Bool
	{	return doubleSlider.dragging;	}
	public function get_vertical():Bool
	{	return doubleSlider.vertical;	}
	
	public function get_lowPosition():Float
	{	return doubleSlider.lowPosition;	}
	public function set_lowPosition(val:Float):Float
	{	return doubleSlider.lowPosition = val;		}
	public function get_highPosition():Float
	{	return doubleSlider.highPosition;	}
	public function set_highPosition(val:Float):Float
	{	return doubleSlider.highPosition = val;		}
	public function get_lowValue():Float
	{	return doubleSlider.lowValue;	}
	public function set_lowValue(val:Float):Float
	{	
		doubleSlider.lowValue = val;
		lowInput.value = doubleSlider.lowValue;
		return doubleSlider.lowValue;
	}
	public function get_highValue():Float
	{	return doubleSlider.highValue;	}
	public function set_highValue(val:Float):Float
	{	
		doubleSlider.highValue = val;
		highInput.value = doubleSlider.highValue;
		return doubleSlider.highValue;
	}
	
	public function get_lowTrack():Sprite
	{	return doubleSlider.lowTrack;	}
	public function get_lowHead():Sprite
	{	return doubleSlider.lowHead;	}
	public function get_highTrack():Sprite
	{	return doubleSlider.highTrack;	}
	public function get_highHead():Sprite
	{	return doubleSlider.highHead;	}
}