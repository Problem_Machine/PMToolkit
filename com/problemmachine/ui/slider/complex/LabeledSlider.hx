package com.problemmachine.ui.slider.complex;

import com.problemmachine.tools.text.TextTools;
import com.problemmachine.ui.slider.Slider;
import com.problemmachine.ui.slider.SliderEvent;
import com.problemmachine.ui.slider.style.DefaultSliderStyle;
import com.problemmachine.ui.slider.style.ISliderStyle;
import com.problemmachine.ui.text.NumberInput;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.FocusEvent;
import flash.text.TextField;
import flash.text.TextFieldType;
import flash.text.TextFormat;

class LabeledSlider extends Sprite
{
	public var label:TextField;
	public var input:NumberInput;
	public var slider:Slider;
	
	public var precision(get, set):Int;
	public var minValue(get, set):Float;
	public var maxValue(get, set):Float;
	public var centerValue(get, set):Float;
	public var enabled(get, set):Bool;
	public var dragging(get, never):Bool;
	public var vertical(get, null):Bool;
	
	public var position(get, set):Float;
	public var value(get, set):Float;
	
	public var track(get, never):Sprite;
	public var head(get, never):Sprite;

	private function new(slider:Slider, label:String, labelWidth:Float, inputWidth:Float, textHeight:Float, 
		inputBack:UInt, vertical:Bool, ?textFormat:TextFormat) 
	{
		super();
		
		if (textFormat == null)
			textFormat = new TextFormat();
		
		this.vertical = vertical;
		this.slider = slider;
		
		this.label = new TextField();
		this.label.defaultTextFormat = textFormat;
		this.label.height = textHeight;
		this.label.width = labelWidth;
		this.label.text = label;
		this.label.selectable = this.label.mouseEnabled = false;
		TextTools.scaleTextToFit(this.label);
		
		this.input = new NumberInput(slider.value, inputWidth, textHeight, textFormat, slider.precision, true, true, textFormat.color, inputBack);
		this.input.addEventListener(NumberInput.VALUE_CHANGE, numberListener);
		this.slider.addEventListener(SliderEvent.ON_CHANGE, sliderChangeListener);
		
		addChild(this.label);
		addChild(this.slider);
		addChild(this.input);
		refresh();
	}
	
	public function refresh():Void
	{
		slider.refresh();
		if (vertical)
		{
			slider.x = Math.max(label.width, Math.max(input.width, slider.width)) * 0.5;
			slider.y = label.height;
			input.y = slider.y + slider.height;
		}
		else
		{	
			slider.x = label.width;
			slider.y = label.height * 0.5;
			input.x = slider.x + slider.width;	
		}
	}
	
	private function sliderChangeListener(e:SliderEvent):Void
	{
		input.value = e.source.value;
		dispatchEvent(new SliderEvent(e.type, this.slider));
	}
	
	private function numberListener(e:Event):Void
	{
		var old:Float = slider.value;
		slider.value = input.value;
		input.value = slider.value;
		if (slider.value != old)
			slider.dispatchEvent(new SliderEvent(SliderEvent.ON_CHANGE, slider));
	}
	
	static public function makeCustomLabeledSlider(track:Sprite, head:Sprite, inputBackColor:UInt, label:String, labelWidth:Float, inputWidth:Float,
		minValue:Float = 0, maxValue:Float = 1, centerValue:Float = 0.5,
		vertical:Bool = false, startVal:Float = 0, precision:Int = 0, startEnabled:Bool = true, ?textFormat:TextFormat):LabeledSlider
	{
		var slider:Slider = Slider.makeCustomSlider(track, head, minValue, maxValue, centerValue, vertical, startVal, precision, startEnabled);
		var textHeight:Float = vertical ? 24 : Math.max(track.height, head.height);
		
		return new LabeledSlider(slider, label, labelWidth, inputWidth, textHeight, inputBackColor, vertical, textFormat);
	}
	
	static public function makeDefaultLabeledSlider(color1:UInt, color2:UInt, inputBackColor:UInt, length:Float, 
		thickness:Float, headSize:Float, label:String, labelWidth:Float, inputWidth:Float,	
		minValue:Float = 0, maxValue:Float = 1, centerValue:Float = 0.5,
		vertical:Bool = false, startVal:Float = 0, precision:Int = 0, startEnabled:Bool = true, ?textFormat:TextFormat):LabeledSlider
	{
		return makeStyleLabeledSlider(new DefaultSliderStyle(), color1, color2, inputBackColor, length, thickness, headSize, 
			label, labelWidth, inputWidth, minValue, maxValue, centerValue, vertical, startVal, precision, startEnabled, textFormat);
	}
	
	static public function makeStyleLabeledSlider(style:ISliderStyle, color1:UInt, color2:UInt, inputBackColor:UInt, length:Float, 
		thickness:Float, headSize:Float, label:String, labelWidth:Float, inputWidth:Float, 
		minValue:Float = 0, maxValue:Float = 1, centerValue:Float = 0.5,
		vertical:Bool = false, startVal:Float = 0, precision:Int = 0, startEnabled:Bool = true, ?textFormat:TextFormat):LabeledSlider
	{
		var slider:Slider = Slider.makeStyleSlider(style, color1, color2, length, thickness, headSize, 
			minValue, maxValue, centerValue, vertical, startVal, precision, startEnabled);
		var textHeight:Float = vertical ? 24 : Math.max(slider.track.height, slider.head.height);
		
		return new LabeledSlider(slider, label, labelWidth, inputWidth, textHeight, inputBackColor, vertical, textFormat);
	}

	private function get_precision():Int
	{	return slider.precision;	}
	private function set_precision(val:Int):Int
	{	
		slider.precision = val;	
		input.precision = val;
		return slider.precision;
	}
	private function get_minValue():Float
	{	return slider.minValue;	}
	private function set_minValue(val:Float):Float
	{	
		slider.minValue = val;	
		input.value = slider.value;
		return slider.minValue;
	}
	private function get_maxValue():Float
	{	return slider.maxValue;	}
	private function set_maxValue(val:Float):Float
	{	
		slider.maxValue = val;	
		input.value = slider.value;
		return slider.maxValue;
	}
	private function get_centerValue():Float
	{	return slider.centerValue;	}
	private function set_centerValue(val:Float):Float
	{	return slider.centerValue = val;	}
	private function get_enabled():Bool
	{	return slider.enabled;	}
	private function set_enabled(val:Bool):Bool
	{	
		return input.mouseEnabled = slider.enabled = val;	
	}
	private function get_dragging():Bool
	{	return slider.dragging;	}
	private function get_vertical():Bool
	{	return slider.vertical;	}
	
	private function get_position():Float
	{	return slider.position;	}
	private function set_position(val:Float):Float
	{	
		return slider.position = val;	
	}
	private function get_value():Float
	{	return slider.value;	}
	private function set_value(val:Float):Float
	{	
		slider.value = val;
		input.value = slider.value;
		return slider.value;
	}
	
	private function get_track():Sprite
	{	return slider.track;	}
	private function get_head():Sprite
	{	return slider.head;	}
}