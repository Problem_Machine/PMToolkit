package com.problemmachine.ui.slider.style ;
import flash.display.Sprite;

class RampSliderStyle implements ISliderStyle
{

	public function new() 
	{
		
	}
	
	public function draw(track:Sprite, head:Sprite, length:Float, thickness:Float, headSize:Float, 
		color1:UInt, color2:UInt, vertical:Bool, ?aux:Dynamic):Void 
	{
		track.graphics.clear();
		head.graphics.clear();
		
		var alpha1:Float = (color1 >>> 24) / 0xFF;
		var alpha2:Float = (color2 >>> 24) / 0xFF;
		track.graphics.beginFill(color1 & 0xFFFFFF, alpha1);
		head.graphics.lineStyle(1, color1 & 0xFFFFFF, alpha1);
		head.graphics.beginFill(color2 & 0xFFFFFF, alpha2);
		if (vertical)
		{
			track.graphics.moveTo(-0.5, length);
			track.graphics.lineTo( -thickness * 0.5, 0);
			track.graphics.lineTo(thickness * 0.5, 0);
			track.graphics.lineTo(0.5, length);
			track.graphics.lineTo(-0.5, length);
			
			head.graphics.drawRect(-headSize * 0.5, -headSize * 0.1, headSize, headSize * 0.2);
		}
		else	
		{
			track.graphics.moveTo(0, thickness * 0.5);
			track.graphics.lineTo(length, thickness * 0.5);
			track.graphics.lineTo(length, -thickness * 0.5);
			track.graphics.lineTo(0, thickness * 0.5 - 1);
			track.graphics.lineTo(0, thickness * 0.5);
			
			head.graphics.drawRect(-headSize * 0.1, -headSize * 0.5, headSize * 0.2, headSize);
		}
	}	
}