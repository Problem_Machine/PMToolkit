package com.problemmachine.ui.slider.style ;
import flash.display.Sprite;

interface ISliderStyle 
{
	function draw(track:Sprite, head:Sprite, length:Float, thickness:Float, headSize:Float, color1:UInt, color2:UInt, vertical:Bool, ?aux:Dynamic):Void;
}