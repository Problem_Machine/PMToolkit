package com.problemmachine.ui.slider.style.double;
import flash.display.GradientType;
import flash.display.Sprite;
import flash.geom.Matrix;

class DefaultDoubleSliderStyle implements IDoubleSliderStyle
{

	public function new() 
	{
		
	}
	
	public function draw(lowTrack:Sprite, highTrack:Sprite, lowHead:Sprite, highHead:Sprite, length:Float, thickness:Float, 
		headSize:Float, lowHeadColor:UInt, highHeadColor:UInt, lowTrackColor:UInt, highTrackColor:UInt, vertical:Bool, ?aux:Dynamic):Void
	{
		var m:Matrix = new Matrix();
		m.createGradientBox(length, thickness);
		
		lowTrack.graphics.beginGradientFill(GradientType.LINEAR, [lowTrackColor, highTrackColor], [0xFF, 0xFF], [0x00, 0xFF], m);
		lowTrack.graphics.drawRect(0, -thickness * 0.5, length - headSize, thickness);
		
		if (aux == true)
		{
			lowTrack.graphics.lineStyle(3, 0xFFFFFF, 0.5);
			lowTrack.graphics.moveTo(length * 0.5 - 1.5, -headSize * 0.5);
			lowTrack.graphics.lineTo(length * 0.5 - 1.5, headSize * 0.5);
		}
		
		m.translate(-headSize, 0);
		
		highTrack.graphics.beginGradientFill(GradientType.LINEAR, [lowTrackColor, highTrackColor], [0xFF, 0xFF], [0x00, 0xFF], m);
		highTrack.graphics.drawRect(0, -thickness * 0.5, length - headSize, thickness);
		
		lowHead.graphics.beginFill(lowTrackColor, 0.8);
		lowHead.graphics.lineStyle(headSize * 0.2, lowHeadColor);
		lowHead.graphics.lineTo(-headSize * 0.5, -headSize * 0.5); 
		lowHead.graphics.lineTo(-headSize * 0.5, headSize * 0.5); 
		lowHead.graphics.lineTo(0, 0);
		
		highHead.graphics.beginFill(highTrackColor, 0.8);
		highHead.graphics.lineStyle(headSize * 0.2, highHeadColor);
		highHead.graphics.lineTo( headSize * 0.5, -headSize * 0.5); 
		highHead.graphics.lineTo( headSize * 0.5, headSize * 0.5); 
		highHead.graphics.lineTo(0, 0);
	}
}