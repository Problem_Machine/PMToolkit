package com.problemmachine.ui.slider.style.double ;
import flash.display.Sprite;

interface IDoubleSliderStyle 
{
	function draw(lowTrack:Sprite, highTrack:Sprite, lowHead:Sprite, highHead:Sprite, length:Float, thickness:Float, 
		headSize:Float, lowHeadColor:UInt, highHeadColor:UInt, lowTrackColor:UInt, highTrackColor:UInt, vertical:Bool, ?aux:Dynamic):Void;
}