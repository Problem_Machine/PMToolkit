package com.problemmachine.ui.slider.style ;
import flash.display.Sprite;

class DefaultSliderStyle implements ISliderStyle
{

	public function new() 
	{
		
	}
	
	public function draw(track:Sprite, head:Sprite, length:Float, thickness:Float, headSize:Float, 
		color1:UInt, color2:UInt, vertical:Bool, ?aux:Dynamic):Void 
	{
		track.graphics.clear();
		head.graphics.clear();
		
		var alpha1:Float = (color1 >>> 24) / 0xFF;
		var alpha2:Float = (color2 >>> 24) / 0xFF;
		track.graphics.beginFill(color1 & 0xFFFFFF, alpha1);
		if (vertical)
			track.graphics.drawRect( -thickness * 0.5, 0, thickness, length);
		else	
			track.graphics.drawRect(0, -thickness * 0.5, length, thickness);
			
		head.graphics.lineStyle(headSize / 10, color1 & 0xFFFFFF, alpha1);
		head.graphics.beginFill(color2 & 0xFFFFFF, alpha2);
		head.graphics.drawCircle(0, 0, headSize / 2);
	}	
}