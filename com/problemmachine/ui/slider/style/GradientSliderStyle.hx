package com.problemmachine.ui.slider.style ;
import flash.display.GradientType;
import flash.display.Sprite;
import flash.geom.Matrix;

class GradientSliderStyle implements ISliderStyle
{

	public function new() 
	{
		
	}
	
	public function draw(track:Sprite, head:Sprite, length:Float, thickness:Float, headSize:Float, 
		color1:UInt, color2:UInt, vertical:Bool, ?aux:Dynamic):Void 
	{
		track.graphics.clear();
		head.graphics.clear();
		
		var m:Matrix = new Matrix();
		track.graphics.lineStyle(1, color1);
		m.createGradientBox(length, thickness, 0, 0, 0);
		track.graphics.beginGradientFill(GradientType.LINEAR, [color2, color1], [1, 1], [0x00, 0xFF], m);
		
		if (vertical)
			track.graphics.drawRect(-thickness * 0.5 + 0.5, 0.5, thickness - 1, length - 1);
		else	
			track.graphics.drawRect(0.5, -thickness * 0.5 + 0.5, length - 1, thickness - 1);
			
		head.graphics.beginFill(color1 & 0xFFFFFF, 1);
		if (vertical)
		{
			head.graphics.drawRect( -headSize * 0.5, -headSize * 0.2, headSize, headSize * 0.4);
			head.graphics.drawRect(-thickness * 0.5, -headSize * 0.1, thickness, headSize * 0.2);
			head.graphics.beginFill(color2 & 0xFFFFFF, 0.5);
			head.graphics.drawRect(-thickness * 0.5, -headSize * 0.1, thickness, headSize * 0.2);
		}
		else
		{
			head.graphics.drawRect( -headSize * 0.2, -headSize * 0.5, headSize * 0.4, headSize);
			head.graphics.drawRect(-headSize * 0.1, -thickness * 0.5, headSize * 0.2, thickness);
			head.graphics.beginFill(color2 & 0xFFFFFF, 0.5);
			head.graphics.drawRect(-headSize * 0.1, -thickness * 0.5, headSize * 0.2, thickness);
		}
	}	
}