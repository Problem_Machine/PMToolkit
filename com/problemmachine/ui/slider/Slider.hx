﻿package com.problemmachine.ui.slider;

import com.problemmachine.ui.slider.style.DefaultSliderStyle;
import com.problemmachine.ui.slider.style.ISliderStyle;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.geom.Rectangle;
import flash.Lib;
import flash.utils.Timer;


class Slider extends Sprite
{
	private var mTimer:Timer;
	
	public var precision(default, default):Int;
	public var minValue(default, set):Float;
	public var maxValue(default, set):Float;
	public var centerValue(default, set):Float;
	public var enabled(default, set):Bool;
	public var dragging(default, null):Bool;
	public var vertical(default, null):Bool;
	
	public var position(default, set):Float;
	public var value(get, set):Float;
	
	public var track(default, null):Sprite;
	public var head(default, null):Sprite;
	
	private function new()
	{
		super();
		
		dragging = false;
		mTimer = new Timer(10);
	}
	
	static public function makeCustomSlider(track:Sprite, head:Sprite, minValue:Float = 0, maxValue:Float = 1, centerValue:Float = 0.5,
		vertical:Bool = false, startVal:Float = 0, precision:Int = 0, startEnabled:Bool = true):Slider
	{
		var slider:Slider = new Slider();
		slider.addChild(track);
		slider.addChild(head);
		
		slider.vertical = vertical;
		slider.track = track;
		slider.head = head;
		slider.minValue = minValue;
		slider.maxValue = maxValue;
		slider.centerValue = centerValue;
		slider.precision = precision;
		slider.value = startVal;
		slider.enabled = startEnabled;
		
		return slider;
	}
	
	static public function makeDefaultSlider(color1:UInt, color2:UInt, length:Float, thickness:Float, headSize:Float, 
		minValue:Float = 0, maxValue:Float = 1, centerValue:Float = 0.5,
		vertical:Bool = false, startVal:Float = 0, precision:Int = 0, startEnabled:Bool = true):Slider
	{
		return makeStyleSlider(new DefaultSliderStyle(), color1, color2, length, thickness, headSize, minValue, 
			maxValue, centerValue, vertical, startVal, precision, startEnabled);
	}
	
	static public function makeStyleSlider(style:ISliderStyle, color1:UInt, color2:UInt, length:Float, thickness:Float, headSize:Float, 
		minValue:Float = 0, maxValue:Float = 1, centerValue:Float = 0.5,
		vertical:Bool = false, startVal:Float = 0, precision:Int = 0, startEnabled:Bool = true):Slider
	{
		var head:Sprite = new Sprite();
		var track:Sprite = new Sprite();
		
		if (color1 >> 24 == 0)
			color1 |= 0xFF000000;
		if (color2 >> 24 == 0)
			color2 |= 0xFF000000;
		
		style.draw(track, head, length, thickness, headSize, color1, color2, vertical);
			
		return makeCustomSlider(track, head, minValue, maxValue, centerValue, vertical, startVal, precision, startEnabled);
	}
	
	private function set_position(val:Float):Float
	{		
		position = Math.max(0, Math.min(1, val));
		syncInterfaceToData();
		return val;
	}
	
	private inline function get_value():Float
	{
		return roundToPrecision(convertPositionToValue(position), precision);
	}
	private inline function set_value(val:Float):Float
	{
		position = convertValueToPosition(val);
		return val;
	}
	
	private function set_minValue(val:Float):Float
	{
		var tempVal:Float = value;
		minValue = val;
		position = convertValueToPosition(tempVal);
		return val;
	}
	private function set_maxValue(val:Float):Float
	{
		var tempVal:Float = value;
		maxValue = val;
		position = convertValueToPosition(tempVal);
		return val;
	}
	private function set_centerValue(val:Float):Float
	{
		var tempVal:Float = value;
		centerValue = val;
		position = convertValueToPosition(tempVal);
		return val;
	}
	
	private function set_enabled(val:Bool):Bool
	{
		if (val != enabled)
		{
			enabled = val;
			head.buttonMode = enabled;
			if (enabled)
			{
				head.addEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
				mTimer.addEventListener(TimerEvent.TIMER, timerListener);
				dispatchEvent(new SliderEvent(SliderEvent.ON_CHANGE, this));
				dispatchEvent(new SliderEvent(SliderEvent.ON_ENABLED, this));
			}
			else
			{
				head.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouseDown);
				Lib.current.stage.removeEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
				mTimer.removeEventListener(TimerEvent.TIMER, timerListener);
				dispatchEvent(new SliderEvent(SliderEvent.ON_DISABLED, this));
			}
		}
		return val;
	}
	
	private function syncDataToInterface():Void
	{
		var p:Float = position;
		var headRect:Rectangle = head.getBounds(head);
		var trackRect:Rectangle = track.getBounds(track);
		if (vertical)
		{
			var loc:Float = head.y + headRect.y - (track.y + trackRect.y);			
			position = 1 - (loc / (track.height - head.height));
		}
		else
		{
			var loc:Float = head.x + headRect.x - (track.x + trackRect.x);			
			position = loc / (track.width - head.width);
		}
		
		if (p != position)
			dispatchEvent(new SliderEvent(SliderEvent.ON_CHANGE, this));
	}
	
	private function syncInterfaceToData():Void
	{
		if (dragging)
			return;
		var headRect:Rectangle = head.getBounds(head);
		var trackRect:Rectangle = track.getBounds(track);
		if (vertical)
		{
			var loc:Float = (1 - position) * (track.height - head.height);
			head.y = (track.y + trackRect.y) + loc - headRect.y;
		}
		else
		{
			var loc:Float = position * (track.width - head.width);
			head.x = (track.x + trackRect.x) + loc - headRect.x;
		}
	}
	
	public function refresh():Void
	{
		syncInterfaceToData();
	}
	
	public function dispose():Void
	{
		enabled = false;
		
		head = null;
		track = null;
		mTimer = null;
	}

	private function handleMouseDown(e:MouseEvent):Void
	{
		Lib.current.stage.addEventListener(MouseEvent.MOUSE_UP, handleMouseUp, false, 0, true);
		
		if (vertical)
		{
			var r:Rectangle = head.getBounds(head);
			r.height = Math.ceil(track.height - r.height) + 1;
			r.y = track.y - r.y;
			r.x = head.x;
			r.width = 0;
			head.startDrag(false, r);
		}
		else
		{
			var r:Rectangle = head.getBounds(head);
			r.width = Math.ceil(track.width - r.width) + 1;
			r.x = track.x - r.x;
			r.y = head.y;
			r.height = 0;
			head.startDrag(false, r);	
		}
		dragging = true;
		mTimer.start();
		dispatchEvent(new SliderEvent(SliderEvent.ON_PRESS, this));
	}
	
	private function handleMouseUp(e:MouseEvent):Void
	{
		Lib.current.stage.removeEventListener(MouseEvent.MOUSE_UP, handleMouseUp);
		
		head.stopDrag();
		mTimer.reset();
		dragging = false;
		
		syncDataToInterface();
		dispatchEvent(new SliderEvent(SliderEvent.ON_RELEASE, this));
	}
	
	private function timerListener(e:TimerEvent):Void
	{
		syncDataToInterface();
	}
	
	private inline function convertValueToPosition(value:Float):Float
	{
		var percent:Float = (value - minValue) / (maxValue - minValue);
		var split:Float = (centerValue - minValue) / (maxValue - minValue);
		if (percent > split)
			return 0.5 + (((value - centerValue) / (maxValue - centerValue)) * 0.5);
		else if (percent < split)
			return ((value - minValue) / (centerValue - minValue)) * 0.5;
		else
			return 0.5;
	}
	
	private inline function convertPositionToValue(position:Float):Float
	{
		if (position > 0.5)
			return centerValue + ((position - 0.5) * 2 * (maxValue - centerValue));
		else if (position < 0.5)
			return minValue + (position * 2 * (centerValue - minValue));
		else
			return centerValue;
	}

	static private inline function roundToPrecision(num:Float, precision:UInt):Float
	{
		var decimalPlaces:Float = Math.pow(10, precision);
		return (Math.fround(decimalPlaces * num) / decimalPlaces);
	}
}