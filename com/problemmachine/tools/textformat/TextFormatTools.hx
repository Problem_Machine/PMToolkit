package com.problemmachine.tools.textformat;
import flash.text.TextFormat;
import flash.text.TextFormatAlign;
import flash.text.TextFormatDisplay;
import haxe.xml.Fast;

class TextFormatTools
{
	static public function convertToXML(textFormat:TextFormat):Xml
	{
		var xml:Xml = Xml.parse(
		"<textformat>\n" +
		"	<align>" + Std.string(textFormat.align) + "</align>\n" + 
		"	<blockIndent>" + Std.string(textFormat.blockIndent) + "</blockIndent>\n" + 
		"	<bold>" + Std.string(textFormat.bold) + "</bold>\n" + 
		"	<bullet>" + Std.string(textFormat.bullet) + "</bullet>\n" + 
		"	<color>" + Std.string(textFormat.color) + "</color>\n" + 
		"	<display>" + Std.string(textFormat.display) + "</display>\n" + 
		"	<font>" + textFormat.font + "</font>\n" + 
		"	<indent>" + Std.string(textFormat.indent) + "</indent>\n" + 
		"	<italic>" + Std.string(textFormat.italic) + "</italic>\n" + 
		"	<kerning>" + Std.string(textFormat.kerning) + "</kerning>\n" + 
		"	<leading>" + Std.string(textFormat.leading) + "</leading>\n" + 
		"	<leftMargin>" + Std.string(textFormat.leftMargin) + "</leftMargin>\n" + 
		"	<letterSpacing>" + Std.string(textFormat.letterSpacing) + "</letterSpacing>\n" + 
		"	<rightMargin>" + Std.string(textFormat.rightMargin) + "</rightMargin>\n" + 
		"	<size>" + Std.string(textFormat.size) + "</size>\n" + 
		"	<target>" + textFormat.target + "</target>\n" + 
		"	<underline>" + Std.string(textFormat.underline) + "</underline>\n" + 
		"	<url>" + textFormat.url + "</url>\n" + 
		"</textformat>").firstElement();
		for (u in textFormat.tabStops)
			xml.addChild(Xml.parse("<tabStop>" + Std.string(u) + "</tabStop>").firstElement());
		return xml;
	}
	
	static public function createFromXML(xml:Xml):TextFormat
	{
		var tf:TextFormat = new TextFormat();
		var fast:Fast = new Fast(xml);
		tf.align = Type.createEnum(TextFormatAlign, fast.node.align.innerData); 
		tf.blockIndent = (fast.node.blockIndent.innerData == Std.string(null)) ? 
			null : Std.parseFloat(fast.node.blockIndent.innerData);
		tf.bold = (fast.node.bold.innerData == Std.string(null)) ? 
			null : fast.node.bold.innerData == Std.string(true);
		tf.bullet = (fast.node.bullet.innerData == Std.string(null)) ? 
			null : fast.node.bullet.innerData == Std.string(true);
		tf.color = (fast.node.color.innerData == Std.string(null)) ? 
			null : cast Std.parseInt(fast.node.color.innerData);
		tf.display = Type.createEnum(TextFormatDisplay, fast.node.display.innerData); 
		tf.font = fast.node.font.innerData;
		tf.indent = (fast.node.indent.innerData == Std.string(null)) ? 
			null : Std.parseFloat(fast.node.indent.innerData);
		tf.italic = (fast.node.italic.innerData == Std.string(null)) ? 
			null : fast.node.italic.innerData == Std.string(true);
		tf.kerning = (fast.node.kerning.innerData == Std.string(null)) ? 
			null : fast.node.kerning.innerData == Std.string(true);
		tf.leading = (fast.node.leading.innerData == Std.string(null)) ? 
			null : Std.parseFloat(fast.node.leading.innerData);
		tf.leftMargin = (fast.node.leftMargin.innerData == Std.string(null)) ? 
			null : Std.parseFloat(fast.node.leftMargin.innerData);
		tf.letterSpacing = (fast.node.letterSpacing.innerData == Std.string(null)) ? 
			null : Std.parseFloat(fast.node.letterSpacing.innerData);
		tf.rightMargin = (fast.node.rightMargin.innerData == Std.string(null)) ? 
			null : Std.parseFloat(fast.node.rightMargin.innerData);
		tf.size = (fast.node.size.innerData == Std.string(null)) ? 
			null : Std.parseFloat(fast.node.size.innerData);
		tf.target = fast.node.target.innerData;
		tf.underline = (fast.node.underline.innerData == Std.string(null)) ? 
			null : fast.node.underline.innerData == Std.string(true);
		tf.url = fast.node.url.innerData;
		for (x in fast.nodes.tabStop)
			tf.tabStops.push(cast Std.parseInt(x.innerData));
			
		return tf;
	}
	
}