package com.problemmachine.tools.xml;
import flash.errors.Error;

class XMLTools
{
	static public inline function getVal(xml:Xml, nodeName:String):String
	{
		if (xml.elementsNamed(nodeName).next().firstChild() != null)
			return xml.elementsNamed(nodeName).next().firstChild().nodeValue;
		else
			return "";
	}
	
	static public function tidyXML(xml:Xml, indentChar:String = "\t", indentCount:UInt = 0):String
	{
		var s:String = "";
		var myIndent:String = "";
		for (i in 0...indentCount)
			myIndent += indentChar;
		switch (xml.nodeType)
		{
			case Xml.Document:
				for (x in xml)
					s += tidyXML(x, indentChar, indentCount);
						
			case Xml.Element:
				s += myIndent + "<" + xml.nodeName;
				for (a in xml.attributes())
					s += " " + a + "=\"" + xml.get(a) + "\"";
				s += ">";
				if (xml.firstElement() != null)
					s += "\n";
				
				for (x in xml)
					s += tidyXML(x, indentChar, indentCount + 1);
				
				if (xml.firstElement() != null)
					s += myIndent;
				s += "</" + xml.nodeName + ">\n";
					
			default:
				for (i in 0...xml.nodeValue.length)
				{
					var char:String = xml.nodeValue.charAt(i);
					if (char != "\t" && char != "\n" && char != "\r")
						s += xml.nodeValue.charAt(i);
				}
		}
		return s;
	}
	
	public static function dataToXML(data:Dynamic):Xml
	{
		if (data == null)
		{
			return Xml.parse(
				"<data>\n" +
				"	<type>Null</type>\n"	+
				"</data>").firstElement();
		}
		else if (Std.is(data, String))
		{
			return Xml.parse(
				"<data>\n" +
				"	<type>String</type>\n"	+
				"	<contents>" + data + "</contents>\n"	+
				"</data>").firstElement();
		}
		else if (Std.is(data, UInt))
		{
			return Xml.parse(
				"<data>\n" +
				"	<type>UInt</type>\n"	+
				"	<contents>" + Std.string(data) + "</contents>\n"	+
				"</data>").firstElement();
		}
		else if (Std.is(data, Int))
		{
			return Xml.parse(
				"<data>\n" +
				"	<type>Int</type>\n"	+
				"	<contents>" + Std.string(data) + "</contents>\n"	+
				"</data>").firstElement();
		}
		else if (Std.is(data, Float))
		{
			return Xml.parse(
				"<data>\n" +
				"	<type>Float</type>\n"	+
				"	<contents>" + Std.string(data) + "</contents>\n"	+
				"</data>").firstElement();
		}
		else if (Std.is(data, Bool))
		{
			return Xml.parse(
				"<data>\n" +
				"	<type>Bool</type>\n"	+
				"	<contents>" + Std.string(data) + "</contents>\n"	+
				"</data>").firstElement();
		}
		else if (Reflect.isEnumValue(data))
		{
			var xml:Xml = Xml.parse(
				"<data>\n" +
				"	<type>Enum</type>\n" +
				"</data>").firstElement();
			var contents:Xml = Xml.parse(
				"<contents>\n" + 
				"	<name>" + Type.getEnum(cast data).getName() + "</name>\n" + 
				"	<value>" + Std.string(data) + "</value>\n" + 
				"</contents>").firstElement();
			var params:Array<Dynamic> = Type.enumParameters(cast data);
			if (params.length > 0)
			{
				var x:Xml = Xml.parse("<parameters/>").firstElement();
				for (d in params)
					x.addChild(dataToXML(d));
				contents.addChild(x);
			}
			xml.addChild(contents);
			return xml;
		}
		else if (Std.is(data, Array))
		{
			var xml:Xml = Xml.parse(
				"<data>\n" +
				"	<type>Array</type>\n"	+
				"</data>").firstElement();
			var x:Xml = Xml.parse("<contents/>").firstElement();
			for (d in cast(data, Array<Dynamic>))
				x.addChild(dataToXML(d));
			xml.addChild(x);
			return xml;
		}
		else
		{
			// this check seems to not work for non-air targets due to a difference in the implementation of hasField
			//if (Reflect.hasField(data, "toXML"))
			{
				var xml:Xml =  Xml.parse(
					"<data>\n" +
					"	<type>" + Type.getClassName(Type.getClass(data)) + "</type>\n"	+
					"</data>").firstElement();
				var x:Xml = Xml.parse("<contents/>").firstElement();
				x.addChild(cast data.toXML());
				xml.addChild(x);
				return xml;
			}
			/*else
				throw new Error("XMLTools.dataToXML(): Type " + 
					Type.getClassName(Type.getClass(data)) + " unrecognized and does not support toXML() functionality");*/
		}
	}
	
	public static function XMLToData(xml:Xml):Dynamic
	{
		var type:String = XMLTools.getVal(xml, "type");
		var contents:Xml = xml.elementsNamed("contents").next();
		switch(type)
		{
			case "Null":
				return null;
			case "String":
				return contents.firstChild() != null ? contents.firstChild().nodeValue : "";
			case "Int":
				return Std.parseInt(contents.firstChild().nodeValue);
			case "UInt":
				return cast(Std.parseInt(contents.firstChild().nodeValue), UInt);
			case "Float":
				return  Std.parseFloat(contents.firstChild().nodeValue);
			case "Bool":
				return  (contents.firstChild().nodeValue == Std.string(true));
			case "Enum":
				var params:Array<Dynamic> = [];
				if (contents.elementsNamed("parameters").hasNext())
					for (x in contents.elementsNamed("parameters").next().elementsNamed("data"))
						params.push(XMLToData(x));
				return Type.createEnum(
					Type.resolveEnum(XMLTools.getVal(contents, "name")), 
					XMLTools.getVal(contents, "value"),
					params);
			case "Array":				
				var arr:Array<Dynamic> = [];
				
				for (x in contents.elementsNamed("data"))
					arr.push(XMLToData(x));
				
				return arr;
			default:
				var cls = Type.resolveClass(type);
				if (cls == null)
					throw new Error ("XMLTools.XMLToData(): UNKNOWN TYPE" + type);
				if (Reflect.hasField(cls, "createFromXML"))
					return Reflect.callMethod(cls, Reflect.field(cls, "createFromXML"), [contents.firstElement()]);
				else
					throw new Error ("XMLTools.XMLToData(): Dynamic type " + type + " does not support createFromXML()");
		}
	}
}