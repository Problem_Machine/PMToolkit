package com.problemmachine.tools.graphics;
import com.problemmachine.bitmap.bitmapmanager.BitmapManager;
import com.problemmachine.bitmap.bitmapmanager.BitmapManagerEvent;
import com.problemmachine.tools.xml.XMLTools;
import flash.display.CapsStyle;
import flash.display.GradientType;
import flash.display.GraphicsBitmapFill;
import flash.display.GraphicsEndFill;
import flash.display.GraphicsGradientFill;
import flash.display.GraphicsPath;
import flash.display.GraphicsPathCommand;
import flash.display.GraphicsPathWinding;
import flash.display.GraphicsSolidFill;
import flash.display.GraphicsStroke;
import flash.display.GraphicsTrianglePath;
import flash.display.IGraphicsData;
import flash.display.IGraphicsFill;
import flash.display.InterpolationMethod;
import flash.display.JointStyle;
import flash.display.LineScaleMode;
import flash.display.SpreadMethod;
import flash.display.TriangleCulling;
import flash.errors.Error;
import flash.geom.Matrix;
import flash.Vector.Vector;
import haxe.xml.Fast;
#if air
import flash.display.GraphicsShaderFill;
#end

class GraphicsTools
{	
	public static function cloneGraphics(gfx:Vector<IGraphicsData>):Vector<IGraphicsData>
	{
		var vec:Vector<IGraphicsData> = new Vector<IGraphicsData>(gfx.length, true);
		for (i in 0...gfx.length)
			vec[i] = cloneGraphicsElement(gfx[i]);
		return vec;
	}
	public static function cloneGraphicsElement(g:IGraphicsData):IGraphicsData
	{
		if (Std.is(g, GraphicsBitmapFill))
			return new GraphicsBitmapFill((cast g).bitmapData, (cast g).matrix.clone(), (cast g).repeat, (cast g).smooth);
		else if (Std.is(g, GraphicsEndFill))
			return new GraphicsEndFill();
		else if (Std.is(g, GraphicsGradientFill))
			return new GraphicsGradientFill((cast g).type, (cast g).colors.copy(), (cast g).alphas.copy(), 
				(cast g).ratios.copy(), (cast g).matrix.clone(), (cast g).spreadMethod, (cast g).interpolationMethod, 
				(cast g).focalPointRatio);
		else if (Std.is(g, GraphicsPath))
			return new GraphicsPath((cast g).commands.concat(), (cast g).data.concat(), (cast g).winding);
		//else if (Std.is(g, GraphicsShaderFill))
		//	return new GraphicsShaderFill((cast g).shader, (cast g).matrix.clone());
		else if (Std.is(g, GraphicsSolidFill))
			return new GraphicsSolidFill((cast g).color, (cast g).alpha);
		else if (Std.is(g, GraphicsStroke))
			return new GraphicsStroke((cast g).thickness, (cast g).pixelHinting, (cast g).scaleMode, (cast g).caps, 
				(cast g).joints, (cast g).miterLimit, cast cloneGraphicsElement((cast g).fill));
		else if (Std.is(g, GraphicsTrianglePath))
			return new GraphicsTrianglePath((cast g).vertices.concat(), (cast g).indices.concat(), 
				(cast g).uvtData.concat(), (cast g).culling);
		throw new Error("GraphicsTools.cloneGraphicsElement(): class " + Type.getClassName(Type.getClass(g)) + " was not recognized");
	}
	
	public static function graphicsVectorToXML(gfx:Vector<IGraphicsData>):Xml
	{
		var xml:Xml = Xml.parse("<graphicsList/>").firstElement();
		for (g in gfx)
			xml.addChild(graphicsToXML(g));
		return xml;
	}
	
	public static function graphicsToXML(gfx:IGraphicsData):Xml
	{
		if (Std.is(gfx, GraphicsBitmapFill))
			return bitmapFillToXML(cast gfx);
		else if (Std.is(gfx, GraphicsEndFill))
			return endFillToXML(cast gfx);
		else if (Std.is(gfx, GraphicsGradientFill))
			return gradientFillToXML(cast gfx);
		else if (Std.is(gfx, GraphicsPath))
			return pathToXML(cast gfx);
		//else if (Std.is(gfx, GraphicsShaderFill))
		//	return shaderFillToXML(cast gfx);
		else if (Std.is(gfx, GraphicsSolidFill))
			return solidFillToXML(cast gfx);
		else if (Std.is(gfx, GraphicsStroke))
			return strokeToXML(cast gfx);
		else if (Std.is(gfx, GraphicsTrianglePath))
			return trianglePathToXML(cast gfx);
		else 
			throw new Error("GraphicsTools.graphicsToXml(): class " + Type.getClassName(Type.getClass(gfx)) + " was not recognized");
	}
	
	static private inline function bitmapFillToXML(gfx:GraphicsBitmapFill):Xml
	{
		var path:String = BitmapManager.reverseLookUp(gfx.bitmapData);
		if (path == null)
			throw new Error("GraphicsTools.bitmapFillToXML() : Could not look up bitmap resource path");
		var xml:Xml = Xml.parse(
			"<bitmapFill>\n" +
			"	<path>" + Std.string(path) + "</path>\n" +
			"	<repeat>" + Std.string(gfx.repeat) + "</repeat>\n" +
			"	<smooth>" + Std.string(gfx.smooth) + "</smooth>\n" +
			"</bitmapFill>").firstElement();
		
		xml.addChild(matrixToXML(gfx.matrix));
		return xml;
	}
	
	static private inline function endFillToXML(gfx:GraphicsEndFill):Xml
	{
		return Xml.parse("<endFill/>").firstElement();
	}
	
	static private inline function gradientFillToXML(gfx:GraphicsGradientFill):Xml
	{
		var xml:Xml = Xml.parse(
			"<gradientFill>\n" +
			"	<focalPointRatio>" + Std.string(gfx.focalPointRatio) + "</focalPointRatio>\n" +
			"	<interpolationMethod>" + Std.string(gfx.interpolationMethod) + "</interpolationMethod>\n" +
			"	<type>" + Std.string(gfx.type) + "</type>\n" +
			"	<spreadMethod>" + Std.string(gfx.spreadMethod) + "</spreadMethod>\n" +
			"</gradientFill>").firstElement();
		for (f in gfx.alphas)
			xml.addChild(Xml.parse("<alpha>" + Std.string(f) + "</alpha>\n").firstElement());
		for (u in gfx.colors)
			xml.addChild(Xml.parse("<color>" + Std.string(u) + "</color>\n").firstElement());
		for (f in gfx.ratios)
			xml.addChild(Xml.parse("<ratio>" + Std.string(f) + "</ratio>\n").firstElement());
			
		xml.addChild(matrixToXML(gfx.matrix));
		return xml;
	}
	
	static private inline function pathToXML(gfx:GraphicsPath):Xml
	{
		var xml:Xml = Xml.parse(
			"<path>\n" +
			"	<winding>" + (gfx.winding == GraphicsPathWinding.NON_ZERO ? "NON_ZERO" : "EVEN_ODD") + "</winding>\n" +
			"</path>").firstElement();
		var dataIndex:Int = 0;
		var count:Int = 0;
		for (i in 0...gfx.commands.length)
		{
			switch(gfx.commands[i])
			{
				case GraphicsPathCommand.NO_OP:
					xml.addChild(Xml.parse(
						"<command number = \"" + (count++) + "\">\n" +
						"	<type>NO_OP</type>\n" +
						"</command>").firstElement());
					
				case GraphicsPathCommand.MOVE_TO:
					xml.addChild(Xml.parse(
						"<command number = \"" + (count++) + "\">\n" +
						"	<type>MOVE_TO</type>\n" +
						"	<x>" + Std.string(gfx.data[dataIndex++]) + "</x>\n" +
						"	<y>" + Std.string(gfx.data[dataIndex++]) + "</y>\n" +
						"</command>").firstElement());
					
				case GraphicsPathCommand.LINE_TO:
					xml.addChild(Xml.parse(
						"<command number = \"" + (count++) + "\">\n" +
						"	<type>LINE_TO</type>\n" +
						"	<x>" + Std.string(gfx.data[dataIndex++]) + "</x>\n" +
						"	<y>" + Std.string(gfx.data[dataIndex++]) + "</y>\n" +
						"</command>").firstElement());
					
				case GraphicsPathCommand.CURVE_TO:				
					xml.addChild(Xml.parse(
						"<command number = \"" + (count++) + "\">\n" +
						"	<type>CURVE_TO</type>\n" +
						"	<controlX>" + Std.string(gfx.data[dataIndex++]) + "</controlX>\n" +
						"	<controlY>" + Std.string(gfx.data[dataIndex++]) + "</controlY>\n" +
						"	<anchorX>" + Std.string(gfx.data[dataIndex++]) + "</anchorX>\n" +
						"	<anchorY>" + Std.string(gfx.data[dataIndex++]) + "</anchorY>\n" +
						"</command>").firstElement());
						
				case GraphicsPathCommand.CUBIC_CURVE_TO:
					xml.addChild(Xml.parse(
						"<command number = \"" + (count++) + "\">\n" +
						"	<type>CUBIC_CURVE_TO</type>\n" +
						"	<controlX1>" + Std.string(gfx.data[dataIndex++]) + "</controlX1>\n" +
						"	<controlY1>" + Std.string(gfx.data[dataIndex++]) + "</controlY1>\n" +
						"	<controlX2>" + Std.string(gfx.data[dataIndex++]) + "</controlX2>\n" +
						"	<controlY2>" + Std.string(gfx.data[dataIndex++]) + "</controlY2>\n" +
						"	<anchorX>" + Std.string(gfx.data[dataIndex++]) + "</anchorX>\n" +
						"	<anchorY>" + Std.string(gfx.data[dataIndex++]) + "</anchorY>\n" +
						"</command>").firstElement());
						
				case GraphicsPathCommand.WIDE_MOVE_TO:
					xml.addChild(Xml.parse(
						"<command number = \"" + (count++) + "\">\n" +
						"	<type>WIDE_MOVE_TO</type>\n" +
						"	<x>" + Std.string(gfx.data[dataIndex++]) + "</x>\n" +
						"	<y>" + Std.string(gfx.data[dataIndex++]) + "</y>\n" +
						"</command>").firstElement());
					
				case GraphicsPathCommand.WIDE_LINE_TO:
					xml.addChild(Xml.parse(
						"<command number = \"" + (count++) + "\">\n" +
						"	<type>WIDE_LINE_TO</type>\n" +
						"	<x>" + Std.string(gfx.data[dataIndex++]) + "</x>\n" +
						"	<y>" + Std.string(gfx.data[dataIndex++]) + "</y>\n" +
						"</command>").firstElement());
			}
		}
		return xml;
	}
	
	#if air
	static private inline function shaderFillToXML(gfx:GraphicsShaderFill):Xml
	{
		throw new Error("GraphicsTools.shaderFillToXML() : Currently not supported (cannot encode shader into xml format)");
		return null;
	}
	#end
	
	static private inline function solidFillToXML(gfx:GraphicsSolidFill):Xml
	{
		return Xml.parse(
			"<solidFill>\n" +
			"	<alpha>" + Std.string(gfx.alpha) + "</alpha>\n" +
			"	<color>" + Std.string(gfx.color) + "</color>\n" +
			"</solidFill>").firstElement();
	}
	
	static private inline function strokeToXML(gfx:GraphicsStroke):Xml
	{
		var xml:Xml = Xml.parse(
			"<stroke>\n" +
			"	<caps>" + Std.string(gfx.caps) + "</caps>\n" +
			"	<joints>" + Std.string(gfx.joints) + "</joints>\n" +
			"	<miterLimit>" + Std.string(gfx.miterLimit) + "</miterLimit>\n" +
			"	<pixelHinting>" + Std.string(gfx.pixelHinting) + "</pixelHinting>\n" +
			"	<scaleMode>" + Std.string(gfx.scaleMode) + "</scaleMode>\n" +
			"	<thickness>" + Std.string(gfx.thickness) + "</thickness>\n" +
			"</stroke>").firstElement();
			
		xml.addChild(graphicsToXML(cast gfx.fill));
		return xml;
	}
	
	static private inline function trianglePathToXML(gfx:GraphicsTrianglePath):Xml
	{
		var xml:Xml = Xml.parse(
			"<trianglePath>\n" +
			"	<culling>" + Std.string(gfx.culling) + "</culling>\n" +
			"</trianglePath>").firstElement();
			
		var vx:Int = 0;
		var uvt:Int = 0;
		var count:Int = 0;
		while (vx < gfx.vertices.length)
		{
			if (gfx.uvtData == null)
				xml.addChild(Xml.parse(
					"<vertex number = \"" + (count++) + "\">\n" +
					"	<x>" + Std.string(gfx.vertices[vx++]) + "</x>\n" +
					"	<y>" + Std.string(gfx.vertices[vx++]) + "</y>\n" +
					"</vertex>").firstElement());
			else if (gfx.uvtData.length == gfx.vertices.length)
				xml.addChild(Xml.parse(
					"<vertex number = \"" + (count++) + "\">\n" +
					"	<x>" + Std.string(gfx.vertices[vx++]) + "</x>\n" +
					"	<y>" + Std.string(gfx.vertices[vx++]) + "</y>\n" +
					"	<u>" + Std.string(gfx.vertices[uvt++]) + "</u>\n" +
					"	<v>" + Std.string(gfx.vertices[uvt++]) + "</v>\n" +
					"</vertex>").firstElement());
			else if (gfx.uvtData.length == gfx.vertices.length * 1.5)
				xml.addChild(Xml.parse(
					"<vertex number = \"" + (count++) + "\">\n" +
					"	<x>" + Std.string(gfx.vertices[vx++]) + "</x>\n" +
					"	<y>" + Std.string(gfx.vertices[vx++]) + "</y>\n" +
					"	<u>" + Std.string(gfx.vertices[uvt++]) + "</u>\n" +
					"	<v>" + Std.string(gfx.vertices[uvt++]) + "</v>\n" +
					"	<t>" + Std.string(gfx.vertices[uvt++]) + "</t>\n" +
					"</vertex>").firstElement());
			else
				xml.addChild(Xml.parse(
					"<vertex number = \"" + (count++) + "\">\n" +
					"	<x>" + Std.string(gfx.vertices[vx++]) + "</x>\n" +
					"	<y>" + Std.string(gfx.vertices[vx++]) + "</y>\n" +
					"</vertex>").firstElement());			
		}
		if (gfx.indices != null)
			for (i in 0...Math.floor(gfx.indices.length / 3))
				xml.addChild(Xml.parse(
					"<tri>\n" +
					"	<a>" + Std.string(gfx.indices[i * 3]) + "</a>\n" +
					"	<b>" + Std.string(gfx.indices[i * 3 + 1]) + "</b>\n" +
					"	<c>" + Std.string(gfx.indices[i * 3 + 2]) + "</c>\n" +
					"</tri>").firstElement());						
					
		return xml;
	}
	
	static private inline function matrixToXML(matrix:Matrix):Xml
	{
		return Xml.parse(
			"<matrix>\n" +
			"	<a>" + Std.string(matrix.a) + "</a>\n" +
			"	<b>" + Std.string(matrix.b) + "</b>\n" +
			"	<c>" + Std.string(matrix.c) + "</c>\n" +
			"	<d>" + Std.string(matrix.d) + "</d>\n" +
			"	<tx>" + Std.string(matrix.c) + "</tx>\n" +
			"	<ty>" + Std.string(matrix.d) + "</ty>\n" +
			"</matrix>").firstElement();
	}
	
	public static function isValidXML(xml:Xml):Bool
	{
		if (xml == null)
			return false;
		switch(xml.nodeName)
		{
			case "graphicsList",
				"bitmapFill",
				"endFill",
				"gradientFill",
				"path",
				"shaderFill",
				"solidFill",
				"stroke",
				"trianglePath":
				return true;
			case null:
				return false;
			default:
				return isValidXML(xml.firstElement());
		}
	}
	
	public static function createFromXML(xml:Xml):Dynamic
	{
		switch(xml.nodeName)
		{
			case "graphicsList":
				var gfx:Vector<IGraphicsData> = new Vector<IGraphicsData>();
				for (x in xml.elements())
					gfx.push(createFromXML(x));
				return gfx;
			case "bitmapFill":
				return XMLToBitmapFill(xml);
			case "endFill":
				return XMLToEndFill(xml);
			case "gradientFill":
				return XMLToGradientFill(xml);
			case "path":
				return XMLToPath(xml);
			//case "shaderFill":
			//	return XMLToShaderFill(xml);
			case "solidFill":
				return XMLToSolidFill(xml);
			case "stroke":
				return XMLToStroke(xml);
			case "trianglePath":
				return XMLToTrianglePath(xml);
			case null:
				throw new Error("GraphicsTools.createFromXML() : Unrecognized Xml");
			default:
				return createFromXML(xml.firstElement());				
		}
	}
	
	static private inline function XMLToBitmapFill(xml:Xml):GraphicsBitmapFill
	{
		var bf:GraphicsBitmapFill = new GraphicsBitmapFill();
		var fxml:Fast = new Fast(xml);
		new BitmapListener(bf, fxml.node.path.innerData);
		BitmapManager.loadAndLock(fxml.node.path.innerData, bf);
		bf.matrix = XMLToMatrix(fxml.node.matrix.x);
		bf.repeat = XMLTools.getVal(xml, "repeat") == Std.string(true);
		bf.smooth = XMLTools.getVal(xml, "smooth") == Std.string(true);
		return bf;
	}
	
	static private inline function XMLToEndFill(xml:Xml):GraphicsEndFill
	{
		return new GraphicsEndFill();
	}
	
	static private inline function XMLToGradientFill(xml:Xml):GraphicsGradientFill
	{
		var gf:GraphicsGradientFill = new GraphicsGradientFill();
		var fxml:Fast = new Fast(xml);
		{
			var arr:Array<Float> = [];
			for (x in fxml.nodes.alpha)
				arr.push(Std.parseFloat(x.innerData));
			gf.alphas = arr;
		}
		{
			var arr:Array<UInt> = [];
			for (x in fxml.nodes.color)
				arr.push(Std.parseInt(x.innerData));
			gf.colors = arr;
		}
		{
			#if openfl
				var arr:Array<UInt> = [];
			#else
				var arr:Array<Float> = [];
			#end
			for (x in fxml.nodes.ratio)
				arr.push(Std.parseInt(x.innerData));
			gf.ratios = arr;
		}
		gf.focalPointRatio = Std.parseFloat(fxml.node.focalPointRatio.innerData);
		gf.interpolationMethod = Type.createEnum(cast InterpolationMethod, fxml.node.interpolationMethod.innerData);
		gf.spreadMethod = Type.createEnum(cast SpreadMethod, fxml.node.spreadMethod.innerData);
		gf.type = Type.createEnum(cast GradientType, fxml.node.type.innerData);
		gf.matrix = XMLToMatrix(fxml.node.matrix.x);
		return gf;
	}
	
	static private inline function XMLToPath(xml:Xml):GraphicsPath
	{
		var fxml:Fast = new Fast(xml);
		var commands:Vector<Int> = new Vector<Int>();
		var data:Vector<Float> = new Vector<Float>();
		for (x in fxml.nodes.command)
		{
			var type:String = x.node.type.innerData;
			
			switch(type)
			{
				case "NO_OP":
					commands.push(0);
					
				case "MOVE_TO":
					commands.push(1);
					data.push(Std.parseFloat(x.node.x.innerData));
					data.push(Std.parseFloat(x.node.y.innerData));
					
				case "LINE_TO":
					commands.push(2);
					data.push(Std.parseFloat(x.node.x.innerData));
					data.push(Std.parseFloat(x.node.y.innerData));
					
				case "CURVE_TO":	
					commands.push(3);
					data.push(Std.parseFloat(x.node.controlX.innerData));
					data.push(Std.parseFloat(x.node.controlY.innerData));
					data.push(Std.parseFloat(x.node.anchorX.innerData));
					data.push(Std.parseFloat(x.node.anchorY.innerData));
						
				case "CUBIC_CURVE_TO":
					commands.push(4);
					data.push(Std.parseFloat(x.node.controlX1.innerData));
					data.push(Std.parseFloat(x.node.controlY1.innerData));
					data.push(Std.parseFloat(x.node.controlX2.innerData));
					data.push(Std.parseFloat(x.node.controlY2.innerData));
					data.push(Std.parseFloat(x.node.anchorX.innerData));
					data.push(Std.parseFloat(x.node.anchorY.innerData));
						
				case "WIDE_MOVE_TO":
					commands.push(5);
					data.push(Std.parseFloat(x.node.x.innerData));
					data.push(Std.parseFloat(x.node.y.innerData));
					data.push(0);
					data.push(0);
					
				case "WIDE_LINE_TO":
					commands.push(6);
					data.push(Std.parseFloat(x.node.x.innerData));
					data.push(Std.parseFloat(x.node.y.innerData));
					data.push(0);
					data.push(0);
			}
		}
		
		var winding:String = fxml.node.winding.innerData;
		#if openfl
			if (winding == "NON_ZERO")
				winding = "nonZero";
			else if (winding == "EVEN_ODD")
				winding = "evenOdd";
			return new GraphicsPath(commands, data, winding);
		#else
			if (winding == "nonZero")
				winding = "NON_ZERO";
			else if (winding == "evenOdd")
				winding = "EVEN_ODD";
			return new GraphicsPath(commands, data, Type.createEnum(GraphicsPathWinding, winding));
		#end
	}
	
	/*
	static private inline function XMLToShaderFill(xml:Xml):GraphicsShaderFill
	{
		throw new Error("GraphicsTools.XMLToShaderFill() : Currently not supported (cannot decode shader from xml format)");
		return null;
	}
	*/
	
	static private inline function XMLToSolidFill(xml:Xml):GraphicsSolidFill
	{
		return new GraphicsSolidFill(Std.parseInt(XMLTools.getVal(xml, "color")), Std.parseFloat(XMLTools.getVal(xml, "alpha")));
	}
	
	static private inline function XMLToStroke(xml:Xml):GraphicsStroke
	{
		var fxml:Fast = new Fast(xml);
		
		var fill:IGraphicsFill = null;
		if (fxml.hasNode.bitmapFill)
			fill = XMLToBitmapFill(fxml.node.bitmapFill.x);
		else if (fxml.hasNode.endFill)
			fill = XMLToEndFill(fxml.node.endFill.x);
		else if (fxml.hasNode.gradientFill)
			fill = XMLToGradientFill(fxml.node.gradientFill.x);
		//else if (fxml.hasNode.shaderFill)
		//	fill = XMLToShaderFill(fxml.node.shaderFill.x);
		else if (fxml.hasNode.solidFill)
			fill = XMLToSolidFill(fxml.node.solidFill.x);
			
		return new GraphicsStroke(	Std.parseFloat(fxml.node.thickness.innerData),
									fxml.node.pixelHinting.innerData == Std.string(true),
									Type.createEnum(cast LineScaleMode, fxml.node.scaleMode.innerData),
									Type.createEnum(cast CapsStyle, fxml.node.caps.innerData),
									Type.createEnum(cast JointStyle, fxml.node.joints.innerData),
									Std.parseFloat(fxml.node.miterLimit.innerData),
									fill);
			
	}
	
	static private inline function XMLToTrianglePath(xml:Xml):GraphicsTrianglePath
	{
		var fxml:Fast = new Fast(xml);
		
		var vertices:Vector<Float> = new Vector<Float>();
		var indices:Vector<Int> = new Vector<Int>();
		var uvtData:Vector<Float> = new Vector<Float>();
		for (x in fxml.nodes.vertex)
		{
			vertices.push(Std.parseFloat(x.node.x.innerData));
			vertices.push(Std.parseFloat(x.node.y.innerData));
			if (x.hasNode.u)
			{
				uvtData.push(Std.parseFloat(x.node.u.innerData));
				uvtData.push(Std.parseFloat(x.node.v.innerData));
				if (x.hasNode.t)
					uvtData.push(Std.parseFloat(x.node.t.innerData));
			}
		}
		if (uvtData.length == 0)
			uvtData = null;
			
		for (x in fxml.nodes.tri)
		{
			indices.push(Std.parseInt(x.node.a.innerData));
			indices.push(Std.parseInt(x.node.b.innerData));
			indices.push(Std.parseInt(x.node.c.innerData));
		}
		if (indices.length == 0)
			indices = null;
			
		return new GraphicsTrianglePath(vertices, indices, uvtData, Type.createEnum(cast TriangleCulling, fxml.node.culling.innerData));
	}
	
	static private inline function XMLToMatrix(xml:Xml):Matrix
	{
		var m:Matrix = new Matrix();
		m.a = Std.parseFloat(XMLTools.getVal(xml, "a"));
		m.b = Std.parseFloat(XMLTools.getVal(xml, "b"));
		m.c = Std.parseFloat(XMLTools.getVal(xml, "c"));
		m.d = Std.parseFloat(XMLTools.getVal(xml, "d"));
		m.tx = Std.parseFloat(XMLTools.getVal(xml, "tx"));
		m.ty = Std.parseFloat(XMLTools.getVal(xml, "ty"));
		return m;
	}
}

private class BitmapListener
{
	private var bf:GraphicsBitmapFill;
	private var path:String;
	
	public function new(bf:GraphicsBitmapFill, path:String)
	{
		this.bf = bf;
		this.path = path;
		BitmapManager.startListeningForCompletion(path, listener);
	}
	
	private function listener(e:BitmapManagerEvent):Void
	{
		BitmapManager.stopListeningForCompletion(path, listener);
		bf.bitmapData = BitmapManager.request(path);
	}
}