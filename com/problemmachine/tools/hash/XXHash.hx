package com.problemmachine.tools.hash;

/**
 * simple version of xxHash function
 * pulled from https://blogs.unity3d.com/2015/01/07/a-primer-on-repeatable-random-numbers/
 * and https://bitbucket.org/runevision/random-numbers-testing/src/113e3cdaf14ab86d3a03a5b2ed1d178549952bcd/Assets/Implementations/HashFunctions/
 */
class XXHash
{
	public var seed:UInt;
	
	private static inline var PRIME32_1:UInt = cast 2654435761;
	private static inline var PRIME32_2:UInt = cast 2246822519;
	private static inline var PRIME32_3:UInt = cast 3266489917;
	private static inline var PRIME32_4:UInt = cast 668265263;
	private static inline var PRIME32_5:UInt = cast 374761393;
	
	public function new(seed:UInt) 
	{
		this.seed = seed;
	}
	
	public inline function getHash(buf:UInt):UInt
	{
		var h32:UInt = seed + PRIME32_5;
		h32 += 4;
		h32 += buf * PRIME32_3;
		h32 = rotateLeft(h32, 17) * PRIME32_4;
		h32 ^= h32 >> 15;
		h32 *= PRIME32_2;
		h32 ^= h32 >> 13;
		h32 *= PRIME32_3;
		h32 ^= h32 >> 16;
		return h32;
	}
	
	private static inline function rotateLeft(value:UInt, count:Int):UInt 
	{
		return (value << count) | (value >> (32 - count));
	}
}