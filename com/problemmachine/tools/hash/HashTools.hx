package com.problemmachine.tools.hash;

class HashTools
{
	public static function hashStringToFloat(str:String):Float
	{
		var hash:Float = 1;
		var len:Int = str.length;
		for (i in 0...len)
			if (i % 2 == 0)
				hash *= (str.charCodeAt(i) + len - i) * 0.02;
			else
				hash /= (str.charCodeAt(i) + len - i) * -0.02;
		return hash;
	}
	
}