package com.problemmachine.tools.file;
import com.problemmachine.tools.xml.XMLTools;
import flash.display.Bitmap;
import flash.display.Loader;
import flash.display.LoaderInfo;
import flash.errors.Error;
import flash.errors.IOError;
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.net.FileFilter;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.utils.ByteArray;
import haxe.io.Bytes;
#if air
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;
import flash.events.FileListEvent;
import flash.errors.SecurityError;
#else
import haxe.Serializer;
import sys.FileSystem;
import sys.io.File;
import sys.io.FileOutput;
import systools.Dialogs;
#end
import haxe.PosInfos;

class FileTools
{
	private static inline var SAFE_OVERWRITE:String = ".~safe";
	
	public static var rootDirectory(get, never):String;
	private static var sStreamInfo:Array<StreamInfo> = [];
	
	#if air
		private static var sBrowsing:
			{file:File, data:Dynamic, listener:Dynamic->Void, 
			cancelListener:Dynamic->Void, errorListener:Dynamic->Void, write:Bool} = null;
	#end
	private static var sDisplayExtensions:Array<String> = ["png", "jpg", "jpeg", "gif", "swf"];
	public static var verboseLoad:Bool = false;
	public static var verboseSave:Bool = true;
	public static var verboseError:Bool = true;
	
	public static var saving(get, never):Bool;
	
	private static function get_saving():Bool
	{
		#if air
			for (si in sStreamInfo)
				if (Std.is(si.stream, FileStream))
					return true;	// since filestreams are currently only used for write ops and immediateread this is accurate
		#elseif cpp
			// ??? TODO -- This is currently not used so not a high priority, but still.
		#end
		return false;
	}
	
	private static inline function get_rootDirectory():String
	{
		#if air
			var f:File = File.applicationDirectory;	
			return f.nativePath;
		#else
			return FileSystem.absolutePath("");
		#end
	}
	
	public static function forceClose():Void
	{
		while (sStreamInfo.length > 0)
		{
			var si:StreamInfo = sStreamInfo.pop();
			#if air
				if (Std.is(si.stream, FileStream))
					cast(si.stream, FileStream).close();
				else if (Std.is(si.stream, URLLoader))
					cast(si.stream, URLLoader).close();
				else if (Std.is(si.stream, LoaderInfo))
					cast(si.stream, LoaderInfo).loader.close();
			#end
			// currently air is the only platform that supports async save/load
				
			si.errorFunction(new FileToolsEvent(FileToolsEvent.CANCEL, si.path, null));
		}
	}
	
	public static inline function addDisplayExtension(ext:String):Void
	{
		ext = ext.toLowerCase();
		if (sDisplayExtensions.indexOf(ext) >= 0)
			return;
		sDisplayExtensions.push(ext);
	}
	
	public static inline function fileExistsAt(path:String):Bool
	{
		path = formatPath(path);
		#if air
			var f:File = File.applicationDirectory.resolvePath(path);
			return (f.exists);
		#elseif cpp
			return FileSystem.exists(path);
		#else
			throw new Error("this platform is not supported");
		#end
	}
	
	public static inline function fileModifiedTime(path:String):Float
	{
		path = formatPath(path);
		#if air
			var f:File = File.applicationDirectory.resolvePath(path);
			return (f.modificationDate.getTime());
		#elseif cpp
			return FileSystem.stat(path).mtime.getTime();
		#else
			throw new Error("this platform is not supported");
		#end
	}
	public static inline function fileCreatedTime(path:String):Float
	{
		path = formatPath(path);
		#if air
			var f:File = File.applicationDirectory.resolvePath(path);
			return (f.creationDate.getTime());
		#elseif cpp
			return FileSystem.stat(path).ctime.getTime();
		#else
			throw new Error("this platform is not supported");
		#end
	}
	
	public static function renameFileAt(directory:String, oldName:String, newName:String, ?posInfo:PosInfos):Bool
	{
		directory = formatPath(directory);
		
		if (!fileExistsAt(directory + oldName))
		{
			trace("FileTools.renameFileAt(): Warning -- Could not find file " + directory + oldName + positionString(posInfo));
			return false;
		}
		else if (fileExistsAt(directory + newName))
		{
			trace("FileTools.renameFileAt(): Warning -- Rename target " + directory + newName + " already exists" + positionString(posInfo));
			return false;
		}
		#if air
			var f:File = File.applicationDirectory.resolvePath(directory + oldName);
			f.moveTo(File.applicationDirectory.resolvePath(directory + newName), false);
			return true;
		#elseif cpp
			FileSystem.rename(directory + oldName, directory + newName);
			return true;
		#else
			throw new Error("this platform is not supported");
		#end
	}
	
	public static inline function deleteFileAt(path:String, ?posInfo:PosInfos):Bool
	{
		path = formatPath(path);
		if (!fileExistsAt(path))
			return false;
		#if air
			var f:File = File.applicationDirectory.resolvePath(path);
			f.deleteFile();
		#elseif cpp
			FileSystem.deleteFile(path);
		#else
			throw new Error("this platform is not supported");
		#end
		if (verboseSave)
			trace("Deleted file at " + path + positionString(posInfo));
		
		return true;
	}
	
	public static function immediateWriteTo(path:String, data:Dynamic, safeOverwrite:Bool = true, ?posInfo:PosInfos):Bool
	{
		path = formatPath(path);
		if (!fileExistsAt(path))
			safeOverwrite = false;
		#if air
			try
			{
				var f:File = new File(File.applicationDirectory.resolvePath(path).nativePath);
				f.canonicalize();
				/*if (!f.parent.exists)
					f.parent.createDirectory();*/
				if (safeOverwrite)
					f = new File(File.applicationDirectory.resolvePath(path + SAFE_OVERWRITE).nativePath);
				var fileStream:FileStream = new FileStream();
				fileStream.open(f, FileMode.WRITE);
				
				if (Std.is(data, ByteArray))
				{
					if (verboseSave) trace ("Writing to " + path + " as ByteArray" + positionString(posInfo));
					fileStream.writeBytes(cast data);
				}
				else if (Std.is(data, Bytes))
				{
					if (verboseSave) trace ("Writing to " + path + " as ByteArray(Bytes)"+ positionString(posInfo));
					fileStream.writeBytes(cast(data, Bytes).getData());
				}
				else if (Std.is(data, String))
				{
					if (verboseSave) trace ("Writing to " + path + " as String" + positionString(posInfo));
					fileStream.writeUTFBytes(data);
				}
				else if (Std.is(data, Xml))
				{
					if (verboseSave) trace ("Writing to " + path + " as String(Xml)" + positionString(posInfo));
					fileStream.writeUTFBytes(XMLTools.tidyXML(cast data));
				}
				else
				{
					if (verboseSave) trace ("Writing to " + path + " as serialized Object" + positionString(posInfo));
					fileStream.writeObject(data);
				}
				
				fileStream.close();
				if (safeOverwrite)
				{
					deleteFileAt(path);
					renameFileAt(directoryOf(path), filenameOf(path) + SAFE_OVERWRITE, filenameOf(path));
				}
				return true;
			}
			catch (e:SecurityError)
			{
				if (verboseError)
					trace("Error trying to save data to " + path + "\n" + e.getStackTrace());
				return false;
			}
		#elseif cpp
			var binary:Bool = !(Std.is(data, String) || Std.is(data, Xml));
			var fout:FileOutput = 
				if (safeOverwrite)
					File.write(path + SAFE_OVERWRITE, binary);
				else
					File.write(path, binary);
				
			if (binary)
			{
				if (Std.is(data, ByteArray))
				{
					if (verboseSave) trace ("Writing to " + path + " as Bytes(ByteArray)" + positionString(posInfo));
					fout.write(Bytes.ofData(cast data));					
				}
				else if (Std.is(data, Bytes))
				{
					if (verboseSave) trace ("Writing to " + path + " as ByteArray(Bytes)" + positionString(posInfo));
					fout.write(cast data);
				}
				else
				{
					if (verboseSave) trace ("Writing to " + path + " as serialized Object" + positionString(posInfo));
					var sz:Serializer = new Serializer();
					sz.serialize(data);
					fout.writeString(sz.toString());
				}
			}
			else
			{
				if (Std.is(data, String))
				{
					if (verboseSave) trace ("Writing to " + path + " as String"+ positionString(posInfo));
					fout.writeString(cast data);
				}
				else
				{
					if (verboseSave) trace ("Writing to " + path + " as String(Xml)"+ positionString(posInfo));
					fout.writeString(XMLTools.tidyXML(cast data));
				}
			}
			fout.close();
			if (safeOverwrite)
			{
				deleteFileAt(path, null);
				renameFileAt(directoryOf(path), filenameOf(path) + SAFE_OVERWRITE, filenameOf(path), null);
			}
			return true;
			
		#else
			throw new Error("this platform is not supported");
		#end
	}
	
	public static function immediateAppendTo(path:String, data:Dynamic, ?posInfo:PosInfos):Bool
	{
		path = formatPath(path);
		#if air
			try
			{
				var f:File = new File(File.applicationDirectory.resolvePath(path).nativePath);
				if (!f.parent.exists)
					f.parent.createDirectory();
				var fileStream:FileStream = new FileStream();
				fileStream.open(f, FileMode.APPEND);
				
				if (Std.is(data, ByteArray))
				{
					if (verboseSave) trace ("Writing to " + path + " as ByteArray"+ positionString(posInfo));
					fileStream.writeBytes(cast data);
				}
				else if (Std.is(data, Bytes))
				{
					if (verboseSave) trace ("Writing to " + path + " as ByteArray(Bytes)"+ positionString(posInfo));
					fileStream.writeBytes(cast(data, Bytes).getData());
				}
				else if (Std.is(data, String))
				{
					if (verboseSave) trace ("Writing to " + path + " as String"+ positionString(posInfo));
					fileStream.writeUTFBytes(data);
				}
				else if (Std.is(data, Xml))
				{
					if (verboseSave) trace ("Writing to " + path + " as String(Xml)"+ positionString(posInfo));
					fileStream.writeUTFBytes(XMLTools.tidyXML(cast data));
				}
				else
				{
					if (verboseSave) trace ("Writing to " + path + " as serialized Object"+ positionString(posInfo));
					fileStream.writeObject(data);
				}
				
				fileStream.close();
				return true;
			}
			catch (e:SecurityError)
			{
				if (verboseError)
					trace("Error trying to save data to " + path + "\n" + e.getStackTrace());
				return false;
			}
		#elseif cpp
			var binary:Bool = !(Std.is(data, String) || Std.is(data, Xml));
			var fout:FileOutput = File.append(path, binary);
			if (binary)
			{
				if (Std.is(data, ByteArray))
				{
					if (verboseSave) trace ("Writing to " + path + " as Bytes(ByteArray)"+ positionString(posInfo));
					fout.write(Bytes.ofData(cast data));					
				}
				else if (Std.is(data, Bytes))
				{
					if (verboseSave) trace ("Writing to " + path + " as ByteArray(Bytes)"+ positionString(posInfo));
					fout.write(Bytes.ofData(cast data));
				}
				else
				{
					if (verboseSave) trace ("Writing to " + path + " as serialized Object"+ positionString(posInfo));
					var sz:Serializer = new Serializer();
					sz.serialize(data);
					fout.writeString(sz.toString());
				}
			}
			else
			{
				if (Std.is(data, String))
				{
					if (verboseSave) trace ("Writing to " + path + " as String"+ positionString(posInfo));
					fout.writeString(cast data);
				}
				else
				{
					if (verboseSave) trace ("Writing to " + path + " as String(Xml)"+ positionString(posInfo));
					fout.writeString(XMLTools.tidyXML(cast data));
				}
			}
			fout.close();
			return true;
			
		#else
			throw new Error("this platform is not supported");
		#end
	}
	
	public static function asyncWriteTo(path:String, data:Dynamic, ?listener:Dynamic->Void, ?errorListener:Dynamic->Void, 
		safeOverwrite:Bool = true, ?posInfo:PosInfos):Void
	{
		path = formatPath(path);
		if (!fileExistsAt(path))
			safeOverwrite = false;
		#if air
			var fileStream:FileStream = null;
			var streamInfo:StreamInfo = null;
			for (si in sStreamInfo)
				if (si.path == path)
				{
					fileStream = si.stream;
					streamInfo = si;
					break;
				}
			if (fileStream != null)
			{
				sStreamInfo.remove(streamInfo);
				fileStream.close();
				if (streamInfo.errorFunction != null)
					streamInfo.errorFunction(new FileToolsEvent(FileToolsEvent.CANCEL, path, null));
			}
			var f:File = new File(File.applicationDirectory.resolvePath(path).nativePath);
			if (!f.parent.exists)
				f.parent.createDirectory();
			if (safeOverwrite)
				f = new File(File.applicationDirectory.resolvePath(path + SAFE_OVERWRITE).nativePath);
			fileStream = new FileStream();
			sStreamInfo.push(new StreamInfo(fileStream, path, listener, errorListener, safeOverwrite));
			fileStream.addEventListener(Event.COMPLETE, completionListener);
			fileStream.addEventListener(IOErrorEvent.IO_ERROR, completionListener);
			fileStream.openAsync(f, FileMode.WRITE);
			
			if (Std.is(data, ByteArray))
			{
				if (verboseSave) trace ("Writing to " + path + " as ByteArray"+ positionString(posInfo));
				fileStream.writeBytes(cast data);
			}
			else if (Std.is(data, Bytes))
			{
				if (verboseSave) trace ("Writing to " + path + " as ByteArray(Bytes)"+ positionString(posInfo));
				fileStream.writeBytes(cast(data, Bytes).getData());
			}
			else if (Std.is(data, String))
			{
				if (verboseSave) trace ("Writing to " + path + " as String"+ positionString(posInfo));
				fileStream.writeUTFBytes(cast data);
			}
			else if (Std.is(data, Xml))
			{
				if (verboseSave) trace ("Writing to " + path + " as String(Xml)"+ positionString(posInfo));
				fileStream.writeUTFBytes(XMLTools.tidyXML(cast data));
			}
			else
			{
				if (verboseSave) trace ("Writing to " + path + " as serialized Object"+ positionString(posInfo));
				fileStream.writeObject(data);
			}
		#else
			throw new Error("this platform is not supported");
		#end
	}
	
	public static function asyncAppendTo(path:String, data:Dynamic, ?listener:Dynamic->Void, ?errorListener:Dynamic->Void, ?posInfo:PosInfos):Void
	{
		path = formatPath(path);
		#if air
			var fileStream:FileStream = null;
			for (si in sStreamInfo)
				if (si.path == path)
				{
					fileStream = si.stream;
					break;
				}
			if (fileStream == null)
			{
				var f:File = new File(File.applicationDirectory.resolvePath(path).nativePath);
				if (!f.parent.exists)
					f.parent.createDirectory();
				fileStream = new FileStream();
				sStreamInfo.push(new StreamInfo(fileStream, path, listener, errorListener));
				fileStream.addEventListener(Event.COMPLETE, completionListener);
				fileStream.addEventListener(IOErrorEvent.IO_ERROR, completionListener);
				fileStream.openAsync(f, FileMode.APPEND);
			}
			
			if (Std.is(data, ByteArray))
			{
				if (verboseSave) trace ("Appending to " + path + " as ByteArray"+ positionString(posInfo));
				fileStream.writeBytes(cast data);
			}
			else if (Std.is(data, Bytes))
			{
				if (verboseSave) trace ("Appending to " + path + " as ByteArray(Bytes)"+ positionString(posInfo));
				fileStream.writeBytes(cast(data, Bytes).getData());
			}
			else if (Std.is(data, String))
			{
				if (verboseSave) trace ("Appending to " + path + " as String"+ positionString(posInfo));
				fileStream.writeUTFBytes(cast data);
			}
			else if (Std.is(data, Xml))
			{
				if (verboseSave) trace ("Appending to " + path + " as String(Xml)"+ positionString(posInfo));
				fileStream.writeUTFBytes(XMLTools.tidyXML(cast data));
			}
			else
			{
				if (verboseSave) trace ("Appending to " + path + " as serialized Object"+ positionString(posInfo));
				fileStream.writeObject(data);
			}
		#else
			throw new Error("this platform is not supported");
		#end
	}
	
	public static function immediateReadFrom(path:String, ?posInfo:PosInfos):ByteArray
	{
		path = formatPath(path);
		if (!fileExistsAt(path))
			throw new IOError("FileTools.immediateLoadBinaryDataFrom() -- Error: File " + path + " does not exist");
		
		#if air
			var f:File = new File(File.applicationDirectory.resolvePath(path).nativePath); // prevents some access issues
			var fs:FileStream = new FileStream();
			fs.open(f, FileMode.READ);
			var bytes:ByteArray = new ByteArray();
			fs.readBytes(bytes);
			fs.close();
			if (verboseLoad) 
				trace ("Successfully loaded " + path+ positionString(posInfo));
			return bytes;
		#elseif cpp
			var bytes:Bytes = sys.io.File.getBytes(path);
			if (verboseLoad) 
				trace ("Successfully loaded " + path+ positionString(posInfo));
			return bytes.getData();
		#else
			throw new Error("This platform is not supported");
		#end
	}
	
	public static function immediateReadAllFilesIn(path:String, ?extensions:Array<String>):Map<String, ByteArray>
	{
		path = formatPath(path);
		var map:Map<String, ByteArray> = new Map<String, ByteArray>();
		if (extensions != null) for (i in 0...extensions.length)
			extensions[i] = extensions[i].toLowerCase();
		#if air
			var file:File = File.applicationDirectory.resolvePath(path);
			var list:Array<File> = file.getDirectoryListing();
			for (f in list)
				if (!f.isDirectory && extensions != null && extensions.indexOf(f.extension.toLowerCase()) >= 0)
					map.set(f.name, immediateReadFrom(f.nativePath));
			return map;
		#else
			var list:Array<String> = FileSystem.readDirectory(path);
			for (s in list)
				if (!FileSystem.isDirectory(path + s) && extensions != null && extensions.indexOf(extensionOf(s)) >= 0)
					map.set(filenameOf(s), immediateReadFrom(path + s));
			return map;
		#end
	}
	
	public static function getAllFilesIn(path:String, recursive:Bool):Array<String>
	{
		var filePaths:Array<String> = [];
		path = formatPath(path);
		#if air
			var file:File = File.applicationDirectory.resolvePath(path);
			var list:Array<File> = file.getDirectoryListing();
			for (f in list)
			{
				if (!f.isDirectory)
					filePaths.push(f.nativePath);
				else if (recursive)
					for (s in getAllFilesIn(f.nativePath, true))
						filePaths.push(s);
			}
		#else
			var directoriesToSearch:Array<String> = [path];
			var filesFound:Array<String> = [];
			while (directoriesToSearch.length > 0)
			{
				var lookIn:String = directoriesToSearch.pop();
				var list:Array<String> = FileSystem.readDirectory(lookIn);
				for (s in list)
				{
					if (!FileSystem.isDirectory(lookIn + "/" + s))
						filePaths.push(FileSystem.fullPath(lookIn + "/" + s));
					else if (recursive)
						directoriesToSearch.push(FileSystem.fullPath(lookIn + "/" + s));
				}
			}
		#end
		return filePaths;
	}
	
	public static function asyncReadFrom(path:String, listener:Dynamic->Void, ?errorListener:Dynamic->Void):Void
	{	
		path = formatPath(path);
		if (sDisplayExtensions.indexOf(extensionOf(path)) >= 0)
		{
			var loader:Loader = new Loader();
			sStreamInfo.push(new StreamInfo(loader.contentLoaderInfo, path, listener, errorListener));
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, completionListener);
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, completionListener);
			loader.load(new URLRequest(path));
		}
		else
		{
			var loader:URLLoader = new URLLoader();
			sStreamInfo.push(new StreamInfo(loader, path, listener, errorListener));
			loader.addEventListener(Event.COMPLETE, completionListener);
			loader.addEventListener(IOErrorEvent.IO_ERROR, completionListener);
			loader.load(new URLRequest(path));
		}
	}
	
	private static function completionListener(e:Event):Void
	{
		var error:Bool = Std.is(e, IOErrorEvent);
		for (i in 0...sStreamInfo.length)
			if (sStreamInfo[i].stream == cast e.target)
			{
				var info:StreamInfo = sStreamInfo[i];
				var data:Dynamic = null;
				if (error)
				{
					if (verboseError)
						trace ("Failed to load " + sStreamInfo[i].path + " : " + cast(e, IOErrorEvent));
					if (info.errorFunction != null)
						info.errorFunction(new FileToolsEvent(FileToolsEvent.ERROR, info.path, data));
					return;
				}
				if (Std.is(info.stream, LoaderInfo))
				{
					data = cast(cast(info.stream, LoaderInfo).content, Bitmap).bitmapData;
					cast(info.stream, LoaderInfo).removeEventListener(Event.COMPLETE, completionListener);
					cast(info.stream, LoaderInfo).removeEventListener(IOErrorEvent.IO_ERROR, completionListener);
				}
				else if (Std.is(sStreamInfo[i].stream, URLLoader))
				{
					data = cast(info.stream, URLLoader).data;
					cast(info.stream, URLLoader).removeEventListener(Event.COMPLETE, completionListener);
					cast(info.stream, URLLoader).removeEventListener(IOErrorEvent.IO_ERROR, completionListener);
				}
				#if air
					else if (Std.is(info.stream, FileStream))
					{
						cast(info.stream, FileStream).close();
						cast(info.stream, FileStream).removeEventListener(Event.COMPLETE, completionListener);
						cast(info.stream, FileStream).removeEventListener(IOErrorEvent.IO_ERROR, completionListener);
						if (info.safeOverwrite)
						{
							deleteFileAt(info.path);
							renameFileAt(directoryOf(info.path), filenameOf(info.path) + SAFE_OVERWRITE, filenameOf(info.path));
						}
					}
				#end
				else
					throw new Error("Unexpected unrecognized loader type (This error should never happen!)");
				
				if (verboseLoad) 
					trace ("Successfully loaded " + info.path);
				
				info.successFunction(new FileToolsEvent(FileToolsEvent.LOADED, info.path, data));
				
				sStreamInfo.remove(info);
				return;
			}
		throw new Error("File IO data unexpectedly lost");
	}
	
	public static function browseAndWrite(?path:String, data:Dynamic,
		?listener:Dynamic->Void, ?cancelListener:Dynamic->Void, ?errorListener:Dynamic->Void, windowTitleText:String = "Save File"):Void
	{
		#if air
			if (sBrowsing != null)
			{
				trace ("Cannot browse for file while browser is open");
				return;
			}
			sBrowsing = {file:File.applicationDirectory, data:data, listener:listener, 
				cancelListener:cancelListener, errorListener:errorListener, write:true};
			if (path != null)
				sBrowsing.file = sBrowsing.file.resolvePath(path);
			
			sBrowsing.file.addEventListener(Event.SELECT, browseListener);
			sBrowsing.file.addEventListener(Event.CANCEL, browseListener);
			sBrowsing.file.addEventListener(IOErrorEvent.IO_ERROR, browseListener);
			
			sBrowsing.file.browseForSave(windowTitleText);
		#elseif cpp
			if (path == null)
				path = "";
				
			var target:String = Dialogs.saveFile(windowTitleText, "Name File", path);
			if (target != null)
				asyncWriteTo(target, data, listener, errorListener);
			else if (cancelListener != null)
				cancelListener(null);
		#end
	}
	
	public static function browseAndRead(?startPath:String, ?filters:Array<FileFilter>, allowMultiple:Bool = false,
		listener:Dynamic->Void, ?cancelListener:Dynamic->Void, ?errorListener:Dynamic->Void, windowTitleText:String = "Open File(s)"):Void
	{
		#if air
			if (sBrowsing != null)
			{
				trace ("Cannot browse for file while browser is open");
				return;
			}
			sBrowsing = {file:File.applicationDirectory, data:null, listener:listener, 
				cancelListener:cancelListener, errorListener:errorListener, write:false};
			if (startPath != null)
				sBrowsing.file = sBrowsing.file.resolvePath(startPath);
			
			sBrowsing.file.addEventListener(FileListEvent.SELECT_MULTIPLE, browseListener);
			sBrowsing.file.addEventListener(Event.SELECT, browseListener);
			sBrowsing.file.addEventListener(Event.CANCEL, browseListener);
			sBrowsing.file.addEventListener(IOErrorEvent.IO_ERROR, browseListener);
			if (allowMultiple)
				sBrowsing.file.browseForOpenMultiple(windowTitleText, filters);
			else
				sBrowsing.file.browseForOpen(windowTitleText, filters);
		#elseif cpp
			if (startPath == null)
				startPath = "";
				
			var filt:FILEFILTERS = null;
			if (filters != null)
			{
				var desc:Array<String> = [];
				var ext:Array<String> = [];
				for (f in filters)
				{
					desc.push(f.description);
					ext.push(f.extension);
				}	
				filt = {count : filters.length, descriptions : desc, extensions : ext};
			}
			var targets:Array<String> = Dialogs.openFile(windowTitleText, "Select File(s)", filt);
			
			if (targets != null && targets.length > 0)
				for (s in targets)
					asyncReadFrom(s, listener, errorListener);
			else if (cancelListener != null)
				cancelListener(null);
		#end
	}
	
	#if air
	private static function browseListener(e:Event):Void
	{
		sBrowsing.file.removeEventListener(FileListEvent.SELECT_MULTIPLE, browseListener);
		sBrowsing.file.removeEventListener(Event.SELECT, browseListener);
		sBrowsing.file.removeEventListener(Event.CANCEL, browseListener);
		sBrowsing.file.removeEventListener(IOErrorEvent.IO_ERROR, browseListener);
		if (e.type == FileListEvent.SELECT_MULTIPLE)
		{
			for (f in cast(e, FileListEvent).files)
				asyncReadFrom(f.nativePath, sBrowsing.listener, sBrowsing.errorListener);
		}
		if (e.type == Event.SELECT)
		{
			if (sBrowsing.write)
				asyncWriteTo(cast(e.target, File).nativePath, sBrowsing.data, sBrowsing.listener, sBrowsing.errorListener);
			else
				asyncReadFrom(cast(e.target, File).nativePath, sBrowsing.listener, sBrowsing.errorListener);
		}
		else if (e.type == Event.CANCEL)
		{
			if (sBrowsing.cancelListener != null)
				sBrowsing.cancelListener(new FileToolsEvent
					(FileToolsEvent.CANCEL, formatPath(sBrowsing.file.nativePath), null));
		}
		else
		{
			trace ("Error trying to browse for file: " + cast(e, IOErrorEvent).text);
			if (sBrowsing.errorListener != null)
				sBrowsing.errorListener(new FileToolsEvent
					(FileToolsEvent.ERROR, formatPath(sBrowsing.file.nativePath), null));
		}
		sBrowsing = null;
	}	
	#end
	
	
	public static function getRelativePathFromAbsolutePath(source:String, path:String):String
	{
		source = formatPath(source);
		path = formatPath(path);
		
		var sourceParts:Array<String> = source.split("\\");
		var pathParts:Array<String> = path.split("\\");
		while (sourceParts[0] == pathParts[0])
		{
			sourceParts.shift();
			pathParts.shift();
		}
		path = "";
		for (i in 0...sourceParts.length)
			if (sourceParts[i] != "")
				path += "..\\";
		for (i in 0...pathParts.length)
		{
			path += pathParts[i];
			if (i != pathParts.length - 1 || pathParts[i].indexOf(".") < 0)
				path += "\\";
		}
		return path;
	}
	public static inline function getAbsolutePathFromRelativePath(source:String, path:String):String
	{
		source = formatPath(source);
		path = formatPath(path);
		
		var sourceParts:Array<String> = source.split("\\");
		var pathParts:Array<String> = path.split("\\");
		while (pathParts[0] == "..")
		{
			pathParts.shift();
			sourceParts.pop();
		}
		path = "";
		for (s in sourceParts)
			path += s + "\\";
		for (i in 0...pathParts.length)
		{
			path += pathParts[i];
			if (i != pathParts.length - 1 || pathParts[i].indexOf(".") < 0)
				path += "\\";
		}
		return path;
	}
	public static inline function directoryOf(path:String):String
	{
		path = formatPath(path);
		var index:Int = path.lastIndexOf("\\");
		if (index < 0)
			return "";
		else
			return path.substring(0, index + 1);
	}
	
	public static inline function filenameOf(path:String, includeExtension:Bool = true):String
	{
		path = formatPath(path);
		var index:Int = path.lastIndexOf("\\");
		if (index > 0)
			path = path.substr(index + 1);
		if (!includeExtension)
		{
			index = path.lastIndexOf(".");
			if (index > 0)
				path = path.substring(0, index);
		}
		return path;
	}
	
	public static inline function extensionOf(path:String):String
	{
		path = formatPath(path);
		var index:Int = path.lastIndexOf(".");
		if (index < 0 || index < path.lastIndexOf("\\"))
			return "";
		else
			return path.substr(index + 1);
	}
	
	public static inline function isDisplayExtension(ext:String):Bool
	{
		ext = ext.toLowerCase();
		return (sDisplayExtensions.indexOf(ext) >= 0);
	}
	
	public static inline function formatPath(path:String):String
	{
		path = path.toLowerCase();
		var arr:Array<String> = path.split("/");
		path = "";
		for (i in 0...arr.length)
		{
			path += arr[i];
			if (i < arr.length - 1)
				path += "\\";
		}
		return path;
	}
	
	public static inline function comparePaths(a:String, b:String):Bool
	{
		return (formatPath(a) == formatPath(b));
	}
	
	private static function positionString(posInfo:PosInfos):String
	{
		if (posInfo == null)
			return "";
		return " - called from " + posInfo.className + "." + posInfo.methodName;
	}
}

private class StreamInfo
{
	public var stream:Dynamic;
	public var path:String;
	public var successFunction:Dynamic->Void;
	public var errorFunction:Dynamic->Void;
	public var safeOverwrite:Bool;
	
	public function new(stream:Dynamic, path:String, ?successFunction:Dynamic->Void, ?errorFunction:Dynamic->Void, safeOverwrite:Bool = false)
	{
		this.stream = stream;
		this.path = path;
		this.successFunction = successFunction;
		this.errorFunction = errorFunction;
		this.safeOverwrite = safeOverwrite;
	}
	
}