package com.problemmachine.tools.file;
import flash.events.Event;

class FileToolsEvent extends Event
{
	public static inline var LOADED:String = "file loaded";
	public static inline var ERROR:String = "load failed";
	public static inline var CANCEL:String = "cancel browse";
	public var path(default, null):String;
	public var data(default, null):Dynamic;

	public function new(type:String, path:String, ?data:Dynamic, bubbles:Bool = false, cancelable:Bool = false) 
	{
		super(type, bubbles, cancelable);
		this.data = data;
		this.path = path;
	}
	
}