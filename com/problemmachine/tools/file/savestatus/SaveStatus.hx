package com.problemmachine.tools.file.savestatus;

enum SaveStatus 
{
	Saved;
	Modified; 
	NoFile;
}