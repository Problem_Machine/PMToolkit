package com.problemmachine.tools.math;

class IntMath
{
	static public inline var HIGHEST:Int = 2147483647;
	static public inline var LOWEST:Int = -2147483647;
	static public inline var U_HIGHEST:UInt = 0xFFFFFFFF;
	
	private function new() { }
	
	static public inline function abs(val:Int):Int
	{
		if (val < 0)
			return -val;
		else
			return val;
	}
	
	static public inline function min(a:Int, b:Int):Int
	{
		if (a < b)
			return a;
		else
			return b;
	}
	
	static public inline function max(a:Int, b:Int):Int
	{
		if (a > b)
			return a;
		else
			return b;
	}
	
}