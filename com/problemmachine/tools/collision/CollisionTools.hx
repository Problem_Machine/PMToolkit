package com.problemmachine.tools.collision;
import flash.geom.Point;
import flash.geom.Rectangle;

class CollisionTools
{
	static public inline function testRectRect(x1:Float, y1:Float, w1:Float, h1:Float, x2:Float, y2:Float, w2:Float, h2:Float, inclusive:Bool = true):Bool
	{
		if (inclusive)
			return !(	x1 > x2 + w2
				|| 		x1 + w1 < x2
				|| 		y1 > y2 + h2
				|| 		y1 + h1 < y2);
		else
			return !(	x1 >= x2 + w2
				|| 		x1 + w1 <= x2
				|| 		y1 >= y2 + h2
				|| 		y1 + h1 <= y2);
	}
	static public inline function testRectRectObj(rect1:Rectangle, rect2:Rectangle, inclusive:Bool = true):Bool
	{
		return testRectRect(rect1.x, rect1.y, rect1.width, rect1.height, rect2.x, rect2.y, rect2.width, rect2.height, inclusive);
	}
	
	static public inline function testRectPoint(x1:Float, y1:Float, w:Float, h:Float, x2:Float, y2:Float):Bool
	{
		return !(	x2 > x1 + w
			|| 		x2 < x1
			|| 		y2 > y1 + h
			|| 		y2 < y1);
	}
	
	static public function testLineLine(ax1:Float, ay1:Float, ax2:Float, ay2:Float, bx1:Float, by1:Float, bx2:Float, by2:Float):Point
	{
		if ((ax1 == bx1 && ay1 == by1) || (ax1 == bx2 && ay1 == by2))
			return new Point(ax1, ay1);
		if ((ax2 == bx1 && ay2 == by1) || (ax2 == bx2 && ay2 == by2))
			return new Point(ax2, ay2);
		
		var d:Float = ((ax1 - ax2) * (by1 - by2)) - ((ay1 - ay2) * (bx1 - bx2));
		
		// if lines are colinear
		if (yIntercept(ax1, ay1, ax2, ay2) == yIntercept(bx1, by1, bx2, by2))
		{// there's probably a better way to do this but whatever
			return new Point((ax1 + ax2 + bx1 + bx2) / 4, (ay1 + ay2 + by1 + by2) / 4);
		}
		
		// If d is zero, and lines are not colinear there is no intersection
		if (d == 0) 
			return null;

		// Get the x and y
		var pre:Float = (ax1 * ay2 - ay1 * ax2);
		var post:Float = (bx1 * by2 - by1 * bx2);
		
		var x:Float = ( pre * (bx1 - bx2) - (ax1 - ax2) * post ) / d; 
		var y:Float = ( pre * (by1 - by2) - (ay1 - ay2) * post ) / d;
		
		// Check if the x and y coordinates are within both lines
		if ( x < Math.min(ax1, ax2) || x > Math.max(ax1, ax2) ||
			x < Math.min(bx1, bx2) || x > Math.max(bx1, bx2) ) 
			return null;
		if ( y < Math.min(ay1, ay2) || y > Math.max(ay1, ay2) ||
			y < Math.min(by1, by2) || y > Math.max(by1, by2) ) 
			return null;
	 
		// Return the point of intersection
		return new Point(x, y);
	}
	
	static public inline function testLineCircle(ax1:Float, ay1:Float, ax2:Float, ay2:Float, bx:Float, by:Float, br:Float):Point
	{
		var p:Point = closestPointOnLine(ax1, ay1, ax2, ay2, bx, by);
		if ((p.x - bx) * (p.x - bx) + (p.y - by) * (p.y - by) <= br * br)
			return p;
		else 
			return null;
	}
	
	static public inline function testRectCircle(ax:Float, ay:Float, aw:Float, ah:Float, bx:Float, by:Float, br:Float):Point
	{
		if (testRectPoint(ax, ay, aw, ah, bx, by))
			return new Point(bx, by);
		else
		{
			var distX:Float = Math.abs(bx - ax + aw * 0.5);
			var distY:Float = Math.abs(by - ay + ah * 0.5);
			
			if (distX > (aw * 0.5 + br) || distY > (ah * 0.5 + br))
				return null;
				
			if (distX <= (aw * 0.5)	|| distY <= (ah * 0.5))
				return new Point(bx, by);
			
			var cDistSq:Float = (distX - aw * 0.5) * (distX - aw * 0.5) + (distY - ah * 0.5) * (distY - ah * 0.5);
			
			if (cDistSq <= br * br)
			{
				if (bx <= ax)
				{
					if (by <= ay)
						return new Point(ax, ay);
					else if (by >= ay + ah)
						return new Point(ax, ay + ah);
					else
						return new Point(ax, by);
				}
				else
				{
					if (by <= ay)
						return new Point(ax + aw, ay);
					else if (by >= ay + ah)
						return new Point(ax + aw, ay + ah);
					else
						return new Point(ax + aw, by);
				}
			}
			else
				return null;
		}
	}
	
	static private inline function yIntercept(ax:Float, ay:Float, bx:Float, by:Float):Float
	{
		if (ax == bx)
			return Math.NaN;
		else
			return (ay - ((ay - by) / (ax - bx) * ax));
	}
	
	static private inline function closestPointOnLine(x1:Float, y1:Float, x2:Float, y2:Float, px:Float, py:Float):Point
	{ 
		var A1:Float = y2 - y1; 
		var B1:Float = x1 - x2; 
		var C1:Float = (y2 - y1) * x1 + (x1 - x2) * y1; 
		var C2:Float = (-B1 * px) + (A1 * py); 
		var det:Float = (A1 * A1) - (-B1 * B1); 
		if (det != 0)
			return new Point(((A1 * C1 - B1 * C2) / det), ((A1 * C2 - -B1 * C1) / det)); 
		else
			return new Point(px, py); 
	}
}