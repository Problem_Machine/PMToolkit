package com.problemmachine.tools.point;
import flash.geom.Point;

class PointTools
{
	static public function rotateDegrees(point:Point, degrees:Float, ?pivot:Point):Point
	{
		return rotateRadians(point, (degrees / 180) * Math.PI, pivot);
	}
	
	static public function rotateRadians(point:Point, radians:Float, ?pivot:Point):Point
	{
		var sin:Float = Math.sin(radians);
		var cos:Float = Math.cos(radians);
		
		var p = new Point();
		if (pivot != null)
		{
			p.x = (point.x - pivot.x) * cos - (point.y - pivot.y) * sin;
			p.y = (point.x - pivot.x) * sin + (point.y - pivot.y) * cos;
			p.x += pivot.x;
			p.y += pivot.y;
		}
		else
		{
			p.x = point.x * cos - point.y * sin;
			p.y = point.x * sin + point.y * cos;
		}
		
		return p;
	}
	
	static public inline function rotate(point:Point, rotations:Float, ?pivot:Point):Point
	{
		return rotateRadians(point, rotations * 2 * Math.PI, pivot);
	}
	
	static public inline function yIntercept(a:Point, b:Point):Float
	{
		if (a.x == b.x)
			return Math.NaN;
		else
			return (b.y - ((b.y - a.y) / (b.x - a.x)) * b.x);
	}
	
	static public inline function dot(a:Point, b:Point, normalize:Bool = false):Float
	{
		if (normalize)
		{
			a = a.clone();
			b = b.clone();
			a.normalize(1);
			b.normalize(1);
		}
		return a.x * b.x + a.y * b.y;
	}
	
	static public inline function cross(a:Point, b:Point):Float
	{
		return (a.x * b.y) - (a.y * b.x);
	}
	
}