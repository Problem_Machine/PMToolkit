package com.problemmachine.tools.openfl.tile;
import com.problemmachine.bitmap.bitmapmanager.BitmapManager;
import com.problemmachine.bitmap.bitmapmanager.BitmapManagerEvent;
import com.problemmachine.tools.collision.CollisionTools;
import com.problemmachine.tools.openfl.tile.event.TileManagerEvent;
import flash.display.BitmapData;
import flash.events.EventDispatcher;
import flash.geom.Point;
import flash.geom.Rectangle;
import openfl.display.Tileset;

class TileCollection extends EventDispatcher
{
	static private inline var MAX_WIDTH:Int = 8192;
	static private inline var MAX_HEIGHT:Int = 8192;
	static private inline var REPACK_THRESHOLD:Float = 0.8;
	static private inline var REPACK_PADDING:Float = 1.2;
	
	public var tileset:Tileset;
	private var mTileReferences:Array<TileReference>;
	private var mPreviousEfficiency:Float;
	
	public function new()
	{
		super();
		tileset = new Tileset(new BitmapData(1, 1, true, 0x00000000));
		mTileReferences = [];
		mPreviousEfficiency = -1;
	}
	
	public function addTilesByPath(path:String, rects:Array<Rectangle>):TileReference
	{
		var tr:TileReference = integrateNewTileData(BitmapManager.immediateLoad(path, this), rects, path);
		mTileReferences.push(tr);
		BitmapManager.startListeningForCompletion(path, tileLoadListener);
		return tr;
	}
	
	@:access(com.problemmachine.tools.openfl.tile.TileReference.mPath)
	private function tileLoadListener(e:BitmapManagerEvent):Void
	{
		var path:String = e.type.toLowerCase();
		for (r in mTileReferences)
			if (r.mPath.toLowerCase() == path)
			{
				integrateUpdatedTileData(r, BitmapManager.request(e.type));
				break;
			}
	}
	
	public function addTilesByData(data:BitmapData, rects:Array<Rectangle>):TileReference
	{
		var tr:TileReference = integrateNewTileData(data, rects, null);
		mTileReferences.push(tr);
		return tr;
	}
	
	private function integrateNewTileData(data:BitmapData, rects:Array<Rectangle>, path:String):TileReference
	{
		var rect:Rectangle = new Rectangle();
		for (r in rects)
		{
			rect.left = Math.min(r.left, rect.left);
			rect.right = Math.max(r.right, rect.right);
			rect.top = Math.min(r.top, rect.top);
			rect.bottom = Math.max(r.bottom, rect.bottom);
		}
		var target:Point = findUnoccupiedSpace(Math.ceil(rect.width), Math.ceil(rect.height));
		if (target == null)
			target = expandToFit(Math.ceil(rect.width), Math.ceil(rect.height));
		tileset.bitmapData.copyPixels(data, rect, target);
		var newRects:Array<Int> = [];
		for (r in rects)
		{
			var newRectangle:Rectangle = r.clone();
			newRectangle.x = r.x - rect.left + target.x;
			newRectangle.y = r.y - rect.top + target.y;
			newRects.push(tileset.addRect(newRectangle));
		}
		rect.x = target.x;
		rect.y = target.y;
		var tr:TileReference = new TileReference(tileset, rect, newRects, new Point(rect.left, rect.top), path);
			
		if (getEfficiency() < mPreviousEfficiency * REPACK_THRESHOLD)
			repackTiles();
		
		return tr;
	}
	
	@:access(openfl.display.Tileset.__data)
	@:access(openfl.display.Tileset.TileData)
	public function removeTiles(reference:TileReference, removeTileReference:Bool = true):Void
	{
		tileset.bitmapData.fillRect(reference.bitmapRect, 0x00000000);
		var ids = reference.tileIDs.copy();
		ids.sort(function(a:Int, b:Int):Int {return b - a; });
		for (i in ids)
		{
			tileset.rectData.splice(i * 4, 4);
			tileset.__data.splice(i, 1);
		}
		if (removeTileReference)
			mTileReferences.remove(reference);
	}
	
	public function updateTilesByPath(reference:TileReference, path:String):Void
	{
		integrateUpdatedTileData(reference, BitmapManager.immediateLoad(path, true));
	}
	
	public function updateTilesByData(reference:TileReference, data:BitmapData):Void
	{
		integrateUpdatedTileData(reference, data);
	}
	
	@:access(com.problemmachine.tools.openfl.tile.TileReference.mBitmapOffset)
	public function integrateUpdatedTileData(reference:TileReference, data:BitmapData):Void
	{
		var target:Point = new Point(reference.bitmapRect.x, reference.bitmapRect.y);
		tileset.bitmapData.fillRect(reference.bitmapRect, 0x00000000);
		var rect:Rectangle = reference.bitmapRect.clone();
		rect.x = reference.mBitmapOffset.x;
		rect.y = reference.mBitmapOffset.y;
		tileset.bitmapData.copyPixels(data, rect, target);
	}
	
	private function findUnoccupiedSpace(width:Int, height:Int):Point
	{
		var target:Point = null;
		for (i in 0...mTileReferences.length)
		{
			var r:TileReference = mTileReferences[i];
			var rightTest:Rectangle = new Rectangle(r.bitmapRect.right, r.bitmapRect.top, width, height);
			var bottomTest:Rectangle = new Rectangle(r.bitmapRect.left, r.bitmapRect.bottom, width, height);
			for (tr in mTileReferences)
			{
				if (tr == r)
					continue;
				if (!rightTest.intersects(tr.bitmapRect) && 
						rightTest.right < tileset.bitmapData.width && rightTest.bottom < tileset.bitmapData.height)
					target = new Point(rightTest.x, rightTest.y);
				if (!bottomTest.intersects(tr.bitmapRect) && 
						bottomTest.right < tileset.bitmapData.width && bottomTest.bottom < tileset.bitmapData.height)
					target = new Point(bottomTest.x, bottomTest.y);
			}
			if (target != null)
				break;
		}
		return target;
	}
	
	private function expandToFit(width:Int, height:Int):Point
	{
		// NOTE: Perhaps add padding when expanding the bitmap to cut down on expensive memory operations
		var candidates:Array<Point> = [];
		if (mTileReferences.length == 0)
			candidates.push(new Point());
		for (i in 0...mTileReferences.length)
		{
			var r:TileReference = mTileReferences[i];
			var rightTest:Rectangle = new Rectangle(r.bitmapRect.right, r.bitmapRect.top, width, height);
			var bottomTest:Rectangle = new Rectangle(r.bitmapRect.left, r.bitmapRect.bottom, width, height);
			var rInvalid:Bool = false;
			var bInvalid:Bool = false;
			for (tr in mTileReferences)
			{
				if (tr == r)
					continue;
				if (!rInvalid && rightTest.intersects(tr.bitmapRect))
					rInvalid = true;
				if (!bInvalid && bottomTest.intersects(tr.bitmapRect))
					bInvalid = true;
				if (rInvalid && bInvalid)
					break;
			}
			if (!rInvalid)
				candidates.push(new Point(rightTest.x, rightTest.y));
			if (!bInvalid)
				candidates.push(new Point(bottomTest.x, bottomTest.y));
		}
		var target:Point = null;
		var expansionArea:Int = 0xFFFFFF;
		for (p in candidates)
		{
			var a:Float = (Math.max(tileset.bitmapData.width, p.x + width) *
						Math.max(tileset.bitmapData.height, p.y + height)) - 
						(tileset.bitmapData.width * tileset.bitmapData.height);
			if (a < expansionArea)
			{
				expansionArea = Math.ceil(a);
				target = p;
			}
		}
		var newBitmapData:BitmapData = new BitmapData(Math.ceil(Math.max(tileset.bitmapData.width, target.x + width)),
						Math.ceil(Math.max(tileset.bitmapData.height, target.y + height)), true, 0x00000000);
		newBitmapData.copyPixels(tileset.bitmapData, tileset.bitmapData.rect, new Point());
		tileset.bitmapData.dispose();
		tileset.bitmapData = newBitmapData;
		
		return target;
	}
	
	private function repackTiles():Void
	{
		var refs:Array<TileReference> = mTileReferences.copy();
		refs.sort(function(a:TileReference, b:TileReference):Int
			{
				if (a.bitmapRect.width * a.bitmapRect.height > b.bitmapRect.width * b.bitmapRect.height)
					return 1;
				else if (a.bitmapRect.width * a.bitmapRect.height < b.bitmapRect.width * b.bitmapRect.height)
					return -1;
				else
					return 0;
			});
		var newRects:Array<Rectangle> = [];
		for (tr in refs)
			newRects.push(tr.bitmapRect.clone());
		
		newRects[0].x = 0;
		newRects[0].y = 0;
		var newWidth:Int = Math.floor(newRects[0].width);
		var newHeight:Int = Math.floor(newRects[0].height);
		
		for (i in 1...newRects.length)
		{
			var r:Rectangle = newRects[i];
			var candidates:Array<Point> = [];
			//	find which spots to the immediate right/bottom of the block are UNOCCUPIED
			for (j in 0...i)
			{
				var r2:Rectangle = newRects[j];
				var rightTest:Rectangle = new Rectangle(r2.right, r2.top, r.width, r.height);
				var bottomTest:Rectangle = new Rectangle(r2.left, r2.bottom, r.width, r.height);
				
				var rInvalid:Bool = false;
				var bInvalid:Bool = false;
				for (k in 0...j)
				{
					if (!rInvalid && rightTest.intersects(newRects[k]))
						rInvalid = true;
					if (!bInvalid && bottomTest.intersects(newRects[k]))
						bInvalid = true;
					if (rInvalid && bInvalid)
						break;
				}
				if (!rInvalid)
					candidates.push(new Point(rightTest.x, rightTest.y));
				if (!bInvalid)
					candidates.push(new Point(bottomTest.x, bottomTest.y));
			}
			
			var expWidth:Int = 0xFFFFFF;
			var expHeight:Int = 0xFFFFFF;
			for (p in candidates)
			{
				var confirm:Bool = false;
				var w = Math.max(newWidth, p.x + r.width);
				var h = Math.max(newHeight, p.y + r.height);
				var a = w * h;
				if (a < expWidth * expHeight)
					confirm = true;
				else if (a == expWidth * expHeight)
				{
					if (w > h && newHeight > newWidth)
						confirm = true;
					else if (h > w && newWidth > newHeight)
						confirm = true;
				}
				
				if (confirm)
				{
					expWidth = Math.ceil(w);
					expHeight = Math.ceil(h);
					r.x = p.x;
					r.y = p.y;
				}
			}
			newWidth = expWidth;
			newHeight = expHeight;
		}
		
		var newBitmapData:BitmapData = new BitmapData(Math.ceil(newWidth * REPACK_PADDING), 
			Math.ceil(newHeight * REPACK_PADDING), true, 0x00000000);
		for (i in 0...mTileReferences.length)
		{
			newBitmapData.copyPixels(tileset.bitmapData, mTileReferences[i].bitmapRect, 
				new Point(newRects[i].x, newRects[i].y));
			mTileReferences[i].bitmapRect.copyFrom(newRects[i]);
		}
		tileset.bitmapData.dispose();
		tileset.bitmapData = newBitmapData;
		mPreviousEfficiency = getEfficiency();
	}
	
	private function getEfficiency():Float
	{
		if (mTileReferences.length == 0)
			return 1;
		var area:Float = 0;
		for (tr in mTileReferences)
			area += tr.bitmapRect.width * tr.bitmapRect.height;
		return area / (tileset.bitmapData.width * tileset.bitmapData.height);
	}
}