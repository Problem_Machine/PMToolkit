package com.problemmachine.tools.display;
import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.Sprite;
import flash.geom.Rectangle;
import flash.Lib;

class DisplayTools
{
	public static function getRealDimensions(obj:DisplayObject, reference:DisplayObjectContainer):Rectangle
	{
		if (obj.mask == null)
			return obj.getBounds(reference);
		else
			return obj.getBounds(reference).intersection(obj.mask.getBounds(reference));
		/*if (reference == null)
			reference = obj.parent;
		if (reference == null)
			reference = cast obj;
		var tmp:Sprite = null;
		if (Std.is(obj, DisplayObjectContainer))
		{
			var doc:DisplayObjectContainer = cast obj;
			tmp = new Sprite();
			while(doc.numChildren > 0)
				tmp.addChild(doc.getChildAt(0));
		}
		var rect:Rectangle = null;
		if (obj.width != 0 && obj.height != 0)
			rect = obj.getBounds(reference);	
		
		if (tmp != null)
		{
			var doc:DisplayObjectContainer = cast obj;
			while(tmp.numChildren > 0)
			{
				var c:DisplayObject = tmp.getChildAt(0);
				doc.addChild(c);
				var r:Rectangle = getRealDimensions(c, reference);
				if (rect == null)
					rect = r
				else if (r != null)
				{
					rect.left = Math.min(rect.left, r.left);
					rect.top = Math.min(rect.top, r.top);
					rect.right = Math.max(rect.right, r.right);
					rect.bottom = Math.max(rect.bottom, r.bottom);
				}
			}
		}
		
		if (rect != null && obj.mask != null && obj.mask.width != 0 && obj.mask.height != 0)
		{
			var mr:Rectangle = obj.mask.getBounds(reference);
			rect.left = Math.max(mr.left, rect.left);
			rect.top =  Math.max(mr.top, rect.top);
			rect.bottom = Math.min(mr.bottom, rect.bottom);
			rect.right = Math.min(mr.right, rect.right);
		}
		return rect;*/
	}
	
}