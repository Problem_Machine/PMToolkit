package com.problemmachine.tools.bitmap;
import com.problemmachine.tools.math.IntMath;
import flash.display.BitmapData;
import flash.geom.Point;
import flash.geom.Rectangle;


class BitmapTools
{
	static private var sTempBitmap:BitmapData = new BitmapData(1, 1, true, 0x00000000);
	
	public static function areBitmapsIntersecting(bmp1:BitmapData, ?clip1:Rectangle, bmp2:BitmapData, ?clip2:Rectangle, offset:Point, 
		alphaThreshold:UInt = 0x100, skipX:Int = 4, skipY:Int = 4):Bool
	{
		if (clip1 == null)
			clip1 = new Rectangle(0, 0, bmp1.width, bmp1.height);
		if (clip2 == null)
			clip2 = new Rectangle(0, 0, bmp2.width, bmp2.height);
		if (offset.x > clip1.width || offset.y > clip1.height || offset.x < -clip2.width || offset.y < -clip2.height)
			return false;
		var rect:Rectangle = clip1.intersection(new Rectangle(offset.x + clip2.x, offset.y + clip2.y, clip2.width, clip2.height));
		var o1x:Int = Math.floor(offset.x + clip1.x);
		var o1y:Int = Math.floor(offset.y + clip1.y);
		var o2x:Int = Math.floor(clip2.x);
		var o2y:Int = Math.floor(clip2.y);
		for (x in 0...Math.ceil(rect.width / skipX))
			for (y in 0...Math.ceil(rect.height / skipY))
			{
				var p:UInt = (bmp1.getPixel32(o1x + (skipX * x), o1y + (skipY * y)) >>> 24) + 
							(bmp2.getPixel32(o2x + (skipX * x), o2y + (skipY * y)) >>> 24);
				if (p > alphaThreshold)
					return true;
			}
		return false;
	}
	
	public static function isRectIntersectingBitmap(bmp:BitmapData, ?clip:Rectangle, rect:Rectangle, 
		alphaThreshold:UInt = 0x05, skipX:Int = 4, skipY:Int = 4)
	{
		if (clip == null)
			clip = new Rectangle(0, 0, bmp.width, bmp.height);
		rect = rect.clone();
		var r2:Rectangle = clip.clone();
		
		rect.left = Math.min(clip.width, Math.max(0, rect.left));
		rect.right = Math.max(0, Math.min(clip.width, rect.right));
		rect.top = Math.min(clip.height, Math.max(0, rect.top));
		rect.bottom = Math.max(0, Math.min(clip.height, rect.bottom));
		for (x in 0...Math.ceil(rect.width / skipX))
			for (y in 0...Math.ceil(rect.height / skipY))
			{
				var p:UInt = bmp.getPixel32(Math.floor(rect.x) + x * skipX, Math.floor(rect.y) + y * skipY);
				p = (p >>> 24);
				if (p > alphaThreshold)
					return true;
			}
		return false;
	}
	
	public static function loopScroll(bmp:BitmapData, x:Int, y:Int):Void
	{
		if (sTempBitmap.width < bmp.width || sTempBitmap.height < bmp.height)
		{
			sTempBitmap.dispose();
			sTempBitmap = new BitmapData(bmp.width, bmp.height, true, 0x00000000);
		}
		else
		{
			sTempBitmap.fillRect(new Rectangle(0, 0, bmp.width, bmp.height), 0x00000000);
		}
		x %= bmp.width;
		y %= bmp.height;
		if (x > 0)
		{
			sTempBitmap.copyPixels(bmp, new Rectangle(bmp.width - x, 0, x, bmp.height), new Point(0, 0));
			bmp.scroll(x, 0);
			bmp.copyPixels(sTempBitmap, new Rectangle(0, 0, x, bmp.height), new Point(0, 0));
		}
		else if (x < 0)
		{
			sTempBitmap.copyPixels(bmp, new Rectangle(0, 0, -x, bmp.height), new Point(bmp.width + x, 0));
			bmp.scroll(x, 0);
			bmp.copyPixels(sTempBitmap, new Rectangle(bmp.width + x, 0, -x, bmp.height), new Point(bmp.width + x, 0));
		}
		if (y > 0)
		{
			sTempBitmap.copyPixels(bmp, new Rectangle(0, bmp.height - y, bmp.width, y), new Point(0, 0));
			bmp.scroll(0, y);
			bmp.copyPixels(sTempBitmap, new Rectangle(0, 0, bmp.width, y), new Point(0, 0));
		}
		else if (y < 0)
		{
			sTempBitmap.copyPixels(bmp, new Rectangle(0, 0, bmp.width, -y), new Point(0, bmp.height + y));
			bmp.scroll(0, y);
			bmp.copyPixels(sTempBitmap, new Rectangle(0, bmp.height + y, bmp.width, - y), new Point(0, bmp.height + y));
		}
	}
}