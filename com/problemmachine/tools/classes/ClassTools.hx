package com.problemmachine.tools.classes;
import flash.errors.Error;

class ClassTools
{
	public static function getClassName(cls:Class<Dynamic>, addSpaces:Bool = false):String
	{
		var name:String = Type.getClassName(cls);
		return formatClassName(name, addSpaces);
	}
	
	public static function formatClassName(name:String, addSpaces:Bool = true):String
	{
		var arr:Array<String> = name.split(".");
		var last:String = arr[arr.length - 1];
		if (addSpaces)
		{
			name = "";
			for (i in 0...last.length)
			{
				if (i != 0 && last.charAt(i) == last.charAt(i).toUpperCase())
					name += " ";
				name += last.charAt(i);
			}
			return name;
		}
		else
			return last;
	}	
	
	public static function isSubclassOf(test:Class<Dynamic>, superCandidate:Class<Dynamic>):Bool
	{
		while (test != null)
		{
			test = Type.getSuperClass(test);
			if (test == superCandidate)
				return true;
		}
		return false;
	}
}