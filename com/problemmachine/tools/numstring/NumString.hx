package com.problemmachine.tools.numstring;

class NumString
{
	public static inline function toFixed(val:Float, precision:Int):String
	{
		precision = Std.int(Math.max(0, precision));
		var pow:Int = Std.int(Math.pow(10, precision));
		var left:Int = Math.floor(Math.round(val * pow) / pow);
		var right:String = Std.string(Math.round((val - left) * pow));
		while (right.length < precision)
			right += "0";
		return (Std.string(left) + "." + right);
	}
	
	public static inline function toPercent(val:Float, precision:Int = 0):String
	{
		precision = Std.int(Math.max(0, precision));
		var pow:Int = Std.int(Math.pow(10, precision));
		var left:Int = Math.floor(Math.round(val * 100 * pow) / pow);
		var s:String = Std.string(left); 
		if (precision > 0)
		{
			var right:String = Std.string(Math.round((val * 100 - left) * Math.pow(10, precision)));
			while (right.length < precision)
				right += "0";
			s += "." + right;
		}
		s += "%";
		return s;
	}
	
	public static function toLeadingZero(val:Float, numDigits:Int = 0):String
	{
		var d:Int = 0;
		var count:Int = Math.ceil(Math.abs(val));
		while (count != 0)
		{
			count = Math.floor(count / 10);
			++d;
		}
		var s:String = Std.string(val);
		for (i in 0...(numDigits - d))
			s = "0" + s;
		return s;
	}
	
	public static function toMinutesSecondsMilliseconds(ms:Int):String
	{
		var min:Int = Math.floor(ms / (60 * 1000));
		var sec:Int = Math.floor(ms / 1000) % 60;
		ms = Math.floor(ms) % 1000;
		var s:String = Std.string(min) + ":";
		if (sec < 10)
			s += "0";
		s += sec + ":";
		if (ms < 100)
			s += "0";
		if (ms < 10)
			s += "0";
		s += ms;
		return s;
	}
}