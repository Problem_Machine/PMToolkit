package com.problemmachine.tools.text;
import flash.text.TextField;
import flash.text.TextFormat;

class TextTools
{
	static public function scaleTextToFit(tf:TextField):Void
	{
		var f:TextFormat = tf.defaultTextFormat;
		f.size = Math.floor(tf.width);
		tf.setTextFormat(f);
		 
		while ((tf.textWidth + 4 > tf.width || tf.textHeight + 4 > tf.height) && f.size >= 1)
		{
			f.size -= 1;
			tf.setTextFormat(f);
		}
	}
	
}