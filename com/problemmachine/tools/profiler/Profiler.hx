package com.problemmachine.tools.profiler;
import flash.Lib.getTimer;

// based on Timely class: http://jacksondunstan.com/articles/2348
class Profiler
{	
	static private var sIterationsPerMS:Int;
	static private var sStartTime:Int = 0;
	
	static public var preciseElapsed(get, never):Float;
	static public var fastElapsed(get, never):Int;
	
	static private function __init__():Void
	{
		var targetMS:Int = getTimer() + 1;
		do	{} while (getTimer() < targetMS);

		var iterationCount:Int = 0;
		targetMS = getTimer() + 10;
		do	{++iterationCount;} while (getTimer() < targetMS);

		sIterationsPerMS = Math.ceil(iterationCount / 10);
	}
	
	
	static public inline function fastStart():Void
	{
		sStartTime = getTimer();
	}
	static public inline function preciseStart():Void
	{
		sStartTime = getTimer() + 1;
		do {} while (getTimer() < sStartTime);
	}
	
	static private inline function get_fastElapsed():Int
	{
		return getTimer() - sStartTime;
	}
	static private inline function get_preciseElapsed():Float
	{
		var iterationCount:Int = 0;
		var targetMS:Int = getTimer() + 1;
		do {++iterationCount; } while (getTimer() < targetMS);

		var portionNotUsed:Float = iterationCount / sIterationsPerMS;

		return (targetMS - sStartTime - portionNotUsed);
	}
	
	static public function fast(o:Dynamic, func:haxe.Constraints.Function, args:Array<Dynamic>, preString:String, postString:String = ""):Dynamic
	{
		var start:Int = getTimer();
		var d:Dynamic = Reflect.callMethod(o, func, args);
		trace (preString + " - Fast profile: " + (getTimer() - start) + "ms");
		return d;
	}
	
	static public function precise(o:Dynamic, func:haxe.Constraints.Function, args:Array<Dynamic>, preString:String):Dynamic
	{
		var burn:Int = 0;
		var start:Int = getTimer() + 1;
		do {++burn; } while (getTimer() < start);
		
		var d:Dynamic = Reflect.callMethod(o, func, args);
		
		var iterationCount:Int = 0;
		var targetMS:Int = getTimer() + 1;
		do {++iterationCount; } while (getTimer() < targetMS);
		burn += iterationCount;

		var portionNotUsed:Float = iterationCount / sIterationsPerMS;

		trace (preString + " - Precise profile: " + (targetMS - start - portionNotUsed) + "ms - " + (burn / sIterationsPerMS) + "microSeconds used by profiler");
		return d;
	}
	
}