package com.problemmachine.tools.keyboard;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.Lib;
import haxe.ds.Vector;

class KeyboardTools
{
	private static var sKeys:Map<UInt, Bool>;
	private static var sKeyDownListeners:Map<UInt, Array<Dynamic->Void>>;
	private static var sKeyUpListeners:Map<UInt, Array<Dynamic->Void>>;
	private static var sAnyKeyDownListeners:Array<Dynamic->Void>;
	private static var sAnyKeyUpListeners:Array<Dynamic->Void>;
	
	#if air
	static private function __init__():Void
	{
		init();
	}
	#end
	static public function init():Void
	{
		if (sKeys != null)
			return;
		sKeys = new Map<UInt, Bool>();
		sKeyDownListeners = new Map<UInt, Array<Dynamic->Void>>();
		sKeyUpListeners = new Map<UInt, Array<Dynamic->Void>>();
		sAnyKeyUpListeners = [];
		sAnyKeyDownListeners = [];
			
		Lib.current.stage.addEventListener(KeyboardEvent.KEY_DOWN, updateKeysListener);
		Lib.current.stage.addEventListener(KeyboardEvent.KEY_UP, updateKeysListener);
	}
	
	static public function startListeningForAnyKeyDown(listener:Dynamic->Void):Void
	{
		if (sAnyKeyDownListeners.indexOf(listener) < 0)
			sAnyKeyDownListeners.push(listener);
	}
	static public function stopListeningForAnyKeyDown(listener:Dynamic->Void):Void
	{
		sAnyKeyDownListeners.remove(listener);
	}
	static public function startListeningForAnyKeyUp(listener:Dynamic->Void):Void
	{
		if (sAnyKeyUpListeners.indexOf(listener) < 0)
			sAnyKeyUpListeners.push(listener);
	}
	static public function stopListeningForAnyKeyUp(listener:Dynamic->Void):Void
	{
		sAnyKeyUpListeners.remove(listener);
	}
	static public function startListeningForKeyDown(keyCode:UInt, listener:Dynamic->Void):Void
	{
		if (sKeyDownListeners.exists(keyCode))
		{
			var arr:Array<Dynamic->Void> = sKeyDownListeners.get(keyCode);
			if (arr.indexOf(listener) < 0)
				arr.push(listener);
		}
		else
			sKeyDownListeners.set(keyCode, [listener]);
	}
	static public function stopListeningForKeyDown(keyCode:UInt, listener:Dynamic->Void):Void
	{
		if (sKeyDownListeners.exists(keyCode))
		{
			var arr:Array<Dynamic->Void> = sKeyDownListeners.get(keyCode);
			arr.remove(listener);
			if (arr.length == 0)
				sKeyDownListeners.remove(keyCode);
		}
	}
	static public function startListeningForKeyUp(keyCode:UInt, listener:Dynamic->Void):Void
	{
		if (sKeyUpListeners.exists(keyCode))
		{
			var arr:Array<Dynamic->Void> = sKeyUpListeners.get(keyCode);
			if (arr.indexOf(listener) < 0)
				arr.push(listener);
		}
		else
			sKeyUpListeners.set(keyCode, [listener]);
	}
	static public function stopListeningForKeyUp(keyCode:UInt, listener:Dynamic->Void):Void
	{
		if (sKeyUpListeners.exists(keyCode))
		{
			var arr:Array<Dynamic->Void> = sKeyUpListeners.get(keyCode);
			arr.remove(listener);
			if (arr.length == 0)
				sKeyUpListeners.remove(keyCode);
		}
	}
	
	static private function updateKeysListener(e:KeyboardEvent):Void
	{
		var arr:Array<Dynamic->Void> = null;
		if (e.type == KeyboardEvent.KEY_DOWN)
		{
			sKeys.set(e.keyCode, true);
			arr = sKeyDownListeners.get(e.keyCode);
		}
		else
		{
			sKeys.set(e.keyCode, false);
			arr = sKeyUpListeners.get(e.keyCode);
		}
		if (arr != null)
			for (l in arr)
				l(e);
		if (e.type == KeyboardEvent.KEY_DOWN)
			for (l in sAnyKeyDownListeners)
				l(e);
		else
			for (l in sAnyKeyUpListeners)
				l(e);
	}
	
	static public function getKeyState(keyCode:UInt):Bool
	{
		return sKeys.get(keyCode) == true;
	}
	
}