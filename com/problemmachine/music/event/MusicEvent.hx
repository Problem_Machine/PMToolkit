package com.problemmachine.music.event;

import flash.events.Event;

class MusicEvent extends Event
{
	static public inline var READY:String = "ready";
	static public inline var UNREADY:String = "unready";
	static public inline var VOLUME_CHANGE:String = "volumeChangeEvent";
	static public inline var STOP:String = "stop";
	static public inline var PLAY:String = "play";
	static public inline var LOOP:String = "loop";
	static public inline var LOOP_CHANGED:String = "loopChanged";
	static public inline var BEGIN_FADE_IN:String = "beginFadeIn";
	static public inline var COMPLETE_FADE_IN:String = "completeFadeIn";
	static public inline var BEGIN_FADE_OUT:String = "beginFadeOut";
	static public inline var COMPLETE_FADE_OUT:String = "completeFadeOut";
	
	public function new(type:String, bubbles:Bool=false, cancelable:Bool=false) 
	{
		super(type, bubbles, cancelable);
		
	}
	
}