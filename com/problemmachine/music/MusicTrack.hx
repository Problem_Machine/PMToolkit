package com.problemmachine.music;
import com.problemmachine.music.event.MusicEvent;
import com.problemmachine.util.lock.Lock.Lock;
import flash.errors.Error;
import flash.events.Event;
import flash.Lib;
import flash.events.EventDispatcher;
import flash.media.SoundChannel;
import flash.media.SoundTransform;
import haxe.xml.Fast;

class MusicTrack extends EventDispatcher
{
	private static var sMusicTracks:Map<String, MusicTrack> = new Map<String, MusicTrack>();
	
	private var mSyncs:Map<String, TrackSync>;
	private var mLock:Lock<Dynamic>;
	private var mPrevPosition:Float;
	
	public var path(default, null):String;
	public var channel(default, null):SoundChannel;
	public var fadeInTime(default, default):Float;
	public var fadeOutTime(default, default):Float;
	public var loopStart(default, set):Float;
	public var loopEnd(default, set):Float;
	public var volume(default, set):Float;
	public var ready(get, never):Bool;
	public var locked(get, never):Bool;
	public var playing(get, never):Bool;
	public var stopped(get, never):Bool;
	public var stopping(default, null):Bool;
	public var length(get, never):Int;
	public static var masterVolume(get, never):Float;

	private function new(path:String) 
	{
		super();
		sMusicTracks.set(path, this);
		this.path = path;
		this.volume = 1;
		mSyncs = new Map<String, TrackSync>();
		mLock = new Lock<Dynamic>();
		stopping = false;
		loopStart = loopEnd = -1;
	}
	
	static public inline function get(path:String):MusicTrack
	{
		if (sMusicTracks.exists(path))
			return sMusicTracks.get(path);
		else
			return new MusicTrack(path);
	}
	
	static public inline function getAndLock(path:String, requester:Dynamic):MusicTrack
	{	
		var t:MusicTrack = get(path);	
		t.lock(requester);
		return t;
	}
	
	static public inline function getCurrentlyPlayingTracks():Array<MusicTrack>
	{
		var tracks:Array<MusicTrack> = new Array<MusicTrack>();
		for (t in sMusicTracks)
			if (t.channel != null)
				tracks.push(t);
		return (tracks);
	}
	
	static private inline function get_masterVolume():Float
	{	return MusicManager.volume;	}
	
	private inline function set_volume(val:Float):Float
	{
		if (Math.max(0, Math.min(1, val)) != volume)
		{
			volume = Math.max(0, Math.min(1, val));
			if (channel != null)
				channel.soundTransform.volume = MusicManager.volume * volume;
			dispatchEvent(new MusicEvent(MusicEvent.VOLUME_CHANGE));
		}
		return val;
	}
	
	private inline function get_ready():Bool
	{	return (MusicManager.getLoadedRatio(path) == 1 && mLock.isLocked && !MusicManager.getBuffering(path));	}
	
	private inline function get_locked():Bool
	{	return (mLock.isLocked);	}
	
	private inline function get_playing():Bool
	{	return (channel != null);	}
	
	private inline function get_stopped():Bool
	{	return (channel == null);	}
	
	private inline function get_length():Int
	{	return (Std.int(MusicManager.getMusicLength(path)));	}
	
	private inline function set_loopStart(val:Float):Float
	{
		if (val != loopStart)
		{
			loopStart = val;
			dispatchEvent(new MusicEvent(MusicEvent.LOOP_CHANGED));
		}
		return val;
	}
	private inline function set_loopEnd(val:Float):Float
	{
		if (val != loopEnd)
		{
			loopEnd = val;
			dispatchEvent(new MusicEvent(MusicEvent.LOOP_CHANGED));
		}
		return val;
	}
	
	@:access(com.problemmachine.music.MusicManager.sMusicBank)
	public function play(startPosition:Float = 0):Void
	{
		if (!ready)
		{
			#if debug
			throw new Error("ERROR: Tried to play unready music track " + path);
			#else
			trace("WARNING: Tried to play unready music track " + path);
			#end
		}
		if (channel != null && !stopping)
			return;
		stopping = false;
		var currentTracks:Array<MusicTrack> = getCurrentlyPlayingTracks();
		for (key in mSyncs.keys())
			for (t in currentTracks)
				if (key == t.path)
				{
					mSyncs.get(key).generateTransition(this);
					return;
				}
		if (fadeInTime > 0)
			MusicManager.addFade(path, Lib.getTimer(), Std.int(fadeInTime * 1000), true, startPosition);
		else
			channel = MusicManager.sMusicBank.get(path).play(startPosition, 0xFFFFFF, new SoundTransform(volume * MusicManager.volume));
		stopping = false;
		dispatchEvent(new MusicEvent(MusicEvent.PLAY));
		mPrevPosition = startPosition;
	}
	
	public function stop(immediate:Bool = false):Void
	{
		if (channel == null || stopping)
			return;
		if (fadeOutTime > 0 && !immediate)
		{
			MusicManager.addFade(path, Lib.getTimer(), Std.int(fadeOutTime * 1000), false);
			stopping = true;
		}
		else
		{
			stopping = false;
			channel.stop();
			channel = null;
		}
	}
	
	static public function stopAll():Void
	{
		for (mt in sMusicTracks)
			mt.stop(true);
	}
	
	public function unlock(requester:Dynamic):Void
	{
		mLock.unlock(requester);
		if(!mLock.isLocked)
			MusicManager.unlockMusic(path);
	}
	
	public function lock(requester:Dynamic):Void
	{
		mLock.lock(requester);
		MusicManager.lockMusic(path);
	}
	
	public inline function getLoadedRatio():Float
	{
		return (MusicManager.getLoadedRatio(path));
	}
	
	public inline function addSync(fromPath:String, ?syncFrom:Array<Int>, ?syncTo:Array<Int>, allowInterp:Bool = true, stopSyncTrack:Bool = true):Void
	{
		if (syncFrom == null)
			syncFrom = [0];
		if (syncTo == null)
			syncTo = [0];
		if (syncFrom.length != syncTo.length)
			trace("WARNING: Sync from and to information doesn't match, some sync points may not function");
		mSyncs.set(fromPath, new TrackSync(fromPath, syncFrom, syncTo, allowInterp, stopSyncTrack));
	}
	
	static public function createFromXML(xml:Xml):MusicTrack
	{
		var fxml:Fast = new Fast(xml);
		var path:String = fxml.node.path.innerData;
		var t:MusicTrack = get(path);
		t.fadeInTime = Std.parseFloat(fxml.node.fadeInTime.innerData);
		t.fadeOutTime = Std.parseFloat(fxml.node.fadeOutTime.innerData);
		if (fxml.hasNode.loop)
		{
			t.loopStart = Std.parseFloat(fxml.node.loop.node.start.innerData);
			t.loopEnd = Std.parseFloat(fxml.node.loop.node.end.innerData);
		}
		
		for (x in fxml.nodes.sync)
		{
			var fromPath:String = x.node.from.innerData;
			var syncFrom:Array<Int> = [];
			for (from in x.nodes.syncFrom)
				syncFrom.push(Std.parseInt(from.innerData));
			var syncTo:Array<Int> = [];
			for (to in x.nodes.syncTo)
				syncTo.push(Std.parseInt(to.innerData));
			if (syncFrom.length < 1)
				syncFrom.push(0);
			if (syncTo.length < 1)
				syncTo.push(0);
			var allowInterp:Bool = (x.node.allowInterp.innerData == Std.string(true));
			var stopSyncTrack:Bool = (x.node.stopSyncTrack.innerData == Std.string(true));
			t.addSync(fromPath, syncFrom, syncTo, allowInterp, stopSyncTrack);
		}
		
		return t;
	}
	
	public function toXML():Xml
	{
		var xml:Xml = Xml.parse(
			"<musictrack>\n" +
			"	<path>" + path + "</path>\n" +
			"	<fadeInTime>" + Std.string(fadeInTime) + "</fadeInTime>\n" +
			"	<fadeOutTime>" + Std.string(fadeOutTime) + "</fadeOutTime>\n" +
			"</musictrack>").firstElement();
		if (loopStart >= 0)
		{
			xml.addChild(Xml.parse(
				"<loop>" + 
				"	<start>" + Std.string(loopStart) +"</start>\n" +
				"	<end>" + Std.string(loopEnd) +"</end>\n" +
				"</loop>").firstElement());
		}
		for (s in mSyncs.iterator())
		{
			var sxml:Xml = Xml.parse(
				"<sync>" +
				"	<track>" + s.track + "</track>\n" +
				"	<allowInterp>" + Std.string(s.allowInterp) + "</allowInterp>\n" +
				"	<stopSyncTrack>" + Std.string(s.stopSyncTrack) + "</stopSyncTrack>\n" +
				"</sync>").firstElement();
			for (i in 0...Std.int(Math.min(s.syncFrom.length, s.syncTo.length)))
			{
				sxml.addChild(Xml.parse("<syncFrom>" + Std.string(s.syncFrom[i]) + "</syncFrom>").firstElement());
				sxml.addChild(Xml.parse("<syncTo>" + Std.string(s.syncTo[i]) + "</syncTo>").firstElement());
			}
			xml.addChild(sxml);
		}
		return xml;
	}
}

private class TrackSync
{
	public var track:String;
	public var syncFrom:Array<Int>;
	public var syncTo:Array<Int>;
	public var allowInterp:Bool;
	public var stopSyncTrack:Bool;
	
	public function new(track:String, ?syncFrom:Array<Int>, ?syncTo:Array<Int>, allowInterp:Bool = true, stopSyncTrack:Bool = true)
	{
		this.track = track;
		if (syncFrom == null)
			this.syncFrom = [0];
		else
			this.syncFrom = syncFrom;
		if (syncTo == null)
			this.syncTo = [0];
		else
			this.syncTo = syncTo;
		this.syncTo = syncTo;
		this.allowInterp = allowInterp;
		this.stopSyncTrack = stopSyncTrack;
	}
	
	public function generateTransition(to:MusicTrack):Void
	{
		var from:MusicTrack = MusicTrack.get(track);
		if (from.channel == null)
		{
			trace("WARNING: Attempted track transition has no track to transition from");
			to.play();
			return;
		}
		var position:Int = Std.int(from.channel.position);
		var fromTime:Int = syncFrom[0];
		var toTime:Int = syncTo[0];
		var startTime:Int = Lib.getTimer();
		// use closest sync point to current position
		for (i in 1...Std.int(Math.min(syncFrom.length, syncTo.length)))
		{
			if (allowInterp && fromTime > position)
				break;
			if (Math.abs(fromTime - position) > Math.abs(syncFrom[i] - position))
			{
				fromTime = syncFrom[i];
				toTime = syncTo[i];
			}
		}
		if (allowInterp)
		{
			if (stopSyncTrack)
				MusicManager.addFade(track, startTime, Std.int(from.fadeOutTime * 1000), false);
			MusicManager.addFade(to.path, startTime, Std.int(to.fadeInTime * 1000), true, position - fromTime + toTime);
		}
		else
		{
			// find proper start time for sync and add that to the current start time
			if (fromTime < position)
				startTime += from.length - position + fromTime;
			else
				startTime += fromTime - position;
			
			if (stopSyncTrack)
				MusicManager.addFade(track, startTime, Std.int(from.fadeOutTime * 1000), false);
			MusicManager.addFade(to.path, startTime, Std.int(to.fadeInTime * 1000), true, toTime);
		}
	}
}