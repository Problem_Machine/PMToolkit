package com.problemmachine.music;
import com.problemmachine.music.event.MusicEvent;
import com.problemmachine.util.lock.Lock.Lock;
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.TimerEvent;
import flash.Lib;
import flash.media.Sound;
import flash.media.SoundChannel;
import flash.media.SoundTransform;
import flash.net.URLRequest;
import flash.utils.Timer;

class MusicManager
{
	private static var sMaximumBytes:Int = 200 * 1024 * 1024;
	private static var sCurrentBytes:Int = 0;
	
	private static inline var TICK_LENGTH:Int = 2;
	private static var sTimer:Timer = null;
	
	static private var sMusicBank:Map<String, Sound> = new Map<String, Sound>();
	static private var sMusicFades:Map<String, Fade> = new Map<String, Fade>();
	static private var sMusicLocks:Map<String, Bool> = new Map<String, Bool>();
	
	static public var mute(default, set):Bool = false;
	static public var volume(get, set):Float;
	static private var sVolume:Float = 1;
	
	private function new() 
	{		
	}
	
	static private function init():Void
	{
		sTimer = new Timer(TICK_LENGTH);
		sTimer.addEventListener(TimerEvent.TIMER, update);
		sTimer.start();
	}
	
	@:access(com.problemmachine.music.MusicTrack.mPrevPosition)
	@:access(com.problemmachine.music.MusicTrack.channel)
	static private function update(?e:Event):Void
	{
		var v:Float = mute ? 0 : volume;
		var removeFades:Array<String> = new Array<String>();
		var time:Int = Lib.getTimer();
		for (t in MusicTrack.getCurrentlyPlayingTracks())
		{
			t.channel.soundTransform.volume = t.volume * v;
			if (t.channel.position > t.loopEnd && t.mPrevPosition < t.loopEnd)
			{
				t.stop(true);
				t.play(t.loopStart);
				t.dispatchEvent(new MusicEvent(MusicEvent.LOOP));
			}
			t.mPrevPosition = t.channel.position;
		}
			
		for (key in sMusicFades.keys())
		{
			var track:MusicTrack = MusicTrack.get(key);
			var f:Fade = sMusicFades.get(key);
			if (!track.playing)
			{
				switch(f.direction)
				{
					case FadeDirection.FadeOut:
						removeFades.push(key);
						continue;
					case FadeDirection.FadeIn:
						if (time > f.startTime)
						{
							track.volume = 0;
							track.channel = MusicManager.sMusicBank.get(key).play(f.playbackStartTime + (time - f.startTime), 0xFFFFFF, new SoundTransform(0));
							track.dispatchEvent(new MusicEvent(MusicEvent.PLAY));
							track.dispatchEvent(new MusicEvent(MusicEvent.BEGIN_FADE_IN));
						}
				}
			}
			var amount:Float = ((time - f.startTime) / f.length);
			amount = Math.max(0, Math.min(1, amount));
			if (f.direction == FadeOut)
			{
				amount = 1 - amount;
				if (time > f.startTime && track.mPrevPosition < f.startTime)
					track.dispatchEvent(new MusicEvent(MusicEvent.BEGIN_FADE_OUT));
			}
			// make sure this works with a linear fade first, afterwards we can experiment with different curves
			//amount *= amount;
			track.channel.soundTransform.volume = (amount * v * track.volume);
			if (time > f.startTime + f.length)
			{
				removeFades.push(key);
				switch(f.direction)
				{
					case FadeDirection.FadeIn:
						track.channel.soundTransform.volume = (v * track.volume);
						track.dispatchEvent(new MusicEvent(MusicEvent.COMPLETE_FADE_IN));
					case FadeDirection.FadeOut:
						track.stop(true);
						track.dispatchEvent(new MusicEvent(MusicEvent.COMPLETE_FADE_OUT));
				}
			}
		}
		for (key in removeFades)
			sMusicFades.remove(key);
	}
	
			@:allow(com.problemmachine.music.MusicTrack)
	static private function lockMusic(fileName:String):Void
	{
		if (sTimer == null)
			init();
		if (!sMusicBank.exists(fileName))
		{
			var s:Sound = new Sound(new URLRequest(fileName));
			s.addEventListener(IOErrorEvent.IO_ERROR, function(e:IOErrorEvent):Void{	trace(e.text); });
			sMusicBank.set(fileName, s);
			s.addEventListener(Event.COMPLETE, finishLoadListener);
		}
		sMusicLocks.set(fileName, true);
	}
	
			@:allow(com.problemmachine.music.MusicTrack)
	static private inline function unlockMusic(fileName:String):Void
	{
		sMusicLocks.set(fileName, false);
	}
	
	static private function finishLoadListener(e:Event):Void
	{
		var s:Sound = cast(e.target, Sound);
		sCurrentBytes += s.bytesTotal;
		if (sCurrentBytes >= sMaximumBytes)
			freeMusic();
		for (key in sMusicBank.keys())
			if (sMusicBank.get(key) == s)
			{
				MusicTrack.get(key).dispatchEvent(new MusicEvent(MusicEvent.READY));
				break;
			}
	}
	
	static public function freeMusic():Void
	{
		var remove:Array<String> = new Array<String>();
		for (key in sMusicBank.keys())
			if (!sMusicLocks.get(key) && !MusicTrack.get(key).playing)
				remove.push(key);
		for (key in remove)
		{
			sCurrentBytes -= sMusicBank.get(key).bytesTotal;
			sMusicBank.remove(key);
			MusicTrack.get(key).dispatchEvent(new MusicEvent(MusicEvent.UNREADY));
		}
		if (sCurrentBytes > sMaximumBytes)
			trace("WARNING: Maximum size of music bank exceeded: " + sCurrentBytes + "/" + sMaximumBytes + " bytes");
	}
	
			@:allow(com.problemmachine.music)
	static private inline function getMusicLength(fileName:String):Float
	{
		return sMusicBank.get(fileName).length;
	}
	
			@:allow(com.problemmachine.music.MusicTrack)
	static private inline function getBuffering(fileName:String):Bool
	{
		if (!sMusicBank.exists(fileName))
			return false;
		else
			return sMusicBank.get(fileName).isBuffering;
	}
	
			@:allow(com.problemmachine.music.MusicTrack)
	static private inline function getLoadedRatio(fileName:String):Float
	{
		if (!sMusicBank.exists(fileName))
			return 0;
		else
		{
			var s:Sound = sMusicBank.get(fileName);
			return (s.bytesLoaded / s.bytesTotal);
		}
	}
	
			@:allow(com.problemmachine.music)
	static private function addFade(fileName:String, startTime:Float, length:Float, 
		fadeIn:Bool, playbackStartTime:Float = 0):Void
	{
		sMusicFades.set(fileName, new Fade(startTime, length, 
			(fadeIn ? FadeDirection.FadeIn : FadeDirection.FadeOut), playbackStartTime));
	}
	
	static private inline function set_mute(val:Bool):Bool
	{
		if (val != mute)
		{
			mute = val;
			update();
		}
		return val;
	}
	
	static private inline function set_volume(val:Float):Float
	{
		if (Math.max(0, Math.min(1, val)) != sVolume)
		{
			sVolume = Math.max(0, Math.min(1, val));
			update();
		}
		return val;
	}
	
	static private inline function get_volume():Float
	{	return (mute ? 0 : sVolume);						}
}

private class Fade
{
	public var startTime(default, null):Float;
	public var length(default, null):Float;
	public var direction(default, null):FadeDirection;
	public var playbackStartTime(default, null):Float;
	
	public function new(startTime:Float, length:Float, direction:FadeDirection, playbackStartTime:Float = 0)
	{
		this.startTime = startTime;
		this.length = length;
		this.direction = direction;
		this.playbackStartTime = playbackStartTime;
	}
}

private enum FadeDirection
{
	FadeIn;
	FadeOut;
}

private class Loop
{
	public var start(default, default):Float;
	public var end(default, default):Float;
	
	public function new(start:Float, end:Float)
	{
		this.start = start;
		this.end = end;
	}
}